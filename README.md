# CasseBrick SOLO
Le jeu SOLO a été fait dans le cadre de la formation Développemment Informatique du CESI. Pour le moment, je n'ai pas retouché à cette partie depuis l'évaluation fin octobre 2023. 

# CasseBrick MULTI
Cette partie est pour répondre à un défi d'un intervenant : modifier ou faire un casse brique multijoueur (local) dans l'objectif de le faire fonctionner sur une bande d'arcade.
Je suis encore sur la partie 'conception': remplir les objectifs que je me suis fixée pour le rendu final.

## Objectifs
 - **Mise en place de bonus et malus :**
    - Bonus : (s'applique au joueur)
        - Barre du joueur devient plus longue                                       **FAIT**
        - Fois 2 sur le multiplicateur de point (combos)                            **FAIT**
        - Une 'zone' au dessus de la barre où toutes les balles ralentissent        **FAIT**
    - Malus : (s'applique aux adversaires)
        - La vitesse des barres augmentent                                          **FAIT**
        - Les balles n'ont plus de collisions avec les briques                      **FAIT**
        - Une 'zone' au dessus de la barre où toutes les balles accélèrent          **FAIT**

 - **Mettre des paramètres pour la fin de partie :**
    - Limite de points                                                              **FAIT**
    - Limite de temps                                                               **FAIT**
    - Point de vie                                                                  **FAIT**

 - **Amélioration des Bots :**                                                      
    - Mettre des niveaux de difficulté :
        - Facil :       mettra 1. secondes pour adapté sa position                  **FAIT**          
        - Normal :      mettra .5 secondes pour adapté sa position                  **FAIT**
        - Difficile :   mettra .2 secondes pour adapté sa position                  **FAIT**
        - -_- :         mettra .1 secondes pour adapté sa position                  **FAIT**
    
    <!-- - Note : théoriquement les temps fonctionnent, mais prennent plus de temps dû à l'execution
        - Voir pour optimiser : 
            - chargement des sprites
            - plus rapide :
                - else if **ou** switch
                - tous les while et for sont nécessaire ?
        - A garder en tête : la limite de la bibliothèque sdl est peut-être atteinte -->

 - **LeaderBoard pendant la partie**                                                ****

 - **Récapitulatif de fin de partie :**
    - combo maximum atteint pour chaque joueur                                      ****
    - nombre de balle 'subit' pour chaque joueur :                                  ****
        - voir pour le détail pour tous les adversaires                             ****
    - nombre de bonus et malus utilisé pour chaque joueur                           ****

 - **Faire la mise en page de tous les menus**                                      **FAIT**

 - **Modification de la partie SOLO en prévision de la fusion SOLO / MULTI**        ****

 - **Mettre des écrans de 'chargement' et y mettre des tips**                       ****

 - **Faire les fonctionnalités 'manette'**                                          ****

 - **Réfléchir à comment intégrer une base de données**                             ****