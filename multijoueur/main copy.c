/*---------------------------------Description---------------------------------*/
/*Nom projet: Brick Breaker             Edit: RaspY             Version: 0_1_0
               Multijoueur

Description Jeu : C'est un casse briques avec plusieurs joueurs.

Description : Apparitions des briques + balles + barres
            Rebond des balles sur les bords de l'écran + barres + briques
            base des différents modes : limite de temps / limite de points

Objectif :  faire les menus

Atteint :   non
*/
/*-----------------------------------------------------------------------------*/

#include <time.h>
#include <SDL2/SDL.h>
#include <unistd.h>
#include "function.h"

//1920x1080 pixels
#define WINDOW_WIDTH 1920
#define WINDOW_HEIGHT 1080
#define FPS 60

#define VITESSEBALLE 4
#define VITESSEBARRE 6

#define NOMBRE_LIGNE 8
#define NOMBRE_COLONNE 8

typedef struct Bricks
{
    int x;
    int y;

    int side_x_le;
    int side_x_ri;
    int side_y_up;
    int side_y_do;

    int vie;
    int spawn;
    int type;
    int rebond;

    int frame;
    int temps;

    char* sourceIMG;
}T_Bricks;

T_Bricks tabBrick[NOMBRE_LIGNE][NOMBRE_COLONNE];

typedef struct Balls
{
    int x;
    int y;

    int side_x_le;
    int side_x_ri;
    int side_y_up;
    int side_y_do;

    int color_r;
    int color_g;
    int color_b;

    int vitesseX;
    int vitesseY;

    int fantome;
}T_Balls;

T_Balls balles[4];

typedef struct Barres
{
    int longueur;
    int largeur; 

    int x;
    int y;

    int side_x_le;
    int side_x_ri;
    int side_y_up;
    int side_y_do;

    int color_r_contour;      
    int color_g_contour;    
    int color_b_contour; 

    int color_r_interieur;      
    int color_g_interieur;    
    int color_b_interieur; 

    int vitesseX;
    int vitesseY;
    
}T_Barres;

T_Barres barres[4];

typedef struct Chiffres
{
    int longueur;
    int largeur;

    char* sourceIMG;
}T_Chiffres;

T_Chiffres tabChiffres[4][10];

typedef struct BonusMalus
{
    int typeB;
    int typeM;

    int activerB;
    int activerM;

    int tempsRecharge;
    int tempsActif;
}T_BonusMalus;

T_BonusMalus tabBonusMalus[4];

typedef struct Bots
{
    int actif;
    int tempsReact;
    int recharge;
}T_Bots;

T_Bots tabBots[4]; //4bots. Pourquoi? et pourquoi pas

int xZoneJeuMulti;
int longueurZoneJeuMulti;

int longueur;
int xdebutBrick;
int ydebutBrick;

int pourcentageTypeBricks;

int rayonBall;

int nombreJoueur;

int tabResultat[4]; 
int unit;
int nombreChiffres;
int longueurResultat;
int positionResultat;

int pointBrickCassee;
int pointBalleToucheAdvers;
int pointBalleToucheSoi;
int pointCombo;

int suiviCombo[4];

int plusProche;
int tempsReactionBots[4]; // il y a 4 niveaux de difficulté

int afficherBricks;

int start;
int page;
int pageMenu;

int hoverNombreJoueur2;
int hoverNombreJoueur3;
int hoverNombreJoueur4;

int hoverStart;
int hoverModeSelectionLeft;
int hoverModeSelectionRight;
int hoverMinuteUp;
int hoverMinuteDown;
int hoverSecondeUp;
int hoverSecondeDown;

int vitesseBarre;

int chargement;

int tempsJeu;

int modeSelectionCentre;
int modeSelection;

int minutesLimite;
int secondesLimite;

int unitSeconde;
int unitMinute;
int chiffreSeconde;
int chiffreMinute;

int scoreLimite;

int unitScore;
int chiffreScore;

int hoverScoreUp[9];
int hoverScoreDown[9];

int heartLimit;

int unitHeart;
int chiffreHeart;

int hoverHeartUp;
int hoverHeartDown;

int playerSelection[4];
int playerDifficulty[4];

int hoverPlayerSelectionLeft[4];
int hoverPlayerSelectionRight[4];
int hoverPlayerDifficultyLeft[4];
int hoverPlayerDifficultyRight[4];

int playerReady[4][4];

int timeMax;
int scoreMax;
int heartMax;


void background(){
    changeColor(0, 0, 0, 255);
    drawRect(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);

    changeColor(0, 21, 36, 255);
    drawRect(xZoneJeuMulti, 0, longueurZoneJeuMulti, longueurZoneJeuMulti);
}

void backgroundMenu(){
    changeColor(0, 21, 36, 255);
    drawRect(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);
}

/*-----------------Initialisation des BALLES pour chaque joueur----------------*/
void init_Balles_J1(){
    balles[0].x = WINDOW_WIDTH / 2;
    balles[0].y = longueurZoneJeuMulti - 50 - 25;

    balles[0].color_r = 244;
    balles[0].color_g = 37;
    balles[0].color_b = 17;

    balles[0].vitesseX = VITESSEBALLE;
    balles[0].vitesseY = -VITESSEBALLE;

    balles[0].fantome = 0;
}

void init_Balles_J2(){
    balles[1].x = WINDOW_WIDTH / 2;
    balles[1].y = 50 + 25;

    balles[1].color_r = 244;
    balles[1].color_g = 214;
    balles[1].color_b = 17;

    balles[1].vitesseX = -VITESSEBALLE;
    balles[1].vitesseY = VITESSEBALLE;

    balles[1].fantome = 0;
}

void init_Balles_J3(){
    balles[2].x = xZoneJeuMulti + 50 + 25;
    balles[2].y = longueurZoneJeuMulti / 2;

    balles[2].color_r = 244;
    balles[2].color_g = 17;
    balles[2].color_b = 180;

    balles[2].vitesseX = VITESSEBALLE;
    balles[2].vitesseY = VITESSEBALLE;

    balles[2].fantome = 0;
}

void init_Balles_J4(){
    balles[3].x = (xZoneJeuMulti + longueurZoneJeuMulti) - 50 - 25;
    balles[3].y = longueurZoneJeuMulti / 2;

    balles[3].color_r = 17;
    balles[3].color_g = 135;
    balles[3].color_b = 244;

    balles[3].vitesseX = -VITESSEBALLE;
    balles[3].vitesseY = -VITESSEBALLE;

    balles[3].fantome = 0;
}

void init_Balles(){
    init_Balles_J1();
    init_Balles_J2();
    init_Balles_J3();
    init_Balles_J4();

    rayonBall = 10;
}
/*-----------------------------------------------------------------------------*/

/*-----------------Initialisation des BALLES pour chaque joueur----------------*/
void init_Barres_J1(){
    barres[0].longueur = 100;
    barres[0].largeur = 10;

    barres[0].x = (WINDOW_WIDTH - barres[0].longueur) / 2;
    barres[0].y = longueurZoneJeuMulti - 50;

    barres[0].color_r_contour = 244;
    barres[0].color_g_contour = 37;
    barres[0].color_b_contour = 17;

    barres[0].color_r_interieur = 91;
    barres[0].color_g_interieur = 19;
    barres[0].color_b_interieur = 12;

    barres[0].vitesseX = 0;
    barres[0].vitesseY = 0;
}

void init_Barres_J2(){
    barres[1].longueur = 100;
    barres[1].largeur = 10;

    barres[1].x = (WINDOW_WIDTH - barres[1].longueur) / 2;
    barres[1].y = 50 - 10;

    barres[1].color_r_contour = 244;
    barres[1].color_g_contour = 214;
    barres[1].color_b_contour = 17;

    barres[1].color_r_interieur = 63;
    barres[1].color_g_interieur = 91;
    barres[1].color_b_interieur = 12;

    barres[1].vitesseX = 0;
    barres[1].vitesseY = 0;
}

void init_Barres_J3(){
    barres[2].longueur = 10;
    barres[2].largeur = 100;

    barres[2].x = xZoneJeuMulti + 50 - barres[2].longueur ;
    barres[2].y = (longueurZoneJeuMulti - barres[2].largeur) / 2;

    barres[2].color_r_contour = 244;
    barres[2].color_g_contour = 17;
    barres[2].color_b_contour = 180;

    barres[2].color_r_interieur = 85;
    barres[2].color_g_interieur = 12;
    barres[2].color_b_interieur = 91;

    barres[2].vitesseX = 0;
    barres[2].vitesseY = 0;
}

void init_Barres_J4(){
    barres[3].longueur = 10;
    barres[3].largeur = 100;

    barres[3].x = (xZoneJeuMulti + longueurZoneJeuMulti) - 50;
    barres[3].y = (longueurZoneJeuMulti - barres[2].largeur) / 2;

    barres[3].color_r_contour = 17;
    barres[3].color_g_contour = 135;
    barres[3].color_b_contour = 244;

    barres[3].color_r_interieur = 12;
    barres[3].color_g_interieur = 13;
    barres[3].color_b_interieur = 91;

    barres[3].vitesseX = 0;
    barres[3].vitesseY = 0;
}

void init_Barres(){
    init_Barres_J1();
    init_Barres_J2();
    init_Barres_J3();
    init_Barres_J4();
}
/*-----------------------------------------------------------------------------*/

/*-------------------------Initialisation des BRIQUES--------------------------*/
void init_vieBricks(int i, int j){
    pourcentageTypeBricks = rand() % 101; //[0-100]
    if((pourcentageTypeBricks >= 0) && (pourcentageTypeBricks < 76)){
        tabBrick[i][j].type = 1;
        tabBrick[i][j].vie = 1;
    }    
    else if((pourcentageTypeBricks < 91)){
        tabBrick[i][j].type = 2;
        tabBrick[i][j].vie = 2;
    }                             
    else if((pourcentageTypeBricks < 99)){
        tabBrick[i][j].type = 3;
        tabBrick[i][j].vie = 3;
    }                             
    else{
        tabBrick[i][j].type = 4;
        tabBrick[i][j].vie = 4;
    }                                                            
}

void init_Bricks(){
    srand(time(NULL));

    longueur = 60;
    xdebutBrick = (WINDOW_WIDTH - (NOMBRE_COLONNE*longueur + (NOMBRE_COLONNE-1)*5) + 10)/2; //5: espace entre briques
    ydebutBrick = (WINDOW_HEIGHT - (NOMBRE_LIGNE*longueur + (NOMBRE_LIGNE)*5) + 16)/2;
    
    for(int i = 0 ; i < NOMBRE_LIGNE ; i++){
        for(int j = 0 ; j < NOMBRE_COLONNE ; j++){
            tabBrick[i][j].x = xdebutBrick + j*longueur + (j-1)*5;
            tabBrick[i][j].y = ydebutBrick + i*longueur + (i-1)*5;
            tabBrick[i][j].spawn = 0;
            
            tabBrick[i][j].rebond = 0;
            tabBrick[i][j].temps = 0;

            init_vieBricks(i, j);
        }
    }
}
/*-----------------------------------------------------------------------------*/

/*----------------Initialisation des CHIFFRES pour chaque joueur---------------*/
void init_Chiffres_J1(){
    tabChiffres[0][0].longueur = 50;
    tabChiffres[0][0].largeur = 46;
    tabChiffres[0][0].sourceIMG = "assets/img0J1";

    tabChiffres[0][1].longueur = 19;
    tabChiffres[0][1].largeur = 46;
    tabChiffres[0][1].sourceIMG = "assets/img1J1";

    tabChiffres[0][2].longueur = 48;
    tabChiffres[0][2].largeur = 46;
    tabChiffres[0][2].sourceIMG = "assets/img2J1";

    tabChiffres[0][3].longueur = 45;
    tabChiffres[0][3].largeur = 46;
    tabChiffres[0][3].sourceIMG = "assets/img3J1";

    tabChiffres[0][4].longueur = 48;
    tabChiffres[0][4].largeur = 46;
    tabChiffres[0][4].sourceIMG = "assets/img4J1";

    tabChiffres[0][5].longueur = 47;
    tabChiffres[0][5].largeur = 46;
    tabChiffres[0][5].sourceIMG = "assets/img5J1";

    tabChiffres[0][6].longueur = 48;
    tabChiffres[0][6].largeur = 46;
    tabChiffres[0][6].sourceIMG = "assets/img6J1";

    tabChiffres[0][7].longueur = 44;
    tabChiffres[0][7].largeur = 46;
    tabChiffres[0][7].sourceIMG = "assets/img7J1";

    tabChiffres[0][8].longueur = 48;
    tabChiffres[0][8].largeur = 46;
    tabChiffres[0][8].sourceIMG = "assets/img8J1";

    tabChiffres[0][9].longueur = 48;
    tabChiffres[0][9].largeur = 46;
    tabChiffres[0][9].sourceIMG = "assets/img9J1";
}

void init_Chiffres_J2(){
    tabChiffres[1][0].longueur = 50;
    tabChiffres[1][0].largeur = 46;
    tabChiffres[1][0].sourceIMG = "assets/img0J2";

    tabChiffres[1][1].longueur = 19;
    tabChiffres[1][1].largeur = 46;
    tabChiffres[1][1].sourceIMG = "assets/img1J2";

    tabChiffres[1][2].longueur = 48;
    tabChiffres[1][2].largeur = 46;
    tabChiffres[1][2].sourceIMG = "assets/img2J2";

    tabChiffres[1][3].longueur = 45;
    tabChiffres[1][3].largeur = 46;
    tabChiffres[1][3].sourceIMG = "assets/img3J2";

    tabChiffres[1][4].longueur = 48;
    tabChiffres[1][4].largeur = 46;
    tabChiffres[1][4].sourceIMG = "assets/img4J2";

    tabChiffres[1][5].longueur = 47;
    tabChiffres[1][5].largeur = 46;
    tabChiffres[1][5].sourceIMG = "assets/img5J2";

    tabChiffres[1][6].longueur = 48;
    tabChiffres[1][6].largeur = 46;
    tabChiffres[1][6].sourceIMG = "assets/img6J2";

    tabChiffres[1][7].longueur = 44;
    tabChiffres[1][7].largeur = 46;
    tabChiffres[1][7].sourceIMG = "assets/img7J2";

    tabChiffres[1][8].longueur = 48;
    tabChiffres[1][8].largeur = 46;
    tabChiffres[1][8].sourceIMG = "assets/img8J2";

    tabChiffres[1][9].longueur = 48;
    tabChiffres[1][9].largeur = 46;
    tabChiffres[1][9].sourceIMG = "assets/img9J2";
}

void init_Chiffres_J3(){
    tabChiffres[2][0].longueur = 50;
    tabChiffres[2][0].largeur = 46;
    tabChiffres[2][0].sourceIMG = "assets/img0J3";

    tabChiffres[2][1].longueur = 19;
    tabChiffres[2][1].largeur = 46;
    tabChiffres[2][1].sourceIMG = "assets/img1J3";

    tabChiffres[2][2].longueur = 48;
    tabChiffres[2][2].largeur = 46;
    tabChiffres[2][2].sourceIMG = "assets/img2J3";

    tabChiffres[2][3].longueur = 45;
    tabChiffres[2][3].largeur = 46;
    tabChiffres[2][3].sourceIMG = "assets/img3J3";

    tabChiffres[2][4].longueur = 48;
    tabChiffres[2][4].largeur = 46;
    tabChiffres[2][4].sourceIMG = "assets/img4J3";

    tabChiffres[2][5].longueur = 47;
    tabChiffres[2][5].largeur = 46;
    tabChiffres[2][5].sourceIMG = "assets/img5J3";

    tabChiffres[2][6].longueur = 48;
    tabChiffres[2][6].largeur = 46;
    tabChiffres[2][6].sourceIMG = "assets/img6J3";

    tabChiffres[2][7].longueur = 44;
    tabChiffres[2][7].largeur = 46;
    tabChiffres[2][7].sourceIMG = "assets/img7J3";

    tabChiffres[2][8].longueur = 48;
    tabChiffres[2][8].largeur = 46;
    tabChiffres[2][8].sourceIMG = "assets/img8J3";

    tabChiffres[2][9].longueur = 48;
    tabChiffres[2][9].largeur = 46;
    tabChiffres[2][9].sourceIMG = "assets/img9J3";
}

void init_Chiffres_J4(){
    tabChiffres[3][0].longueur = 50;
    tabChiffres[3][0].largeur = 46;
    tabChiffres[3][0].sourceIMG = "assets/img0J4";

    tabChiffres[3][1].longueur = 19;
    tabChiffres[3][1].largeur = 46;
    tabChiffres[3][1].sourceIMG = "assets/img1J4";

    tabChiffres[3][2].longueur = 48;
    tabChiffres[3][2].largeur = 46;
    tabChiffres[3][2].sourceIMG = "assets/img2J4";

    tabChiffres[3][3].longueur = 45;
    tabChiffres[3][3].largeur = 46;
    tabChiffres[3][3].sourceIMG = "assets/img3J4";

    tabChiffres[3][4].longueur = 48;
    tabChiffres[3][4].largeur = 46;
    tabChiffres[3][4].sourceIMG = "assets/img4J4";

    tabChiffres[3][5].longueur = 47;
    tabChiffres[3][5].largeur = 46;
    tabChiffres[3][5].sourceIMG = "assets/img5J4";

    tabChiffres[3][6].longueur = 48;
    tabChiffres[3][6].largeur = 46;
    tabChiffres[3][6].sourceIMG = "assets/img6J4";

    tabChiffres[3][7].longueur = 44;
    tabChiffres[3][7].largeur = 46;
    tabChiffres[3][7].sourceIMG = "assets/img7J4";

    tabChiffres[3][8].longueur = 48;
    tabChiffres[3][8].largeur = 46;
    tabChiffres[3][8].sourceIMG = "assets/img8J4";

    tabChiffres[3][9].longueur = 48;
    tabChiffres[3][9].largeur = 46;
    tabChiffres[3][9].sourceIMG = "assets/img9J4";
}

void init_Chiffres(){
    init_Chiffres_J1();
    init_Chiffres_J2();
    init_Chiffres_J3();
    init_Chiffres_J4();
}
/*-----------------------------------------------------------------------------*/

/*----------------Initialisation des RESULTAT en début de partie---------------*/
void init_Resultat(){
    for(int i = 0 ; i < nombreJoueur ; i++){
        tabResultat[i] = 0;
        suiviCombo[i] = 0;
    }
}
/*-----------------------------------------------------------------------------*/

/*--------------------Initialisation des BONUS et des MALUS--------------------*/
void init_BonusMalus(int joueur){
    tabBonusMalus[joueur].typeB = rand() % 3;
    tabBonusMalus[joueur].typeM = rand() % 3;

    tabBonusMalus[joueur].typeM = 2;

    tabBonusMalus[joueur].activerB = 0;
    tabBonusMalus[joueur].activerM = 0;
    tabBonusMalus[joueur].tempsRecharge = 0;
    tabBonusMalus[joueur].tempsActif = 0;
}
/*-----------------------------------------------------------------------------*/

void init_Bots(int bot){
    tabBots[bot].actif = 1;
    tabBots[bot].tempsReact = tempsReactionBots[3];
    tabBots[bot].recharge = 0;
}

/*-------------------------Initialisation de la partie-------------------------*/
void init_game(){
    xZoneJeuMulti = (WINDOW_WIDTH - WINDOW_HEIGHT) / 2;
    longueurZoneJeuMulti = WINDOW_HEIGHT;

    vitesseBarre = 6;

    nombreJoueur = 4;
    page = 0;
    pageMenu = 0;
    start = 0;

    init_Balles();

    init_Barres();

    init_Bricks();

    init_Chiffres();

    init_Resultat();

    tempsReactionBots[0] = 1 * FPS; //facil
    tempsReactionBots[1] = FPS / 2; //normal
    tempsReactionBots[2] = FPS / 4; //difficil
    tempsReactionBots[3] = FPS / 8; //-_-

    //le for ici, pour pouvoir reset les variables BonusMalus 
    //directement pour le joueur concerné
    for(int i = 0 ; i < nombreJoueur ; i++){
        init_BonusMalus(i);
        init_Bots(i);
    }

    nombreChiffres = 0;

    pointBrickCassee = 500;
    pointBalleToucheAdvers = 50;
    pointBalleToucheSoi = -100;
    pointCombo = 200;

    hoverNombreJoueur2 = 0;
    hoverNombreJoueur3 = 0;
    hoverNombreJoueur4 = 0;

    hoverStart = 0;

    chargement = 0;
    tempsJeu = 0;

    modeSelectionCentre = (1665 - 1265 + 25) / 2; // (chevronRight - chevronLeft + widhtChevron) / 2
    modeSelectionCentre = 1265 + modeSelectionCentre; // chevronLeft + moitié

    hoverModeSelectionLeft = 0;
    hoverModeSelectionRight = 0;
    hoverMinuteUp = 0;
    hoverMinuteDown = 0;
    hoverSecondeUp = 0;
    hoverSecondeDown = 0;

    modeSelection = 1;

    minutesLimite = 0;
    secondesLimite = 0;
    chiffreSeconde = 1;

    scoreLimite = 0;

    heartLimit = 1;

    for(int i = 0 ; i < 4 ; i++){
        playerSelection[i] = 1;
        playerDifficulty[i] = 1;
    }

    for(int i = 0 ; i < 4 ; i++){
        for(int j = 0 ; j < 4 ; j++){
            playerReady[i][j] = -1;
        }
    }
}
/*-----------------------------------------------------------------------------*/

void resetPartie(){
    init_Balles();

    init_Barres();

    init_Bricks();

    init_Chiffres();

    init_Resultat();


    //le for ici, pour pouvoir reset les variables BonusMalus 
    //directement pour le joueur concerné
    for(int i = 0 ; i < nombreJoueur ; i++){
        init_BonusMalus(i);
    }

    nombreChiffres = 0;

    tempsJeu = 0;
}


/*-------------Actualisation des COORDONNEES des côtés des BALLES--------------*/
void updateSide_Balles(int i){
    balles[i].side_x_le = balles[i].x - rayonBall;
    balles[i].side_x_ri = balles[i].x + rayonBall;
    balles[i].side_y_up = balles[i].y - rayonBall;
    balles[i].side_y_do = balles[i].y + rayonBall;
}
/*-----------------------------------------------------------------------------*/

/*-------------Actualisation des COORDONNEES des côtés des BARRES--------------*/
void updateSide_Barres(int i){
    barres[i].side_x_le = barres[i].x;
    barres[i].side_x_ri = barres[i].x + barres[i].longueur;
    barres[i].side_y_up = barres[i].y;
    barres[i].side_y_do = barres[i].y + barres[i].largeur;
}
/*-----------------------------------------------------------------------------*/

/*-------------Actualisation des COORDONNEES des côtés des BRIQUES-------------*/
void updateSide_Bricks(int i, int j){
    tabBrick[i][j].side_x_le = tabBrick[i][j].x;
    tabBrick[i][j].side_x_ri = tabBrick[i][j].x + longueur;
    tabBrick[i][j].side_y_up = tabBrick[i][j].y;
    tabBrick[i][j].side_y_do = tabBrick[i][j].y + longueur;
}
/*-----------------------------------------------------------------------------*/


/*------------------------Gestion des REBONDS des BALLES-----------------------*/
//---------Sur les BORDS de l'écran
    void rebondBalle_Bord(int i, int j){
        //Balle touche J1
        if((balles[i].vitesseY > 0) && (balles[i].side_y_do >= WINDOW_HEIGHT)){
            if      ((balles[i].color_r == barres[1].color_r_contour) || 
                     (balles[i].color_g == barres[1].color_g_contour) || 
                     (balles[i].color_b == barres[1].color_b_contour)){
                tabResultat[1] += pointBalleToucheAdvers;
            }
            else if ((balles[i].color_r == barres[2].color_r_contour) || 
                     (balles[i].color_g == barres[2].color_g_contour) || 
                     (balles[i].color_b == barres[2].color_b_contour)){
                tabResultat[2] += pointBalleToucheAdvers;
            }
            else if ((balles[i].color_r == barres[3].color_r_contour) || 
                     (balles[i].color_g == barres[3].color_g_contour) || 
                     (balles[i].color_b == barres[3].color_b_contour)){
                tabResultat[3] += pointBalleToucheAdvers;
            }

            tabResultat[0] += pointBalleToucheSoi;
            tabResultat[0] += pointCombo * suiviCombo[0];
            suiviCombo[0] = 0;
            balles[i].vitesseY = -balles[i].vitesseY;
        }
        
        //Balle touche J2
        else if((balles[i].vitesseY < 0) && (balles[i].side_y_up <= 0)){
            if      ((balles[i].color_r == barres[0].color_r_contour) || 
                     (balles[i].color_g == barres[0].color_g_contour) || 
                     (balles[i].color_b == barres[0].color_b_contour)){
                tabResultat[0] += pointBalleToucheAdvers;
            }
            else if ((balles[i].color_r == barres[2].color_r_contour) || 
                     (balles[i].color_g == barres[1].color_g_contour) || 
                     (balles[i].color_b == barres[1].color_b_contour)){
                tabResultat[2] += pointBalleToucheAdvers;
            }
            else if ((balles[i].color_r == barres[3].color_r_contour) || 
                     (balles[i].color_g == barres[3].color_g_contour) || 
                     (balles[i].color_b == barres[3].color_b_contour)){
                tabResultat[3] += pointBalleToucheAdvers;
            }

            tabResultat[1] += pointBalleToucheSoi;
            tabResultat[1] += pointCombo * suiviCombo[1];
            suiviCombo[1] = 0;
            balles[i].vitesseY = -balles[i].vitesseY;
        }
        
        //Balle touche J3
        else if((balles[i].vitesseX < 0) && (balles[i].side_x_le <= xZoneJeuMulti)){
            if      ((balles[i].color_r == barres[0].color_r_contour) || 
                     (balles[i].color_g == barres[0].color_g_contour) || 
                     (balles[i].color_b == barres[0].color_b_contour)){
                tabResultat[0] += pointBalleToucheAdvers;
            }
            else if ((balles[i].color_r == barres[1].color_r_contour) || 
                     (balles[i].color_g == barres[1].color_g_contour) || 
                     (balles[i].color_b == barres[1].color_b_contour)){
                tabResultat[1] += pointBalleToucheAdvers;
            }
            else if ((balles[i].color_r == barres[3].color_r_contour) || 
                     (balles[i].color_g == barres[3].color_g_contour) || 
                     (balles[i].color_b == barres[3].color_b_contour)){
                tabResultat[3] += pointBalleToucheAdvers;
            }

            tabResultat[2] += pointBalleToucheSoi;
            tabResultat[2] += pointCombo * suiviCombo[2];
            suiviCombo[2] = 0;
            balles[i].vitesseX = -balles[i].vitesseX;
        }
        
        //Balle touche J4
        else if((balles[i].vitesseX > 0) && (balles[i].side_x_ri >= xZoneJeuMulti + longueurZoneJeuMulti)){
            if      ((balles[i].color_r == barres[0].color_r_contour) || 
                     (balles[i].color_g == barres[0].color_g_contour) || 
                     (balles[i].color_b == barres[0].color_b_contour)){
                tabResultat[0] += pointBalleToucheAdvers;
            }
            else if ((balles[i].color_r == barres[1].color_r_contour) || 
                     (balles[i].color_g == barres[1].color_g_contour) || 
                     (balles[i].color_b == barres[1].color_b_contour)){
                tabResultat[1] += pointBalleToucheAdvers;
            }
            else if ((balles[i].color_r == barres[2].color_r_contour) || 
                     (balles[i].color_g == barres[2].color_g_contour) || 
                     (balles[i].color_b == barres[2].color_b_contour)){
                tabResultat[2] += pointBalleToucheAdvers;
            }
            tabResultat[3] += pointBalleToucheSoi;
            tabResultat[3] += pointCombo * suiviCombo[3];
            suiviCombo[3] = 0;
            balles[i].vitesseX = -balles[i].vitesseX;
        }
    }
//---------------------------------
//---------Sur les BARRES----------
    void changement_couleurBalle(int i, int j){
        balles[i].color_r = barres[j].color_r_contour;
        balles[i].color_g = barres[j].color_g_contour;
        balles[i].color_b = barres[j].color_b_contour;
    }

    void rebondBalle_Barre_J1(int i){
        if(((balles[i].vitesseX > 0) && (balles[i].y >= barres[0].y) && (balles[i].y <= barres[0].y + barres[0].largeur) && (balles[i].side_x_ri >= barres[0].x) && (balles[i].x <= barres[0].x)) ||
           ((balles[i].vitesseX < 0) && (balles[i].y >= barres[0].y) && (balles[i].y <= barres[0].y + barres[0].largeur) && (balles[i].side_x_le <= barres[0].x + barres[0].longueur) && (balles[i].x >= barres[0].x + barres[0].longueur))
           ){
            balles[i].vitesseX = -balles[i].vitesseX;
            changement_couleurBalle(i, 0);
        }
        else if(((balles[i].vitesseY > 0) && (balles[i].x >= barres[0].x) && (balles[i].x <= barres[0].x + barres[0].longueur) && (balles[i].side_y_do >= barres[0].y) && (balles[i].y <= barres[0].y))){
            balles[i].vitesseY = -balles[i].vitesseY;
            changement_couleurBalle(i, 0);
        }
        else if(((balles[i].vitesseX > 0) && (balles[i].vitesseY > 0) && (balles[i].y <= barres[0].y) && (balles[i].side_y_do >= barres[0].y) && (balles[i].x <= barres[0].x) && (balles[i].side_x_ri >= barres[0].x)) ||
                ((balles[i].vitesseX < 0) && (balles[i].vitesseY > 0) && (balles[i].y <= barres[0].y) && (balles[i].side_y_do >= barres[0].y) && (balles[i].x >= barres[0].x + barres[0].longueur) && (balles[i].side_x_le <= barres[0].x + barres[0].longueur))
                ){
            balles[i].vitesseX = -balles[i].vitesseX;
            balles[i].vitesseY = -balles[i].vitesseY;
            changement_couleurBalle(i, 0);
        }
    }
    
    void rebondBalle_Barre_J2(int i){
        if(((balles[i].vitesseX > 0) && (balles[i].y >= barres[1].y) && (balles[i].y <= barres[1].y + barres[1].largeur) && (balles[i].side_x_ri >= barres[1].x) && (balles[i].x <= barres[1].x)) ||
           ((balles[i].vitesseX < 0) && (balles[i].y >= barres[1].y) && (balles[i].y <= barres[1].y + barres[1].largeur) && (balles[i].side_x_le <= barres[1].x + barres[1].longueur) && (balles[i].x >= barres[1].x + barres[1].longueur))
           ){
            balles[i].vitesseX = -balles[i].vitesseX;
            changement_couleurBalle(i, 1);
        }
        else if(((balles[i].vitesseY < 0) && (balles[i].x >= barres[1].x) && (balles[i].x <= barres[1].x + barres[1].longueur) && (balles[i].side_y_up <= barres[1].y + barres[1].largeur) && (balles[i].y >= barres[1].y + barres[1].largeur))){
            balles[i].vitesseY = -balles[i].vitesseY;
            changement_couleurBalle(i, 1);
        }
        else if(((balles[i].vitesseX > 0) && (balles[i].vitesseY < 0) && (balles[i].y >= barres[1].y + barres[1].longueur) && (balles[i].side_y_up <= barres[1].y + barres[1].longueur) && (balles[i].x <= barres[1].x) && (balles[i].side_x_ri >= barres[1].x)) ||
                ((balles[i].vitesseX < 0) && (balles[i].vitesseY < 0) && (balles[i].y >= barres[1].y + barres[1].longueur) && (balles[i].side_y_up <= barres[1].y + barres[1].longueur) && (balles[i].x >= barres[1].x + barres[1].longueur) && (balles[i].side_x_le <= barres[1].x + barres[1].longueur))
                ){
            balles[i].vitesseX = -balles[i].vitesseX;
            balles[i].vitesseY = -balles[i].vitesseY;
            changement_couleurBalle(i, 1);
        }
    }
    
    void rebondBalle_Barre_J3(int i){
        if(((balles[i].vitesseX < 0) && (balles[i].y >= barres[2].y) && (balles[i].y <= barres[2].y + barres[2].largeur) && (balles[i].side_x_le <= barres[2].x + barres[2].longueur) && (balles[i].x >= barres[2].x + barres[2].longueur))){
            balles[i].vitesseX = -balles[i].vitesseX;
            changement_couleurBalle(i, 2);
        }
        else if(((balles[i].vitesseY > 0) && (balles[i].x >= barres[2].x) && (balles[i].x <= barres[2].x + barres[2].longueur) && (balles[i].side_y_do >= barres[2].y) && (balles[i].y <= barres[2].y)) ||
                ((balles[i].vitesseY < 0) && (balles[i].x >= barres[2].x) && (balles[i].x <= barres[2].x + barres[2].longueur) && (balles[i].side_y_up <= barres[2].y + barres[2].largeur) && (balles[i].y >= barres[1].y + barres[2].largeur))
                ){
            balles[i].vitesseY = -balles[i].vitesseY;
            changement_couleurBalle(i, 2);
        }
        else if(((balles[i].vitesseY < 0) && (balles[i].vitesseX < 0) && (balles[i].x >= barres[2].x + barres[2].longueur) && (balles[i].side_x_le <= barres[2].x + barres[2].longueur) && (balles[i].y >= barres[2].y + barres[2].largeur) && (balles[i].side_y_up <= barres[2].y + barres[2].largeur)) ||
                ((balles[i].vitesseY > 0) && (balles[i].vitesseX < 0) && (balles[i].x >= barres[2].x + barres[2].longueur) && (balles[i].side_x_le <= barres[2].x + barres[2].longueur) && (balles[i].y >= barres[2].y) && (balles[i].side_y_do <= barres[2].y))
                ){
            balles[i].vitesseX = -balles[i].vitesseX;
            balles[i].vitesseY = -balles[i].vitesseY;
            changement_couleurBalle(i, 2);
        }
    }
    
    void rebondBalle_Barre_J4(int i){
        if(((balles[i].vitesseX > 0) && (balles[i].y >= barres[3].y) && (balles[i].y <= barres[3].y + barres[3].largeur) && (balles[i].side_x_ri >= barres[3].x) && (balles[i].x <= barres[3].x))){
            balles[i].vitesseX = -balles[i].vitesseX;
            changement_couleurBalle(i, 3);
        }
        else if(((balles[i].vitesseY > 0) && (balles[i].x >= barres[3].x) && (balles[i].x <= barres[3].x + barres[3].longueur) && (balles[i].side_y_do >= barres[3].y) && (balles[i].y <= barres[3].y)) ||
                ((balles[i].vitesseY < 0) && (balles[i].x >= barres[3].x) && (balles[i].x <= barres[3].x + barres[3].longueur) && (balles[i].side_y_up <= barres[3].y + barres[3].largeur) && (balles[i].y >= barres[3].y + barres[3].largeur))
                ){
            balles[i].vitesseY = -balles[i].vitesseY;
            changement_couleurBalle(i, 3);
        }
        else if(((balles[i].vitesseY < 0) && (balles[i].vitesseX > 0) && (balles[i].x <= barres[3].x) && (balles[i].side_x_ri >= barres[3].x) && (balles[i].y >= barres[3].y + barres[3].largeur) && (balles[i].side_y_up <= barres[3].y + barres[3].largeur)) ||
                ((balles[i].vitesseY > 0) && (balles[i].vitesseX > 0) && (balles[i].x >= barres[3].x) && (balles[i].side_x_le <= barres[3].x) && (balles[i].y <= barres[3].y) && (balles[i].side_y_do >= barres[3].y + barres[3].largeur))){
            balles[i].vitesseX = -balles[i].vitesseX;
            balles[i].vitesseY = -balles[i].vitesseY;
            changement_couleurBalle(i, 3);
        }
    }

    void rebondBalle_Barre(int i){
        rebondBalle_Barre_J1(i);
        rebondBalle_Barre_J2(i);
        rebondBalle_Barre_J3(i);
        rebondBalle_Barre_J4(i);
    }
//---------------------------------
//---------Sur les BRIQUES---------
    void rebondBalle_Bricks(int i, int j, int k){
        if((balles[k].vitesseX < 0) && (balles[k].x - rayonBall <= tabBrick[i][j].side_x_ri) && (balles[k].x > tabBrick[i][j].side_x_ri) && (balles[k].y >= tabBrick[i][j].side_y_up) && (balles[k].y <= tabBrick[i][j].side_y_do) ||
           (balles[k].vitesseX > 0) && (balles[k].x + rayonBall >= tabBrick[i][j].side_x_le) && (balles[k].x < tabBrick[i][j].side_x_le) && (balles[k].y >= tabBrick[i][j].side_y_up) && (balles[k].y <= tabBrick[i][j].side_y_do) 
        ){
            balles[k].vitesseX = -balles[k].vitesseX;
            tabBrick[i][j].vie--;
            tabBrick[i][j].rebond = 1;

            if(tabBrick[i][j].vie == 0){
                if((balles[k].color_r == barres[0].color_r_contour) &&
                   (balles[k].color_g == barres[0].color_g_contour) &&
                   (balles[k].color_b == barres[0].color_b_contour)
                ){
                    tabResultat[0] += pointBrickCassee * tabBrick[i][j].type;
                    suiviCombo[0]++;
                }
                else if((balles[k].color_r == barres[1].color_r_contour) &&
                        (balles[k].color_g == barres[1].color_g_contour) &&
                        (balles[k].color_b == barres[1].color_b_contour)
                ){
                    tabResultat[1] += pointBrickCassee * tabBrick[i][j].type;
                    suiviCombo[1]++;
                }
                else if((balles[k].color_r == barres[2].color_r_contour) &&
                        (balles[k].color_g == barres[2].color_g_contour) &&
                        (balles[k].color_b == barres[2].color_b_contour)
                ){
                    tabResultat[2] += pointBrickCassee * tabBrick[i][j].type;
                    suiviCombo[2]++;
                }
                else if((balles[k].color_r == barres[3].color_r_contour) &&
                        (balles[k].color_g == barres[3].color_g_contour) &&
                        (balles[k].color_b == barres[3].color_b_contour)
                ){
                    tabResultat[3] += pointBrickCassee * tabBrick[i][j].type;
                    suiviCombo[3]++;
                }
            }
        }

        else if((balles[k].vitesseY < 0) && (balles[k].y - rayonBall <= tabBrick[i][j].side_y_do) && (balles[k].y > tabBrick[i][j].side_y_do) && (balles[k].x >= tabBrick[i][j].side_x_le) && (balles[k].x <= tabBrick[i][j].side_x_ri) ||
                (balles[k].vitesseY > 0) && (balles[k].y + rayonBall >= tabBrick[i][j].side_y_up) && (balles[k].y < tabBrick[i][j].side_y_up) && (balles[k].x >= tabBrick[i][j].side_x_le) && (balles[k].x <= tabBrick[i][j].side_x_ri)
        ){
            balles[k].vitesseY = -balles[k].vitesseY;
            tabBrick[i][j].vie--;
            tabBrick[i][j].rebond = 1;

            if(tabBrick[i][j].vie == 0){
                if((balles[k].color_r == barres[0].color_r_contour) &&
                   (balles[k].color_g == barres[0].color_g_contour) &&
                   (balles[k].color_b == barres[0].color_b_contour)
                ){
                    tabResultat[0] += pointBrickCassee * tabBrick[i][j].type;
                }
                else if((balles[k].color_r == barres[1].color_r_contour) &&
                        (balles[k].color_g == barres[1].color_g_contour) &&
                        (balles[k].color_b == barres[1].color_b_contour)
                ){
                    tabResultat[1] += pointBrickCassee * tabBrick[i][j].type;
                }
                else if((balles[k].color_r == barres[2].color_r_contour) &&
                        (balles[k].color_g == barres[2].color_g_contour) &&
                        (balles[k].color_b == barres[2].color_b_contour)
                ){
                    tabResultat[2] += pointBrickCassee * tabBrick[i][j].type;
                }
                else if((balles[k].color_r == barres[3].color_r_contour) &&
                        (balles[k].color_g == barres[3].color_g_contour) &&
                        (balles[k].color_b == barres[3].color_b_contour)
                ){
                    tabResultat[3] += pointBrickCassee * tabBrick[i][j].type;
                }
            }
        }
        //Coins : Haut Droit -- Haut Gauche -- Bas Droit -- Bas Gauche
        else if((tabBrick[i - 1][j].vie > 0) && (balles[k].vitesseX > 0) && (balles[k].vitesseY > 0) && (balles[k].x < tabBrick[i][j].side_x_le) && (balles[k].x + rayonBall >= tabBrick[i][j].side_x_le) && (balles[k].y < tabBrick[i][j].side_y_up) && (balles[k].y + rayonBall >= tabBrick[i][j].side_y_up) ||
                (tabBrick[i - 1][j].vie > 0) && (balles[k].vitesseX < 0) && (balles[k].vitesseY > 0) && (balles[k].x > tabBrick[i][j].side_x_ri) && (balles[k].x - rayonBall <= tabBrick[i][j].side_x_ri) && (balles[k].y < tabBrick[i][j].side_y_up) && (balles[k].y + rayonBall >= tabBrick[i][j].side_y_up) ||
                (tabBrick[i + 1][j].vie > 0) && (balles[k].vitesseX > 0) && (balles[k].vitesseY < 0) && (balles[k].x < tabBrick[i][j].side_x_le) && (balles[k].x + rayonBall >= tabBrick[i][j].side_x_le) && (balles[k].y > tabBrick[i][j].side_y_do) && (balles[k].y - rayonBall <= tabBrick[i][j].side_y_do) ||
                (tabBrick[i + 1][j].vie > 0) && (balles[k].vitesseX < 0) && (balles[k].vitesseY < 0) && (balles[k].x > tabBrick[i][j].side_x_ri) && (balles[k].x - rayonBall <= tabBrick[i][j].side_x_ri) && (balles[k].y > tabBrick[i][j].side_y_do) && (balles[k].y - rayonBall <= tabBrick[i][j].side_y_do)
        ){
            balles[k].vitesseX = -balles[k].vitesseX;
            tabBrick[i][j].vie--; 
            tabBrick[i][j].rebond = 1;  

            if(tabBrick[i][j].vie == 0){
                if((balles[k].color_r == barres[0].color_r_contour) &&
                   (balles[k].color_g == barres[0].color_g_contour) &&
                   (balles[k].color_b == barres[0].color_b_contour)
                ){
                    tabResultat[0] += pointBrickCassee * tabBrick[i][j].type;
                }
                else if((balles[k].color_r == barres[1].color_r_contour) &&
                        (balles[k].color_g == barres[1].color_g_contour) &&
                        (balles[k].color_b == barres[1].color_b_contour)
                ){
                    tabResultat[1] += pointBrickCassee * tabBrick[i][j].type;
                }
                else if((balles[k].color_r == barres[2].color_r_contour) &&
                        (balles[k].color_g == barres[2].color_g_contour) &&
                        (balles[k].color_b == barres[2].color_b_contour)
                ){
                    tabResultat[2] += pointBrickCassee * tabBrick[i][j].type;
                }
                else if((balles[k].color_r == barres[3].color_r_contour) &&
                        (balles[k].color_g == barres[3].color_g_contour) &&
                        (balles[k].color_b == barres[3].color_b_contour)
                ){
                    tabResultat[3] += pointBrickCassee * tabBrick[i][j].type;
                }
            }              
        }
        //Coins : Haut Droit -- Haut Gauche -- Bas Droit -- Bas Gauche
        else if((tabBrick[i][j - 1].vie > 0) && (balles[k].vitesseX > 0) && (balles[k].vitesseY > 0) && (balles[k].x < tabBrick[i][j].side_x_le) && (balles[k].x + rayonBall >= tabBrick[i][j].side_x_le) && (balles[k].y < tabBrick[i][j].side_y_up) && (balles[k].y + rayonBall >= tabBrick[i][j].side_y_up) ||
                (tabBrick[i][j + 1].vie > 0) && (balles[k].vitesseX < 0) && (balles[k].vitesseY > 0) && (balles[k].x > tabBrick[i][j].side_x_ri) && (balles[k].x - rayonBall <= tabBrick[i][j].side_x_ri) && (balles[k].y < tabBrick[i][j].side_y_up) && (balles[k].y + rayonBall >= tabBrick[i][j].side_y_up) ||
                (tabBrick[i][j - 1].vie > 0) && (balles[k].vitesseX > 0) && (balles[k].vitesseY < 0) && (balles[k].x < tabBrick[i][j].side_x_le) && (balles[k].x + rayonBall >= tabBrick[i][j].side_x_le) && (balles[k].y > tabBrick[i][j].side_y_do) && (balles[k].y - rayonBall <= tabBrick[i][j].side_y_do) ||
                (tabBrick[i][j + 1].vie > 0) && (balles[k].vitesseX < 0) && (balles[k].vitesseY < 0) && (balles[k].x > tabBrick[i][j].side_x_ri) && (balles[k].x - rayonBall <= tabBrick[i][j].side_x_ri) && (balles[k].y > tabBrick[i][j].side_y_do) && (balles[k].y - rayonBall <= tabBrick[i][j].side_y_do)
        ){
            balles[k].vitesseY = -balles[k].vitesseY;
            tabBrick[i][j].vie--;   
            tabBrick[i][j].rebond = 1;    

            if(tabBrick[i][j].vie == 0){
                if((balles[k].color_r == barres[0].color_r_contour) &&
                   (balles[k].color_g == barres[0].color_g_contour) &&
                   (balles[k].color_b == barres[0].color_b_contour)
                ){
                    tabResultat[0] += pointBrickCassee * tabBrick[i][j].type;
                }
                else if((balles[k].color_r == barres[1].color_r_contour) &&
                        (balles[k].color_g == barres[1].color_g_contour) &&
                        (balles[k].color_b == barres[1].color_b_contour)
                ){
                    tabResultat[1] += pointBrickCassee * tabBrick[i][j].type;
                }
                else if((balles[k].color_r == barres[2].color_r_contour) &&
                        (balles[k].color_g == barres[2].color_g_contour) &&
                        (balles[k].color_b == barres[2].color_b_contour)
                ){
                    tabResultat[2] += pointBrickCassee * tabBrick[i][j].type;
                }
                else if((balles[k].color_r == barres[3].color_r_contour) &&
                        (balles[k].color_g == barres[3].color_g_contour) &&
                        (balles[k].color_b == barres[3].color_b_contour)
                ){
                    tabResultat[3] += pointBrickCassee * tabBrick[i][j].type;
                }
            }         
        }
        else if((balles[k].vitesseX > 0) && (balles[k].vitesseY > 0) && (balles[k].x < tabBrick[i][j].side_x_le) && (balles[k].x + rayonBall >= tabBrick[i][j].side_x_le) && (balles[k].y < tabBrick[i][j].side_y_up) && (balles[k].y + rayonBall >= tabBrick[i][j].side_y_up) ||
                (balles[k].vitesseX < 0) && (balles[k].vitesseY > 0) && (balles[k].x > tabBrick[i][j].side_x_ri) && (balles[k].x - rayonBall <= tabBrick[i][j].side_x_ri) && (balles[k].y < tabBrick[i][j].side_y_up) && (balles[k].y + rayonBall >= tabBrick[i][j].side_y_up) ||
                (balles[k].vitesseX > 0) && (balles[k].vitesseY < 0) && (balles[k].x < tabBrick[i][j].side_x_le) && (balles[k].x + rayonBall >= tabBrick[i][j].side_x_le) && (balles[k].y > tabBrick[i][j].side_y_do) && (balles[k].y - rayonBall <= tabBrick[i][j].side_y_do) ||
                (balles[k].vitesseX < 0) && (balles[k].vitesseY < 0) && (balles[k].x > tabBrick[i][j].side_x_ri) && (balles[k].x - rayonBall <= tabBrick[i][j].side_x_ri) && (balles[k].y > tabBrick[i][j].side_y_do) && (balles[k].y - rayonBall <= tabBrick[i][j].side_y_do)
        ){
            balles[k].vitesseX = -balles[k].vitesseX;
            balles[k].vitesseY = -balles[k].vitesseY;
            tabBrick[i][j].vie--;
            tabBrick[i][j].rebond = 1;

            if(tabBrick[i][j].vie == 0){
                if((balles[k].color_r == barres[0].color_r_contour) &&
                   (balles[k].color_g == barres[0].color_g_contour) &&
                   (balles[k].color_b == barres[0].color_b_contour)
                ){
                    tabResultat[0] += pointBrickCassee * tabBrick[i][j].type;
                }
                else if((balles[k].color_r == barres[1].color_r_contour) &&
                        (balles[k].color_g == barres[1].color_g_contour) &&
                        (balles[k].color_b == barres[1].color_b_contour)
                ){
                    tabResultat[1] += pointBrickCassee * tabBrick[i][j].type;
                }
                else if((balles[k].color_r == barres[2].color_r_contour) &&
                        (balles[k].color_g == barres[2].color_g_contour) &&
                        (balles[k].color_b == barres[2].color_b_contour)
                ){
                    tabResultat[2] += pointBrickCassee * tabBrick[i][j].type;
                }
                else if((balles[k].color_r == barres[3].color_r_contour) &&
                        (balles[k].color_g == barres[3].color_g_contour) &&
                        (balles[k].color_b == barres[3].color_b_contour)
                ){
                    tabResultat[3] += pointBrickCassee * tabBrick[i][j].type;
                }
            }
        }
    }
//---------------------------------
/*-----------------------------------------------------------------------------*/

/*------------------------Gestion des REBONDS des BARRES-----------------------*/
void rebondBarre_Bord(int i){
    if(barres[i].x <= xZoneJeuMulti){
        barres[i].x = xZoneJeuMulti + 1;
        barres[i].vitesseX = 0;
    }
    else if(barres[i].side_x_ri >= (xZoneJeuMulti + longueurZoneJeuMulti)){
        barres[i].x = (xZoneJeuMulti + longueurZoneJeuMulti) - barres[i].longueur - 1;
        barres[i].vitesseX = 0;
    }

    if(barres[i].side_y_up <= 0){
        barres[i].y = 1;
        barres[i].vitesseY = 0;
    }
    else if(barres[i].side_y_do >= longueurZoneJeuMulti){
        barres[i].y = WINDOW_HEIGHT - barres[i].largeur - 1;
        barres[i].vitesseY = 0;
    }
}
/*-----------------------------------------------------------------------------*/


/*----------------------------Apparition des BALLES----------------------------*/
void drawBalle(int i, int j){
    //permet de faire la mise qu'une fois
    if(j == 0){
        changeColor(balles[i].color_r, balles[i].color_g, balles[i].color_b, 255);
        drawCircle(balles[i].x, balles[i].y, rayonBall);
        if(start == 1){
            balles[i].x += balles[i].vitesseX;
            balles[i].y += balles[i].vitesseY;
        }
    }
    
    updateSide_Balles(i);
    updateSide_Barres(i);

    rebondBalle_Bord(i, j);
    rebondBalle_Barre(i);
}
/*-----------------------------------------------------------------------------*/

/*----------------------------Apparition des BARRES----------------------------*/
void gestionBot_J1(){
    for(int k = 0 ; k < nombreJoueur ; k++){
        if(k == 0) plusProche = 0;
        else if((balles[k].vitesseY > 0) && (balles[k].y < barres[0].side_y_up)){
            if(balles[k].y > balles[plusProche].y)      plusProche = k;
        }
    }

    if(tabBots[0].recharge >= tabBots[0].tempsReact){
        if(balles[plusProche].x > barres[0].x + (barres[0].longueur / 2))   barres[0].vitesseX = vitesseBarre;
        else                                                                barres[0].vitesseX = -vitesseBarre;

        tabBots[0].recharge = 0;
    }    
}

void gestionBot_J2(){
    for(int k = 0 ; k < nombreJoueur ; k++){
        if(k == 0) plusProche = 0;
        else if((balles[k].vitesseY < 0) && (balles[k].y > barres[1].side_y_do)){
            if(balles[k].y < balles[plusProche].y)      plusProche = k;
        }
    }

    if(tabBots[1].recharge >= tabBots[1].tempsReact){
        if(balles[plusProche].x > barres[1].x + (barres[1].longueur / 2))   barres[1].vitesseX = vitesseBarre;
        else                                                                barres[1].vitesseX = -vitesseBarre;

        tabBots[1].recharge = 0;
    }  
}

void gestionBot_J3(){
    for(int k = 0 ; k < nombreJoueur ; k++){
        if(k == 0) plusProche = 0;
        else if((balles[k].vitesseX < 0) && (balles[k].x > barres[2].side_x_ri)){
            if(balles[k].x < balles[plusProche].x)      plusProche = k;
        }
    }
    
    if(tabBots[2].recharge >= tabBots[2].tempsReact){
        if(balles[plusProche].y > barres[2].y + (barres[2].largeur / 2))    barres[2].vitesseY = vitesseBarre;
        else                                                                barres[2].vitesseY = -vitesseBarre;
        
        tabBots[2].recharge = 0;
    } 
}

void gestionBot_J4(){
    for(int k = 0 ; k < nombreJoueur ; k++){
        if(k == 0) plusProche = 0;
        else if((balles[k].vitesseX > 0) && (balles[k].x < barres[3].side_x_le)){
            if(balles[k].x > balles[plusProche].x)      plusProche = k;
        }
    }

    if(tabBots[3].recharge >= tabBots[3].tempsReact){
        if(balles[plusProche].y > barres[3].y + (barres[3].largeur / 2))    barres[3].vitesseY = vitesseBarre;
        else                                                                barres[3].vitesseY = -vitesseBarre; 
        
        tabBots[3].recharge = 0;
    }     
}

void gestionBot(){
    if(tabBots[0].actif == 1)       gestionBot_J1();
    if(tabBots[1].actif == 1)       gestionBot_J2();
    if(tabBots[2].actif == 1)       gestionBot_J3();
    if(tabBots[3].actif == 1)       gestionBot_J4();    
}

void drawBarre(int i, int j){
    //permet de faire la mise qu'une fois
    if(j == 0){
        changeColor(barres[i].color_r_contour, barres[i].color_g_contour, barres[i].color_b_contour, 255);
        drawRect(barres[i].x, barres[i].y, barres[i].longueur, barres[i].largeur);
        changeColor(barres[i].color_r_interieur, barres[i].color_g_interieur, barres[i].color_b_interieur, 255);
        drawRect(barres[i].x + 2, barres[i].y + 2, barres[i].longueur - 4, barres[i].largeur - 4);

        if(start == 1){
            barres[i].x += barres[i].vitesseX;
            barres[i].y += barres[i].vitesseY;
        }
    }

    gestionBot();

    rebondBarre_Bord(i);
}
/*-----------------------------------------------------------------------------*/

/*----------------------------Apparition des BRIQUES---------------------------*/
void spawnBricks(int i, int j){
    if((tabBrick[i][j].frame == 0) && (tabBrick[i][j].spawn == 0)){
        if     (tabBrick[i][j].vie == 1)    tabBrick[i][j].sourceIMG = "assets/brick6Type1.bmp";
        else if(tabBrick[i][j].vie == 2)    tabBrick[i][j].sourceIMG = "assets/brick6Type2.bmp";
        else if(tabBrick[i][j].vie == 3)    tabBrick[i][j].sourceIMG = "assets/brick6Type3.bmp";
        else                                tabBrick[i][j].sourceIMG = "assets/brick6Type4.bmp";
    }         
      
    else if((tabBrick[i][j].frame == 5) && (tabBrick[i][j].spawn == 0)){
        if     (tabBrick[i][j].vie == 1)    tabBrick[i][j].sourceIMG = "assets/brick5Type1.bmp";
        else if(tabBrick[i][j].vie == 2)    tabBrick[i][j].sourceIMG = "assets/brick5Type2.bmp";
        else if(tabBrick[i][j].vie == 3)    tabBrick[i][j].sourceIMG = "assets/brick5Type3.bmp";
        else                                tabBrick[i][j].sourceIMG = "assets/brick5Type4.bmp";
    }   
      
    else if((tabBrick[i][j].frame == 10) && (tabBrick[i][j].spawn == 0)){
        if     (tabBrick[i][j].vie == 1)    tabBrick[i][j].sourceIMG = "assets/brick4Type1.bmp";
        else if(tabBrick[i][j].vie == 2)    tabBrick[i][j].sourceIMG = "assets/brick4Type2.bmp";
        else if(tabBrick[i][j].vie == 3)    tabBrick[i][j].sourceIMG = "assets/brick4Type3.bmp";
        else                                tabBrick[i][j].sourceIMG = "assets/brick4Type4.bmp";
    }    
      
    else if((tabBrick[i][j].frame == 15) && (tabBrick[i][j].spawn == 0)){
        if     (tabBrick[i][j].vie == 1)    tabBrick[i][j].sourceIMG = "assets/brick3Type1.bmp";
        else if(tabBrick[i][j].vie == 2)    tabBrick[i][j].sourceIMG = "assets/brick3Type2.bmp";
        else if(tabBrick[i][j].vie == 3)    tabBrick[i][j].sourceIMG = "assets/brick3Type3.bmp";
        else                                tabBrick[i][j].sourceIMG = "assets/brick3Type4.bmp";
    }    
      
    else if((tabBrick[i][j].frame == 20) && (tabBrick[i][j].spawn == 0)){
        if     (tabBrick[i][j].vie == 1)    tabBrick[i][j].sourceIMG = "assets/brick2Type1.bmp";
        else if(tabBrick[i][j].vie == 2)    tabBrick[i][j].sourceIMG = "assets/brick2Type2.bmp";
        else if(tabBrick[i][j].vie == 3)    tabBrick[i][j].sourceIMG = "assets/brick2Type3.bmp";
        else                                tabBrick[i][j].sourceIMG = "assets/brick2Type4.bmp";
    }    
      
    else if((tabBrick[i][j].frame == 25) && (tabBrick[i][j].spawn == 0)){
        if     (tabBrick[i][j].vie == 1)    tabBrick[i][j].sourceIMG = "assets/brick1Type1.bmp";
        else if(tabBrick[i][j].vie == 2)    tabBrick[i][j].sourceIMG = "assets/brick1Type2.bmp";
        else if(tabBrick[i][j].vie == 3)    tabBrick[i][j].sourceIMG = "assets/brick1Type3.bmp";
        else                                tabBrick[i][j].sourceIMG = "assets/brick1Type4.bmp";
    }    
      
    else if((tabBrick[i][j].frame == 30) && (tabBrick[i][j].spawn == 0)){
        if     (tabBrick[i][j].vie == 1)    tabBrick[i][j].sourceIMG = "assets/brick0Type1.bmp";
        else if(tabBrick[i][j].vie == 2)    tabBrick[i][j].sourceIMG = "assets/brick0Type2.bmp";
        else if(tabBrick[i][j].vie == 3)    tabBrick[i][j].sourceIMG = "assets/brick0Type3.bmp";
        else                                tabBrick[i][j].sourceIMG = "assets/brick0Type4.bmp";
    }     
      
    if(tabBrick[i][j].frame == 30) tabBrick[i][j].spawn = 1;
}

void breakBricks(int i, int j){
    if     ((tabBrick[i][j].frame == 0) && (tabBrick[i][j].rebond == 1) && (tabBrick[i][j].vie == 0))       tabBrick[i][j].sourceIMG = "assets/brick0Type1.bmp";
      
    else if((tabBrick[i][j].frame == 5) && (tabBrick[i][j].rebond == 1) && (tabBrick[i][j].vie == 0))       tabBrick[i][j].sourceIMG = "assets/brick1Type1.bmp";
      
    else if((tabBrick[i][j].frame == 10) && (tabBrick[i][j].rebond == 1) && (tabBrick[i][j].vie == 0))       tabBrick[i][j].sourceIMG = "assets/brick2Type1.bmp";
      
    else if((tabBrick[i][j].frame == 15) && (tabBrick[i][j].rebond == 1) && (tabBrick[i][j].vie == 0))       tabBrick[i][j].sourceIMG = "assets/brick3Type1.bmp";
      
    else if((tabBrick[i][j].frame == 20) && (tabBrick[i][j].rebond == 1) && (tabBrick[i][j].vie == 0))       tabBrick[i][j].sourceIMG = "assets/brick4Type1.bmp";
      
    else if((tabBrick[i][j].frame == 25) && (tabBrick[i][j].rebond == 1) && (tabBrick[i][j].vie == 0))      tabBrick[i][j].sourceIMG = "assets/brick5Type1.bmp";
      
    else if((tabBrick[i][j].frame == 30) && (tabBrick[i][j].rebond == 1) && (tabBrick[i][j].vie == 0))      tabBrick[i][j].sourceIMG = "assets/brick6Type1.bmp";
      

    if((tabBrick[i][j].frame == 30) && (tabBrick[i][j].rebond == 1) && (tabBrick[i][j].vie == 0)) tabBrick[i][j].rebond = 2;

}

void showBricks(int i, int j){
    if(tabBrick[i][j].spawn == 1){
        if     (tabBrick[i][j].vie == 1)    tabBrick[i][j].sourceIMG = "assets/brick0Type1.bmp";
        else if(tabBrick[i][j].vie == 2)    tabBrick[i][j].sourceIMG = "assets/brick0Type2.bmp";
        else if(tabBrick[i][j].vie == 3)    tabBrick[i][j].sourceIMG = "assets/brick0Type3.bmp";
        else                                tabBrick[i][j].sourceIMG = "assets/brick0Type4.bmp";
    }
}

void drawBricks(int i, int j, int k){
    /*----------------------------Briques----------------------------*/
    if(tabBrick[i][j].vie > 0){
        spawnBricks(i, j);
        showBricks(i, j);

        if(k == 0){
            sprite(tabBrick[i][j].x, tabBrick[i][j].y, tabBrick[i][j].sourceIMG);
        }
        
        
        updateSide_Bricks(i, j);

        if(balles[k].fantome == 0){
            rebondBalle_Bricks(i, j, k);
        }
    }
    else if(tabBrick[i][j].rebond == 1){
        breakBricks(i, j);
        sprite(tabBrick[i][j].x, tabBrick[i][j].y, tabBrick[i][j].sourceIMG);
    }
    else{
        tabBrick[i][j].temps++;
        if(tabBrick[i][j].temps >= 45 * FPS){ //respawn en 45sec
            tabBrick[i][j].spawn = 0;
            tabBrick[i][j].rebond = 0;
            tabBrick[i][j].frame = 0;

            init_vieBricks(i, j);
            tabBrick[i][j].temps = 0;
        }
    }

    if(tabBrick[i][j].frame == 30){
        tabBrick[i][j].frame = 0;
    }else{
        tabBrick[i][j].frame++;
    }
    /*---------------------------------------------------------------*/
}
/*-----------------------------------------------------------------------------*/

void drawResultat(int joueur){
    unit = tabResultat[joueur];
    nombreChiffres = 0;
    longueurResultat = 0;

    do{
        if     (unit % 10 == 0)     longueurResultat += tabChiffres[joueur][0].longueur + 5; //espace entre chiffre
        else if(unit % 10 == 1)     longueurResultat += tabChiffres[joueur][1].longueur + 5;
        else if(unit % 10 == 2)     longueurResultat += tabChiffres[joueur][2].longueur + 5;
        else if(unit % 10 == 3)     longueurResultat += tabChiffres[joueur][3].longueur + 5;
        else if(unit % 10 == 4)     longueurResultat += tabChiffres[joueur][4].longueur + 5;
        else if(unit % 10 == 5)     longueurResultat += tabChiffres[joueur][5].longueur + 5;
        else if(unit % 10 == 6)     longueurResultat += tabChiffres[joueur][6].longueur + 5;
        else if(unit % 10 == 7)     longueurResultat += tabChiffres[joueur][7].longueur + 5;
        else if(unit % 10 == 8)     longueurResultat += tabChiffres[joueur][8].longueur + 5;
        else if(unit % 10 == 9)     longueurResultat += tabChiffres[joueur][9].longueur + 5;

        unit = unit / 10;
    }
    while(unit > 0);

    unit = tabResultat[joueur];

    if      (joueur == 0)       positionResultat = (WINDOW_WIDTH - longueurResultat) / 2;
    else if (joueur == 1)       positionResultat = (WINDOW_WIDTH - longueurResultat) / 2;
    else if (joueur == 2)       positionResultat = (WINDOW_HEIGHT - longueurResultat) / 2;
    else if (joueur == 3){
        positionResultat = (WINDOW_HEIGHT - longueurResultat) / 2;
        longueurResultat = 0;
    }      

    do{
        if     (unit % 10 == 0){
            if      (joueur == 0){
                longueurResultat -= tabChiffres[joueur][0].longueur + 5;
                sprite(positionResultat + longueurResultat, WINDOW_HEIGHT - 120, "assets/img0J1.bmp");
            }
            else if (joueur == 1){
                longueurResultat -= tabChiffres[joueur][0].longueur + 5;
                sprite(positionResultat + longueurResultat, 120 - tabChiffres[joueur][0].largeur, "assets/img0J2.bmp");
            }
            else if (joueur == 2){
                longueurResultat -= tabChiffres[joueur][0].longueur + 5;
                sprite(xZoneJeuMulti + 120 - tabChiffres[joueur][0].largeur, positionResultat + longueurResultat, "assets/img0J3.bmp");
            }
            else if (joueur == 3){
                sprite(xZoneJeuMulti + longueurZoneJeuMulti - 120, positionResultat + longueurResultat, "assets/img0J4.bmp");
                longueurResultat += tabChiffres[joueur][0].longueur + 5;
            }
        }
        else if(unit % 10 == 1){
            if      (joueur == 0){
                longueurResultat -= tabChiffres[joueur][1].longueur + 5;
                sprite(positionResultat + longueurResultat, WINDOW_HEIGHT - 120, "assets/img1J1.bmp");
            }
            else if (joueur == 1){
                longueurResultat -= tabChiffres[joueur][1].longueur + 5;
                sprite(positionResultat + longueurResultat, 120 - tabChiffres[joueur][0].largeur, "assets/img1J2.bmp");
            }
            else if (joueur == 2){
                longueurResultat -= tabChiffres[joueur][1].longueur + 5;
                sprite(xZoneJeuMulti + 120 - tabChiffres[joueur][0].largeur, positionResultat + longueurResultat, "assets/img1J3.bmp");
            }
            else if (joueur == 3){
                sprite(xZoneJeuMulti + longueurZoneJeuMulti - 120, positionResultat + longueurResultat, "assets/img1J4.bmp");
                longueurResultat += tabChiffres[joueur][1].longueur + 5;
            }
        }
        else if(unit % 10 == 2){
            if      (joueur == 0){
                longueurResultat -= tabChiffres[joueur][2].longueur + 5;
                sprite(positionResultat + longueurResultat, WINDOW_HEIGHT - 120, "assets/img2J1.bmp");
            }
            else if (joueur == 1){
                longueurResultat -= tabChiffres[joueur][2].longueur + 5;
                sprite(positionResultat + longueurResultat, 120 - tabChiffres[joueur][0].largeur, "assets/img2J2.bmp");
            }
            else if (joueur == 2){
                longueurResultat -= tabChiffres[joueur][2].longueur + 5;
                sprite(xZoneJeuMulti + 120 - tabChiffres[joueur][0].largeur, positionResultat + longueurResultat, "assets/img2J3.bmp");
            }
            else if (joueur == 3){
                sprite(xZoneJeuMulti + longueurZoneJeuMulti - 120, positionResultat + longueurResultat, "assets/img2J4.bmp");
                longueurResultat += tabChiffres[joueur][2].longueur + 5;
            }
        }
        else if(unit % 10 == 3){
            if      (joueur == 0){
                longueurResultat -= tabChiffres[joueur][3].longueur + 5;
                sprite(positionResultat + longueurResultat, WINDOW_HEIGHT - 120, "assets/img3J1.bmp");
            }
            else if (joueur == 1){
                longueurResultat -= tabChiffres[joueur][3].longueur + 5;
                sprite(positionResultat + longueurResultat, 120 - tabChiffres[joueur][0].largeur, "assets/img3J2.bmp");
            }
            else if (joueur == 2){
                longueurResultat -= tabChiffres[joueur][3].longueur + 5;
                sprite(xZoneJeuMulti + 120 - tabChiffres[joueur][0].largeur, positionResultat + longueurResultat, "assets/img3J3.bmp");
            }
            else if (joueur == 3){
                sprite(xZoneJeuMulti + longueurZoneJeuMulti - 120, positionResultat + longueurResultat, "assets/img3J4.bmp");
                longueurResultat += tabChiffres[joueur][3].longueur + 5;
            }
        }
        else if(unit % 10 == 4){
            if      (joueur == 0){
                longueurResultat -= tabChiffres[joueur][4].longueur + 5;
                sprite(positionResultat + longueurResultat, WINDOW_HEIGHT - 120, "assets/img4J1.bmp");
            }
            else if (joueur == 1){
                longueurResultat -= tabChiffres[joueur][4].longueur + 5;
                sprite(positionResultat + longueurResultat, 120 - tabChiffres[joueur][0].largeur, "assets/img4J2.bmp");
            }
            else if (joueur == 2){
                longueurResultat -= tabChiffres[joueur][4].longueur + 5;
                sprite(xZoneJeuMulti + 120 - tabChiffres[joueur][0].largeur, positionResultat + longueurResultat, "assets/img4J3.bmp");
            }
            else if (joueur == 3){
                sprite(xZoneJeuMulti + longueurZoneJeuMulti - 120, positionResultat + longueurResultat, "assets/img4J4.bmp");
                longueurResultat += tabChiffres[joueur][4].longueur + 5;
            }
        }
        else if(unit % 10 == 5){
            if      (joueur == 0){
                longueurResultat -= tabChiffres[joueur][5].longueur + 5;
                sprite(positionResultat + longueurResultat, WINDOW_HEIGHT - 120, "assets/img5J1.bmp");
            }
            else if (joueur == 1){
                longueurResultat -= tabChiffres[joueur][5].longueur + 5;
                sprite(positionResultat + longueurResultat, 120 - tabChiffres[joueur][0].largeur, "assets/img5J2.bmp");
            }
            else if (joueur == 2){
                longueurResultat -= tabChiffres[joueur][5].longueur + 5;
                sprite(xZoneJeuMulti + 120 - tabChiffres[joueur][0].largeur, positionResultat + longueurResultat, "assets/img5J3.bmp");
            }
            else if (joueur == 3){
                sprite(xZoneJeuMulti + longueurZoneJeuMulti - 120, positionResultat + longueurResultat, "assets/img5J4.bmp");
                longueurResultat += tabChiffres[joueur][5].longueur + 5;
            }
        }
        else if(unit % 10 == 6){
            if      (joueur == 0){
                longueurResultat -= tabChiffres[joueur][6].longueur + 5;
                sprite(positionResultat + longueurResultat, WINDOW_HEIGHT - 120, "assets/img6J1.bmp");
            }
            else if (joueur == 1){
                longueurResultat -= tabChiffres[joueur][6].longueur + 5;
                sprite(positionResultat + longueurResultat, 120 - tabChiffres[joueur][0].largeur, "assets/img6J2.bmp");
            }
            else if (joueur == 2){
                longueurResultat -= tabChiffres[joueur][6].longueur + 5;
                sprite(xZoneJeuMulti + 120 - tabChiffres[joueur][0].largeur, positionResultat + longueurResultat, "assets/img6J3.bmp");
            }
            else if (joueur == 3){
                sprite(xZoneJeuMulti + longueurZoneJeuMulti - 120, positionResultat + longueurResultat, "assets/img6J4.bmp");
                longueurResultat += tabChiffres[joueur][6].longueur + 5;
            }
        }
        else if(unit % 10 == 7){
            if      (joueur == 0){
                longueurResultat -= tabChiffres[joueur][7].longueur + 5;
                sprite(positionResultat + longueurResultat, WINDOW_HEIGHT - 120, "assets/img7J1.bmp");
            }
            else if (joueur == 1){
                longueurResultat -= tabChiffres[joueur][7].longueur + 5;
                sprite(positionResultat + longueurResultat, 120 - tabChiffres[joueur][0].largeur, "assets/img7J2.bmp");
            }
            else if (joueur == 2){
                longueurResultat -= tabChiffres[joueur][7].longueur + 5;
                sprite(xZoneJeuMulti + 120 - tabChiffres[joueur][0].largeur, positionResultat + longueurResultat, "assets/img7J3.bmp");
            }
            else if (joueur == 3){
                sprite(xZoneJeuMulti + longueurZoneJeuMulti - 120, positionResultat + longueurResultat, "assets/img7J4.bmp");
                longueurResultat += tabChiffres[joueur][7].longueur + 5;
            }
        }
        else if(unit % 10 == 8){
            if      (joueur == 0){
                longueurResultat -= tabChiffres[joueur][8].longueur + 5;
                sprite(positionResultat + longueurResultat, WINDOW_HEIGHT - 120, "assets/img8J1.bmp");
            }
            else if (joueur == 1){
                longueurResultat -= tabChiffres[joueur][8].longueur + 5;
                sprite(positionResultat + longueurResultat, 120 - tabChiffres[joueur][0].largeur, "assets/img8J2.bmp");
            }
            else if (joueur == 2){
                longueurResultat -= tabChiffres[joueur][8].longueur + 5;
                sprite(xZoneJeuMulti + 120 - tabChiffres[joueur][0].largeur, positionResultat + longueurResultat, "assets/img8J3.bmp");
            }
            else if (joueur == 3){
                sprite(xZoneJeuMulti + longueurZoneJeuMulti - 120, positionResultat + longueurResultat, "assets/img8J4.bmp");
                longueurResultat += tabChiffres[joueur][8].longueur + 5;
            }
        }
        else if(unit % 10 == 9){
            if      (joueur == 0){
                longueurResultat -= tabChiffres[joueur][9].longueur + 5;
                sprite(positionResultat + longueurResultat, WINDOW_HEIGHT - 120, "assets/img9J1.bmp");
            }
            else if (joueur == 1){
                longueurResultat -= tabChiffres[joueur][9].longueur + 5;
                sprite(positionResultat + longueurResultat, 120 - tabChiffres[joueur][0].largeur, "assets/img9J2.bmp");
            }
            else if (joueur == 2){
                longueurResultat -= tabChiffres[joueur][9].longueur + 5;
                sprite(xZoneJeuMulti + 120 - tabChiffres[joueur][0].largeur, positionResultat + longueurResultat, "assets/img9J3.bmp");
            }
            else if (joueur == 3){
                sprite(xZoneJeuMulti + longueurZoneJeuMulti - 120, positionResultat + longueurResultat, "assets/img9J4.bmp");
                longueurResultat += tabChiffres[joueur][9].longueur + 5;
            }
        }
        
        unit = unit / 10;
    }
    while(unit > 0);    
}


void showZoneJ1(){
    changeColor(255, 0, 0, 100);
    drawRect(xdebutBrick - 5, (WINDOW_HEIGHT - 56 - 223), (NOMBRE_COLONNE * (longueur + 5) - 3), (223 + 1));
    
    for(int i = 0 ; i < (223) ; i++){
        drawLine((xZoneJeuMulti + 56 + 3 + i), //xC
                 (WINDOW_HEIGHT - 56), //yC
                 (xZoneJeuMulti + 56 + 3 + i), //xA
                 (WINDOW_HEIGHT - 56 - i) //yA
                );

        drawLine((xZoneJeuMulti + longueurZoneJeuMulti - 56 - 3 - i), //xD
                 (WINDOW_HEIGHT - 56), //yD
                 (xZoneJeuMulti + longueurZoneJeuMulti - 56 - 3 - i), //xB
                 (WINDOW_HEIGHT - 56 - i) //yB
                );
    }
}

void showZoneJ2(){
    changeColor(255, 255, 0, 100);
    drawRect(xdebutBrick - 5, (56), (NOMBRE_COLONNE * (longueur + 5) - 3), (223 + 1));
    

    for(int i = 0 ; i < (223) ; i++){
        drawLine((xZoneJeuMulti + 56 + 3 + i), //xC
                 (56), //yC
                 (xZoneJeuMulti + 56 + 3 + i), //xA
                 (56 + i) //yA
                );

        drawLine((xZoneJeuMulti + longueurZoneJeuMulti - 56 - 3 - i), //xD
                 (56), //yD
                 (xZoneJeuMulti + longueurZoneJeuMulti - 56 - 3 - i), //xB
                 (56 + i) //yB
                );
    }
}

void showZoneJ3(){
    changeColor(255, 0, 102, 100);
    drawRect((xZoneJeuMulti + 56), (ydebutBrick - 6), (223), (NOMBRE_COLONNE * (longueur + 5) - 3));
    
    for(int i = 0 ; i < (223) ; i++){
        drawLine((xZoneJeuMulti + 56), //xD
                 (WINDOW_HEIGHT - 56 - 3 - i), //yD 
                 (xZoneJeuMulti + 56 + i), //xB
                 (WINDOW_HEIGHT - 56 - 3 - i) //yB
                );

        drawLine((xZoneJeuMulti + 56), //xC
                 (56 + 3 + i), //yC
                 (xZoneJeuMulti + 56+ i), //xA
                 (56 + 3 + i) //yA
                );
    }
}

void showZoneJ4(){
    changeColor(0, 0, 255, 100);
    drawRect((xZoneJeuMulti + longueurZoneJeuMulti - 56 - 223), (ydebutBrick - 6), (223), (NOMBRE_COLONNE * (longueur + 5) - 3));
    
    for(int i = 0 ; i < (223) ; i++){
        drawLine((xZoneJeuMulti + longueurZoneJeuMulti - 56 - 1), //xD
                 (WINDOW_HEIGHT - 56 - 3 - i), //yD
                 (xZoneJeuMulti + longueurZoneJeuMulti - 56 - 1 - i), //xB
                 (WINDOW_HEIGHT - 56 - 3 - i) //yB
                );

        drawLine((xZoneJeuMulti + longueurZoneJeuMulti - 56 - 1), //x
                 (56 + 3 + i), //y
                 (xZoneJeuMulti + longueurZoneJeuMulti - 56 - 1 - i), //xB
                 (56 + 3 + i) //yD
                );
        
    }
}


void viewZoneJ1(int bonus, int malus){
    int xA = (xZoneJeuMulti + 56 + 3 + 223);
    int yA = (WINDOW_HEIGHT - 56 - 223);
    int xC = (xZoneJeuMulti + 56 + 3);
    int yC = (WINDOW_HEIGHT - 56);

    int a1 = (yA - yC) / (xA - xC);
    int b1 = yA - a1 * xA;

    int xB = (xZoneJeuMulti + longueurZoneJeuMulti - 56 - 3 - 223);
    int yB = (WINDOW_HEIGHT - 56 - 223);
    int xD = (xZoneJeuMulti + longueurZoneJeuMulti - 56 - 3);
    int yD = (WINDOW_HEIGHT - 56);

    int a2 = (yB - yD) / (xB - xD);
    int b2 = yB - a2 * xB;

    for(int i = 0 ; i < nombreJoueur ; i++){
        if(((balles[i].y + rayonBall >= a1 * balles[i].x + b1) && (balles[i].y + rayonBall <= yC) && (balles[i].x >= xC) && (balles[i].x <= xA)) ||
           ((balles[i].y - rayonBall >= yA) && (balles[i].y + rayonBall <= yC) && (balles[i].x - rayonBall >= xA) && (balles[i].x + rayonBall <= xB)) ||
           ((balles[i].y + rayonBall >= a2 * balles[i].x + b2) && (balles[i].y + rayonBall <= yC) && (balles[i].x >= xB) && (balles[i].x <= xD)))
        {
            if(bonus == 1){
                if(balles[i].vitesseX < 0)         balles[i].vitesseX = - VITESSEBALLE + 2;
                else                               balles[i].vitesseX = VITESSEBALLE - 2;

                if(balles[i].vitesseY < 0)         balles[i].vitesseY = - VITESSEBALLE + 2;
                else                               balles[i].vitesseY = VITESSEBALLE - 2;
            }
            if(malus == 1){
                if(balles[i].vitesseX < 0)         balles[i].vitesseX = - VITESSEBALLE - 2;
                else                               balles[i].vitesseX = VITESSEBALLE + 2;

                if(balles[i].vitesseY < 0)         balles[i].vitesseY = - VITESSEBALLE - 2;
                else                               balles[i].vitesseY = VITESSEBALLE + 2;
            }
                                         
        }
    }
}

void viewZoneJ2(int bonus, int malus){
    int xA = (xZoneJeuMulti + 56 + 3 + 223);
    int yA = (56 + 223);
    int xC = (xZoneJeuMulti + 56 + 3);
    int yC = (56);

    int a1 = (yA - yC) / (xA - xC);
    int b1 = yA - a1 * xA;

    int xB = (xZoneJeuMulti + longueurZoneJeuMulti - 56 - 3 - 223);
    int yB = (56 + 223);
    int xD = (xZoneJeuMulti + longueurZoneJeuMulti - 56 - 3);
    int yD = (56);

    int a2 = (yB - yD) / (xB - xD);
    int b2 = yB - a2 * xB;

    for(int i = 0 ; i < nombreJoueur ; i++){
        if(((balles[i].y - rayonBall <= a1 * balles[i].x + b1) && (balles[i].y - rayonBall >= yC) && (balles[i].x >= xC) && (balles[i].x <= xA)) ||
           ((balles[i].y - rayonBall <= a2 * balles[i].x + b2) && (balles[i].y - rayonBall >= yC) && (balles[i].x >= xB) && (balles[i].x <= xD)) ||
           ((balles[i].y + rayonBall <= yA) && (balles[i].y - rayonBall >= yC) && (balles[i].x - rayonBall >= xA) && (balles[i].x + rayonBall <= xB))
          ){
            if(bonus == 1){
                if(balles[i].vitesseX < 0)         balles[i].vitesseX = - VITESSEBALLE + 2;
                else                               balles[i].vitesseX = VITESSEBALLE - 2;

                if(balles[i].vitesseY < 0)         balles[i].vitesseY = - VITESSEBALLE + 2;
                else                               balles[i].vitesseY = VITESSEBALLE - 2;
            }
            if(malus == 1){
                if(balles[i].vitesseX < 0)         balles[i].vitesseX = - VITESSEBALLE - 2;
                else                               balles[i].vitesseX = VITESSEBALLE + 2;

                if(balles[i].vitesseY < 0)         balles[i].vitesseY = - VITESSEBALLE - 2;
                else                               balles[i].vitesseY = VITESSEBALLE + 2;
            }
          }
    }
}

void viewZoneJ3(int bonus, int malus){
    int xA = (xZoneJeuMulti + 56 + 223);
    int yA = (56 + 3 + 223);
    int xC = (xZoneJeuMulti + 56);
    int yC = (56 + 3);

    int a1 = (yA - yC) / (xA - xC);
    int b1 = yA - a1 * xA;

    int xB = (xZoneJeuMulti + 56 + 223);
    int yB = (WINDOW_HEIGHT - 56 - 3 - 223);
    int xD = (xZoneJeuMulti + 56);
    int yD = (WINDOW_HEIGHT - 56 - 3);

    int a2 = (yB - yD) / (xB - xD);
    int b2 = yB - a2 * xB;

    changeColor(0,255,255,255);
    drawPoint(xA, yA);
    drawPoint(xB, yB);
    drawPoint(xC, yC);
    drawPoint(xD, yD);

    for(int i = 0 ; i < nombreJoueur ; i++){
        if(((balles[i].y + rayonBall >= a1 * balles[i].x + b1) && (balles[i].x >= xC) && (balles[i].x <= xA)) &&
           ((balles[i].y - rayonBall <= a2 * balles[i].x + b2) && (balles[i].x >= xD) && (balles[i].x <= xB))
          ){
            if(bonus == 1){
                if(balles[i].vitesseX < 0)         balles[i].vitesseX = - VITESSEBALLE + 2;
                else                               balles[i].vitesseX = VITESSEBALLE - 2;

                if(balles[i].vitesseY < 0)         balles[i].vitesseY = - VITESSEBALLE + 2;
                else                               balles[i].vitesseY = VITESSEBALLE - 2;
            }
            if(malus == 1){
                if(balles[i].vitesseX < 0)         balles[i].vitesseX = - VITESSEBALLE - 2;
                else                               balles[i].vitesseX = VITESSEBALLE + 2;

                if(balles[i].vitesseY < 0)         balles[i].vitesseY = - VITESSEBALLE - 2;
                else                               balles[i].vitesseY = VITESSEBALLE + 2;
            }
          }
    }
}

void viewZoneJ4(int bonus, int malus){
    int xA = (xZoneJeuMulti + longueurZoneJeuMulti - 56 - 1 - 223);
    int yA = (56 + 3 + 223);
    int xC = (xZoneJeuMulti + longueurZoneJeuMulti - 56 - 1);
    int yC = (56 + 3);

    int a1 = (yA - yC) / (xA - xC);
    int b1 = yA - a1 * xA;

    int xB = (xZoneJeuMulti + longueurZoneJeuMulti - 56 - 1 - 223);
    int yB = (WINDOW_HEIGHT - 56 - 3 - 223);
    int xD = (xZoneJeuMulti + longueurZoneJeuMulti - 56 - 1);
    int yD = (WINDOW_HEIGHT - 56 - 3);

    int a2 = (yB - yD) / (xB - xD);
    int b2 = yB - a2 * xB;

    changeColor(0,255,255,255);
    drawPoint(xA, yA);
    drawPoint(xB, yB);
    drawPoint(xC, yC);
    drawPoint(xD, yD);

    for(int i = 0 ; i < nombreJoueur ; i++){
        if(((balles[i].y + rayonBall >= a1 * balles[i].x + b1) && (balles[i].x <= xC) && (balles[i].x >= xA)) &&
           ((balles[i].y - rayonBall <= a2 * balles[i].x + b2) && (balles[i].x <= xD) && (balles[i].x >= xB))
          ){
            if(bonus == 1){
                if(balles[i].vitesseX < 0)         balles[i].vitesseX = - VITESSEBALLE + 2;
                else                               balles[i].vitesseX = VITESSEBALLE - 2;

                if(balles[i].vitesseY < 0)         balles[i].vitesseY = - VITESSEBALLE + 2;
                else                               balles[i].vitesseY = VITESSEBALLE - 2;
            }
            if(malus == 1){
                if(balles[i].vitesseX < 0)         balles[i].vitesseX = - VITESSEBALLE - 2;
                else                               balles[i].vitesseX = VITESSEBALLE + 2;

                if(balles[i].vitesseY < 0)         balles[i].vitesseY = - VITESSEBALLE - 2;
                else                               balles[i].vitesseY = VITESSEBALLE + 2;
            }
          }
    }
}


void gestionBonusMalus(int joueur){
    if((joueur == 0) && (tabBonusMalus[joueur].tempsRecharge == 5 * FPS))           printf("BonusMalus prêt pour J1\n");
    else if((joueur == 1) && (tabBonusMalus[joueur].tempsRecharge == 5 * FPS))      printf("BonusMalus prêt pour J2\n");
    else if((joueur == 2) && (tabBonusMalus[joueur].tempsRecharge == 5 * FPS))      printf("BonusMalus prêt pour J3\n");
    else if((joueur == 3) && (tabBonusMalus[joueur].tempsRecharge == 5 * FPS))      printf("BonusMalus prêt pour J4\n");


    if(tabBonusMalus[joueur].activerB == 1){
        if((tabBonusMalus[joueur].typeB == 0) && (tabBonusMalus[joueur].tempsActif <= 2 * FPS)){ //2sec
            if(joueur < 2)      barres[joueur].longueur = 150;
            else                barres[joueur].largeur = 150;

            if(tabBonusMalus[joueur].tempsActif == 2 * FPS){
                printf("la");
                tabBonusMalus[joueur].tempsRecharge = 0;
                tabBonusMalus[joueur].activerB = 0;
                tabBonusMalus[joueur].tempsActif = 0;

                tabBonusMalus[joueur].typeB = rand() % 3;

                if(joueur < 2)          barres[joueur].longueur = 100;
                else                    barres[joueur].largeur = 100;

                pointCombo = 200;
                tabBonusMalus[joueur].tempsActif = 0;
            }
        }
        
        else if((tabBonusMalus[joueur].typeB == 1) && (tabBonusMalus[joueur].tempsActif <= 2 * FPS)){//2sec
            pointCombo = pointCombo * 2;

            if(tabBonusMalus[joueur].tempsActif == 2 * FPS){
                tabBonusMalus[joueur].tempsRecharge = 0;
                tabBonusMalus[joueur].activerB = 0;
                tabBonusMalus[joueur].tempsActif = 0;

                tabBonusMalus[joueur].typeB = rand() % 3;

                if(joueur < 2)          barres[joueur].longueur = 100;
                else                    barres[joueur].largeur = 100;

                pointCombo = 200;
                tabBonusMalus[joueur].tempsActif = 0;
            }
        }
        
        else if((tabBonusMalus[joueur].typeB == 2) && (tabBonusMalus[joueur].tempsActif <= 2 * FPS)){//2sec
            if(joueur == 0){
                showZoneJ1();
                viewZoneJ1(tabBonusMalus[joueur].activerB, tabBonusMalus[joueur].activerM);
            }
            else if(joueur == 1){
                showZoneJ2();
                viewZoneJ2(tabBonusMalus[joueur].activerB, tabBonusMalus[joueur].activerM);
            }
            else if(joueur == 2){
                showZoneJ3();
                viewZoneJ3(tabBonusMalus[joueur].activerB, tabBonusMalus[joueur].activerM);
            }
            else if(joueur == 3){
                showZoneJ4();
                viewZoneJ4(tabBonusMalus[joueur].activerB, tabBonusMalus[joueur].activerM);
            }

            if(tabBonusMalus[joueur].tempsActif == 2 * FPS){
                tabBonusMalus[joueur].tempsRecharge = 0;
                tabBonusMalus[joueur].activerB = 0;
                tabBonusMalus[joueur].tempsActif = 0;

                tabBonusMalus[joueur].typeB = rand() % 3;

                if(joueur < 2)          barres[joueur].longueur = 100;
                else                    barres[joueur].largeur = 100;

                pointCombo = 200;
                tabBonusMalus[joueur].tempsActif = 0;

                for(int i = 0 ; i < nombreJoueur ; i++){

                    if(balles[i].vitesseX < 0)      balles[i].vitesseX = -VITESSEBALLE;
                    else                            balles[i].vitesseX = VITESSEBALLE;

                    if(balles[i].vitesseY < 0)      balles[i].vitesseY = -VITESSEBALLE;
                    else                            balles[i].vitesseY = VITESSEBALLE;
                }
            }
        }

        tabBonusMalus[joueur].tempsActif++;
    }

    if(tabBonusMalus[joueur].activerM == 1){
        for(int i = 0 ; i < nombreJoueur ; i++){
            if((tabBonusMalus[joueur].typeM == 0) && (tabBonusMalus[joueur].tempsActif <= 2 * FPS) && (i != joueur)){ //2sec
                vitesseBarre = 10;

                if(tabBonusMalus[joueur].tempsActif == 2 * FPS){
                    tabBonusMalus[joueur].tempsRecharge = 0;
                    tabBonusMalus[joueur].activerM = 0;
                    tabBonusMalus[joueur].tempsActif = 0;

                    tabBonusMalus[joueur].typeM = rand() % 3;

                    vitesseBarre = 6;
                }
            }

            else if((tabBonusMalus[joueur].typeM == 1) && (tabBonusMalus[joueur].tempsActif <= 2 * FPS) && (i != joueur)){//2sec
                if(joueur == 0){
                    showZoneJ1();
                    viewZoneJ1(tabBonusMalus[joueur].activerB, tabBonusMalus[joueur].activerM);
                }
                else if(joueur == 1){
                    showZoneJ2();
                    viewZoneJ2(tabBonusMalus[joueur].activerB, tabBonusMalus[joueur].activerM);
                }
                else if(joueur == 2){
                    showZoneJ3();
                    viewZoneJ3(tabBonusMalus[joueur].activerB, tabBonusMalus[joueur].activerM);
                }
                else if(joueur == 3){
                    showZoneJ4();
                    viewZoneJ4(tabBonusMalus[joueur].activerB, tabBonusMalus[joueur].activerM);
                }

                if(tabBonusMalus[joueur].tempsActif == 2 * FPS){
                    tabBonusMalus[joueur].tempsRecharge = 0;
                    tabBonusMalus[joueur].activerM = 0;
                    tabBonusMalus[joueur].tempsActif = 0;

                    tabBonusMalus[joueur].typeM = rand() % 3;

                    if(joueur < 2)          barres[joueur].longueur = 100;
                    else                    barres[joueur].largeur = 100;

                    tabBonusMalus[joueur].tempsActif = 0;

                    for(int i = 0 ; i < nombreJoueur ; i++){

                        if(balles[i].vitesseX < 0)      balles[i].vitesseX = -VITESSEBALLE;
                        else                            balles[i].vitesseX = VITESSEBALLE;

                        if(balles[i].vitesseY < 0)      balles[i].vitesseY = -VITESSEBALLE;
                        else                            balles[i].vitesseY = VITESSEBALLE;
                    }
                }
            }

            else if((tabBonusMalus[joueur].typeM == 2) && (tabBonusMalus[joueur].tempsActif <= 20 * FPS) && (i != joueur)){//20sec
                if     ((balles[i].color_r == barres[joueur].color_r_contour) &&
                        (balles[i].color_g == barres[joueur].color_g_contour) && 
                        (balles[i].color_b == barres[joueur].color_b_contour))         
                    balles[i].fantome = 0;
                else 
                    balles[i].fantome = 1;

                if(tabBonusMalus[joueur].tempsActif == 20 * FPS){
                    tabBonusMalus[joueur].tempsRecharge = 0;
                    tabBonusMalus[joueur].activerM = 0;
                    tabBonusMalus[joueur].tempsActif = 0;

                    tabBonusMalus[joueur].typeM = rand() % 3;

                    if(joueur < 2)          barres[joueur].longueur = 100;
                    else                    barres[joueur].largeur = 100;

                    for(int i = 0 ; i < nombreJoueur ; i++){

                        if(balles[i].vitesseX < 0)      balles[i].vitesseX = -VITESSEBALLE;
                        else                            balles[i].vitesseX = VITESSEBALLE;

                        if(balles[i].vitesseY < 0)      balles[i].vitesseY = -VITESSEBALLE;
                        else                            balles[i].vitesseY = VITESSEBALLE;

                        balles[i].fantome = 0;
                    }
                }
            }
        }

        tabBonusMalus[joueur].tempsActif++;
    }

    tabBonusMalus[joueur].tempsRecharge++;
}



void drawMenus(){
    // backgroundMenu();

    switch (pageMenu){
        case 0:
            if(hoverStart == 1)         sprite(0, 0, "assets/menu/texte/menuStartRed.bmp");
            else                        sprite(0, 0, "assets/menu/texte/menuStart.bmp");
            break;

        case 1:
            backgroundMenu();
            sprite(0, 0, "assets/menu/texte/enteteSettings.bmp");
            sprite(235, 430, "assets/menu/texte/settings1.bmp");

            switch (modeSelection){
                case 1:     //TIME
                    sprite(modeSelectionCentre - 101, 430, "assets/menu/texte/modeSelection1.bmp"); // 101 : widhtTime / 2
                    sprite(1665, 435, "assets/menu/chevron/modeRight.bmp");

                    sprite(535, 570, "assets/menu/texte/settings2.bmp");

                    //-----------minutes------------
                    int coordMinute = 847;
                    int espaceMinute = 3;
                    chiffreMinute = 0;

                    unitMinute = minutesLimite;

                    do{
                        if(chiffreMinute == 1)     espaceMinute = -espaceMinute;
                        
                        if     (unitMinute % 10 == 0)  sprite(coordMinute + espaceMinute - chiffreMinute * 64, 865, "assets/menu/chiffre/0.bmp");
                        else if(unitMinute % 10 == 1)  sprite(coordMinute + espaceMinute - chiffreMinute * 27, 865, "assets/menu/chiffre/1.bmp");
                        else if(unitMinute % 10 == 2)  sprite(coordMinute + espaceMinute - chiffreMinute * 62, 865, "assets/menu/chiffre/2.bmp");
                        else if(unitMinute % 10 == 3)  sprite(coordMinute + espaceMinute - chiffreMinute * 59, 865, "assets/menu/chiffre/3.bmp");
                        else if(unitMinute % 10 == 4)  sprite(coordMinute + espaceMinute - chiffreMinute * 63, 865, "assets/menu/chiffre/4.bmp");
                        else if(unitMinute % 10 == 5)  sprite(coordMinute + espaceMinute - chiffreMinute * 62, 865, "assets/menu/chiffre/5.bmp");
                        else if(unitMinute % 10 == 6)  sprite(coordMinute + espaceMinute - chiffreMinute * 63, 865, "assets/menu/chiffre/6.bmp");
                        else if(unitMinute % 10 == 7)  sprite(coordMinute + espaceMinute - chiffreMinute * 57, 865, "assets/menu/chiffre/7.bmp");
                        else if(unitMinute % 10 == 8)  sprite(coordMinute + espaceMinute - chiffreMinute * 63, 865, "assets/menu/chiffre/8.bmp");
                        else if(unitMinute % 10 == 9)  sprite(coordMinute + espaceMinute - chiffreMinute * 63, 865, "assets/menu/chiffre/9.bmp");
                        
                        unitMinute = unitMinute / 10;
                        chiffreMinute++;
                    }while(chiffreMinute < 2); 
                    //------------------------------

                    sprite(((WINDOW_WIDTH / 2) - (16 / 2)), 865, "assets/menu/chiffre/points.bmp");
                    
                    //-----------secondes-----------
                    
                    int coordSeconde = 1070;
                    int espaceSeconde = 3;
                    chiffreSeconde = 0;

                    unitSeconde = secondesLimite;

                    do{
                        if(chiffreSeconde == 1)     espaceSeconde = -espaceSeconde;
                        
                        if     (unitSeconde % 10 == 0)  sprite(coordSeconde + espaceSeconde - chiffreSeconde * 64, 865, "assets/menu/chiffre/0.bmp");
                        else if(unitSeconde % 10 == 1)  sprite(coordSeconde + espaceSeconde - chiffreSeconde * 27, 865, "assets/menu/chiffre/1.bmp");
                        else if(unitSeconde % 10 == 2)  sprite(coordSeconde + espaceSeconde - chiffreSeconde * 62, 865, "assets/menu/chiffre/2.bmp");
                        else if(unitSeconde % 10 == 3)  sprite(coordSeconde + espaceSeconde - chiffreSeconde * 59, 865, "assets/menu/chiffre/3.bmp");
                        else if(unitSeconde % 10 == 4)  sprite(coordSeconde + espaceSeconde - chiffreSeconde * 63, 865, "assets/menu/chiffre/4.bmp");
                        else if(unitSeconde % 10 == 5)  sprite(coordSeconde + espaceSeconde - chiffreSeconde * 62, 865, "assets/menu/chiffre/5.bmp");
                        else if(unitSeconde % 10 == 6)  sprite(coordSeconde + espaceSeconde - chiffreSeconde * 63, 865, "assets/menu/chiffre/6.bmp");
                        else if(unitSeconde % 10 == 7)  sprite(coordSeconde + espaceSeconde - chiffreSeconde * 57, 865, "assets/menu/chiffre/7.bmp");
                        else if(unitSeconde % 10 == 8)  sprite(coordSeconde + espaceSeconde - chiffreSeconde * 63, 865, "assets/menu/chiffre/8.bmp");
                        else if(unitSeconde % 10 == 9)  sprite(coordSeconde + espaceSeconde - chiffreSeconde * 63, 865, "assets/menu/chiffre/9.bmp");
                        
                        unitSeconde = unitSeconde / 10;
                        chiffreSeconde++;
                    }while(chiffreSeconde < 2);
                    //------------------------------

                    if(minutesLimite >= 5){
                        sprite(coordMinute - (55 / 2), 950, "assets/menu/chevron/timeDown.bmp");
                    }
                    else if(minutesLimite <= 0){
                        sprite(coordMinute - (55 / 2), 805, "assets/menu/chevron/timeUp.bmp");
                    }
                    else{
                        sprite(coordMinute - (55 / 2), 805, "assets/menu/chevron/timeUp.bmp"); 
                        sprite(coordMinute - (55 / 2), 950, "assets/menu/chevron/timeDown.bmp");
                    }

                    if(secondesLimite >= 55){
                        sprite(coordSeconde - (55 / 2), 950, "assets/menu/chevron/timeDown.bmp");
                    }
                    else if(secondesLimite <= 0){
                        sprite(coordSeconde - (55 / 2), 805, "assets/menu/chevron/timeUp.bmp");
                    }
                    else{
                        sprite(coordSeconde - (55 / 2), 805, "assets/menu/chevron/timeUp.bmp"); 
                        sprite(coordSeconde - (55 / 2), 950, "assets/menu/chevron/timeDown.bmp");
                    }    
                    break;

                case 2:     //SCORE
                    sprite(1265, 435, "assets/menu/chevron/modeLeft.bmp");
                    sprite(modeSelectionCentre - 108, 430, "assets/menu/texte/modeSelection2.bmp"); // 101 : widhtTime / 2
                    sprite(1665, 435, "assets/menu/chevron/modeRight.bmp");

                    sprite(445, 570, "assets/menu/texte/settings3.bmp");

                    if(scoreLimite < 0)     scoreLimite = 0;

                    int coordScoreCentre = 1269;
                    int espaceScore = -6;
                    chiffreScore = 0;

                    unitScore = scoreLimite;
                    int i = 8;
                    do{
                        if     (unitScore % 10 == 0)  sprite(coordScoreCentre - 64 / 2, 865, "assets/menu/chiffre/0.bmp");
                        else if(unitScore % 10 == 1)  sprite(coordScoreCentre - 27 / 2, 865, "assets/menu/chiffre/1.bmp");
                        else if(unitScore % 10 == 2)  sprite(coordScoreCentre - 62 / 2, 865, "assets/menu/chiffre/2.bmp");
                        else if(unitScore % 10 == 3)  sprite(coordScoreCentre - 59 / 2, 865, "assets/menu/chiffre/3.bmp");
                        else if(unitScore % 10 == 4)  sprite(coordScoreCentre - 63 / 2, 865, "assets/menu/chiffre/4.bmp");
                        else if(unitScore % 10 == 5)  sprite(coordScoreCentre - 62 / 2, 865, "assets/menu/chiffre/5.bmp");
                        else if(unitScore % 10 == 6)  sprite(coordScoreCentre - 63 / 2, 865, "assets/menu/chiffre/6.bmp");
                        else if(unitScore % 10 == 7)  sprite(coordScoreCentre - 57 / 2, 865, "assets/menu/chiffre/7.bmp");
                        else if(unitScore % 10 == 8)  sprite(coordScoreCentre - 63 / 2, 865, "assets/menu/chiffre/8.bmp");
                        else if(unitScore % 10 == 9)  sprite(coordScoreCentre - 63 / 2, 865, "assets/menu/chiffre/9.bmp");

                        sprite(coordScoreCentre - 32 / 2, 827, "assets/menu/chevron/pointsUp.bmp");
                        sprite(coordScoreCentre - 32 / 2, 941, "assets/menu/chevron/pointsDown.bmp");

                        chiffreScore++;

                        if((chiffreScore % 3 == 0) && (chiffreScore < 9)){ 
                            coordScoreCentre -= (27 + espaceScore);
                            sprite(coordScoreCentre - (27 + 27 / 2) , 865, "assets/menu/chiffre/virgule.bmp");
                        }

                        coordScoreCentre -= 64 - espaceScore;

                        unitScore = unitScore / 10;
                    }while(chiffreScore < 9);
                    break;

                case 3:     //HEART
                    sprite(1265, 435, "assets/menu/chevron/modeLeft.bmp");
                    sprite(modeSelectionCentre - 174, 430, "assets/menu/texte/modeSelection3.bmp"); // 101 : widhtTime / 2

                    sprite(535, 570, "assets/menu/texte/settings4.bmp");

                    int coordHeartCentre = 1000;

                    unitHeart = heartLimit;

                    sprite(830, 840, "assets/menu/autre/healthSettings.bmp");

                    if     (unitHeart == 1)     sprite(coordHeartCentre - 27 / 2, 865, "assets/menu/chiffre/1.bmp");
                    else if(unitHeart == 2)     sprite(coordHeartCentre - 62 / 2, 865, "assets/menu/chiffre/2.bmp");
                    else if(unitHeart == 3)     sprite(coordHeartCentre - 59 / 2, 865, "assets/menu/chiffre/3.bmp");
                    else if(unitHeart == 4)     sprite(coordHeartCentre - 63 / 2, 865, "assets/menu/chiffre/4.bmp");
                    else if(unitHeart == 5)     sprite(coordHeartCentre - 62 / 2, 865, "assets/menu/chiffre/5.bmp");

                    if(unitHeart <= 1)          sprite(coordHeartCentre - 32 / 2, 827, "assets/menu/chevron/heartUp.bmp");
                    else if(unitHeart >= 5)     sprite(coordHeartCentre - 32 / 2, 941, "assets/menu/chevron/heartDown.bmp");
                    else{
                        sprite(coordHeartCentre - 32 / 2, 827, "assets/menu/chevron/heartUp.bmp");
                        sprite(coordHeartCentre - 32 / 2, 941, "assets/menu/chevron/heartDown.bmp");
                    }

                    
                    break;   

                default:
                    break;
            }
            break;

        case 2:
            backgroundMenu();
            sprite(0, 0, "assets/menu/texte/enteteSettings.bmp");
            sprite(0, 310, "assets/menu/texte/joueurSelection.bmp");
            int centre;

            for(int i = 0 ; i < 4 ; i++){
                switch (i){
                    case 0:
                        centre = 500;
                        if (playerReady[0][0] < 0)      sprite(centre - 64 / 2, 900, "assets/menu/touche/Z.bmp");
                        else                            sprite(centre - 64 / 2, 900, "assets/menu/touche/ZReady.bmp");

                        if (playerReady[0][1] < 0)      sprite(centre - 64 / 2, 970, "assets/menu/touche/S.bmp");
                        else                            sprite(centre - 64 / 2, 970, "assets/menu/touche/SReady.bmp");

                        if (playerReady[0][2] < 0)      sprite(centre - (64 / 2 + 70), 970, "assets/menu/touche/Q.bmp");
                        else                            sprite(centre - (64 / 2 + 70), 970, "assets/menu/touche/QReady.bmp");

                        if (playerReady[0][3] < 0)      sprite(centre + (64 / 2 + 6), 970, "assets/menu/touche/D.bmp");
                        else                            sprite(centre + (64 / 2 + 6), 970, "assets/menu/touche/DReady.bmp");
                        
                        break;
                    case 1:
                        centre = 805;
                        if (playerReady[1][0] < 0)      sprite(centre - 64 / 2, 900, "assets/menu/touche/O.bmp");
                        else                            sprite(centre - 64 / 2, 900, "assets/menu/touche/OReady.bmp");

                        if (playerReady[1][1] < 0)      sprite(centre - 64 / 2, 970, "assets/menu/touche/L.bmp");
                        else                            sprite(centre - 64 / 2, 970, "assets/menu/touche/LReady.bmp");

                        if (playerReady[1][2] < 0)      sprite(centre - (64 / 2 + 70), 970, "assets/menu/touche/K.bmp");
                        else                            sprite(centre - (64 / 2 + 70), 970, "assets/menu/touche/KReady.bmp");

                        if (playerReady[1][3] < 0)      sprite(centre + (64 / 2 + 6), 970, "assets/menu/touche/M.bmp");
                        else                            sprite(centre + (64 / 2 + 6), 970, "assets/menu/touche/MReady.bmp");
                        break;
                    case 2:
                        centre = 1110;
                        if (playerReady[2][0] < 0)      sprite(centre - 64 / 2, 900, "assets/menu/touche/G.bmp");
                        else                            sprite(centre - 64 / 2, 900, "assets/menu/touche/GReady.bmp");

                        if (playerReady[2][1] < 0)      sprite(centre - 64 / 2, 970, "assets/menu/touche/V.bmp");
                        else                            sprite(centre - 64 / 2, 970, "assets/menu/touche/VReady.bmp");

                        if (playerReady[2][2] < 0)      sprite(centre - (64 / 2 + 70), 970, "assets/menu/touche/C.bmp");
                        else                            sprite(centre - (64 / 2 + 70), 970, "assets/menu/touche/CReady.bmp");

                        if (playerReady[2][3] < 0)      sprite(centre + (64 / 2 + 6), 970, "assets/menu/touche/B.bmp");
                        else                            sprite(centre + (64 / 2 + 6), 970, "assets/menu/touche/BReady.bmp");
                        break;
                    case 3:
                        centre = 1415;
                        if (playerReady[3][0] < 0)      sprite(centre - 64 / 2, 900, "assets/menu/touche/Up.bmp");
                        else                            sprite(centre - 64 / 2, 900, "assets/menu/touche/UpReady.bmp");

                        if (playerReady[3][1] < 0)      sprite(centre - 64 / 2, 970, "assets/menu/touche/Down.bmp");
                        else                            sprite(centre - 64 / 2, 970, "assets/menu/touche/DownReady.bmp");

                        if (playerReady[3][2] < 0)      sprite(centre - (64 / 2 + 70), 970, "assets/menu/touche/Left.bmp");
                        else                            sprite(centre - (64 / 2 + 70), 970, "assets/menu/touche/LeftReady.bmp");

                        if (playerReady[3][3] < 0)      sprite(centre + (64 / 2 + 6), 970, "assets/menu/touche/Right.bmp");
                        else                            sprite(centre + (64 / 2 + 6), 970, "assets/menu/touche/RightReady.bmp");
                        break;
                    default:
                        break;
                }

                switch (playerSelection[i]){
                    case 1:
                        sprite(centre - 87 / 2, 600, "assets/menu/texte/human.bmp");
                        sprite(centre + (87 / 2 + 10), 607, "assets/menu/chevron/playerRight.bmp");
                        break;
                    case 2:
                        sprite(centre - (87 / 2 + 10 + 17), 607, "assets/menu/chevron/playerLeft.bmp");
                        sprite(centre - 45 / 2, 600, "assets/menu/texte/bot.bmp");
                        sprite(centre + (87 / 2 + 10), 607, "assets/menu/chevron/playerRight.bmp");
                        break;
                    case 3:
                        sprite(centre - (87 / 2 + 10 + 17), 607, "assets/menu/chevron/playerLeft.bmp");
                        sprite(centre - 63 / 2, 600, "assets/menu/texte/none.bmp");
                        break;
                    default:
                        break;
                }
                
                if(playerSelection[i] == 2){
                    switch (playerDifficulty[i]){
                        case 1:
                            sprite(centre - 57 / 2, 700, "assets/menu/texte/easy.bmp");
                            sprite(centre + (87 / 2 + 10), 707, "assets/menu/chevron/playerRight.bmp");
                            break;
                        case 2:
                            sprite(centre - (87 / 2 + 10 + 17), 707, "assets/menu/chevron/playerLeft.bmp");
                            sprite(centre - 89 / 2, 700, "assets/menu/texte/normal.bmp");
                            sprite(centre + (87 / 2 + 10), 707, "assets/menu/chevron/playerRight.bmp");
                            break;
                        case 3:
                            sprite(centre - (87 / 2 + 10 + 17), 707, "assets/menu/chevron/playerLeft.bmp");
                            sprite(centre - 30 / 2, 700, "assets/menu/texte/hard.bmp");
                            sprite(centre + (87 / 2 + 10), 707, "assets/menu/chevron/playerRight.bmp");
                            break;
                        case 4:
                            sprite(centre - (87 / 2 + 10 + 17), 707, "assets/menu/chevron/playerLeft.bmp");
                            sprite(centre - 57 / 2, 700, "assets/menu/texte/difficulté4.bmp");
                            break;
                        default:
                            break;
                    }
                }
               
                if(playerReady[i][0] + playerReady[i][1] + playerReady[i][2] + playerReady[i][3] == 4){
                    sprite(centre - 73 / 2, 780, "assets/menu/autre/pandaFire.bmp");
                }
                else{
                    sprite(centre - 74 / 2, 780, "assets/menu/autre/pandaSleep.bmp");
                }
            }
            break;
        default:
            break;
    }
}

void finPartie(){
    for(int i = 0 ; i < nombreJoueur ; i++){
        // if(tabResultat[i] >= 2000){
        //     start = 0;
        //     page = 0;
        //     chargement = 0;
        // }

        if(tempsJeu >= 20 * FPS){
            start = 0;
            page = 0;
            chargement = 0;
        }
    }
}

void drawPartie(){
    background();

    if(chargement == 0){
        resetPartie();
        chargement = 1;
    }
    
    //Separer les 'for' pour que l'ordre d'affichage soit dans le bon ordre
    for(int i = 0 ; i < nombreJoueur ; i++){
        drawResultat(i);
    }

    for(int i = 0 ; i < nombreJoueur ; i++){
        gestionBonusMalus(i);
    }

    for(int i = 0 ; i < nombreJoueur ; i++){
        for(int j = 0 ; j < nombreJoueur; j++){
            drawBalle(i, j);
        }
    }

    for(int i = 0 ; i < nombreJoueur ; i++){
        for(int j = 0 ; j < nombreJoueur; j++){
            drawBarre(i, j);
        }
    }

    for(int i = 0 ; i < NOMBRE_LIGNE ; i++){
        for(int j = 0 ; j < NOMBRE_COLONNE ; j++){
            for(int k = 0 ; k < nombreJoueur ; k++){
                drawBricks(i,j,k);
            }
        }
    }

    tempsJeu++;
    tabBots[0].recharge++;
    
    finPartie();
}


void MouseButton(int mouseX, int mouseY){
    if((hoverStart == 1) && (pageMenu == 0)){
        pageMenu = 1;
    }

    if((hoverModeSelectionLeft == 1) && (modeSelection > 1) && (modeSelection <= 3)){
        modeSelection--;
    }
    if((hoverModeSelectionRight == 1) && (modeSelection >= 1) && (modeSelection < 3)){
        modeSelection++;
    }


    if((hoverMinuteUp == 1) && (minutesLimite >= 0) && (minutesLimite < 5)){ 
        minutesLimite++;
    }
    if((hoverMinuteDown == 1) && (minutesLimite > 0) && (minutesLimite <= 5)){ 
        minutesLimite--;
    }
    if((hoverSecondeUp == 1) && (secondesLimite >= 0) && (secondesLimite < 55)) {    
        secondesLimite += 5;
    }
    if((hoverSecondeDown == 1) && (secondesLimite > 0) && (secondesLimite <= 55)){   
        secondesLimite -= 5;
    }

    int power = 1;
    for(int i = 0 ; i < 9 ; i++){
        if(hoverScoreUp[i] == 1)        scoreLimite += power;
        if(hoverScoreDown[i] == 1)      scoreLimite -= power;
        
        power = power * 10;
    }

    if((hoverHeartUp == 1) && (heartLimit >= 1) && (heartLimit < 5)){
        heartLimit++;
    }
    if((hoverHeartDown == 1) && (heartLimit > 1) && (heartLimit <= 5)){
        heartLimit--;
    }

    for(int i = 0 ; i < 4 ; i++){
        if((hoverPlayerSelectionLeft[i] == 1) && (playerSelection[i] > 1) && (playerSelection[i] <= 3)){
            playerSelection[i]--;
        }
        if((hoverPlayerSelectionRight[i] == 1) && (playerSelection[i] >= 1) && (playerSelection[i] < 3)){
            playerSelection[i]++;
        }

        if((hoverPlayerDifficultyLeft[i] == 1) && (playerDifficulty[i] > 1) && (playerDifficulty[i] <= 4)){
            playerDifficulty[i]--;
        }
        if((hoverPlayerDifficultyRight[i] == 1) && (playerDifficulty[i] >= 1) && (playerDifficulty[i] < 4)){
            playerDifficulty[i]++;
        }
    }
}

void MouseMotion(int mouseX, int mouseY){
    if((pageMenu == 0) && (mouseX >= 835) && (mouseX <= 1085) && (mouseY >= 655) && (mouseY <= 715)){ 
        hoverStart = 1;
    }
    else if((pageMenu == 1)){
        if((mouseX >= 1265) && (mouseX <= 1265 + 25) && (mouseY >= 435) && (mouseY <= 435 + 40)){ //25 : widhtChevron -- 40 : heighChevron
            hoverModeSelectionLeft = 1;
        }
        else if((mouseX >= 1665) && (mouseX <= 1665 + 25) && (mouseY >= 435) && (mouseY <= 435 + 40)){ //25 : widhtChevron -- 40 : heighChevron
            hoverModeSelectionRight = 1;
        }
        else{
            hoverModeSelectionLeft = 0;
            hoverModeSelectionRight = 0;
        }
        int coord;
        switch (modeSelection){
            case 1:
                if((mouseX >= 847 - (55 / 2)) && (mouseX <= 847 + (55 / 2)) && (mouseY >= 805) && (mouseY <= 805 + 35)){
                    hoverMinuteUp = 1;
                }
                else if((mouseX >= 847 - (55 / 2)) && (mouseX <= 847 + (55 / 2)) && (mouseY >= 950) && (mouseY <= 950 + 35)){
                    hoverMinuteDown = 1;
                }
                else{
                    hoverMinuteUp = 0;
                    hoverMinuteDown = 0;
                }

                if((mouseX >= 1070 - (55 / 2)) && (mouseX <= 1070 + (55 / 2)) && (mouseY >= 805) && (mouseY <= 805 + 35)){
                    hoverSecondeUp = 1;
                }
                else if((mouseX >= 1070 - (55 / 2)) && (mouseX <= 1070 + (55 / 2)) && (mouseY >= 950) && (mouseY <= 950 + 35)){
                    hoverSecondeDown = 1;
                }
                else{                    
                    hoverSecondeUp = 0;
                    hoverSecondeDown = 0;
                }
                break;
            case 2:
                coord = 1269;
                for(int i = 0 ; i < 9 ; i++){
                    if((mouseX >= coord - 32 / 2) && (mouseX <= coord + 32 / 2) && (mouseY >= 827) && (mouseY <= 827 + 21)){
                        hoverScoreUp[i] = 1;
                    }
                    else{
                        hoverScoreUp[i] = 0;
                    }

                    if((mouseX >= coord - 32 / 2) && (mouseX <= coord + 32 / 2) && (mouseY >= 941) && (mouseY <= 941 + 21)){
                        hoverScoreDown[i] = 1;
                    }
                    else{
                        hoverScoreDown[i] = 0;
                    }

                    if(((i + 1) % 3 == 0) && ((i + 1) < 9)) coord -= (27 / 2 + 6); // pas compris -- pas besoin se /2 pour l'affichage

                    coord -= (64 + 6);
                }
                break;

            case 3:
                coord = 1000;
                if((mouseX >= coord - 32 / 2) && (mouseX <= coord + 32 / 2) && (mouseY >= 827) && (mouseY <= 827 + 21)){
                    hoverHeartUp = 1;
                }
                else{
                    hoverHeartUp = 0;
                }

                if((mouseX >= coord - 32 / 2) && (mouseX <= coord + 32 / 2) && (mouseY >= 941) && (mouseY <= 941 + 21)){
                    hoverHeartDown = 1;
                }
                else{
                    hoverHeartDown = 0;
                }
                break;
            default:
                break;
        }
    }
    else if((pageMenu == 2)){
        int centre;
        for(int i = 0 ; i < 4 ; i++){
            switch (i){
                case 0:
                    centre = 500;
                    break;
                case 1:
                    centre = 805;
                    break;
                case 2:
                    centre = 1110;
                    break;
                case 3:
                    centre = 1415;
                    break;
                default:
                    break;
            }

            if((mouseX >= (centre - (87 / 2 + 10 + 17))) && (mouseX <= (centre - (87 / 2 + 10))) && (mouseY >= (607)) && (mouseY <= (607 + 25))){
                switch (centre){
                case 500:
                    hoverPlayerSelectionLeft[0] = 1;
                    break;
                case 805:
                    hoverPlayerSelectionLeft[1] = 1;
                    break;
                case 1110:
                    hoverPlayerSelectionLeft[2] = 1;
                    break;
                case 1415:
                    hoverPlayerSelectionLeft[3] = 1;
                    break;
                default:
                    break;
                }
            }
            else{
                hoverPlayerSelectionLeft[i] = 0;
            }

            if((mouseX >= (centre + (87 / 2 + 10))) && (mouseX <= (centre + (87 / 2 + 10 + 17))) && (mouseY >= (607)) && (mouseY <= (607 + 25))){
                switch (centre){
                case 500:
                    hoverPlayerSelectionRight[0] = 1;
                    break;
                case 805:
                    hoverPlayerSelectionRight[1] = 1;
                    break;
                case 1110:
                    hoverPlayerSelectionRight[2] = 1;
                    break;
                case 1415:
                    hoverPlayerSelectionRight[3] = 1;
                    break;
                default:
                    break;
                }
            }
            else{
                hoverPlayerSelectionRight[i] = 0;
            }
        
            if((mouseX >= (centre - (87 / 2 + 10 + 17))) && (mouseX <= (centre - (87 / 2 + 10))) && (mouseY >= (707)) && (mouseY <= (707 + 25))){
                switch (centre){
                case 500:
                    hoverPlayerDifficultyLeft[0] = 1;
                    break;
                case 805:
                    hoverPlayerDifficultyLeft[1] = 1;
                    break;
                case 1110:
                    hoverPlayerDifficultyLeft[2] = 1;
                    break;
                case 1415:
                    hoverPlayerDifficultyLeft[3] = 1;
                    break;
                default:
                    break;
                }
            }
            else{
                hoverPlayerDifficultyLeft[i] = 0;
            }

            if((mouseX >= (centre + (87 / 2 + 10))) && (mouseX <= (centre + (87 / 2 + 10 + 17))) && (mouseY >= (707)) && (mouseY <= (707 + 25))){
                switch (centre){
                case 500:
                    hoverPlayerDifficultyRight[0] = 1;
                    break;
                case 805:
                    hoverPlayerDifficultyRight[1] = 1;
                    break;
                case 1110:
                    hoverPlayerDifficultyRight[2] = 1;
                    break;
                case 1415:
                    hoverPlayerDifficultyRight[3] = 1;
                    break;
                default:
                    break;
                }
            }
            else{
                hoverPlayerDifficultyRight[i] = 0;
            }
        }
    }
    else{
        hoverStart = 0;
        hoverModeSelectionLeft = 0;
        hoverModeSelectionRight = 0;
        hoverMinuteUp = 0;
        hoverMinuteDown = 0;
        hoverSecondeUp = 0;
        hoverSecondeDown = 0;
    }

    // if((page == 0) && (mouseX >= 292) && (mouseX <= 292 + 390) && (mouseY >= 630) && (mouseY <= 630 + 390))            hoverNombreJoueur2 = 1;
    // else if((page == 0) && (mouseX >= 762) && (mouseX <= 762 + 390) && (mouseY >= 630) && (mouseY <= 630 + 390))       hoverNombreJoueur3 = 1;
    // else if((page == 0) && (mouseX >= 1231) && (mouseX <= 1231 + 390) && (mouseY >= 630) && (mouseY <= 630 + 390))     hoverNombreJoueur4 = 1;
    // else{
    //     hoverNombreJoueur2 = 0;
    //     hoverNombreJoueur3 = 0;
    //     hoverNombreJoueur4 = 0;
    // }
}

void drawGame(){
    if(page == 0)                       drawMenus();
    if((page == 1) || (start == 1))     drawPartie();

    actualize();
    usleep(1000000 / FPS); // 60 images par seconde | 1000000 = 1 seconde
}

void KeyPressed(SDL_Keycode touche){
    switch (touche) {
        /*---------------------Déplacement BARRE Joueur 1----------------------*/
        case SDLK_q:
            if(start == 1){
                barres[0].vitesseX = -vitesseBarre;
            }      
            if(pageMenu == 2){
                playerReady[0][2] = -playerReady[0][2];
            }
            break;
        case SDLK_d:
            if(start == 1){
                barres[0].vitesseX = vitesseBarre;
            }
            if(pageMenu == 2){
                playerReady[0][3] = -playerReady[0][3];
            }
            break;
        case SDLK_z:
            if(start == 1){
                if((tabBonusMalus[0].tempsRecharge >= 5 * FPS) && (tabBonusMalus[0].activerB == 0) && (tabBonusMalus[0].activerM == 0)){
                    tabBonusMalus[0].activerB = 1;
                }
                printf("%d\n", tabBonusMalus[0].tempsRecharge);
            }
            if(pageMenu == 2){
                playerReady[0][0] = -playerReady[0][0];
            }
            break;
        case SDLK_s:
            if(start == 1){
                if((tabBonusMalus[0].tempsRecharge >= 5 * FPS) && (tabBonusMalus[0].activerM == 0) && (tabBonusMalus[0].activerB == 0)){
                    tabBonusMalus[0].activerM = 1;
                }
            }
            if(pageMenu == 2){
                playerReady[0][1] = -playerReady[0][1];
            }
            break;
        /*---------------------------------------------------------------------*/
        /*---------------------Déplacement BARRE Joueur 2----------------------*/
        case SDLK_k:
            if(start == 1){
                barres[1].vitesseX = -vitesseBarre;
            }
            if(pageMenu == 2){
                playerReady[1][2] = -playerReady[1][2];
            }
            break;
        case SDLK_m:
            if(start == 1){
                barres[1].vitesseX = vitesseBarre;
            }
            if(pageMenu == 2){
                playerReady[1][3] = -playerReady[1][3];
            }
            break;
        case SDLK_o:
            if(start == 1){
                if(tabBonusMalus[1].tempsRecharge >= 5 * FPS){
                    tabBonusMalus[1].activerB = 1;
                }
            }
            if(pageMenu == 2){
                playerReady[1][0] = -playerReady[1][0];
            }
            break;
        case SDLK_l:
            if(start == 1){
                if(tabBonusMalus[1].tempsRecharge >= 5 * FPS){
                    tabBonusMalus[1].activerM = 1;
                }
            }
            if(pageMenu == 2){
                playerReady[1][1] = -playerReady[1][1];
            }
            break;
        /*---------------------------------------------------------------------*/
        /*---------------------Déplacement BARRE Joueur 3----------------------*/
        case SDLK_c:
            if(start == 1){
                barres[2].vitesseY = -vitesseBarre;
            }
            if(pageMenu == 2){
                playerReady[2][2] = -playerReady[2][2];
            }
            break;
        case SDLK_b:
            if(start == 1){
                barres[2].vitesseY = vitesseBarre;
            }
            if(pageMenu == 2){
                playerReady[2][3] = -playerReady[2][3];
            }
            break;
        case SDLK_g:
            if(start == 1){
                if(tabBonusMalus[2].tempsRecharge >= 5 * FPS){
                    tabBonusMalus[2].activerB = 1;
                }
            }
            if(pageMenu == 2){
                playerReady[2][0] = -playerReady[2][0];
            }
            break;
        case SDLK_v:
            if(start == 1){
                if(tabBonusMalus[2].tempsRecharge >= 5 * FPS){
                    tabBonusMalus[2].activerM = 1;
                }
            }
            if(pageMenu == 2){
                playerReady[2][1] = -playerReady[2][1];
            }
            break;
        /*---------------------------------------------------------------------*/
        /*---------------------Déplacement BARRE Joueur 4----------------------*/
        case SDLK_RIGHT:
            if(start == 1){
                barres[3].vitesseY = -vitesseBarre;
            }
            if(pageMenu == 2){
                playerReady[3][3] = -playerReady[3][3];
            }
            break;
        case SDLK_LEFT:
            if(start == 1){
                barres[3].vitesseY = vitesseBarre;
            }
            if(pageMenu == 2){
                playerReady[3][2] = -playerReady[3][2];
            }
            break;
        case SDLK_UP:
            if(start == 1){
                if(tabBonusMalus[3].tempsRecharge >= 5 * FPS){
                    tabBonusMalus[3].activerB = 1;
                }
            }
            if(pageMenu == 2){
                playerReady[3][0] = -playerReady[3][0];
            }
            break;
        case SDLK_DOWN:
            if(start == 1){
                if(tabBonusMalus[3].tempsRecharge >= 5 * FPS){
                    tabBonusMalus[3].activerM = 1;
                }
            }
            if(pageMenu == 2){
                playerReady[3][1] = -playerReady[3][1];
            }
            break;
        /*---------------------------------------------------------------------*/
        case SDLK_SPACE:
            if(page == 1)       start = 1;
            break;
        case SDLK_p:
            pageMenu = 2;
            break;
        case SDLK_ESCAPE:
            freeAndTerminate();
            break;
        default:
            break;
    }
}

void gameLoop() {
    int programLaunched = 1;
    while (programLaunched == 1) {
        SDL_Event event;
        while (SDL_PollEvent(&event)) {
            switch (event.type) {
                case SDL_QUIT:
                    programLaunched = 0;
                    break;
                case SDL_MOUSEBUTTONUP:
                    printf("position de la souris x : %d , y : %d\n", event.motion.x, event.motion.y);
                    break;
                case SDL_KEYDOWN:
                    KeyPressed(event.key.keysym.sym);
                    break;
                case SDL_JOYBUTTONDOWN:
                    break;
                case SDL_MOUSEBUTTONDOWN:
                    /* clique de la souris
                     * event.motion.y | event.motion.x pour les positions de la souris
                     */
                    MouseButton(event.motion.x, event.motion.y);
                    break;
                case SDL_MOUSEMOTION:
                    MouseMotion(event.motion.x, event.motion.y);
                    break;
                default:
                    break;
            }
        }
        drawGame();
    }
}

int main(){
    init(WINDOW_WIDTH, WINDOW_HEIGHT);
    init_game();
    gameLoop();
    printf("Fin du programme\n");
    freeAndTerminate();
}