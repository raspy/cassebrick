/*
Mettre les données des balles et des barres dans des tableaux comme les briques : moins lisible, certes,
mais code plus simple notamment lors des rebonds de la balle

*/



#include <SDL2/SDL.h>
#include <unistd.h>
#include "function.h"

#define WINDOW_WIDTH 950
#define WINDOW_HEIGHT 950
#define FPS 60

int nombreLigne = 8;
int nombreColonne = 8;

typedef struct Bricks
{
    int x;
    int y;
    int vie;
    int spawn;
    int type;
    int frame;
    int rebond;
    char* sourceIMG;
}T_Bricks;

T_Bricks tabBrick[8][8];

typedef struct Balls
{
    int x;
    int y;
    int color_red;
    int color_green;
    int color_blue;
}T_Balls;

T_Balls balle1 = {0, 0, 244, 37, 17};

typedef struct Barres
{
    int x;
    int y;
    int color_red;      //Couleur du contour seulement
    int color_green;    //Couleur du contour seulement
    int color_blue;     //Couleur du contour seulement
    int vitesse;
    int longueur;
    int largeur; 
}T_Barres;
T_Barres barreJoueur1_ROUGE = {0, 0, 244, 37, 17, 0, 100, 10};
T_Barres barreJoueur2_JAUNE = {0, 0, 244, 214, 17, 0, 100, 10};

int longueur;
int xdebutBrick;
int ydebutBrick;

int rayonBall;
int vitesseBallX;
int vitesseBallY;

char* scr;

void spawnBricks(int i, int j){
    if((tabBrick[i][j].frame == 0) && (tabBrick[i][j].spawn == 0))         tabBrick[i][j].sourceIMG = "assets/brick7.bmp";
      
    else if((tabBrick[i][j].frame == 1) && (tabBrick[i][j].spawn == 0))    tabBrick[i][j].sourceIMG = "assets/brick6.bmp";
      
    else if((tabBrick[i][j].frame == 2) && (tabBrick[i][j].spawn == 0))    tabBrick[i][j].sourceIMG = "assets/brick5.bmp";
      
    else if((tabBrick[i][j].frame == 3) && (tabBrick[i][j].spawn == 0))    tabBrick[i][j].sourceIMG = "assets/brick4.bmp";
      
    else if((tabBrick[i][j].frame == 4) && (tabBrick[i][j].spawn == 0))    tabBrick[i][j].sourceIMG = "assets/brick3.bmp";
      
    else if((tabBrick[i][j].frame == 5) && (tabBrick[i][j].spawn == 0))    tabBrick[i][j].sourceIMG = "assets/brick2.bmp";
      
    else if((tabBrick[i][j].frame == 6) && (tabBrick[i][j].spawn == 0))    tabBrick[i][j].sourceIMG = "assets/brick1.bmp";

    else if((tabBrick[i][j].frame == 7) && (tabBrick[i][j].spawn == 0))    tabBrick[i][j].sourceIMG = "assets/brick0.bmp";
      

    if(tabBrick[i][j].frame == 7) tabBrick[i][j].spawn = 1;
}

void breakBricks(int i, int j){
    if((tabBrick[i][j].frame == 0) && (tabBrick[i][j].rebond == 1) && (tabBrick[i][j].vie == 0))         tabBrick[i][j].sourceIMG = "assets/brick0.bmp";
      
    else if((tabBrick[i][j].frame == 1) && (tabBrick[i][j].rebond == 1) && (tabBrick[i][j].vie == 0))    tabBrick[i][j].sourceIMG = "assets/brick1.bmp";
      
    else if((tabBrick[i][j].frame == 2) && (tabBrick[i][j].rebond == 1) && (tabBrick[i][j].vie == 0))    tabBrick[i][j].sourceIMG = "assets/brick2.bmp";
      
    else if((tabBrick[i][j].frame == 3) && (tabBrick[i][j].rebond == 1) && (tabBrick[i][j].vie == 0))    tabBrick[i][j].sourceIMG = "assets/brick3.bmp";
      
    else if((tabBrick[i][j].frame == 4) && (tabBrick[i][j].rebond == 1) && (tabBrick[i][j].vie == 0))    tabBrick[i][j].sourceIMG = "assets/brick4.bmp";
      
    else if((tabBrick[i][j].frame == 5) && (tabBrick[i][j].rebond == 1) && (tabBrick[i][j].vie == 0))    tabBrick[i][j].sourceIMG = "assets/brick5.bmp";
      
    else if((tabBrick[i][j].frame == 6) && (tabBrick[i][j].rebond == 1) && (tabBrick[i][j].vie == 0))    tabBrick[i][j].sourceIMG = "assets/brick6.bmp";

    else if((tabBrick[i][j].frame == 7) && (tabBrick[i][j].rebond == 1) && (tabBrick[i][j].vie == 0))    tabBrick[i][j].sourceIMG = "assets/brick7.bmp";
      

    if((tabBrick[i][j].frame == 7) && (tabBrick[i][j].rebond == 1) && (tabBrick[i][j].vie == 0)) tabBrick[i][j].rebond = 2;
}

void rebondBalle_Bord(){
    if((vitesseBallX < 0) && (balle1.x - rayonBall <= 0) || (vitesseBallX > 0) && (balle1.x + rayonBall >= WINDOW_WIDTH) ||
       (vitesseBallX < 0) && (balle1.y >= barreJoueur1_ROUGE.y) && (balle1.y <= barreJoueur1_ROUGE.y + barreJoueur1_ROUGE.largeur) && (balle1.x - rayonBall <= barreJoueur1_ROUGE.x + barreJoueur1_ROUGE.longueur) && (balle1.x > barreJoueur1_ROUGE.x + barreJoueur1_ROUGE.longueur) ||
       (vitesseBallX > 0) && (balle1.y >= barreJoueur1_ROUGE.y) && (balle1.y <= barreJoueur1_ROUGE.y + barreJoueur1_ROUGE.largeur) && (balle1.x + rayonBall >= barreJoueur1_ROUGE.x) && (balle1.x < barreJoueur1_ROUGE.x)
      ){
        vitesseBallX = -vitesseBallX;
    }

    else if((vitesseBallY < 0) && (balle1.y - rayonBall <= 0) || (vitesseBallY > 0) && (balle1.y + rayonBall >= WINDOW_HEIGHT) ||
            (vitesseBallY > 0) && (balle1.x >= barreJoueur1_ROUGE.x) && (balle1.x <= barreJoueur1_ROUGE.x + barreJoueur1_ROUGE.longueur) && (balle1.y + rayonBall >= barreJoueur1_ROUGE.y) && (balle1.y < barreJoueur1_ROUGE.y)
    
      ){
        vitesseBallY = -vitesseBallY;
    }

    else if((vitesseBallX > 0) && (vitesseBallY > 0) && (balle1.x < barreJoueur1_ROUGE.x) && (balle1.y < barreJoueur1_ROUGE.y) && (balle1.x + rayonBall >= barreJoueur1_ROUGE.x) && (balle1.y + rayonBall >= barreJoueur1_ROUGE.y) ||
            (vitesseBallX < 0) && (vitesseBallY > 0) && (balle1.x > barreJoueur1_ROUGE.x + barreJoueur1_ROUGE.longueur) && (balle1.x - rayonBall <= barreJoueur1_ROUGE.x + barreJoueur1_ROUGE.longueur) && (balle1.y < barreJoueur1_ROUGE.y) && (balle1.y + rayonBall >= barreJoueur1_ROUGE.y)
    ){
        vitesseBallX = -vitesseBallX;
        vitesseBallY = -vitesseBallY;
    }
}

void rebondBalle_Barre(){
    if(barreJoueur1_ROUGE.x <= 0){
        barreJoueur1_ROUGE.x = 1;
        barreJoueur1_ROUGE.vitesse = 0;
    }
    else if(barreJoueur1_ROUGE.x + barreJoueur1_ROUGE.longueur >= WINDOW_WIDTH){
        barreJoueur1_ROUGE.x = WINDOW_WIDTH - barreJoueur1_ROUGE.longueur - 1;
        barreJoueur1_ROUGE.vitesse = 0;
    }
}

void rebondBalle_Bricks(int i, int j){
    if((vitesseBallX < 0) && (balle1.x - rayonBall <= tabBrick[i][j].x + longueur) && (balle1.x > tabBrick[i][j].x + longueur) && (balle1.y >= tabBrick[i][j].y) && (balle1.y <= tabBrick[i][j].y + longueur) ||
       (vitesseBallX > 0) && (balle1.x + rayonBall >= tabBrick[i][j].x) && (balle1.x < tabBrick[i][j].x) && (balle1.y >= tabBrick[i][j].y) && (balle1.y <= tabBrick[i][j].y + longueur) 
    ){
        vitesseBallX = -vitesseBallX;
        tabBrick[i][j].vie--;
        tabBrick[i][j].rebond = 1;
    }

    else if((vitesseBallY < 0) && (balle1.y - rayonBall <= tabBrick[i][j].y + longueur) && (balle1.y > tabBrick[i][j].y + longueur) && (balle1.x >= tabBrick[i][j].x) && (balle1.x <= tabBrick[i][j].x + longueur) ||
       (vitesseBallY > 0) && (balle1.y + rayonBall >= tabBrick[i][j].y) && (balle1.y < tabBrick[i][j].y) && (balle1.x >= tabBrick[i][j].x) && (balle1.x <= tabBrick[i][j].x + longueur)
    ){
        vitesseBallY = -vitesseBallY;
        tabBrick[i][j].vie--;
        tabBrick[i][j].rebond = 1;
    }
    //Coins : Haut Droit -- Haut Gauche -- Bas Droit -- Bas Gauche
    else if((tabBrick[i - 1][j].vie > 0) && (vitesseBallX > 0) && (vitesseBallY > 0) && (balle1.x < tabBrick[i][j].x) && (balle1.x + rayonBall >= tabBrick[i][j].x) && (balle1.y < tabBrick[i][j].y) && (balle1.y + rayonBall >= tabBrick[i][j].y) ||
            (tabBrick[i - 1][j].vie > 0) && (vitesseBallX < 0) && (vitesseBallY > 0) && (balle1.x > tabBrick[i][j].x + longueur) && (balle1.x - rayonBall <= tabBrick[i][j].x + longueur) && (balle1.y < tabBrick[i][j].y) && (balle1.y + rayonBall >= tabBrick[i][j].y) ||
            (tabBrick[i + 1][j].vie > 0) && (vitesseBallX > 0) && (vitesseBallY < 0) && (balle1.x < tabBrick[i][j].x) && (balle1.x + rayonBall >= tabBrick[i][j].x) && (balle1.y > tabBrick[i][j].y + longueur) && (balle1.y - rayonBall <= tabBrick[i][j].y + longueur) ||
            (tabBrick[i + 1][j].vie > 0) && (vitesseBallX < 0) && (vitesseBallY < 0) && (balle1.x > tabBrick[i][j].x + longueur) && (balle1.x - rayonBall <= tabBrick[i][j].x + longueur) && (balle1.y > tabBrick[i][j].y + longueur) && (balle1.y - rayonBall <= tabBrick[i][j].y + longueur)
    ){
        vitesseBallX = -vitesseBallX;
        tabBrick[i][j].vie--; 
        tabBrick[i][j].rebond = 1;                
    }
    //Coins : Haut Droit -- Haut Gauche -- Bas Droit -- Bas Gauche
    else if((tabBrick[i][j - 1].vie > 0) && (vitesseBallX > 0) && (vitesseBallY > 0) && (balle1.x < tabBrick[i][j].x) && (balle1.x + rayonBall >= tabBrick[i][j].x) && (balle1.y < tabBrick[i][j].y) && (balle1.y + rayonBall >= tabBrick[i][j].y) ||
            (tabBrick[i][j + 1].vie > 0) && (vitesseBallX < 0) && (vitesseBallY > 0) && (balle1.x > tabBrick[i][j].x + longueur) && (balle1.x - rayonBall <= tabBrick[i][j].x + longueur) && (balle1.y < tabBrick[i][j].y) && (balle1.y + rayonBall >= tabBrick[i][j].y) ||
            (tabBrick[i][j - 1].vie > 0) && (vitesseBallX > 0) && (vitesseBallY < 0) && (balle1.x < tabBrick[i][j].x) && (balle1.x + rayonBall >= tabBrick[i][j].x) && (balle1.y > tabBrick[i][j].y + longueur) && (balle1.y - rayonBall <= tabBrick[i][j].y + longueur) ||
            (tabBrick[i][j + 1].vie > 0) && (vitesseBallX < 0) && (vitesseBallY < 0) && (balle1.x > tabBrick[i][j].x + longueur) && (balle1.x - rayonBall <= tabBrick[i][j].x + longueur) && (balle1.y > tabBrick[i][j].y + longueur) && (balle1.y - rayonBall <= tabBrick[i][j].y + longueur)
    ){
        vitesseBallY = -vitesseBallY;
        tabBrick[i][j].vie--;   
        tabBrick[i][j].rebond = 1;             
    }
    else if((vitesseBallX > 0) && (vitesseBallY > 0) && (balle1.x < tabBrick[i][j].x) && (balle1.x + rayonBall >= tabBrick[i][j].x) && (balle1.y < tabBrick[i][j].y) && (balle1.y + rayonBall >= tabBrick[i][j].y) ||
            (vitesseBallX < 0) && (vitesseBallY > 0) && (balle1.x > tabBrick[i][j].x + longueur) && (balle1.x - rayonBall <= tabBrick[i][j].x + longueur) && (balle1.y < tabBrick[i][j].y) && (balle1.y + rayonBall >= tabBrick[i][j].y) ||
            (vitesseBallX > 0) && (vitesseBallY < 0) && (balle1.x < tabBrick[i][j].x) && (balle1.x + rayonBall >= tabBrick[i][j].x) && (balle1.y > tabBrick[i][j].y + longueur) && (balle1.y - rayonBall <= tabBrick[i][j].y + longueur) ||
            (vitesseBallX < 0) && (vitesseBallY < 0) && (balle1.x > tabBrick[i][j].x + longueur) && (balle1.x - rayonBall <= tabBrick[i][j].x + longueur) && (balle1.y > tabBrick[i][j].y + longueur) && (balle1.y - rayonBall <= tabBrick[i][j].y + longueur)
       ){
        vitesseBallX = -vitesseBallX;
        vitesseBallY = -vitesseBallY;
        tabBrick[i][j].vie--;
        tabBrick[i][j].rebond = 1;
    }
}

void init_game(){
    /*-----------------------------Balle-----------------------------*/
    balle1.x = WINDOW_WIDTH / 2;
    balle1.y = WINDOW_HEIGHT - 50 - 25;
    rayonBall = 10;
    vitesseBallX = 4;
    vitesseBallY = -4;
    /*---------------------------------------------------------------*/

    /*-----------------------------Barre-----------------------------*/
    barreJoueur1_ROUGE.x = (WINDOW_WIDTH - barreJoueur1_ROUGE.longueur) / 2;
    barreJoueur1_ROUGE.y = WINDOW_HEIGHT - 50; //barreJoueur2_JAUNE

    barreJoueur2_JAUNE.x = (WINDOW_WIDTH - barreJoueur2_JAUNE.longueur) / 2;
    barreJoueur2_JAUNE.y = 50; //barreJoueur2_JAUNE
    /*---------------------------------------------------------------*/

    /*----------------------------Briques----------------------------*/
    longueur = 60;
    xdebutBrick = (WINDOW_WIDTH - (nombreColonne*60 + (nombreColonne-1)*5))/2;
    ydebutBrick = (WINDOW_HEIGHT - (nombreLigne*60 + (nombreLigne-1)*5))/2;
    
    for(int i = 0 ; i < nombreLigne ; i++){
        for(int j = 0 ; j < nombreColonne ; j++){
            tabBrick[i][j].x = xdebutBrick + j*longueur + (j-1)*5;
            tabBrick[i][j].y = ydebutBrick + i*longueur + (i-1)*5;
            tabBrick[i][j].vie = 1;
            tabBrick[i][j].spawn = 0;
            tabBrick[i][j].type = 0;
            tabBrick[i][j].rebond = 0;
        }
    }
    /*---------------------------------------------------------------*/
}

void drawGame(){
    changeColor(0, 21, 36, 255);
    drawRect(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);

    /*-----------------------------Balle-----------------------------*/
    changeColor(255,0,0,255);
    drawCircle(balle1.x, balle1.y, rayonBall);

    balle1.x += vitesseBallX;
    balle1.y += vitesseBallY;

    rebondBalle_Bord();
    /*---------------------------------------------------------------*/

    /*-----------------------------Barre-----------------------------*/
    changeColor(244, 37, 17, 255);
    drawRect(barreJoueur1_ROUGE.x, barreJoueur1_ROUGE.y, barreJoueur1_ROUGE.longueur, barreJoueur1_ROUGE.largeur);
    changeColor(91, 19, 12, 255);
    drawRect(barreJoueur1_ROUGE.x + 2, barreJoueur1_ROUGE.y + 2, barreJoueur1_ROUGE.longueur - 4, barreJoueur1_ROUGE.largeur - 4);

    barreJoueur1_ROUGE.x += barreJoueur1_ROUGE.vitesse;

    changeColor(244, 214, 17, 255);
    drawRect(barreJoueur2_JAUNE.x, barreJoueur2_JAUNE.y, barreJoueur2_JAUNE.longueur, barreJoueur2_JAUNE.largeur);
    changeColor(63, 91, 12, 255);
    drawRect(barreJoueur2_JAUNE.x + 2, barreJoueur2_JAUNE.y + 2, barreJoueur2_JAUNE.longueur - 4, barreJoueur2_JAUNE.largeur - 4);

    barreJoueur2_JAUNE.x += barreJoueur2_JAUNE.vitesse;

    rebondBalle_Barre();
    /*---------------------------------------------------------------*/

    /*----------------------------Briques----------------------------*/
    for(int i = 0 ; i < nombreLigne ; i++){
        for(int j = 0 ; j < nombreColonne ; j++){
            if(tabBrick[i][j].vie > 0){
                spawnBricks(i, j);
                sprite(tabBrick[i][j].x, tabBrick[i][j].y, tabBrick[i][j].sourceIMG);

                rebondBalle_Bricks(i, j);
            }else if(tabBrick[i][j].rebond == 1){
                breakBricks(i, j);
                sprite(tabBrick[i][j].x, tabBrick[i][j].y, tabBrick[i][j].sourceIMG);
            }

            if(tabBrick[i][j].frame == 7){
                tabBrick[i][j].frame = 0;
            }else{
                tabBrick[i][j].frame++;
            }
        }
    }
    /*---------------------------------------------------------------*/

    actualize();
    usleep(1000000 / FPS); // 60 images par seconde | 1000000 = 1 seconde
}

void KeyPressed(SDL_Keycode touche){
    switch (touche) {
        case SDLK_q:
            barreJoueur1_ROUGE.vitesse = -5;
            break;
        case SDLK_d:
            barreJoueur1_ROUGE.vitesse = 5;
            break;
        case SDLK_ESCAPE:
            freeAndTerminate();
            break;
        default:
            break;
    }
}

void gameLoop() {
    int programLaunched = 1;
    while (programLaunched == 1) {
        SDL_Event event;
        while (SDL_PollEvent(&event)) {
            switch (event.type) {
                case SDL_QUIT:
                    programLaunched = 0;
                    break;
                case SDL_MOUSEBUTTONUP:
                    printf("position de la souris x : %d , y : %d\n", event.motion.x, event.motion.y);
                    break;
                case SDL_KEYDOWN:
                    KeyPressed(event.key.keysym.sym);
                    break;
                case SDL_JOYBUTTONDOWN:
                    break;
                default:
                    break;
            }
        }
        drawGame();
    }
}

int main(){
    init(WINDOW_WIDTH, WINDOW_HEIGHT);
    init_game();
    gameLoop();
    printf("Fin du programme\n");
    freeAndTerminate();
}