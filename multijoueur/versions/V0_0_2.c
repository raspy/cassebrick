/*
Mettre les données des balles et des barres dans des tableaux comme les briques : moins lisible, certes,
mais code plus simple notamment lors des rebonds de la balle

*/



#include <SDL2/SDL.h>
#include <unistd.h>
#include "function.h"

#define WINDOW_WIDTH 950
#define WINDOW_HEIGHT 950
#define FPS 60

#define VITESSEBALLE 2
#define VITESSEBARRE 3

int nombreLigne = 8;
int nombreColonne = 8;

typedef struct Bricks
{
    int x;
    int y;
    int vie;
    int spawn;
    int type;
    int frame;
    int rebond;
    char* sourceIMG;
}T_Bricks;

T_Bricks tabBrick[8][8];

typedef struct Balls
{
    int x;
    int y;
    int color_red;
    int color_green;
    int color_blue;
    int vitesseX;
    int vitesseY;
}T_Balls;

T_Balls balles[4];

typedef struct Barres
{
    int x;
    int y;
    int color_red_contour;      
    int color_green_contour;    
    int color_blue_contour;    
    int color_red_interieur;      
    int color_green_interieur;    
    int color_blue_interieur;  
    int vitesse;
    int longueur;
    int largeur; 
}T_Barres;

T_Barres barres[4];

int longueurBarre;
int largeurBarre;

int longueur;
int xdebutBrick;
int ydebutBrick;

int rayonBall;

char* scr;

void spawnBricks(int i, int j){
    if((tabBrick[i][j].frame == 0) && (tabBrick[i][j].spawn == 0))         tabBrick[i][j].sourceIMG = "assets/brick7.bmp";
      
    else if((tabBrick[i][j].frame == 2) && (tabBrick[i][j].spawn == 0))    tabBrick[i][j].sourceIMG = "assets/brick6.bmp";
      
    else if((tabBrick[i][j].frame == 4) && (tabBrick[i][j].spawn == 0))    tabBrick[i][j].sourceIMG = "assets/brick5.bmp";
      
    else if((tabBrick[i][j].frame == 6) && (tabBrick[i][j].spawn == 0))    tabBrick[i][j].sourceIMG = "assets/brick4.bmp";
      
    else if((tabBrick[i][j].frame == 8) && (tabBrick[i][j].spawn == 0))    tabBrick[i][j].sourceIMG = "assets/brick3.bmp";
      
    else if((tabBrick[i][j].frame == 10) && (tabBrick[i][j].spawn == 0))    tabBrick[i][j].sourceIMG = "assets/brick2.bmp";
      
    else if((tabBrick[i][j].frame == 12) && (tabBrick[i][j].spawn == 0))    tabBrick[i][j].sourceIMG = "assets/brick1.bmp";

    else if((tabBrick[i][j].frame == 14) && (tabBrick[i][j].spawn == 0))    tabBrick[i][j].sourceIMG = "assets/brick0.bmp";
      

    if(tabBrick[i][j].frame == 14) tabBrick[i][j].spawn = 1;
}

void breakBricks(int i, int j){
    if((tabBrick[i][j].frame == 0) && (tabBrick[i][j].rebond == 1) && (tabBrick[i][j].vie == 0))         tabBrick[i][j].sourceIMG = "assets/brick0.bmp";
      
    else if((tabBrick[i][j].frame == 2) && (tabBrick[i][j].rebond == 1) && (tabBrick[i][j].vie == 0))    tabBrick[i][j].sourceIMG = "assets/brick1.bmp";
      
    else if((tabBrick[i][j].frame == 4) && (tabBrick[i][j].rebond == 1) && (tabBrick[i][j].vie == 0))    tabBrick[i][j].sourceIMG = "assets/brick2.bmp";
      
    else if((tabBrick[i][j].frame == 6) && (tabBrick[i][j].rebond == 1) && (tabBrick[i][j].vie == 0))    tabBrick[i][j].sourceIMG = "assets/brick3.bmp";
      
    else if((tabBrick[i][j].frame == 8) && (tabBrick[i][j].rebond == 1) && (tabBrick[i][j].vie == 0))    tabBrick[i][j].sourceIMG = "assets/brick4.bmp";
      
    else if((tabBrick[i][j].frame == 10) && (tabBrick[i][j].rebond == 1) && (tabBrick[i][j].vie == 0))    tabBrick[i][j].sourceIMG = "assets/brick5.bmp";
      
    else if((tabBrick[i][j].frame == 12) && (tabBrick[i][j].rebond == 1) && (tabBrick[i][j].vie == 0))    tabBrick[i][j].sourceIMG = "assets/brick6.bmp";

    else if((tabBrick[i][j].frame == 14) && (tabBrick[i][j].rebond == 1) && (tabBrick[i][j].vie == 0))    tabBrick[i][j].sourceIMG = "assets/brick7.bmp";
      

    if((tabBrick[i][j].frame == 14) && (tabBrick[i][j].rebond == 1) && (tabBrick[i][j].vie == 0)) tabBrick[i][j].rebond = 2;
}

void rebondBalle_Bord(int i, int j){
    if((balles[i].vitesseX < 0) && (balles[i].x - rayonBall <= 0) || (balles[i].vitesseX > 0) && (balles[i].x + rayonBall >= WINDOW_WIDTH) ||
       (balles[i].vitesseX < 0) && (balles[i].y >= barres[j].y) && (balles[i].y <= barres[j].y + largeurBarre) && (balles[i].x - rayonBall <= barres[j].x + longueurBarre) && (balles[i].x > barres[j].x + longueurBarre) ||
       (balles[i].vitesseX > 0) && (balles[i].y >= barres[j].y) && (balles[i].y <= barres[j].y + largeurBarre) && (balles[i].x + rayonBall >= barres[j].x) && (balles[i].x < barres[j].x)
      ){
        balles[i].vitesseX = -balles[i].vitesseX;
    }

    else if((balles[i].vitesseY < 0) && (balles[i].y - rayonBall <= 0) || (balles[i].vitesseY > 0) && (balles[i].y + rayonBall >= WINDOW_HEIGHT) ||
            (balles[i].vitesseY > 0) && (balles[i].y - rayonBall >= 50 + largeurBarre) && (balles[i].x >= barres[j].x) && (balles[i].x <= barres[j].x + longueurBarre) && (balles[i].y + rayonBall >= barres[j].y) && (balles[i].y < barres[j].y) ||
            (balles[i].vitesseY < 0) && (balles[i].y - rayonBall <= WINDOW_HEIGHT - (50 + largeurBarre)) && (balles[i].x >= barres[j].x) && (balles[i].x <= barres[j].x + longueurBarre) && (balles[i].y - rayonBall <= barres[j].y + largeurBarre) && (balles[i].y > barres[j].y + largeurBarre)  
    
      ){
        balles[i].vitesseY = -balles[i].vitesseY;
    }

    else if((balles[i].vitesseX > 0) && (balles[i].vitesseY > 0) && (balles[i].x < barres[j].x) && (balles[i].y < barres[j].y) && (balles[i].x + rayonBall >= barres[j].x) && (balles[i].y + rayonBall >= barres[j].y) ||
            (balles[i].vitesseX < 0) && (balles[i].vitesseY > 0) && (balles[i].x > barres[j].x + longueurBarre) && (balles[i].x - rayonBall <= barres[j].x + longueurBarre) && (balles[i].y < barres[j].y) && (balles[i].y + rayonBall >= barres[j].y)
    ){
        balles[i].vitesseX = -balles[i].vitesseX;
        balles[i].vitesseY = -balles[i].vitesseY;
    }
}

void rebondBalle_Barre(int i){
    if(barres[i].x <= 0){
        barres[i].x = 1;
        barres[i].vitesse = 0;
    }
    else if(barres[i].x + longueurBarre >= WINDOW_WIDTH){
        barres[i].x = WINDOW_WIDTH - longueurBarre - 1;
        barres[i].vitesse = 0;
    }
}

void rebondBalle_Bricks(int i, int j, int k){
    if((balles[k].vitesseX < 0) && (balles[k].x - rayonBall <= tabBrick[i][j].x + longueur) && (balles[k].x > tabBrick[i][j].x + longueur) && (balles[k].y >= tabBrick[i][j].y) && (balles[k].y <= tabBrick[i][j].y + longueur) ||
       (balles[k].vitesseX > 0) && (balles[k].x + rayonBall >= tabBrick[i][j].x) && (balles[k].x < tabBrick[i][j].x) && (balles[k].y >= tabBrick[i][j].y) && (balles[k].y <= tabBrick[i][j].y + longueur) 
    ){
        balles[k].vitesseX = -balles[k].vitesseX;
        tabBrick[i][j].vie--;
        tabBrick[i][j].rebond = 1;
    }

    else if((balles[k].vitesseY < 0) && (balles[k].y - rayonBall <= tabBrick[i][j].y + longueur) && (balles[k].y > tabBrick[i][j].y + longueur) && (balles[k].x >= tabBrick[i][j].x) && (balles[k].x <= tabBrick[i][j].x + longueur) ||
       (balles[k].vitesseY > 0) && (balles[k].y + rayonBall >= tabBrick[i][j].y) && (balles[k].y < tabBrick[i][j].y) && (balles[k].x >= tabBrick[i][j].x) && (balles[k].x <= tabBrick[i][j].x + longueur)
    ){
        balles[k].vitesseY = -balles[k].vitesseY;
        tabBrick[i][j].vie--;
        tabBrick[i][j].rebond = 1;
    }
    //Coins : Haut Droit -- Haut Gauche -- Bas Droit -- Bas Gauche
    else if((tabBrick[i - 1][j].vie > 0) && (balles[k].vitesseX > 0) && (balles[k].vitesseY > 0) && (balles[k].x < tabBrick[i][j].x) && (balles[k].x + rayonBall >= tabBrick[i][j].x) && (balles[k].y < tabBrick[i][j].y) && (balles[k].y + rayonBall >= tabBrick[i][j].y) ||
            (tabBrick[i - 1][j].vie > 0) && (balles[k].vitesseX < 0) && (balles[k].vitesseY > 0) && (balles[k].x > tabBrick[i][j].x + longueur) && (balles[k].x - rayonBall <= tabBrick[i][j].x + longueur) && (balles[k].y < tabBrick[i][j].y) && (balles[k].y + rayonBall >= tabBrick[i][j].y) ||
            (tabBrick[i + 1][j].vie > 0) && (balles[k].vitesseX > 0) && (balles[k].vitesseY < 0) && (balles[k].x < tabBrick[i][j].x) && (balles[k].x + rayonBall >= tabBrick[i][j].x) && (balles[k].y > tabBrick[i][j].y + longueur) && (balles[k].y - rayonBall <= tabBrick[i][j].y + longueur) ||
            (tabBrick[i + 1][j].vie > 0) && (balles[k].vitesseX < 0) && (balles[k].vitesseY < 0) && (balles[k].x > tabBrick[i][j].x + longueur) && (balles[k].x - rayonBall <= tabBrick[i][j].x + longueur) && (balles[k].y > tabBrick[i][j].y + longueur) && (balles[k].y - rayonBall <= tabBrick[i][j].y + longueur)
    ){
        balles[k].vitesseX = -balles[k].vitesseX;
        tabBrick[i][j].vie--; 
        tabBrick[i][j].rebond = 1;                
    }
    //Coins : Haut Droit -- Haut Gauche -- Bas Droit -- Bas Gauche
    else if((tabBrick[i][j - 1].vie > 0) && (balles[k].vitesseX > 0) && (balles[k].vitesseY > 0) && (balles[k].x < tabBrick[i][j].x) && (balles[k].x + rayonBall >= tabBrick[i][j].x) && (balles[k].y < tabBrick[i][j].y) && (balles[k].y + rayonBall >= tabBrick[i][j].y) ||
            (tabBrick[i][j + 1].vie > 0) && (balles[k].vitesseX < 0) && (balles[k].vitesseY > 0) && (balles[k].x > tabBrick[i][j].x + longueur) && (balles[k].x - rayonBall <= tabBrick[i][j].x + longueur) && (balles[k].y < tabBrick[i][j].y) && (balles[k].y + rayonBall >= tabBrick[i][j].y) ||
            (tabBrick[i][j - 1].vie > 0) && (balles[k].vitesseX > 0) && (balles[k].vitesseY < 0) && (balles[k].x < tabBrick[i][j].x) && (balles[k].x + rayonBall >= tabBrick[i][j].x) && (balles[k].y > tabBrick[i][j].y + longueur) && (balles[k].y - rayonBall <= tabBrick[i][j].y + longueur) ||
            (tabBrick[i][j + 1].vie > 0) && (balles[k].vitesseX < 0) && (balles[k].vitesseY < 0) && (balles[k].x > tabBrick[i][j].x + longueur) && (balles[k].x - rayonBall <= tabBrick[i][j].x + longueur) && (balles[k].y > tabBrick[i][j].y + longueur) && (balles[k].y - rayonBall <= tabBrick[i][j].y + longueur)
    ){
        balles[k].vitesseY = -balles[k].vitesseY;
        tabBrick[i][j].vie--;   
        tabBrick[i][j].rebond = 1;             
    }
    else if((balles[k].vitesseX > 0) && (balles[k].vitesseY > 0) && (balles[k].x < tabBrick[i][j].x) && (balles[k].x + rayonBall >= tabBrick[i][j].x) && (balles[k].y < tabBrick[i][j].y) && (balles[k].y + rayonBall >= tabBrick[i][j].y) ||
            (balles[k].vitesseX < 0) && (balles[k].vitesseY > 0) && (balles[k].x > tabBrick[i][j].x + longueur) && (balles[k].x - rayonBall <= tabBrick[i][j].x + longueur) && (balles[k].y < tabBrick[i][j].y) && (balles[k].y + rayonBall >= tabBrick[i][j].y) ||
            (balles[k].vitesseX > 0) && (balles[k].vitesseY < 0) && (balles[k].x < tabBrick[i][j].x) && (balles[k].x + rayonBall >= tabBrick[i][j].x) && (balles[k].y > tabBrick[i][j].y + longueur) && (balles[k].y - rayonBall <= tabBrick[i][j].y + longueur) ||
            (balles[k].vitesseX < 0) && (balles[k].vitesseY < 0) && (balles[k].x > tabBrick[i][j].x + longueur) && (balles[k].x - rayonBall <= tabBrick[i][j].x + longueur) && (balles[k].y > tabBrick[i][j].y + longueur) && (balles[k].y - rayonBall <= tabBrick[i][j].y + longueur)
       ){
        balles[k].vitesseX = -balles[k].vitesseX;
        balles[k].vitesseY = -balles[k].vitesseY;
        tabBrick[i][j].vie--;
        tabBrick[i][j].rebond = 1;
    }
}

void drawBalle(int i, int j){
    /*-----------------------------Balle 1---------------------------*/
    changeColor(balles[i].color_red, balles[i].color_green, balles[i].color_blue, 255);
    drawCircle(balles[i].x, balles[i].y, rayonBall);

    balles[i].x += balles[i].vitesseX;
    balles[i].y += balles[i].vitesseY;
    /*---------------------------------------------------------------*/

    rebondBalle_Bord(i, j);
}

void drawBarre(int i){
    changeColor(barres[i].color_red_contour, barres[i].color_green_contour, barres[i].color_blue_contour, 255);
    drawRect(barres[i].x, barres[i].y, longueurBarre, largeurBarre);
    changeColor(barres[i].color_red_interieur, barres[i].color_green_interieur, barres[i].color_blue_interieur, 255);
    drawRect(barres[i].x + 2, barres[i].y + 2, longueurBarre - 4, largeurBarre - 4);

    barres[i].x += barres[i].vitesse;

    rebondBalle_Barre(i);
}

void drawBricks(int i, int j, int k){
    /*----------------------------Briques----------------------------*/
    if(tabBrick[i][j].vie > 0){
        spawnBricks(i, j);
        sprite(tabBrick[i][j].x, tabBrick[i][j].y, tabBrick[i][j].sourceIMG);

        rebondBalle_Bricks(i, j, k);
    }else if(tabBrick[i][j].rebond == 1){
        breakBricks(i, j);
        sprite(tabBrick[i][j].x, tabBrick[i][j].y, tabBrick[i][j].sourceIMG);
    }

    if(tabBrick[i][j].frame == 14){
        tabBrick[i][j].frame = 0;
    }else{
        tabBrick[i][j].frame++;
    }
    /*---------------------------------------------------------------*/
}

void init_game(){
    /*-----------------------------Balle-----------------------------*/
    balles[0].x = WINDOW_WIDTH / 2;
    balles[0].y = WINDOW_HEIGHT - 50 - 25;
    balles[0].color_red = 244;
    balles[0].color_green = 37;
    balles[0].color_blue = 17;
    balles[0].vitesseX = VITESSEBALLE;
    balles[0].vitesseY = -VITESSEBALLE;


    balles[1].x = WINDOW_WIDTH / 2;
    balles[1].y = 50 + 25;
    balles[1].color_red = 244;
    balles[1].color_green = 214;
    balles[1].color_blue = 17;
    balles[1].vitesseX = VITESSEBALLE;
    balles[1].vitesseY = VITESSEBALLE;

    rayonBall = 10;
    /*---------------------------------------------------------------*/

    /*-----------------------------Barre-----------------------------*/
    longueurBarre = 100;
    largeurBarre = 10;

    barres[0].x = (WINDOW_WIDTH - longueurBarre) / 2;
    barres[0].y = WINDOW_HEIGHT - 50;
    barres[0].color_red_contour = 244;
    barres[0].color_green_contour = 37;
    barres[0].color_blue_contour = 17;
    barres[0].color_red_interieur = 91;
    barres[0].color_green_interieur = 19;
    barres[0].color_blue_interieur = 12;

    barres[1].x = (WINDOW_WIDTH - longueurBarre) / 2;
    barres[1].y = 50;
    barres[1].color_red_contour = 244;
    barres[1].color_green_contour = 214;
    barres[1].color_blue_contour = 17;
    barres[1].color_red_interieur = 63;
    barres[1].color_green_interieur = 91;
    barres[1].color_blue_interieur = 12;
    /*---------------------------------------------------------------*/

    /*----------------------------Briques----------------------------*/
    longueur = 60;
    xdebutBrick = (WINDOW_WIDTH - (nombreColonne*60 + (nombreColonne-1)*5))/2;
    ydebutBrick = (WINDOW_HEIGHT - (nombreLigne*60 + (nombreLigne-1)*5))/2;
    
    for(int i = 0 ; i < nombreLigne ; i++){
        for(int j = 0 ; j < nombreColonne ; j++){
            tabBrick[i][j].x = xdebutBrick + j*longueur + (j-1)*5;
            tabBrick[i][j].y = ydebutBrick + i*longueur + (i-1)*5;
            tabBrick[i][j].vie = 1;
            tabBrick[i][j].spawn = 0;
            tabBrick[i][j].type = 0;
            tabBrick[i][j].rebond = 0;
        }
    }
    /*---------------------------------------------------------------*/
}

void drawGame(){
    changeColor(0, 21, 36, 255);
    drawRect(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);

    for(int i=0 ; i<2 ; i++){
        for(int j=0 ; j<2; j++){
            drawBalle(i, j);
            drawBarre(i);
        }
    }

    for(int i = 0 ; i < nombreLigne ; i++){
        for(int j = 0 ; j < nombreColonne ; j++){
            for(int k=0 ; k<2 ; k++){
                drawBricks(i,j,k);
            }
        }
    }
    

    actualize();
    usleep(1000000 / FPS); // 60 images par seconde | 1000000 = 1 seconde
}

void KeyPressed(SDL_Keycode touche){
    switch (touche) {
        case SDLK_q:
            barres[0].vitesse = -VITESSEBARRE;
            break;
        case SDLK_d:
            barres[0].vitesse = VITESSEBARRE;
            break;
        case SDLK_k:
            barres[1].vitesse = -VITESSEBARRE;
            break;
        case SDLK_m:
            barres[1].vitesse = VITESSEBARRE;
            break;
        case SDLK_ESCAPE:
            freeAndTerminate();
            break;
        default:
            break;
    }
}

void gameLoop() {
    int programLaunched = 1;
    while (programLaunched == 1) {
        SDL_Event event;
        while (SDL_PollEvent(&event)) {
            switch (event.type) {
                case SDL_QUIT:
                    programLaunched = 0;
                    break;
                case SDL_MOUSEBUTTONUP:
                    printf("position de la souris x : %d , y : %d\n", event.motion.x, event.motion.y);
                    break;
                case SDL_KEYDOWN:
                    KeyPressed(event.key.keysym.sym);
                    break;
                case SDL_JOYBUTTONDOWN:
                    break;
                default:
                    break;
            }
        }
        drawGame();
    }
}

int main(){
    init(WINDOW_WIDTH, WINDOW_HEIGHT);
    init_game();
    gameLoop();
    printf("Fin du programme\n");
    freeAndTerminate();
}