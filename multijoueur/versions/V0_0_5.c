/*---------------------------------Description---------------------------------*/
/*Nom projet: Brick Breaker             Edit: RaspY             Version: 0_0_5
               Multijoueur

Description Jeu : C'est un casse briques avec plusieurs joueurs.

Description : Apparitions des briques + balles + barres
            Rebond des balles sur les bords de l'écran + barres + briques

Objectif :  Rajout des bots + corrections des rebonds de la balle sur les barres

Atteint :   OUI
*/
/*-----------------------------------------------------------------------------*/

#include <time.h>

#include <SDL2/SDL.h>
#include <unistd.h>
#include "function.h"

#define WINDOW_WIDTH 950
#define WINDOW_HEIGHT 950
#define FPS 60

#define VITESSEBALLE 4
#define VITESSEBARRE 6

int nombreLigne = 8;
int nombreColonne = 8;

typedef struct Bricks
{
    int x;
    int y;

    int side_x_le;
    int side_x_ri;
    int side_y_up;
    int side_y_do;

    int vie;
    int spawn;
    int type;
    int rebond;

    int frame;
    int temps;

    char* sourceIMG;
}T_Bricks;

T_Bricks tabBrick[8][8];

typedef struct Balls
{
    int x;
    int y;

    int side_x_le;
    int side_x_ri;
    int side_y_up;
    int side_y_do;

    int color_r;
    int color_g;
    int color_b;

    int vitesseX;
    int vitesseY;
}T_Balls;

T_Balls balles[4];

typedef struct Barres
{
    int longueur;
    int largeur; 

    int x;
    int y;

    int side_x_le;
    int side_x_ri;
    int side_y_up;
    int side_y_do;

    int color_r_contour;      
    int color_g_contour;    
    int color_b_contour; 

    int color_r_interieur;      
    int color_g_interieur;    
    int color_b_interieur; 

    int vitesseX;
    int vitesseY;
    
}T_Barres;

T_Barres barres[4];

typedef struct Chiffres
{
    int longueur;
    int largeur;

    char* sourceIMG;
}T_Chiffres;

T_Chiffres tabChiffres[4][10];

int longueur;
int xdebutBrick;
int ydebutBrick;

int pourcentageTypeBricks;

int rayonBall;

int nombreJoueur;

int tabResultat[4];
int unit;
int nombreChiffres;
int longueurResultat;
int positionResultat;

int pointBrickCassee;
int pointBalleToucheAdvers;
int pointBalleToucheSoi;
int pointCombo;

int suiviCombo[4];

int plusProche;

/*-----------------Initialisation des BALLES pour chaque joueur----------------*/
void init_Balles_J1(){
    balles[0].x = WINDOW_WIDTH / 2;
    balles[0].y = WINDOW_HEIGHT - 50 - 25;

    balles[0].color_r = 244;
    balles[0].color_g = 37;
    balles[0].color_b = 17;

    balles[0].vitesseX = VITESSEBALLE;
    balles[0].vitesseY = -VITESSEBALLE;
}

void init_Balles_J2(){
    balles[1].x = WINDOW_WIDTH / 2;
    balles[1].y = 50 + 25;

    balles[1].color_r = 244;
    balles[1].color_g = 214;
    balles[1].color_b = 17;

    balles[1].vitesseX = -VITESSEBALLE;
    balles[1].vitesseY = VITESSEBALLE;
}

void init_Balles_J3(){
    balles[2].x = 50 + 25;
    balles[2].y = WINDOW_HEIGHT / 2;

    balles[2].color_r = 244;
    balles[2].color_g = 17;
    balles[2].color_b = 180;

    balles[2].vitesseX = VITESSEBALLE;
    balles[2].vitesseY = VITESSEBALLE;
}

void init_Balles_J4(){
    balles[3].x = WINDOW_WIDTH - 50 - 25;
    balles[3].y = WINDOW_HEIGHT / 2;

    balles[3].color_r = 17;
    balles[3].color_g = 135;
    balles[3].color_b = 244;

    balles[3].vitesseX = -VITESSEBALLE;
    balles[3].vitesseY = -VITESSEBALLE;
}

void init_Balles(){
    init_Balles_J1();
    init_Balles_J2();
    init_Balles_J3();
    init_Balles_J4();

    rayonBall = 10;
}
/*-----------------------------------------------------------------------------*/

/*-----------------Initialisation des BALLES pour chaque joueur----------------*/
void init_Barres_J1(){
    barres[0].longueur = 100;
    barres[0].largeur = 10;

    barres[0].x = (WINDOW_WIDTH - barres[0].longueur) / 2;
    barres[0].y = WINDOW_HEIGHT - 50;

    barres[0].color_r_contour = 244;
    barres[0].color_g_contour = 37;
    barres[0].color_b_contour = 17;

    barres[0].color_r_interieur = 91;
    barres[0].color_g_interieur = 19;
    barres[0].color_b_interieur = 12;

    barres[0].vitesseX = 0;
    barres[0].vitesseY = 0;
}

void init_Barres_J2(){
    barres[1].longueur = 100;
    barres[1].largeur = 10;

    barres[1].x = (WINDOW_WIDTH - barres[1].longueur) / 2;
    barres[1].y = 50;

    barres[1].color_r_contour = 244;
    barres[1].color_g_contour = 214;
    barres[1].color_b_contour = 17;

    barres[1].color_r_interieur = 63;
    barres[1].color_g_interieur = 91;
    barres[1].color_b_interieur = 12;

    barres[1].vitesseX = 0;
    barres[1].vitesseY = 0;
}

void init_Barres_J3(){
    barres[2].longueur = 10;
    barres[2].largeur = 100;

    barres[2].x = 50;
    barres[2].y = (WINDOW_HEIGHT - barres[2].largeur) / 2;

    barres[2].color_r_contour = 244;
    barres[2].color_g_contour = 17;
    barres[2].color_b_contour = 180;

    barres[2].color_r_interieur = 85;
    barres[2].color_g_interieur = 12;
    barres[2].color_b_interieur = 91;

    barres[2].vitesseX = 0;
    barres[2].vitesseY = 0;
}

void init_Barres_J4(){
    barres[3].longueur = 10;
    barres[3].largeur = 100;

    barres[3].x = WINDOW_WIDTH - 50;
    barres[3].y = (WINDOW_HEIGHT - barres[2].largeur) / 2;

    barres[3].color_r_contour = 17;
    barres[3].color_g_contour = 135;
    barres[3].color_b_contour = 244;

    barres[3].color_r_interieur = 12;
    barres[3].color_g_interieur = 13;
    barres[3].color_b_interieur = 91;

    barres[3].vitesseX = 0;
    barres[3].vitesseY = 0;
}

void init_Barres(){
    init_Barres_J1();
    init_Barres_J2();
    init_Barres_J3();
    init_Barres_J4();
}
/*-----------------------------------------------------------------------------*/

/*-------------------------Initialisation des BRIQUES--------------------------*/
void init_vieBricks(int i, int j){
    pourcentageTypeBricks = rand() % 101; //[0-100]
    if((pourcentageTypeBricks >= 0) && (pourcentageTypeBricks < 76)){
        tabBrick[i][j].type = 1;
        tabBrick[i][j].vie = 1;
    }    
    else if((pourcentageTypeBricks < 91)){
        tabBrick[i][j].type = 2;
        tabBrick[i][j].vie = 2;
    }                             
    else if((pourcentageTypeBricks < 99)){
        tabBrick[i][j].type = 3;
        tabBrick[i][j].vie = 3;
    }                             
    else{
        tabBrick[i][j].type = 4;
        tabBrick[i][j].vie = 4;
    }                                                            
}

void init_Bricks(){
    srand(time(NULL));

    longueur = 60;
    xdebutBrick = (WINDOW_WIDTH - (nombreColonne*longueur + (nombreColonne-1)*5))/2; //5: espace entre briques
    ydebutBrick = (WINDOW_HEIGHT - (nombreLigne*longueur + (nombreLigne-1)*5))/2;
    
    for(int i = 0 ; i < nombreLigne ; i++){
        for(int j = 0 ; j < nombreColonne ; j++){
            tabBrick[i][j].x = xdebutBrick + j*longueur + (j-1)*5;
            tabBrick[i][j].y = ydebutBrick + i*longueur + (i-1)*5;
            tabBrick[i][j].spawn = 0;
            
            tabBrick[i][j].rebond = 0;
            tabBrick[i][j].temps = 0;

            init_vieBricks(i, j);
        }
    }
}
/*-----------------------------------------------------------------------------*/

/*----------------Initialisation des CHIFFRES pour chaque joueur---------------*/
void init_Chiffres_J1(){
    tabChiffres[0][0].longueur = 50;
    tabChiffres[0][0].largeur = 46;
    tabChiffres[0][0].sourceIMG = "assets/img0J1";

    tabChiffres[0][1].longueur = 19;
    tabChiffres[0][1].largeur = 46;
    tabChiffres[0][1].sourceIMG = "assets/img1J1";

    tabChiffres[0][2].longueur = 48;
    tabChiffres[0][2].largeur = 46;
    tabChiffres[0][2].sourceIMG = "assets/img2J1";

    tabChiffres[0][3].longueur = 45;
    tabChiffres[0][3].largeur = 46;
    tabChiffres[0][3].sourceIMG = "assets/img3J1";

    tabChiffres[0][4].longueur = 48;
    tabChiffres[0][4].largeur = 46;
    tabChiffres[0][4].sourceIMG = "assets/img4J1";

    tabChiffres[0][5].longueur = 47;
    tabChiffres[0][5].largeur = 46;
    tabChiffres[0][5].sourceIMG = "assets/img5J1";

    tabChiffres[0][6].longueur = 48;
    tabChiffres[0][6].largeur = 46;
    tabChiffres[0][6].sourceIMG = "assets/img6J1";

    tabChiffres[0][7].longueur = 44;
    tabChiffres[0][7].largeur = 46;
    tabChiffres[0][7].sourceIMG = "assets/img7J1";

    tabChiffres[0][8].longueur = 48;
    tabChiffres[0][8].largeur = 46;
    tabChiffres[0][8].sourceIMG = "assets/img8J1";

    tabChiffres[0][9].longueur = 48;
    tabChiffres[0][9].largeur = 46;
    tabChiffres[0][9].sourceIMG = "assets/img9J1";
}

void init_Chiffres_J2(){
    tabChiffres[1][0].longueur = 50;
    tabChiffres[1][0].largeur = 46;
    tabChiffres[1][0].sourceIMG = "assets/img0J2";

    tabChiffres[1][1].longueur = 19;
    tabChiffres[1][1].largeur = 46;
    tabChiffres[1][1].sourceIMG = "assets/img1J2";

    tabChiffres[1][2].longueur = 48;
    tabChiffres[1][2].largeur = 46;
    tabChiffres[1][2].sourceIMG = "assets/img2J2";

    tabChiffres[1][3].longueur = 45;
    tabChiffres[1][3].largeur = 46;
    tabChiffres[1][3].sourceIMG = "assets/img3J2";

    tabChiffres[1][4].longueur = 48;
    tabChiffres[1][4].largeur = 46;
    tabChiffres[1][4].sourceIMG = "assets/img4J2";

    tabChiffres[1][5].longueur = 47;
    tabChiffres[1][5].largeur = 46;
    tabChiffres[1][5].sourceIMG = "assets/img5J2";

    tabChiffres[1][6].longueur = 48;
    tabChiffres[1][6].largeur = 46;
    tabChiffres[1][6].sourceIMG = "assets/img6J2";

    tabChiffres[1][7].longueur = 44;
    tabChiffres[1][7].largeur = 46;
    tabChiffres[1][7].sourceIMG = "assets/img7J2";

    tabChiffres[1][8].longueur = 48;
    tabChiffres[1][8].largeur = 46;
    tabChiffres[1][8].sourceIMG = "assets/img8J2";

    tabChiffres[1][9].longueur = 48;
    tabChiffres[1][9].largeur = 46;
    tabChiffres[1][9].sourceIMG = "assets/img9J2";
}

void init_Chiffres_J3(){
    tabChiffres[2][0].longueur = 50;
    tabChiffres[2][0].largeur = 46;
    tabChiffres[2][0].sourceIMG = "assets/img0J3";

    tabChiffres[2][1].longueur = 19;
    tabChiffres[2][1].largeur = 46;
    tabChiffres[2][1].sourceIMG = "assets/img1J3";

    tabChiffres[2][2].longueur = 48;
    tabChiffres[2][2].largeur = 46;
    tabChiffres[2][2].sourceIMG = "assets/img2J3";

    tabChiffres[2][3].longueur = 45;
    tabChiffres[2][3].largeur = 46;
    tabChiffres[2][3].sourceIMG = "assets/img3J3";

    tabChiffres[2][4].longueur = 48;
    tabChiffres[2][4].largeur = 46;
    tabChiffres[2][4].sourceIMG = "assets/img4J3";

    tabChiffres[2][5].longueur = 47;
    tabChiffres[2][5].largeur = 46;
    tabChiffres[2][5].sourceIMG = "assets/img5J3";

    tabChiffres[2][6].longueur = 48;
    tabChiffres[2][6].largeur = 46;
    tabChiffres[2][6].sourceIMG = "assets/img6J3";

    tabChiffres[2][7].longueur = 44;
    tabChiffres[2][7].largeur = 46;
    tabChiffres[2][7].sourceIMG = "assets/img7J3";

    tabChiffres[2][8].longueur = 48;
    tabChiffres[2][8].largeur = 46;
    tabChiffres[2][8].sourceIMG = "assets/img8J3";

    tabChiffres[2][9].longueur = 48;
    tabChiffres[2][9].largeur = 46;
    tabChiffres[2][9].sourceIMG = "assets/img9J3";
}

void init_Chiffres_J4(){
    tabChiffres[3][0].longueur = 50;
    tabChiffres[3][0].largeur = 46;
    tabChiffres[3][0].sourceIMG = "assets/img0J4";

    tabChiffres[3][1].longueur = 19;
    tabChiffres[3][1].largeur = 46;
    tabChiffres[3][1].sourceIMG = "assets/img1J4";

    tabChiffres[3][2].longueur = 48;
    tabChiffres[3][2].largeur = 46;
    tabChiffres[3][2].sourceIMG = "assets/img2J4";

    tabChiffres[3][3].longueur = 45;
    tabChiffres[3][3].largeur = 46;
    tabChiffres[3][3].sourceIMG = "assets/img3J4";

    tabChiffres[3][4].longueur = 48;
    tabChiffres[3][4].largeur = 46;
    tabChiffres[3][4].sourceIMG = "assets/img4J4";

    tabChiffres[3][5].longueur = 47;
    tabChiffres[3][5].largeur = 46;
    tabChiffres[3][5].sourceIMG = "assets/img5J4";

    tabChiffres[3][6].longueur = 48;
    tabChiffres[3][6].largeur = 46;
    tabChiffres[3][6].sourceIMG = "assets/img6J4";

    tabChiffres[3][7].longueur = 44;
    tabChiffres[3][7].largeur = 46;
    tabChiffres[3][7].sourceIMG = "assets/img7J4";

    tabChiffres[3][8].longueur = 48;
    tabChiffres[3][8].largeur = 46;
    tabChiffres[3][8].sourceIMG = "assets/img8J4";

    tabChiffres[3][9].longueur = 48;
    tabChiffres[3][9].largeur = 46;
    tabChiffres[3][9].sourceIMG = "assets/img9J4";
}

void init_Chiffres(){
    init_Chiffres_J1();
    init_Chiffres_J2();
    init_Chiffres_J3();
    init_Chiffres_J4();
}
/*-----------------------------------------------------------------------------*/

/*-------------------------Initialisation de la partie-------------------------*/
void init_Resultat(){
    for(int i = 0 ; i < nombreJoueur ; i++){
        tabResultat[i] = 0;
        suiviCombo[i] = 0;
    }
}
/*-----------------------------------------------------------------------------*/

/*-------------------------Initialisation de la partie-------------------------*/
void init_game(){
    nombreJoueur = 4;

    init_Balles();

    init_Barres();

    init_Bricks();

    init_Chiffres();

    init_Resultat();

    nombreChiffres = 0;

    pointBrickCassee = 500;
    pointBalleToucheAdvers = 50;
    pointBalleToucheSoi = -100;
    pointCombo = 200;
}
/*-----------------------------------------------------------------------------*/

/*-------------Actualisation des COORDONNEES des côtés des BALLES--------------*/
void updateSide_Balles(int i){
    balles[i].side_x_le = balles[i].x - rayonBall;
    balles[i].side_x_ri = balles[i].x + rayonBall;
    balles[i].side_y_up = balles[i].y - rayonBall;
    balles[i].side_y_do = balles[i].y + rayonBall;
}
/*-----------------------------------------------------------------------------*/

/*-------------Actualisation des COORDONNEES des côtés des BARRES--------------*/
void updateSide_Barres(int i){
    barres[i].side_x_le = barres[i].x;
    barres[i].side_x_ri = barres[i].x + barres[i].longueur;
    barres[i].side_y_up = barres[i].y;
    barres[i].side_y_do = barres[i].y + barres[i].largeur;
}
/*-----------------------------------------------------------------------------*/

/*-------------Actualisation des COORDONNEES des côtés des BRIQUES-------------*/
void updateSide_Bricks(int i, int j){
    tabBrick[i][j].side_x_le = tabBrick[i][j].x;
    tabBrick[i][j].side_x_ri = tabBrick[i][j].x + longueur;
    tabBrick[i][j].side_y_up = tabBrick[i][j].y;
    tabBrick[i][j].side_y_do = tabBrick[i][j].y + longueur;
}
/*-----------------------------------------------------------------------------*/

/*------------------------Gestion des REBONDS des BALLES-----------------------*/
//---------Sur les BORDS de l'écran
    void rebondBalle_Bord(int i, int j){
        //Balle touche J1
        if((balles[i].vitesseY > 0) && (balles[i].y + rayonBall >= WINDOW_HEIGHT)){
            if      ((balles[i].color_r == barres[1].color_r_contour) || 
                     (balles[i].color_g == barres[1].color_g_contour) || 
                     (balles[i].color_b == barres[1].color_b_contour)){
                tabResultat[1] += pointBalleToucheAdvers;
            }
            else if ((balles[i].color_r == barres[2].color_r_contour) || 
                     (balles[i].color_g == barres[2].color_g_contour) || 
                     (balles[i].color_b == barres[2].color_b_contour)){
                tabResultat[2] += pointBalleToucheAdvers;
            }
            else if ((balles[i].color_r == barres[3].color_r_contour) || 
                     (balles[i].color_g == barres[3].color_g_contour) || 
                     (balles[i].color_b == barres[3].color_b_contour)){
                tabResultat[3] += pointBalleToucheAdvers;
            }

            tabResultat[0] += pointBalleToucheSoi;
            tabResultat[0] += pointCombo * suiviCombo[0];
            suiviCombo[0] = 0;
            balles[i].vitesseY = -balles[i].vitesseY;
        }
        
        //Balle touche J2
        else if((balles[i].vitesseY < 0) && (balles[i].y - rayonBall <= 0)){
            if      ((balles[i].color_r == barres[0].color_r_contour) || 
                     (balles[i].color_g == barres[0].color_g_contour) || 
                     (balles[i].color_b == barres[0].color_b_contour)){
                tabResultat[0] += pointBalleToucheAdvers;
            }
            else if ((balles[i].color_r == barres[2].color_r_contour) || 
                     (balles[i].color_g == barres[1].color_g_contour) || 
                     (balles[i].color_b == barres[1].color_b_contour)){
                tabResultat[2] += pointBalleToucheAdvers;
            }
            else if ((balles[i].color_r == barres[3].color_r_contour) || 
                     (balles[i].color_g == barres[3].color_g_contour) || 
                     (balles[i].color_b == barres[3].color_b_contour)){
                tabResultat[3] += pointBalleToucheAdvers;
            }

            tabResultat[1] += pointBalleToucheSoi;
            tabResultat[1] += pointCombo * suiviCombo[1];
            suiviCombo[1] = 0;
            balles[i].vitesseY = -balles[i].vitesseY;
        }
        
        //Balle touche J3
        else if((balles[i].vitesseX < 0) && (balles[i].x - rayonBall <= 0)){
            if      ((balles[i].color_r == barres[0].color_r_contour) || 
                     (balles[i].color_g == barres[0].color_g_contour) || 
                     (balles[i].color_b == barres[0].color_b_contour)){
                tabResultat[0] += pointBalleToucheAdvers;
            }
            else if ((balles[i].color_r == barres[1].color_r_contour) || 
                     (balles[i].color_g == barres[1].color_g_contour) || 
                     (balles[i].color_b == barres[1].color_b_contour)){
                tabResultat[1] += pointBalleToucheAdvers;
            }
            else if ((balles[i].color_r == barres[3].color_r_contour) || 
                     (balles[i].color_g == barres[3].color_g_contour) || 
                     (balles[i].color_b == barres[3].color_b_contour)){
                tabResultat[3] += pointBalleToucheAdvers;
            }

            tabResultat[2] += pointBalleToucheSoi;
            tabResultat[2] += pointCombo * suiviCombo[2];
            suiviCombo[2] = 0;
            balles[i].vitesseX = -balles[i].vitesseX;
        }
        //Balle touche J4
        else if((balles[i].vitesseX > 0) && (balles[i].x + rayonBall >= WINDOW_WIDTH)){
            if      ((balles[i].color_r == barres[0].color_r_contour) || 
                     (balles[i].color_g == barres[0].color_g_contour) || 
                     (balles[i].color_b == barres[0].color_b_contour)){
                tabResultat[0] += pointBalleToucheAdvers;
            }
            else if ((balles[i].color_r == barres[1].color_r_contour) || 
                     (balles[i].color_g == barres[1].color_g_contour) || 
                     (balles[i].color_b == barres[1].color_b_contour)){
                tabResultat[1] += pointBalleToucheAdvers;
            }
            else if ((balles[i].color_r == barres[2].color_r_contour) || 
                     (balles[i].color_g == barres[2].color_g_contour) || 
                     (balles[i].color_b == barres[2].color_b_contour)){
                tabResultat[2] += pointBalleToucheAdvers;
            }
            tabResultat[3] += pointBalleToucheSoi;
            tabResultat[3] += pointCombo * suiviCombo[3];
            suiviCombo[3] = 0;
            balles[i].vitesseX = -balles[i].vitesseX;
        }
    }
//---------------------------------
//---------Sur les BARRES----------
    void changement_couleurBalle(int i, int j){
        balles[i].color_r = barres[j].color_r_contour;
        balles[i].color_g = barres[j].color_g_contour;
        balles[i].color_b = barres[j].color_b_contour;
    }

    void rebondBalle_Barre_J1(int i){
        if(((balles[i].vitesseX > 0) && (balles[i].y >= barres[0].y) && (balles[i].y <= barres[0].y + barres[0].largeur) && (balles[i].x + rayonBall >= barres[0].x) && (balles[i].x <= barres[0].x)) ||
           ((balles[i].vitesseX < 0) && (balles[i].y >= barres[0].y) && (balles[i].y <= barres[0].y + barres[0].largeur) && (balles[i].x - rayonBall <= barres[0].x + barres[0].longueur) && (balles[i].x >= barres[0].x + barres[0].longueur))
           ){
            balles[i].vitesseX = -balles[i].vitesseX;
            changement_couleurBalle(i, 0);
        }
        else if(((balles[i].vitesseY > 0) && (balles[i].x >= barres[0].x) && (balles[i].x <= barres[0].x + barres[0].longueur) && (balles[i].y + rayonBall >= barres[0].y) && (balles[i].y <= barres[0].y))){
            balles[i].vitesseY = -balles[i].vitesseY;
            changement_couleurBalle(i, 0);
        }
        else if(((balles[i].vitesseX > 0) && (balles[i].vitesseY > 0) && (balles[i].y <= barres[0].y) && (balles[i].y + rayonBall >= barres[0].y) && (balles[i].x <= barres[0].x) && (balles[i].x + rayonBall >= barres[0].x)) ||
                ((balles[i].vitesseX < 0) && (balles[i].vitesseY > 0) && (balles[i].y <= barres[0].y) && (balles[i].y + rayonBall >= barres[0].y) && (balles[i].x >= barres[0].x + barres[0].longueur) && (balles[i].x - rayonBall <= barres[0].x + barres[0].longueur))
                ){
            balles[i].vitesseX = -balles[i].vitesseX;
            balles[i].vitesseY = -balles[i].vitesseY;
            changement_couleurBalle(i, 0);
        }
    }
    
    void rebondBalle_Barre_J2(int i){
        if(((balles[i].vitesseX > 0) && (balles[i].y >= barres[1].y) && (balles[i].y <= barres[1].y + barres[1].largeur) && (balles[i].x + rayonBall >= barres[1].x) && (balles[i].x <= barres[1].x)) ||
           ((balles[i].vitesseX < 0) && (balles[i].y >= barres[1].y) && (balles[i].y <= barres[1].y + barres[1].largeur) && (balles[i].x - rayonBall <= barres[1].x + barres[1].longueur) && (balles[i].x >= barres[1].x + barres[1].longueur))
           ){
            balles[i].vitesseX = -balles[i].vitesseX;
            changement_couleurBalle(i, 1);
        }
        else if(((balles[i].vitesseY < 0) && (balles[i].x >= barres[1].x) && (balles[i].x <= barres[1].x + barres[1].longueur) && (balles[i].y - rayonBall <= barres[1].y + barres[1].largeur) && (balles[i].y >= barres[1].y + barres[1].largeur))){
            balles[i].vitesseY = -balles[i].vitesseY;
            changement_couleurBalle(i, 1);
        }
        else if(((balles[i].vitesseX > 0) && (balles[i].vitesseY < 0) && (balles[i].y >= barres[1].y + barres[1].longueur) && (balles[i].y - rayonBall <= barres[1].y + barres[1].longueur) && (balles[i].x <= barres[1].x) && (balles[i].x + rayonBall >= barres[1].x)) ||
                ((balles[i].vitesseX < 0) && (balles[i].vitesseY < 0) && (balles[i].y >= barres[1].y + barres[1].longueur) && (balles[i].y - rayonBall <= barres[1].y + barres[1].longueur) && (balles[i].x >= barres[1].x + barres[1].longueur) && (balles[i].x - rayonBall <= barres[1].x + barres[1].longueur))
                ){
            balles[i].vitesseX = -balles[i].vitesseX;
            balles[i].vitesseY = -balles[i].vitesseY;
            changement_couleurBalle(i, 1);
        }
    }
    
    void rebondBalle_Barre_J3(int i){
        if(((balles[i].vitesseX < 0) && (balles[i].y >= barres[2].y) && (balles[i].y <= barres[2].y + barres[2].largeur) && (balles[i].x - rayonBall <= barres[2].x + barres[2].longueur) && (balles[i].x >= barres[2].x + barres[2].longueur))){
            balles[i].vitesseX = -balles[i].vitesseX;
            changement_couleurBalle(i, 2);
        }
        else if(((balles[i].vitesseY > 0) && (balles[i].x >= barres[2].x) && (balles[i].x <= barres[2].x + barres[2].longueur) && (balles[i].y + rayonBall >= barres[2].y) && (balles[i].y <= barres[2].y)) ||
                ((balles[i].vitesseY < 0) && (balles[i].x >= barres[2].x) && (balles[i].x <= barres[2].x + barres[2].longueur) && (balles[i].y - rayonBall <= barres[2].y + barres[2].largeur) && (balles[i].y >= barres[1].y + barres[2].largeur))
                ){
            balles[i].vitesseY = -balles[i].vitesseY;
            changement_couleurBalle(i, 2);
        }
        else if(((balles[i].vitesseY < 0) && (balles[i].vitesseX < 0) && (balles[i].x >= barres[2].x + barres[2].longueur) && (balles[i].x - rayonBall <= barres[2].x + barres[2].longueur) && (balles[i].y >= barres[2].y + barres[2].largeur) && (balles[i].y - rayonBall <= barres[2].y + barres[2].largeur)) ||
                ((balles[i].vitesseY > 0) && (balles[i].vitesseX < 0) && (balles[i].x >= barres[2].x + barres[2].longueur) && (balles[i].x - rayonBall <= barres[2].x + barres[2].longueur) && (balles[i].y >= barres[2].y) && (balles[i].y + rayonBall <= barres[2].y))
                ){
            balles[i].vitesseX = -balles[i].vitesseX;
            balles[i].vitesseY = -balles[i].vitesseY;
            changement_couleurBalle(i, 2);
        }
    }
    
    void rebondBalle_Barre_J4(int i){
        if(((balles[i].vitesseX > 0) && (balles[i].y >= barres[3].y) && (balles[i].y <= barres[3].y + barres[3].largeur) && (balles[i].x + rayonBall >= barres[3].x) && (balles[i].x <= barres[3].x))){
            balles[i].vitesseX = -balles[i].vitesseX;
            changement_couleurBalle(i, 3);
        }
        else if(((balles[i].vitesseY > 0) && (balles[i].x >= barres[3].x) && (balles[i].x <= barres[3].x + barres[3].longueur) && (balles[i].y + rayonBall >= barres[3].y) && (balles[i].y <= barres[3].y)) ||
                ((balles[i].vitesseY < 0) && (balles[i].x >= barres[3].x) && (balles[i].x <= barres[3].x + barres[3].longueur) && (balles[i].y - rayonBall <= barres[3].y + barres[3].largeur) && (balles[i].y >= barres[3].y + barres[3].largeur))
                ){
            balles[i].vitesseY = -balles[i].vitesseY;
            changement_couleurBalle(i, 3);
        }
        else if(((balles[i].vitesseY < 0) && (balles[i].vitesseX > 0) && (balles[i].x <= barres[3].x) && (balles[i].x + rayonBall >= barres[3].x) && (balles[i].y >= barres[3].y + barres[3].largeur) && (balles[i].y - rayonBall <= barres[3].y + barres[3].largeur)) ||
                ((balles[i].vitesseY > 0) && (balles[i].vitesseX > 0) && (balles[i].x >= barres[3].x) && (balles[i].x - rayonBall <= barres[3].x) && (balles[i].y <= barres[3].y) && (balles[i].y + rayonBall >= barres[3].y + barres[3].largeur))){
            balles[i].vitesseX = -balles[i].vitesseX;
            balles[i].vitesseY = -balles[i].vitesseY;
            changement_couleurBalle(i, 3);
        }
    }

    void rebondBalle_Barre(int i){
        rebondBalle_Barre_J1(i);
        rebondBalle_Barre_J2(i);
        rebondBalle_Barre_J3(i);
        rebondBalle_Barre_J4(i);
    }
//---------------------------------
//---------Sur les BRIQUES---------
    void rebondBalle_Bricks(int i, int j, int k){
        if((balles[k].vitesseX < 0) && (balles[k].x - rayonBall <= tabBrick[i][j].x + longueur) && (balles[k].x > tabBrick[i][j].x + longueur) && (balles[k].y >= tabBrick[i][j].y) && (balles[k].y <= tabBrick[i][j].y + longueur) ||
           (balles[k].vitesseX > 0) && (balles[k].x + rayonBall >= tabBrick[i][j].x) && (balles[k].x < tabBrick[i][j].x) && (balles[k].y >= tabBrick[i][j].y) && (balles[k].y <= tabBrick[i][j].y + longueur) 
        ){
            balles[k].vitesseX = -balles[k].vitesseX;
            tabBrick[i][j].vie--;
            tabBrick[i][j].rebond = 1;

            if(tabBrick[i][j].vie == 0){
                if((balles[k].color_r == barres[0].color_r_contour) &&
                   (balles[k].color_g == barres[0].color_g_contour) &&
                   (balles[k].color_b == barres[0].color_b_contour)
                ){
                    tabResultat[0] += pointBrickCassee * tabBrick[i][j].type;
                    suiviCombo[0]++;
                }
                else if((balles[k].color_r == barres[1].color_r_contour) &&
                        (balles[k].color_g == barres[1].color_g_contour) &&
                        (balles[k].color_b == barres[1].color_b_contour)
                ){
                    tabResultat[1] += pointBrickCassee * tabBrick[i][j].type;
                    suiviCombo[1]++;
                }
                else if((balles[k].color_r == barres[2].color_r_contour) &&
                        (balles[k].color_g == barres[2].color_g_contour) &&
                        (balles[k].color_b == barres[2].color_b_contour)
                ){
                    tabResultat[2] += pointBrickCassee * tabBrick[i][j].type;
                    suiviCombo[2]++;
                }
                else if((balles[k].color_r == barres[3].color_r_contour) &&
                        (balles[k].color_g == barres[3].color_g_contour) &&
                        (balles[k].color_b == barres[3].color_b_contour)
                ){
                    tabResultat[3] += pointBrickCassee * tabBrick[i][j].type;
                    suiviCombo[3]++;
                }
            }
        }

        else if((balles[k].vitesseY < 0) && (balles[k].y - rayonBall <= tabBrick[i][j].y + longueur) && (balles[k].y > tabBrick[i][j].y + longueur) && (balles[k].x >= tabBrick[i][j].x) && (balles[k].x <= tabBrick[i][j].x + longueur) ||
                (balles[k].vitesseY > 0) && (balles[k].y + rayonBall >= tabBrick[i][j].y) && (balles[k].y < tabBrick[i][j].y) && (balles[k].x >= tabBrick[i][j].x) && (balles[k].x <= tabBrick[i][j].x + longueur)
        ){
            balles[k].vitesseY = -balles[k].vitesseY;
            tabBrick[i][j].vie--;
            tabBrick[i][j].rebond = 1;

            if(tabBrick[i][j].vie == 0){
                if((balles[k].color_r == barres[0].color_r_contour) &&
                   (balles[k].color_g == barres[0].color_g_contour) &&
                   (balles[k].color_b == barres[0].color_b_contour)
                ){
                    tabResultat[0] += pointBrickCassee * tabBrick[i][j].type;
                }
                else if((balles[k].color_r == barres[1].color_r_contour) &&
                        (balles[k].color_g == barres[1].color_g_contour) &&
                        (balles[k].color_b == barres[1].color_b_contour)
                ){
                    tabResultat[1] += pointBrickCassee * tabBrick[i][j].type;
                }
                else if((balles[k].color_r == barres[2].color_r_contour) &&
                        (balles[k].color_g == barres[2].color_g_contour) &&
                        (balles[k].color_b == barres[2].color_b_contour)
                ){
                    tabResultat[2] += pointBrickCassee * tabBrick[i][j].type;
                }
                else if((balles[k].color_r == barres[3].color_r_contour) &&
                        (balles[k].color_g == barres[3].color_g_contour) &&
                        (balles[k].color_b == barres[3].color_b_contour)
                ){
                    tabResultat[3] += pointBrickCassee * tabBrick[i][j].type;
                }
            }
        }
        //Coins : Haut Droit -- Haut Gauche -- Bas Droit -- Bas Gauche
        else if((tabBrick[i - 1][j].vie > 0) && (balles[k].vitesseX > 0) && (balles[k].vitesseY > 0) && (balles[k].x < tabBrick[i][j].x) && (balles[k].x + rayonBall >= tabBrick[i][j].x) && (balles[k].y < tabBrick[i][j].y) && (balles[k].y + rayonBall >= tabBrick[i][j].y) ||
                (tabBrick[i - 1][j].vie > 0) && (balles[k].vitesseX < 0) && (balles[k].vitesseY > 0) && (balles[k].x > tabBrick[i][j].x + longueur) && (balles[k].x - rayonBall <= tabBrick[i][j].x + longueur) && (balles[k].y < tabBrick[i][j].y) && (balles[k].y + rayonBall >= tabBrick[i][j].y) ||
                (tabBrick[i + 1][j].vie > 0) && (balles[k].vitesseX > 0) && (balles[k].vitesseY < 0) && (balles[k].x < tabBrick[i][j].x) && (balles[k].x + rayonBall >= tabBrick[i][j].x) && (balles[k].y > tabBrick[i][j].y + longueur) && (balles[k].y - rayonBall <= tabBrick[i][j].y + longueur) ||
                (tabBrick[i + 1][j].vie > 0) && (balles[k].vitesseX < 0) && (balles[k].vitesseY < 0) && (balles[k].x > tabBrick[i][j].x + longueur) && (balles[k].x - rayonBall <= tabBrick[i][j].x + longueur) && (balles[k].y > tabBrick[i][j].y + longueur) && (balles[k].y - rayonBall <= tabBrick[i][j].y + longueur)
        ){
            balles[k].vitesseX = -balles[k].vitesseX;
            tabBrick[i][j].vie--; 
            tabBrick[i][j].rebond = 1;  

            if(tabBrick[i][j].vie == 0){
                if((balles[k].color_r == barres[0].color_r_contour) &&
                   (balles[k].color_g == barres[0].color_g_contour) &&
                   (balles[k].color_b == barres[0].color_b_contour)
                ){
                    tabResultat[0] += pointBrickCassee * tabBrick[i][j].type;
                }
                else if((balles[k].color_r == barres[1].color_r_contour) &&
                        (balles[k].color_g == barres[1].color_g_contour) &&
                        (balles[k].color_b == barres[1].color_b_contour)
                ){
                    tabResultat[1] += pointBrickCassee * tabBrick[i][j].type;
                }
                else if((balles[k].color_r == barres[2].color_r_contour) &&
                        (balles[k].color_g == barres[2].color_g_contour) &&
                        (balles[k].color_b == barres[2].color_b_contour)
                ){
                    tabResultat[2] += pointBrickCassee * tabBrick[i][j].type;
                }
                else if((balles[k].color_r == barres[3].color_r_contour) &&
                        (balles[k].color_g == barres[3].color_g_contour) &&
                        (balles[k].color_b == barres[3].color_b_contour)
                ){
                    tabResultat[3] += pointBrickCassee * tabBrick[i][j].type;
                }
            }              
        }
        //Coins : Haut Droit -- Haut Gauche -- Bas Droit -- Bas Gauche
        else if((tabBrick[i][j - 1].vie > 0) && (balles[k].vitesseX > 0) && (balles[k].vitesseY > 0) && (balles[k].x < tabBrick[i][j].x) && (balles[k].x + rayonBall >= tabBrick[i][j].x) && (balles[k].y < tabBrick[i][j].y) && (balles[k].y + rayonBall >= tabBrick[i][j].y) ||
                (tabBrick[i][j + 1].vie > 0) && (balles[k].vitesseX < 0) && (balles[k].vitesseY > 0) && (balles[k].x > tabBrick[i][j].x + longueur) && (balles[k].x - rayonBall <= tabBrick[i][j].x + longueur) && (balles[k].y < tabBrick[i][j].y) && (balles[k].y + rayonBall >= tabBrick[i][j].y) ||
                (tabBrick[i][j - 1].vie > 0) && (balles[k].vitesseX > 0) && (balles[k].vitesseY < 0) && (balles[k].x < tabBrick[i][j].x) && (balles[k].x + rayonBall >= tabBrick[i][j].x) && (balles[k].y > tabBrick[i][j].y + longueur) && (balles[k].y - rayonBall <= tabBrick[i][j].y + longueur) ||
                (tabBrick[i][j + 1].vie > 0) && (balles[k].vitesseX < 0) && (balles[k].vitesseY < 0) && (balles[k].x > tabBrick[i][j].x + longueur) && (balles[k].x - rayonBall <= tabBrick[i][j].x + longueur) && (balles[k].y > tabBrick[i][j].y + longueur) && (balles[k].y - rayonBall <= tabBrick[i][j].y + longueur)
        ){
            balles[k].vitesseY = -balles[k].vitesseY;
            tabBrick[i][j].vie--;   
            tabBrick[i][j].rebond = 1;    

            if(tabBrick[i][j].vie == 0){
                if((balles[k].color_r == barres[0].color_r_contour) &&
                   (balles[k].color_g == barres[0].color_g_contour) &&
                   (balles[k].color_b == barres[0].color_b_contour)
                ){
                    tabResultat[0] += pointBrickCassee * tabBrick[i][j].type;
                }
                else if((balles[k].color_r == barres[1].color_r_contour) &&
                        (balles[k].color_g == barres[1].color_g_contour) &&
                        (balles[k].color_b == barres[1].color_b_contour)
                ){
                    tabResultat[1] += pointBrickCassee * tabBrick[i][j].type;
                }
                else if((balles[k].color_r == barres[2].color_r_contour) &&
                        (balles[k].color_g == barres[2].color_g_contour) &&
                        (balles[k].color_b == barres[2].color_b_contour)
                ){
                    tabResultat[2] += pointBrickCassee * tabBrick[i][j].type;
                }
                else if((balles[k].color_r == barres[3].color_r_contour) &&
                        (balles[k].color_g == barres[3].color_g_contour) &&
                        (balles[k].color_b == barres[3].color_b_contour)
                ){
                    tabResultat[3] += pointBrickCassee * tabBrick[i][j].type;
                }
            }         
        }
        else if((balles[k].vitesseX > 0) && (balles[k].vitesseY > 0) && (balles[k].x < tabBrick[i][j].x) && (balles[k].x + rayonBall >= tabBrick[i][j].x) && (balles[k].y < tabBrick[i][j].y) && (balles[k].y + rayonBall >= tabBrick[i][j].y) ||
                (balles[k].vitesseX < 0) && (balles[k].vitesseY > 0) && (balles[k].x > tabBrick[i][j].x + longueur) && (balles[k].x - rayonBall <= tabBrick[i][j].x + longueur) && (balles[k].y < tabBrick[i][j].y) && (balles[k].y + rayonBall >= tabBrick[i][j].y) ||
                (balles[k].vitesseX > 0) && (balles[k].vitesseY < 0) && (balles[k].x < tabBrick[i][j].x) && (balles[k].x + rayonBall >= tabBrick[i][j].x) && (balles[k].y > tabBrick[i][j].y + longueur) && (balles[k].y - rayonBall <= tabBrick[i][j].y + longueur) ||
                (balles[k].vitesseX < 0) && (balles[k].vitesseY < 0) && (balles[k].x > tabBrick[i][j].x + longueur) && (balles[k].x - rayonBall <= tabBrick[i][j].x + longueur) && (balles[k].y > tabBrick[i][j].y + longueur) && (balles[k].y - rayonBall <= tabBrick[i][j].y + longueur)
        ){
            balles[k].vitesseX = -balles[k].vitesseX;
            balles[k].vitesseY = -balles[k].vitesseY;
            tabBrick[i][j].vie--;
            tabBrick[i][j].rebond = 1;

            if(tabBrick[i][j].vie == 0){
                if((balles[k].color_r == barres[0].color_r_contour) &&
                   (balles[k].color_g == barres[0].color_g_contour) &&
                   (balles[k].color_b == barres[0].color_b_contour)
                ){
                    tabResultat[0] += pointBrickCassee * tabBrick[i][j].type;
                }
                else if((balles[k].color_r == barres[1].color_r_contour) &&
                        (balles[k].color_g == barres[1].color_g_contour) &&
                        (balles[k].color_b == barres[1].color_b_contour)
                ){
                    tabResultat[1] += pointBrickCassee * tabBrick[i][j].type;
                }
                else if((balles[k].color_r == barres[2].color_r_contour) &&
                        (balles[k].color_g == barres[2].color_g_contour) &&
                        (balles[k].color_b == barres[2].color_b_contour)
                ){
                    tabResultat[2] += pointBrickCassee * tabBrick[i][j].type;
                }
                else if((balles[k].color_r == barres[3].color_r_contour) &&
                        (balles[k].color_g == barres[3].color_g_contour) &&
                        (balles[k].color_b == barres[3].color_b_contour)
                ){
                    tabResultat[3] += pointBrickCassee * tabBrick[i][j].type;
                }
            }
        }
    }
//---------------------------------
/*-----------------------------------------------------------------------------*/

/*------------------------Gestion des REBONDS des BARRES-----------------------*/
void rebondBarre_Bord(int i){
    if(barres[i].x <= 0){
        barres[i].x = 1;
        barres[i].vitesseX = 0;
    }
    else if(barres[i].x + barres[i].longueur >= WINDOW_WIDTH){
        barres[i].x = WINDOW_WIDTH - barres[i].longueur - 1;
        barres[i].vitesseX = 0;
    }

    if(barres[i].y <= 0){
        barres[i].y = 1;
        barres[i].vitesseY = 0;
    }
    else if(barres[i].y + barres[i].largeur >= WINDOW_HEIGHT){
        barres[i].y = WINDOW_HEIGHT - barres[i].largeur - 1;
        barres[i].vitesseY = 0;
    }
}
/*-----------------------------------------------------------------------------*/

/*----------------------------Apparition des BALLES----------------------------*/
void drawBalle(int i, int j){
    changeColor(balles[i].color_r, balles[i].color_g, balles[i].color_b, 255);
    drawCircle(balles[i].x, balles[i].y, rayonBall);

    if(j == 0){
        balles[i].x += balles[i].vitesseX;
        balles[i].y += balles[i].vitesseY;
    }
    
    updateSide_Balles(i);
    updateSide_Barres(i);

    rebondBalle_Bord(i, j);
    rebondBalle_Barre(i);
}
/*-----------------------------------------------------------------------------*/

/*----------------------------Apparition des BARRES----------------------------*/
void gestionBot_J2(){
    for(int k = 0 ; k < nombreJoueur ; k++){
        if(k == 0) plusProche = 0;
        else if((balles[k].vitesseY < 0) && (balles[k].y > barres[1].y + barres[1].largeur)){
            if(balles[k].y < balles[plusProche].y)      plusProche = k;
        }
    }
    
    if(balles[plusProche].x > barres[1].x + (barres[1].longueur / 2))   barres[1].vitesseX = VITESSEBARRE;
    else                                                                barres[1].vitesseX = -VITESSEBARRE;
}

void gestionBot_J3(){
    for(int k = 0 ; k < nombreJoueur ; k++){
        if(k == 0) plusProche = 0;
        else if((balles[k].vitesseX < 0) && (balles[k].x > barres[2].x + barres[2].longueur)){
            if(balles[k].x < balles[plusProche].x)      plusProche = k;
        }
    }
    
    if(balles[plusProche].y > barres[2].y + (barres[2].largeur / 2))    barres[2].vitesseY = VITESSEBARRE;
    else                                                                barres[2].vitesseY = -VITESSEBARRE;
}

void gestionBot_J4(){
    for(int k = 0 ; k < nombreJoueur ; k++){
        if(k == 0) plusProche = 0;
        else if((balles[k].vitesseX > 0) && (balles[k].x < barres[3].x)){
            if(balles[k].x > balles[plusProche].x)      plusProche = k;
        }
    }
    
    if(balles[plusProche].y > barres[3].y + (barres[3].largeur / 2))    barres[3].vitesseY = VITESSEBARRE;
    else                                                                barres[3].vitesseY = -VITESSEBARRE;    
}

void gestionBot(){
    gestionBot_J2();
    gestionBot_J3();
    gestionBot_J4();
}

void drawBarre(int i, int j){
    changeColor(barres[i].color_r_contour, barres[i].color_g_contour, barres[i].color_b_contour, 255);
    drawRect(barres[i].x, barres[i].y, barres[i].longueur, barres[i].largeur);
    changeColor(barres[i].color_r_interieur, barres[i].color_g_interieur, barres[i].color_b_interieur, 255);
    drawRect(barres[i].x + 2, barres[i].y + 2, barres[i].longueur - 4, barres[i].largeur - 4);

    //permet de faire la mise qu'une fois
    if(j == 0){
        barres[i].x += barres[i].vitesseX;
        barres[i].y += barres[i].vitesseY;
    }

    gestionBot();

    rebondBarre_Bord(i);
}
/*-----------------------------------------------------------------------------*/

/*----------------------------Apparition des BRIQUES---------------------------*/
void spawnBricks(int i, int j){
    if((tabBrick[i][j].frame == 0) && (tabBrick[i][j].spawn == 0)){
        if     (tabBrick[i][j].vie == 1)    tabBrick[i][j].sourceIMG = "assets/brick6Type1.bmp";
        else if(tabBrick[i][j].vie == 2)    tabBrick[i][j].sourceIMG = "assets/brick6Type2.bmp";
        else if(tabBrick[i][j].vie == 3)    tabBrick[i][j].sourceIMG = "assets/brick6Type3.bmp";
        else                                tabBrick[i][j].sourceIMG = "assets/brick6Type4.bmp";
    }         
      
    else if((tabBrick[i][j].frame == 2) && (tabBrick[i][j].spawn == 0)){
        if     (tabBrick[i][j].vie == 1)    tabBrick[i][j].sourceIMG = "assets/brick5Type1.bmp";
        else if(tabBrick[i][j].vie == 2)    tabBrick[i][j].sourceIMG = "assets/brick5Type2.bmp";
        else if(tabBrick[i][j].vie == 3)    tabBrick[i][j].sourceIMG = "assets/brick5Type3.bmp";
        else                                tabBrick[i][j].sourceIMG = "assets/brick5Type4.bmp";
    }   
      
    else if((tabBrick[i][j].frame == 4) && (tabBrick[i][j].spawn == 0)){
        if     (tabBrick[i][j].vie == 1)    tabBrick[i][j].sourceIMG = "assets/brick4Type1.bmp";
        else if(tabBrick[i][j].vie == 2)    tabBrick[i][j].sourceIMG = "assets/brick4Type2.bmp";
        else if(tabBrick[i][j].vie == 3)    tabBrick[i][j].sourceIMG = "assets/brick4Type3.bmp";
        else                                tabBrick[i][j].sourceIMG = "assets/brick4Type4.bmp";
    }    
      
    else if((tabBrick[i][j].frame == 6) && (tabBrick[i][j].spawn == 0)){
        if     (tabBrick[i][j].vie == 1)    tabBrick[i][j].sourceIMG = "assets/brick3Type1.bmp";
        else if(tabBrick[i][j].vie == 2)    tabBrick[i][j].sourceIMG = "assets/brick3Type2.bmp";
        else if(tabBrick[i][j].vie == 3)    tabBrick[i][j].sourceIMG = "assets/brick3Type3.bmp";
        else                                tabBrick[i][j].sourceIMG = "assets/brick3Type4.bmp";
    }    
      
    else if((tabBrick[i][j].frame == 8) && (tabBrick[i][j].spawn == 0)){
        if     (tabBrick[i][j].vie == 1)    tabBrick[i][j].sourceIMG = "assets/brick2Type1.bmp";
        else if(tabBrick[i][j].vie == 2)    tabBrick[i][j].sourceIMG = "assets/brick2Type2.bmp";
        else if(tabBrick[i][j].vie == 3)    tabBrick[i][j].sourceIMG = "assets/brick2Type3.bmp";
        else                                tabBrick[i][j].sourceIMG = "assets/brick2Type4.bmp";
    }    
      
    else if((tabBrick[i][j].frame == 10) && (tabBrick[i][j].spawn == 0)){
        if     (tabBrick[i][j].vie == 1)    tabBrick[i][j].sourceIMG = "assets/brick1Type1.bmp";
        else if(tabBrick[i][j].vie == 2)    tabBrick[i][j].sourceIMG = "assets/brick1Type2.bmp";
        else if(tabBrick[i][j].vie == 3)    tabBrick[i][j].sourceIMG = "assets/brick1Type3.bmp";
        else                                tabBrick[i][j].sourceIMG = "assets/brick1Type4.bmp";
    }    
      
    else if((tabBrick[i][j].frame == 12) && (tabBrick[i][j].spawn == 0)){
        if     (tabBrick[i][j].vie == 1)    tabBrick[i][j].sourceIMG = "assets/brick0Type1.bmp";
        else if(tabBrick[i][j].vie == 2)    tabBrick[i][j].sourceIMG = "assets/brick0Type2.bmp";
        else if(tabBrick[i][j].vie == 3)    tabBrick[i][j].sourceIMG = "assets/brick0Type3.bmp";
        else                                tabBrick[i][j].sourceIMG = "assets/brick0Type4.bmp";
    }     
      
    if(tabBrick[i][j].frame == 12) tabBrick[i][j].spawn = 1;
}

void breakBricks(int i, int j){
    if     ((tabBrick[i][j].frame == 0) && (tabBrick[i][j].rebond == 1) && (tabBrick[i][j].vie == 0))       tabBrick[i][j].sourceIMG = "assets/brick0Type1.bmp";
      
    else if((tabBrick[i][j].frame == 2) && (tabBrick[i][j].rebond == 1) && (tabBrick[i][j].vie == 0))       tabBrick[i][j].sourceIMG = "assets/brick1Type1.bmp";
      
    else if((tabBrick[i][j].frame == 4) && (tabBrick[i][j].rebond == 1) && (tabBrick[i][j].vie == 0))       tabBrick[i][j].sourceIMG = "assets/brick2Type1.bmp";
      
    else if((tabBrick[i][j].frame == 6) && (tabBrick[i][j].rebond == 1) && (tabBrick[i][j].vie == 0))       tabBrick[i][j].sourceIMG = "assets/brick3Type1.bmp";
      
    else if((tabBrick[i][j].frame == 8) && (tabBrick[i][j].rebond == 1) && (tabBrick[i][j].vie == 0))       tabBrick[i][j].sourceIMG = "assets/brick4Type1.bmp";
      
    else if((tabBrick[i][j].frame == 10) && (tabBrick[i][j].rebond == 1) && (tabBrick[i][j].vie == 0))      tabBrick[i][j].sourceIMG = "assets/brick5Type1.bmp";
      
    else if((tabBrick[i][j].frame == 12) && (tabBrick[i][j].rebond == 1) && (tabBrick[i][j].vie == 0))      tabBrick[i][j].sourceIMG = "assets/brick6Type1.bmp";
      

    if((tabBrick[i][j].frame == 12) && (tabBrick[i][j].rebond == 1) && (tabBrick[i][j].vie == 0)) tabBrick[i][j].rebond = 2;

}

void showBricks(int i, int j){
    if(tabBrick[i][j].spawn == 1){
        if     (tabBrick[i][j].vie == 1)    tabBrick[i][j].sourceIMG = "assets/brick0Type1.bmp";
        else if(tabBrick[i][j].vie == 2)    tabBrick[i][j].sourceIMG = "assets/brick0Type2.bmp";
        else if(tabBrick[i][j].vie == 3)    tabBrick[i][j].sourceIMG = "assets/brick0Type3.bmp";
        else                                tabBrick[i][j].sourceIMG = "assets/brick0Type4.bmp";
    }
}

void drawBricks(int i, int j, int k){
    /*----------------------------Briques----------------------------*/
    if(tabBrick[i][j].vie > 0){
        spawnBricks(i, j);
        showBricks(i, j);
        sprite(tabBrick[i][j].x, tabBrick[i][j].y, tabBrick[i][j].sourceIMG);

        updateSide_Bricks(i, j);

        rebondBalle_Bricks(i, j, k);
    }
    else if(tabBrick[i][j].rebond == 1){
        breakBricks(i, j);
        sprite(tabBrick[i][j].x, tabBrick[i][j].y, tabBrick[i][j].sourceIMG);
    }
    else{
        tabBrick[i][j].temps++;
        if(tabBrick[i][j].temps == 15 * FPS){
            tabBrick[i][j].spawn = 0;
            tabBrick[i][j].rebond = 0;
            tabBrick[i][j].frame = 0;

            init_vieBricks(i, j);
        }
    }

    if(tabBrick[i][j].frame == 12){
        tabBrick[i][j].frame = 0;
    }else{
        tabBrick[i][j].frame++;
    }
    /*---------------------------------------------------------------*/
}
/*-----------------------------------------------------------------------------*/

void drawResultat(int joueur){
    unit = tabResultat[joueur];
    nombreChiffres = 0;
    longueurResultat = 0;

    do{
        if     (unit % 10 == 0)     longueurResultat += tabChiffres[joueur][0].longueur + 5; //espace entre chiffre
        else if(unit % 10 == 1)     longueurResultat += tabChiffres[joueur][1].longueur + 5;
        else if(unit % 10 == 2)     longueurResultat += tabChiffres[joueur][2].longueur + 5;
        else if(unit % 10 == 3)     longueurResultat += tabChiffres[joueur][3].longueur + 5;
        else if(unit % 10 == 4)     longueurResultat += tabChiffres[joueur][4].longueur + 5;
        else if(unit % 10 == 5)     longueurResultat += tabChiffres[joueur][5].longueur + 5;
        else if(unit % 10 == 6)     longueurResultat += tabChiffres[joueur][6].longueur + 5;
        else if(unit % 10 == 7)     longueurResultat += tabChiffres[joueur][7].longueur + 5;
        else if(unit % 10 == 8)     longueurResultat += tabChiffres[joueur][8].longueur + 5;
        else if(unit % 10 == 9)     longueurResultat += tabChiffres[joueur][9].longueur + 5;

        unit = unit / 10;
    }
    while(unit > 0);

    unit = tabResultat[joueur];

    if      (joueur == 0)       positionResultat = (WINDOW_WIDTH - longueurResultat) / 2;
    else if (joueur == 1)       positionResultat = (WINDOW_WIDTH - longueurResultat) / 2;
    else if (joueur == 2)       positionResultat = (WINDOW_HEIGHT - longueurResultat) / 2;
    else if (joueur == 3){
        positionResultat = (WINDOW_HEIGHT - longueurResultat) / 2;
        longueurResultat = 0;
    }      

    do{
        if     (unit % 10 == 0){
            if      (joueur == 0){
                longueurResultat -= tabChiffres[joueur][0].longueur + 5;
                sprite(positionResultat + longueurResultat, WINDOW_HEIGHT - 120, "assets/img0J1.bmp");
            }
            else if (joueur == 1){
                longueurResultat -= tabChiffres[joueur][0].longueur + 5;
                sprite(positionResultat + longueurResultat, 120 - tabChiffres[joueur][0].largeur, "assets/img0J2.bmp");
            }
            else if (joueur == 2){
                longueurResultat -= tabChiffres[joueur][0].longueur + 5;
                sprite(120 - tabChiffres[joueur][0].largeur, positionResultat + longueurResultat, "assets/img0J3.bmp");
            }
            else if (joueur == 3){
                sprite(WINDOW_WIDTH - 120, positionResultat + longueurResultat, "assets/img0J4.bmp");
                longueurResultat += tabChiffres[joueur][0].longueur + 5;
            }
        }
        else if(unit % 10 == 1){
            if      (joueur == 0){
                longueurResultat -= tabChiffres[joueur][1].longueur + 5;
                sprite(positionResultat + longueurResultat, WINDOW_HEIGHT - 120, "assets/img1J1.bmp");
            }
            else if (joueur == 1){
                longueurResultat -= tabChiffres[joueur][1].longueur + 5;
                sprite(positionResultat + longueurResultat, 120 - tabChiffres[joueur][0].largeur, "assets/img1J2.bmp");
            }
            else if (joueur == 2){
                longueurResultat -= tabChiffres[joueur][1].longueur + 5;
                sprite(120 - tabChiffres[joueur][0].largeur, positionResultat + longueurResultat, "assets/img1J3.bmp");
            }
            else if (joueur == 3){
                sprite(WINDOW_WIDTH - 120, positionResultat + longueurResultat, "assets/img1J4.bmp");
                longueurResultat += tabChiffres[joueur][1].longueur + 5;
            }
        }
        else if(unit % 10 == 2){
            if      (joueur == 0){
                longueurResultat -= tabChiffres[joueur][2].longueur + 5;
                sprite(positionResultat + longueurResultat, WINDOW_HEIGHT - 120, "assets/img2J1.bmp");
            }
            else if (joueur == 1){
                longueurResultat -= tabChiffres[joueur][2].longueur + 5;
                sprite(positionResultat + longueurResultat, 120 - tabChiffres[joueur][0].largeur, "assets/img2J2.bmp");
            }
            else if (joueur == 2){
                longueurResultat -= tabChiffres[joueur][2].longueur + 5;
                sprite(120 - tabChiffres[joueur][0].largeur, positionResultat + longueurResultat, "assets/img2J3.bmp");
            }
            else if (joueur == 3){
                sprite(WINDOW_WIDTH - 120, positionResultat + longueurResultat, "assets/img2J4.bmp");
                longueurResultat += tabChiffres[joueur][2].longueur + 5;
            }
        }
        else if(unit % 10 == 3){
            if      (joueur == 0){
                longueurResultat -= tabChiffres[joueur][3].longueur + 5;
                sprite(positionResultat + longueurResultat, WINDOW_HEIGHT - 120, "assets/img3J1.bmp");
            }
            else if (joueur == 1){
                longueurResultat -= tabChiffres[joueur][3].longueur + 5;
                sprite(positionResultat + longueurResultat, 120 - tabChiffres[joueur][0].largeur, "assets/img3J2.bmp");
            }
            else if (joueur == 2){
                longueurResultat -= tabChiffres[joueur][3].longueur + 5;
                sprite(120 - tabChiffres[joueur][0].largeur, positionResultat + longueurResultat, "assets/img3J3.bmp");
            }
            else if (joueur == 3){
                sprite(WINDOW_WIDTH - 120, positionResultat + longueurResultat, "assets/img3J4.bmp");
                longueurResultat += tabChiffres[joueur][3].longueur + 5;
            }
        }
        else if(unit % 10 == 4){
            if      (joueur == 0){
                longueurResultat -= tabChiffres[joueur][4].longueur + 5;
                sprite(positionResultat + longueurResultat, WINDOW_HEIGHT - 120, "assets/img4J1.bmp");
            }
            else if (joueur == 1){
                longueurResultat -= tabChiffres[joueur][4].longueur + 5;
                sprite(positionResultat + longueurResultat, 120 - tabChiffres[joueur][0].largeur, "assets/img4J2.bmp");
            }
            else if (joueur == 2){
                longueurResultat -= tabChiffres[joueur][4].longueur + 5;
                sprite(120 - tabChiffres[joueur][0].largeur, positionResultat + longueurResultat, "assets/img4J3.bmp");
            }
            else if (joueur == 3){
                sprite(WINDOW_WIDTH - 120, positionResultat + longueurResultat, "assets/img4J4.bmp");
                longueurResultat += tabChiffres[joueur][4].longueur + 5;
            }
        }
        else if(unit % 10 == 5){
            if      (joueur == 0){
                longueurResultat -= tabChiffres[joueur][5].longueur + 5;
                sprite(positionResultat + longueurResultat, WINDOW_HEIGHT - 120, "assets/img5J1.bmp");
            }
            else if (joueur == 1){
                longueurResultat -= tabChiffres[joueur][5].longueur + 5;
                sprite(positionResultat + longueurResultat, 120 - tabChiffres[joueur][0].largeur, "assets/img5J2.bmp");
            }
            else if (joueur == 2){
                longueurResultat -= tabChiffres[joueur][5].longueur + 5;
                sprite(120 - tabChiffres[joueur][0].largeur, positionResultat + longueurResultat, "assets/img5J3.bmp");
            }
            else if (joueur == 3){
                sprite(WINDOW_WIDTH - 120, positionResultat + longueurResultat, "assets/img5J4.bmp");
                longueurResultat += tabChiffres[joueur][5].longueur + 5;
            }
        }
        else if(unit % 10 == 6){
            if      (joueur == 0){
                longueurResultat -= tabChiffres[joueur][6].longueur + 5;
                sprite(positionResultat + longueurResultat, WINDOW_HEIGHT - 120, "assets/img6J1.bmp");
            }
            else if (joueur == 1){
                longueurResultat -= tabChiffres[joueur][6].longueur + 5;
                sprite(positionResultat + longueurResultat, 120 - tabChiffres[joueur][0].largeur, "assets/img6J2.bmp");
            }
            else if (joueur == 2){
                longueurResultat -= tabChiffres[joueur][6].longueur + 5;
                sprite(120 - tabChiffres[joueur][0].largeur, positionResultat + longueurResultat, "assets/img6J3.bmp");
            }
            else if (joueur == 3){
                sprite(WINDOW_WIDTH - 120, positionResultat + longueurResultat, "assets/img6J4.bmp");
                longueurResultat += tabChiffres[joueur][6].longueur + 5;
            }
        }
        else if(unit % 10 == 7){
            if      (joueur == 0){
                longueurResultat -= tabChiffres[joueur][7].longueur + 5;
                sprite(positionResultat + longueurResultat, WINDOW_HEIGHT - 120, "assets/img7J1.bmp");
            }
            else if (joueur == 1){
                longueurResultat -= tabChiffres[joueur][7].longueur + 5;
                sprite(positionResultat + longueurResultat, 120 - tabChiffres[joueur][0].largeur, "assets/img7J2.bmp");
            }
            else if (joueur == 2){
                longueurResultat -= tabChiffres[joueur][7].longueur + 5;
                sprite(120 - tabChiffres[joueur][0].largeur, positionResultat + longueurResultat, "assets/img7J3.bmp");
            }
            else if (joueur == 3){
                sprite(WINDOW_WIDTH - 120, positionResultat + longueurResultat, "assets/img7J4.bmp");
                longueurResultat += tabChiffres[joueur][7].longueur + 5;
            }
        }
        else if(unit % 10 == 8){
            if      (joueur == 0){
                longueurResultat -= tabChiffres[joueur][8].longueur + 5;
                sprite(positionResultat + longueurResultat, WINDOW_HEIGHT - 120, "assets/img8J1.bmp");
            }
            else if (joueur == 1){
                longueurResultat -= tabChiffres[joueur][8].longueur + 5;
                sprite(positionResultat + longueurResultat, 120 - tabChiffres[joueur][0].largeur, "assets/img8J2.bmp");
            }
            else if (joueur == 2){
                longueurResultat -= tabChiffres[joueur][8].longueur + 5;
                sprite(120 - tabChiffres[joueur][0].largeur, positionResultat + longueurResultat, "assets/img8J3.bmp");
            }
            else if (joueur == 3){
                sprite(WINDOW_WIDTH - 120, positionResultat + longueurResultat, "assets/img8J4.bmp");
                longueurResultat += tabChiffres[joueur][8].longueur + 5;
            }
        }
        else if(unit % 10 == 9){
            if      (joueur == 0){
                longueurResultat -= tabChiffres[joueur][9].longueur + 5;
                sprite(positionResultat + longueurResultat, WINDOW_HEIGHT - 120, "assets/img9J1.bmp");
            }
            else if (joueur == 1){
                longueurResultat -= tabChiffres[joueur][9].longueur + 5;
                sprite(positionResultat + longueurResultat, 120 - tabChiffres[joueur][0].largeur, "assets/img9J2.bmp");
            }
            else if (joueur == 2){
                longueurResultat -= tabChiffres[joueur][9].longueur + 5;
                sprite(120 - tabChiffres[joueur][0].largeur, positionResultat + longueurResultat, "assets/img9J3.bmp");
            }
            else if (joueur == 3){
                sprite(WINDOW_WIDTH - 120, positionResultat + longueurResultat, "assets/img9J4.bmp");
                longueurResultat += tabChiffres[joueur][9].longueur + 5;
            }
        }
        
        unit = unit / 10;
    }
    while(unit > 0);    
}


void drawGame(){
    changeColor(0, 21, 36, 255);
    drawRect(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);

    //Separer les 'for' pour que l'ordre d'affichage soit dans le bon ordre
    for(int i = 0 ; i < nombreJoueur ; i++){
        drawResultat(i);
    }

    for(int i = 0 ; i < nombreJoueur ; i++){
        for(int j = 0 ; j < nombreJoueur; j++){
            drawBalle(i, j);
        }
    }

    for(int i = 0 ; i < nombreJoueur ; i++){
        for(int j = 0 ; j < nombreJoueur; j++){
            drawBarre(i, j);
        }
    }

    for(int i = 0 ; i < nombreLigne ; i++){
        for(int j = 0 ; j < nombreColonne ; j++){
            for(int k = 0 ; k < nombreJoueur ; k++){
                drawBricks(i,j,k);
            }
        }
    }

    actualize();
    usleep(1000000 / FPS); // 60 images par seconde | 1000000 = 1 seconde
}

void KeyPressed(SDL_Keycode touche){
    switch (touche) {
        /*---------------------Déplacement BARRE Joueur 1----------------------*/
        case SDLK_q:
            barres[0].vitesseX = -VITESSEBARRE;
            break;
        case SDLK_d:
            barres[0].vitesseX = VITESSEBARRE;
            break;
        /*---------------------------------------------------------------------*/
        /*---------------------Déplacement BARRE Joueur 2----------------------*/
        case SDLK_k:
            barres[1].vitesseX = -VITESSEBARRE;
            break;
        case SDLK_m:
            barres[1].vitesseX = VITESSEBARRE;
            break;
        /*---------------------------------------------------------------------*/
        /*---------------------Déplacement BARRE Joueur 3----------------------*/
        case SDLK_c:
            barres[2].vitesseY = -VITESSEBARRE;
            break;
        case SDLK_b:
            barres[2].vitesseY = VITESSEBARRE;
            break;
        /*---------------------------------------------------------------------*/
        /*---------------------Déplacement BARRE Joueur 4----------------------*/
        case SDLK_RIGHT:
            barres[3].vitesseY = -VITESSEBARRE;
            break;
        case SDLK_LEFT:
            barres[3].vitesseY = VITESSEBARRE;
            break;
        /*---------------------------------------------------------------------*/
        case SDLK_ESCAPE:
            freeAndTerminate();
            break;
        default:
            break;
    }
}

void gameLoop() {
    int programLaunched = 1;
    while (programLaunched == 1) {
        SDL_Event event;
        while (SDL_PollEvent(&event)) {
            switch (event.type) {
                case SDL_QUIT:
                    programLaunched = 0;
                    break;
                case SDL_MOUSEBUTTONUP:
                    printf("position de la souris x : %d , y : %d\n", event.motion.x, event.motion.y);
                    break;
                case SDL_KEYDOWN:
                    KeyPressed(event.key.keysym.sym);
                    break;
                case SDL_JOYBUTTONDOWN:
                    break;
                default:
                    break;
            }
        }
        drawGame();
    }
}

int main(){
    init(WINDOW_WIDTH, WINDOW_HEIGHT);
    init_game();
    gameLoop();
    printf("Fin du programme\n");
    freeAndTerminate();
}