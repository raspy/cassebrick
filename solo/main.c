/*---------------------------------Description---------------------------------*/
/*Nom projet: Brick Breaker             Edit: RaspY             Version: 0_2_8

Description Jeu : Jeu qui consiste à intercepter la balle grâce à une barre 
            restant en bas de l'écran, que l'on peut déplacer de droite à gauche. 
            Si la balle touche le bas de l'écran (en dessous de la barre) la 
            balle est détruite, réinitialiser au dessus de la barre et relancer 
            tant qu'il reste dans la vie. Cette dernière diminue à chaque fois 
            que la balle est détruite. 
            Le jeu s'arrête lorsqu'il n'y a plus de briques sur l'écran ou qu'il 
            ne reste plus de vie.

Description : Mise en place de bonus : dégât sur toute la ligne - colonne - plus
            de dégât pour la balle

Objectif :  ajout des bonus + fix

Atteint :   OUI
*/
/*-----------------------------------------------------------------------------*/

#include <stdio.h> 
#include <stdlib.h> 
#include <math.h>
#include <time.h> 


#include <SDL2/SDL.h>
#include <unistd.h>
#include "function.h"

#define WINDOW_WIDTH 500
#define WINDOW_HEIGHT 900
#define FPS 60

#define VITESSE_BALL 1
// #define VITESSE_BARR 2
#define NB_LIGNE_BRICK 10
#define NB_COLONNE_BRICK 6

#define MY_SCROLL_SPEED 20

int vitesseBarre;

/*----------------Initialisation des variables liées à la BALLE----------------*/
typedef struct Balle
{
    int rayon;
    int x;
    int y;
    int vitesseX;
    int vitesseY;

    int xDroite;
    int xGauche;
    int yHaut;
    int yBas;

    int degat;
    int vie;
}Balle;

Balle ball={10, WINDOW_WIDTH/2, WINDOW_HEIGHT-75, VITESSE_BALL, -VITESSE_BALL, 0, 0, 0, 0, 1, 2};
/*-----------------------------------------------------------------------------*/

/*----------------Initialisation des variables liées à la BARRE----------------*/
typedef struct Barre
{
    int longueur;
    int largeur;
    int vitesse;
    int x;
    int y;
}Barre;

Barre barr={75, 20, 0, 0, 0};
/*-----------------------------------------------------------------------------*/

/*-----------------Initialisation des variables liées au BRICK-----------------*/
typedef struct Bricks
{
    int x;
    int y;
    int vie;
    int spawn;

    int xContourBrickGauche;
    int xContourBrickDroite;
    int yContourBrickHaut;
    int yContourBrickBas;
}Bricks;

Bricks tabBricks[NB_LIGNE_BRICK][NB_COLONNE_BRICK];


int espaceEntreBrick;
int espaceXDebutBricks;
int espaceYDebutBricks;

int longueurBrick;
int largeurBrick;
int longueurBrickContour;
int largeurBrickContour;

int init_chargeBricks;
int chargementBrick;
int nbRestantBrick;
/*-----------------------------------------------------------------------------*/

/*-----------------Initialisation des variables liées au COINS-----------------*/
typedef struct Artefact
{
    int spawn;
    int x;
    int y;
    int type;    
}Artefact;

Artefact tabArtefact[NB_LIGNE_BRICK][NB_COLONNE_BRICK];

int vitesseCoins;
int rayonCoins;
int rayonContourCoins;

int vitesseBonus;
int rayonBonus;
int rayonContourBonus;

int pourcentageSpawnBonus;

int pourcentageSpawnCoin;

int money;
int valeurCoin;
int puissanceBonus;
/*-----------------------------------------------------------------------------*/

typedef struct Couleur
{
    int rouge;
    int vert;
    int bleu;
}Couleur;

Couleur tabCouleurPlacementSkin[8];
Couleur couleurBalle;
Couleur couleurTrainee;
Couleur couleurBarre;


int start; //0: Avant game ; 1: Game ; 2: Pause Game Balle Perdue

int chargementCoeur;
int niveau;
int pauseGame;
int pauseMenu;
int pageMenu;
int nombreLigne;
int nombreColonne;

int ecartBandeJeuY;

int unit;
int placementImgDoite;

int longueurImgChiffreNoir;
int largeurImgChiffreNoir;
int largeurImgCoinNoir;

int longueurImgChiffreRouge;
int largeurImgChiffreRouge;
int largeurImgCoinRouge;

int longueurImgChiffrePetit;
int largeurImgChiffrePetit;
int largeurImgCoinPetit;

int longueurImgVide;
int element;

int hoverNiveau1;
int hoverNiveau2;
int hoverNiveau3;
int hoverNiveau4;
int hoverNiveau5;

int ecartLogo;

int hoverLogoNiveau;
int hoverLogoUp;
int hoverLogoSkin;

int ecartCadreX;
int ecartCadreY;
int cadreUpJoueurH;
int cadreUpBalleH;
int cadreUpBarreH;
int cadreUpBonusH;
int cadreUpCoinH;

int ecartXEmplacementUP;
int ecartYEmplacementUP;

int longueurEmplacementUP;
int longueurEmplacementUPPrix;
int largeurEmplacementUP;
int espaceXEmplacementUP;
int espaceYEmplacementUP;

int coordXEmplacementUP;
int coordXEmplacementUPCoin;
int coordYEmplacementUP;

int coordYCadreUPJoueur;
int coordYCadreUPBalle;
int coordYCadreUPBarre;
int coordYCadreUPBonus;
int coordYCadreUPCoin;

int espaceCadre;

int varScroll;

int tabPrixUP[10];
int max[10];

int hoverUPEmplacementCoinJoueur;
int hoverUPEmplacementCoinBalle1;
int hoverUPEmplacementCoinBalle2;
int hoverUPEmplacementCoinBarre1;
int hoverUPEmplacementCoinBarre2;
int hoverUPEmplacementCoin1;
int hoverUPEmplacementCoin2;
int hoverUPEmplacementCoinBonus1;
int hoverUPEmplacementCoinBonus2;

int bonus1actif;
int bonus2actif;
int bonus3actif;

int coordYSkinCadreBalle;
int coordYSkinCadreBarre;
int coordYSkinCadreBrique;

int hoverCouleurSkinBalle;
int hoverCouleurSkinTrainee;
int hoverCouleurSkinBarre;

int hoverOUI;
int hoverNON;

int daltonien;

void resetBalleEtBarre(){
    ball.x = WINDOW_WIDTH/2; 
    ball.y = WINDOW_HEIGHT-ecartBandeJeuY;

    //Permet de faire relancer la balle vers le haut de l'écran
    if(ball.vitesseY > 0){
        ball.vitesseY = -ball.vitesseY;
    }

    barr.x = WINDOW_WIDTH/2 - barr.longueur/2;
    barr.y = WINDOW_HEIGHT - 2*barr.largeur;

    sprite(0, 500, "assets/tuto.bmp");
}

void afficherPrixUp(int coordY, int prix){
    element = 1;
    int prixInit = prix;

    longueurImgChiffrePetit = 19;
    largeurImgChiffrePetit = 20;
    largeurImgCoinPetit = 23;

    longueurImgVide = 1;

    largeurEmplacementUP = 83;

    placementImgDoite = WINDOW_WIDTH - (WINDOW_WIDTH - (coordXEmplacementUPCoin)) + (3 * largeurEmplacementUP / 4);


    if(prixInit<money){
        sprite((placementImgDoite), coordY + (largeurEmplacementUP / 2 - largeurImgChiffrePetit / 2), "assets/chiffre/imgOrangeCoin.bmp");
    }else{
        sprite((placementImgDoite), coordY + (largeurEmplacementUP / 2 - largeurImgChiffrePetit / 2), "assets/chiffre/imgGrisCoin.bmp");
    }
    

    do
    {
        switch (prix % 10)
        {
        case 0:
            if(prixInit<=money){
                sprite((placementImgDoite - element*(longueurImgChiffrePetit+longueurImgVide)), coordY + (largeurEmplacementUP / 2 - largeurImgChiffrePetit / 2), "assets/chiffre/imgOrange0.bmp");
            }else{
                sprite((placementImgDoite - element*(longueurImgChiffrePetit+longueurImgVide)), coordY + (largeurEmplacementUP / 2 - largeurImgChiffrePetit / 2), "assets/chiffre/imgGris0.bmp");
            }
            break;
        case 1:
            if(prixInit<=money){
                sprite((placementImgDoite - element*(longueurImgChiffrePetit+longueurImgVide)), coordY + (largeurEmplacementUP / 2 - largeurImgChiffrePetit / 2), "assets/chiffre/imgOrange1.bmp");
            }else{
                sprite((placementImgDoite - element*(longueurImgChiffrePetit+longueurImgVide)), coordY + (largeurEmplacementUP / 2 - largeurImgChiffrePetit / 2), "assets/chiffre/imgGris1.bmp");
            }
            break;
        case 2:
            if(prixInit<=money){
                sprite((placementImgDoite - element*(longueurImgChiffrePetit+longueurImgVide)), coordY + (largeurEmplacementUP / 2 - largeurImgChiffrePetit / 2), "assets/chiffre/imgOrange2.bmp");
            }else{
                sprite((placementImgDoite - element*(longueurImgChiffrePetit+longueurImgVide)), coordY + (largeurEmplacementUP / 2 - largeurImgChiffrePetit / 2), "assets/chiffre/imgGris2.bmp");
            }
            
            break;
        case 3:
            if(prixInit<=money){
                sprite((placementImgDoite - element*(longueurImgChiffrePetit+longueurImgVide)), coordY + (largeurEmplacementUP / 2 - largeurImgChiffrePetit / 2), "assets/chiffre/imgOrange3.bmp");
            }else{
                sprite((placementImgDoite - element*(longueurImgChiffrePetit+longueurImgVide)), coordY + (largeurEmplacementUP / 2 - largeurImgChiffrePetit / 2), "assets/chiffre/imgGris3.bmp");
            }
            
            break;
        case 4:
            if(prixInit<=money){
                sprite((placementImgDoite - element*(longueurImgChiffrePetit+longueurImgVide)), coordY + (largeurEmplacementUP / 2 - largeurImgChiffrePetit / 2), "assets/chiffre/imgOrange4.bmp");
            }else{
                sprite((placementImgDoite - element*(longueurImgChiffrePetit+longueurImgVide)), coordY + (largeurEmplacementUP / 2 - largeurImgChiffrePetit / 2), "assets/chiffre/imgGris4.bmp");
            }
            break;
        case 5:
            if(prixInit<=money){
                sprite((placementImgDoite - element*(longueurImgChiffrePetit+longueurImgVide)), coordY + (largeurEmplacementUP / 2 - largeurImgChiffrePetit / 2), "assets/chiffre/imgOrange5.bmp");
            }else{
                sprite((placementImgDoite - element*(longueurImgChiffrePetit+longueurImgVide)), coordY + (largeurEmplacementUP / 2 - largeurImgChiffrePetit / 2), "assets/chiffre/imgGris5.bmp");
            }
            break;
        case 6:
            if(prixInit<=money){
                sprite((placementImgDoite - element*(longueurImgChiffrePetit+longueurImgVide)), coordY + (largeurEmplacementUP / 2 - largeurImgChiffrePetit / 2), "assets/chiffre/imgOrange6.bmp");
            }else{
                sprite((placementImgDoite - element*(longueurImgChiffrePetit+longueurImgVide)), coordY + (largeurEmplacementUP / 2 - largeurImgChiffrePetit / 2), "assets/chiffre/imgGris6.bmp");
            }
            break;
        case 7:
            if(prixInit<=money){
                sprite((placementImgDoite - element*(longueurImgChiffrePetit+longueurImgVide)), coordY + (largeurEmplacementUP / 2 - largeurImgChiffrePetit / 2), "assets/chiffre/imgOrange7.bmp");
            }else{
                sprite((placementImgDoite - element*(longueurImgChiffrePetit+longueurImgVide)), coordY + (largeurEmplacementUP / 2 - largeurImgChiffrePetit / 2), "assets/chiffre/imgGris7.bmp");
            }
            break;
        case 8:
            if(prixInit<=money){
                sprite((placementImgDoite - element*(longueurImgChiffrePetit+longueurImgVide)), coordY + (largeurEmplacementUP / 2 - largeurImgChiffrePetit / 2), "assets/chiffre/imgOrange8.bmp");
            }else{
                sprite((placementImgDoite - element*(longueurImgChiffrePetit+longueurImgVide)), coordY + (largeurEmplacementUP / 2 - largeurImgChiffrePetit / 2), "assets/chiffre/imgGris8.bmp");
            }
            break;
        case 9:
            if(prixInit<=money){
                sprite((placementImgDoite - element*(longueurImgChiffrePetit+longueurImgVide)), coordY + (largeurEmplacementUP / 2 - largeurImgChiffrePetit / 2), "assets/chiffre/imgOrange9.bmp");
            }else{
                sprite((placementImgDoite - element*(longueurImgChiffrePetit+longueurImgVide)), coordY + (largeurEmplacementUP / 2 - largeurImgChiffrePetit / 2), "assets/chiffre/imgGris9.bmp");
            }
            break;
        default:
            break;
        }
        prix = prix / 10;
        element++;
    }while(prix>0);
}

void afficherBonus(int i, int j){
    if(tabArtefact[i][j].type == 1){
        if(((tabArtefact[i][j].spawn > 0) && (tabArtefact[i][j].spawn <= pourcentageSpawnBonus)) && (tabBricks[i][j].spawn == 1)){
            changeColor(12, 192, 223, 255);
            drawCircle((tabArtefact[i][j].x), (tabArtefact[i][j].y), rayonContourBonus);

            changeColor(70, 114, 159, 255);
            drawCircle((tabArtefact[i][j].x), (tabArtefact[i][j].y), rayonBonus);

            if((tabArtefact[i][j].y + rayonContourBonus + vitesseBonus) < WINDOW_HEIGHT){
                tabArtefact[i][j].y += vitesseBonus;
            }else{
                tabArtefact[i][j].spawn = 0;
            }

            if(((tabArtefact[i][j].y + rayonContourCoins) >= barr.y) && 
               ((tabArtefact[i][j].y - rayonContourCoins) < barr.y) &&
               ((tabArtefact[i][j].x - rayonContourCoins) >= barr.x) && 
               ((tabArtefact[i][j].x + rayonContourCoins) <= (barr.x + barr.longueur))
              ){
                tabArtefact[i][j].spawn = 0;
                bonus1actif = 1;
            }
        }
    }
    else if(tabArtefact[i][j].type == 2){
        if(((tabArtefact[i][j].spawn > 0) && (tabArtefact[i][j].spawn <= pourcentageSpawnBonus)) && (tabBricks[i][j].spawn == 1)){
            changeColor(62, 212, 140, 255);
            drawCircle((tabArtefact[i][j].x), (tabArtefact[i][j].y), rayonContourBonus);

            changeColor(73, 134, 119, 255);
            drawCircle((tabArtefact[i][j].x), (tabArtefact[i][j].y), rayonBonus);

            if((tabArtefact[i][j].y + rayonContourBonus + vitesseBonus) < WINDOW_HEIGHT){
                tabArtefact[i][j].y += vitesseBonus;
            }else{
                tabArtefact[i][j].spawn = 0;
            }

            if(((tabArtefact[i][j].y + rayonContourCoins) >= barr.y) && 
               ((tabArtefact[i][j].y - rayonContourCoins) < barr.y) &&
               ((tabArtefact[i][j].x - rayonContourCoins) >= barr.x) && 
               ((tabArtefact[i][j].x + rayonContourCoins) <= (barr.x + barr.longueur))
              ){
                tabArtefact[i][j].spawn = 0;
                bonus2actif = 1;
            }
        }
    }
    else if(tabArtefact[i][j].type == 3){
        if(((tabArtefact[i][j].spawn > 0) && (tabArtefact[i][j].spawn <= pourcentageSpawnBonus)) && (tabBricks[i][j].spawn == 1)){
            changeColor(239, 91, 116, 255);
            drawCircle((tabArtefact[i][j].x), (tabArtefact[i][j].y), rayonContourBonus);

            changeColor(175, 64, 64, 255);
            drawCircle((tabArtefact[i][j].x), (tabArtefact[i][j].y), rayonBonus);

            if((tabArtefact[i][j].y + rayonContourBonus + vitesseBonus) < WINDOW_HEIGHT){
                tabArtefact[i][j].y += vitesseBonus;
            }else{
                tabArtefact[i][j].spawn = 0;
            }

            if(((tabArtefact[i][j].y + rayonContourCoins) >= barr.y) && 
               ((tabArtefact[i][j].y - rayonContourCoins) < barr.y) &&
               ((tabArtefact[i][j].x - rayonContourCoins) >= barr.x) && 
               ((tabArtefact[i][j].x + rayonContourCoins) <= (barr.x + barr.longueur))
              ){
                tabArtefact[i][j].spawn = 0;
                bonus3actif = 1;
            }
        }
    }
}

void afficherCoins(int i, int j){
    if(((tabArtefact[i][j].spawn > 0) && (tabArtefact[i][j].spawn <= pourcentageSpawnCoin)) && (tabBricks[i][j].spawn == 1)){
        changeColor(255, 247, 56, 255);
        drawCircle((tabArtefact[i][j].x), (tabArtefact[i][j].y), rayonContourCoins);

        changeColor(166, 162, 61, 255);
        drawCircle((tabArtefact[i][j].x), (tabArtefact[i][j].y), rayonCoins);

        if(tabArtefact[i][j].y + rayonContourCoins + vitesseCoins < WINDOW_HEIGHT){
            tabArtefact[i][j].y += vitesseCoins;
        }else{
            tabArtefact[i][j].spawn = 0;
        }

        if(((tabArtefact[i][j].y + rayonContourCoins) >= barr.y) && 
           ((tabArtefact[i][j].y - rayonContourCoins) < barr.y) &&
           ((tabArtefact[i][j].x - rayonContourCoins) >= barr.x) && 
           ((tabArtefact[i][j].x + rayonContourCoins) <= barr.x + barr.longueur)
          ){
            tabArtefact[i][j].spawn = 0;
            money++;
        }
    }
}

void drawCoins(){
    unit = money;

    longueurImgChiffreNoir = 22;
    largeurImgChiffreNoir = 24;
    largeurImgCoinNoir = 21;

    longueurImgChiffreRouge = 31;
    largeurImgChiffreRouge = 34;
    largeurImgCoinRouge = 21;
    longueurImgVide = 8;

    placementImgDoite = WINDOW_WIDTH - 50 - longueurImgVide;

    element = 1;
    if(niveau != 0){
        sprite((placementImgDoite), (ecartBandeJeuY / 2 - largeurImgCoinNoir / 2), "assets/chiffre/imgNoirCoin.bmp");
    }else if(pageMenu == 1){
        sprite((placementImgDoite), (ecartCadreY / 2 - largeurImgCoinRouge / 2), "assets/chiffre/imgRougeCoin.bmp");
    }
    

    do
    {
        switch (unit % 10)
        {
        case 0:
            if(niveau != 0){
                sprite((placementImgDoite - element*(longueurImgChiffreNoir+longueurImgVide)), (ecartBandeJeuY / 2 - largeurImgChiffreNoir / 2), "assets/chiffre/imgNoir0.bmp");
            }else if(pageMenu == 1){
                sprite((placementImgDoite - element*(longueurImgChiffreRouge+longueurImgVide)), (ecartCadreY / 2 - largeurImgChiffreRouge / 2), "assets/chiffre/imgRouge0.bmp");
            }
            
            break;
        case 1:
            if(niveau != 0){
                sprite((placementImgDoite - element*(longueurImgChiffreNoir+longueurImgVide)), (ecartBandeJeuY / 2 - largeurImgChiffreNoir / 2), "assets/chiffre/imgNoir1.bmp");
            }else if(pageMenu == 1){
                sprite((placementImgDoite - element*(longueurImgChiffreRouge+longueurImgVide)), (ecartCadreY / 2 - largeurImgChiffreRouge / 2), "assets/chiffre/imgRouge1.bmp");
            }
            
            break;
        case 2:
            if(niveau != 0){
                sprite((placementImgDoite - element*(longueurImgChiffreNoir+longueurImgVide)), (ecartBandeJeuY / 2 - largeurImgChiffreNoir / 2), "assets/chiffre/imgNoir2.bmp");
            }else if(pageMenu == 1){
                sprite((placementImgDoite - element*(longueurImgChiffreRouge+longueurImgVide)), (ecartCadreY / 2 - largeurImgChiffreRouge / 2), "assets/chiffre/imgRouge2.bmp");
            }
            break;
        case 3:
            if(niveau != 0){
                sprite((placementImgDoite - element*(longueurImgChiffreNoir+longueurImgVide)), (ecartBandeJeuY / 2 - largeurImgChiffreNoir / 2), "assets/chiffre/imgNoir3.bmp");
            }else if(pageMenu == 1){
                sprite((placementImgDoite - element*(longueurImgChiffreRouge+longueurImgVide)), (ecartCadreY / 2 - largeurImgChiffreRouge / 2), "assets/chiffre/imgRouge3.bmp");
            }
            break;
        case 4:
            if(niveau != 0){
                sprite((placementImgDoite - element*(longueurImgChiffreNoir+longueurImgVide)), (ecartBandeJeuY / 2 - largeurImgChiffreNoir / 2), "assets/chiffre/imgNoir4.bmp");
            }else if(pageMenu == 1){
                sprite((placementImgDoite - element*(longueurImgChiffreRouge+longueurImgVide)), (ecartCadreY / 2 - largeurImgChiffreRouge / 2), "assets/chiffre/imgRouge4.bmp");
            }
            break;
        case 5:
            if(niveau != 0){
                sprite((placementImgDoite - element*(longueurImgChiffreNoir+longueurImgVide)), (ecartBandeJeuY / 2 - largeurImgChiffreNoir / 2), "assets/chiffre/imgNoir5.bmp");
            }else if(pageMenu == 1){
                sprite((placementImgDoite - element*(longueurImgChiffreRouge+longueurImgVide)), (ecartCadreY / 2 - largeurImgChiffreRouge / 2), "assets/chiffre/imgRouge5.bmp");
            }
            break;
        case 6:
            if(niveau != 0){
                sprite((placementImgDoite - element*(longueurImgChiffreNoir+longueurImgVide)), (ecartBandeJeuY / 2 - largeurImgChiffreNoir / 2), "assets/chiffre/imgNoir6.bmp");
            }else if(pageMenu == 1){
                sprite((placementImgDoite - element*(longueurImgChiffreRouge+longueurImgVide)), (ecartCadreY / 2 - largeurImgChiffreRouge / 2), "assets/chiffre/imgRouge6.bmp");
            }
            break;
        case 7:
            if(niveau != 0){
                sprite((placementImgDoite - element*(longueurImgChiffreNoir+longueurImgVide)), (ecartBandeJeuY / 2 - largeurImgChiffreNoir / 2), "assets/chiffre/imgNoir7.bmp");
            }else if(pageMenu == 1){
                sprite((placementImgDoite - element*(longueurImgChiffreRouge+longueurImgVide)), (ecartCadreY / 2 - largeurImgChiffreRouge / 2), "assets/chiffre/imgRouge7.bmp");
            }
            break;
        case 8:
            if(niveau != 0){
                sprite((placementImgDoite - element*(longueurImgChiffreNoir+longueurImgVide)), (ecartBandeJeuY / 2 - largeurImgChiffreNoir / 2), "assets/chiffre/imgNoir8.bmp");
            }else if(pageMenu == 1){
                sprite((placementImgDoite - element*(longueurImgChiffreRouge+longueurImgVide)), (ecartCadreY / 2 - largeurImgChiffreRouge / 2), "assets/chiffre/imgRouge8.bmp");
            }
            break;
        case 9:
            if(niveau != 0){
                sprite((placementImgDoite - element*(longueurImgChiffreNoir+longueurImgVide)), (ecartBandeJeuY / 2 - largeurImgChiffreNoir / 2), "assets/chiffre/imgNoir9.bmp");
            }else if(pageMenu == 1){
                sprite((placementImgDoite - element*(longueurImgChiffreRouge+longueurImgVide)), (ecartCadreY / 2 - largeurImgChiffreRouge / 2), "assets/chiffre/imgRouge9.bmp");
            }
            break;
        default:
            break;
        }
        unit = unit / 10;
        element++;
    }while(unit>0);
}

/*-----------------------------------COEURS------------------------------------*/
void drawCoeurs(){
    //---------------------------------AFFICHE---------------------------------
    for(int i=0 ; i<ball.vie ; i++){
        sprite(i*48,13,"assets/heart.bmp"); //i * largeurImg, coordonnée Y
    }
    //--------------------------PERTE DE VIE et PAUSE--------------------------
    if(ball.yBas >= WINDOW_HEIGHT){
        ball.vie--;
        resetBalleEtBarre();

        for(int i=0 ; i<nombreLigne ; i++){
            for(int j=0 ; j<nombreColonne ; j++){
                if((tabArtefact[i][j].spawn == 1) && (tabBricks[i][j].spawn == 1) && (tabBricks[i][j].vie == 0)){
                    tabBricks[i][j].spawn = 0;
                }
            }
        }
        start = 0;
    }
}
/*-----------------------------------------------------------------------------*/

/*-----------------------------------BRICKS------------------------------------*/
void initBricks(){
    if(niveau==1){
        for(int i=0 ; i<nombreLigne ; i++){
            for(int j=0 ; j<nombreColonne; j++){
                tabBricks[i][j].vie = 1;
                tabBricks[i][j].spawn = 2;
                nbRestantBrick++;
            }
        }
        chargementBrick = 0;
    }
    else if(niveau==2){
        for(int i=0 ; i<nombreLigne ; i++){
            for(int j=0 ; j<nombreColonne; j++){
                if(((i==2) || (i==3)) && ((j==2) || (j==3))){
                    tabBricks[i][j].vie = 1;
                    tabBricks[i][j].spawn = 2;
                    nbRestantBrick++;
                }
                else if(((i==4) || (i==5)) && ((j>=1) && (j<=4))){ //juste pour que se soit plus lisible
                    tabBricks[i][j].vie = 1;
                    tabBricks[i][j].spawn = 2;
                    nbRestantBrick++;
                }
                else{
                    tabBricks[i][j].vie = 2;
                    tabBricks[i][j].spawn = 2;
                    nbRestantBrick++;
                }
            }
        }
        chargementBrick = 0;
    }
    else if(niveau==3){
        for(int i=0 ; i<nombreLigne ; i++){
            for(int j=0 ; j<nombreColonne; j++){
                if(i<=1){
                    tabBricks[i][j].vie = 3;
                    tabBricks[i][j].spawn = 2;
                    nbRestantBrick++;
                }
                else if(((i>1) && (i<6)) || ((i==7) && (i<=9) && ((j<2) || (j>3)))){
                    tabBricks[i][j].vie = 2;
                    tabBricks[i][j].spawn = 2;
                    nbRestantBrick++;
                }
                else if(((i>7) && (i<=9)) && ((j<2) || (j>3))){
                    tabBricks[i][j].vie = 1;
                    tabBricks[i][j].spawn = 2;
                    nbRestantBrick++;
                }
                else{
                    tabBricks[i][j].vie = 0;
                    tabBricks[i][j].spawn = 0;
                }
            }
        }
        chargementBrick = 0;
    }
    else if(niveau==4){
        for(int i=0 ; i<nombreLigne ; i++){
            for(int j=0 ; j<nombreColonne; j++){
                if((i>=3) && (i<=6)){
                    if((((i==3) || (i==6)) && ((j<=1) || (j>=4))) ||
                       (((i>3) && (i<6)) && ((j>1) && (j<4)))
                      ){
                        tabBricks[i][j].vie = 4;
                        tabBricks[i][j].spawn = 2;
                        nbRestantBrick++;
                    }
                    else{
                        tabBricks[i][j].vie = 2;
                        tabBricks[i][j].spawn = 2;
                        nbRestantBrick++; 
                    }
                }
                else{
                    if((((i==2) || (i==7)) && ((j<=1) || (j>=4))) ||
                       (((i<2) || (i>7)) && ((j>1) && (j<4)))
                      ){
                        tabBricks[i][j].vie = 3;
                        tabBricks[i][j].spawn = 2;
                        nbRestantBrick++;
                    }
                    else{
                        tabBricks[i][j].vie = 1;
                        tabBricks[i][j].spawn = 2;
                        nbRestantBrick++;
                    }
                }
            }
        }
        chargementBrick = 0;
    }
    else if(niveau==5){
        for(int i=0 ; i<nombreLigne ; i++){
            for(int j=0 ; j<nombreColonne; j++){
                if(((i==0) && ((j==0) || (j==5))) ||
                   ((i==9) && ((j==0) || (j==5)))
                  ){
                    tabBricks[i][j].vie = 1;
                    tabBricks[i][j].spawn = 2;
                    nbRestantBrick++;
                }
                else if(((i>=4) && (i<=5) && (j>=2) && (j<=3))){
                    tabBricks[i][j].vie = 3;
                    tabBricks[i][j].spawn = 2;
                    nbRestantBrick++;
                }
                else if((((i==3) || (i==6)) && (j>=2) && (j<=3))){
                    tabBricks[i][j].vie = 4;
                    tabBricks[i][j].spawn = 2;
                    nbRestantBrick++;
                }
                else if((((i>=3) && (i<=6)) && ((j>3) || (j<2))) ||
                        (((i==2) || (i==7)) && (j>=1) && (j<=4)) ||
                        (((i==1) || (i==8)) && (j>=2) && (j<=3))
                ){
                    tabBricks[i][j].vie = 5;
                    tabBricks[i][j].spawn = 2;
                    nbRestantBrick++;
                }
                else{
                    tabBricks[i][j].vie = 6;
                    tabBricks[i][j].spawn = 2;
                    nbRestantBrick++;
                }
            }
        }
        chargementBrick = 0;
    }
}

void brickCasse(int i, int j){
    int cpt = 0;

    if(bonus1actif == 1){
        for(int k=0 ; k<nombreColonne ; k++){
            for(int l=puissanceBonus ; l>=0 ; l--){
                tabBricks[i + l][k].vie -= ball.degat;
                if(tabBricks[i + l][k].vie <= 0 && tabBricks[i + l][k].spawn == 2){
                    nbRestantBrick--;   
                    tabBricks[i + l][k].spawn--; 
                    cpt++;
                }

                tabBricks[i - l][k].vie -= ball.degat;
                if(tabBricks[i - l][k].vie <= 0 && tabBricks[i - l][k].spawn == 2){
                    nbRestantBrick--;   
                    tabBricks[i - l][k].spawn--; 
                    cpt++;
                }
            }
            bonus1actif = 0;
        }
    }else if(bonus2actif == 1){
        for(int k=0 ; k<nombreLigne ; k++){
            for(int l=puissanceBonus ; l>=0 ; l--){
                tabBricks[k][j + l].vie -= ball.degat;
                if(tabBricks[k][j + l].vie <= 0 && tabBricks[k][j + l].spawn == 2){
                    nbRestantBrick--;   
                    tabBricks[k][j + l].spawn--; 
                    cpt++;
                }

                tabBricks[k][j - l].vie -= ball.degat;
                if(tabBricks[k][j - l].vie <= 0 && tabBricks[k][j - l].spawn == 2){
                    nbRestantBrick--;   
                    tabBricks[k][j - l].spawn--; 
                    cpt++;
                }            
            }
            bonus2actif = 0;
        }
    }else if(bonus3actif == 1){
        tabBricks[i][j].vie -= ball.degat + puissanceBonus;
        if(tabBricks[i][j].vie <= 0 && tabBricks[i][j].spawn == 2){
            nbRestantBrick--;
            tabBricks[i][j].spawn--;   
            cpt++; 
        }
    }else{
        tabBricks[i][j].vie -= ball.degat;
        if(tabBricks[i][j].vie <= 0 && tabBricks[i][j].spawn == 2){
            nbRestantBrick--; 
            tabBricks[i][j].spawn--;   
            cpt++;
        }
    }
    // printf("%d\n", nbRestantBrick);
    // printf(" - %d\n", cpt);

    // for(int f=0 ; f<nombreLigne ; f++){
    //     for(int g=0 ; g<nombreColonne ; g++){
    //         printf("%d ",tabBricks[f][g].spawn);
    //     }
    //     printf("\n");
    // }
}

void rebonBalleBricks(int i, int j){
    /*---------------------------Rebond----------------------------*/
    //GAUCHE et DROITE
    if(((ball.vitesseX > 0) && (ball.x < tabBricks[i][j].xContourBrickGauche) && (ball.xDroite >= tabBricks[i][j].xContourBrickGauche)) && (ball.y >= tabBricks[i][j].yContourBrickHaut) && (ball.y <= tabBricks[i][j].yContourBrickBas) ||
       ((ball.vitesseX < 0) && (ball.x > tabBricks[i][j].xContourBrickDroite) && (ball.xGauche <= tabBricks[i][j].xContourBrickDroite)) && (ball.y >= tabBricks[i][j].yContourBrickHaut) && (ball.y <= tabBricks[i][j].yContourBrickBas)
      ){
        ball.vitesseX = -ball.vitesseX;
        brickCasse(i,j);
    }
    //BAS et HAUT
    else if(((ball.vitesseY < 0) && (ball.y > tabBricks[i][j].yContourBrickBas) && (ball.yHaut <= tabBricks[i][j].yContourBrickBas)) && (ball.x >= tabBricks[i][j].xContourBrickGauche) && (ball.x <= tabBricks[i][j].xContourBrickDroite) ||
            ((ball.vitesseY > 0) && (ball.y < tabBricks[i][j].yContourBrickHaut) && (ball.yBas >= tabBricks[i][j].yContourBrickHaut)) && (ball.x >= tabBricks[i][j].xContourBrickGauche) && (ball.x <= tabBricks[i][j].xContourBrickDroite)
           ){
        ball.vitesseY = -ball.vitesseY;
        brickCasse(i,j);
    }
    else{
        if((i>=0) && (i<=nombreLigne-1)){
            //COINS : HAUTGAUCHE / BASGAUCHE / HAUTDROITE / BASGAUCHE
            if((ball.vitesseX > 0) && (ball.vitesseY > 0) && (tabBricks[i-1][j].vie > 0) && (ball.x < tabBricks[i][j].xContourBrickGauche) && (ball.xDroite >= tabBricks[i][j].xContourBrickGauche) && (ball.y < tabBricks[i][j].yContourBrickHaut) && (ball.yBas > tabBricks[i][j].yContourBrickHaut) ||
               (ball.vitesseX > 0) && (ball.vitesseY < 0) && (tabBricks[i+1][j].vie > 0) && (ball.x < tabBricks[i][j].xContourBrickGauche) && (ball.xDroite >= tabBricks[i][j].xContourBrickGauche) && (ball.y > tabBricks[i][j].yContourBrickBas) && (ball.yHaut < tabBricks[i][j].yContourBrickBas) ||
               (ball.vitesseX < 0) && (ball.vitesseY > 0) && (tabBricks[i-1][j].vie > 0) && (ball.x > tabBricks[i][j].xContourBrickDroite) && (ball.xGauche <= tabBricks[i][j].xContourBrickDroite) && (ball.y < tabBricks[i][j].yContourBrickHaut) && (ball.yBas > tabBricks[i][j].yContourBrickHaut) ||
               (ball.vitesseX < 0) && (ball.vitesseY < 0) && (tabBricks[i+1][j].vie > 0) && (ball.x > tabBricks[i][j].xContourBrickDroite) && (ball.xGauche <= tabBricks[i][j].xContourBrickDroite) && (ball.y > tabBricks[i][j].yContourBrickBas) && (ball.yHaut < tabBricks[i][j].yContourBrickBas)
              ){
                ball.vitesseX = -ball.vitesseX;
                brickCasse(i,j);
            }
            else if((ball.vitesseX > 0) && (ball.vitesseY > 0) && (tabBricks[i-1][j].vie < 0) && (ball.x < tabBricks[i][j].xContourBrickGauche) && (ball.xDroite >= tabBricks[i][j].xContourBrickGauche) && (ball.y < tabBricks[i][j].yContourBrickHaut) && (ball.yBas > tabBricks[i][j].yContourBrickHaut) ||
                    (ball.vitesseX > 0) && (ball.vitesseY < 0) && (tabBricks[i+1][j].vie < 0) && (ball.x < tabBricks[i][j].xContourBrickGauche) && (ball.xDroite >= tabBricks[i][j].xContourBrickGauche) && (ball.y > tabBricks[i][j].yContourBrickBas) && (ball.yHaut < tabBricks[i][j].yContourBrickBas) ||
                    (ball.vitesseX < 0) && (ball.vitesseY > 0) && (tabBricks[i-1][j].vie < 0) && (ball.x > tabBricks[i][j].xContourBrickDroite) && (ball.xGauche <= tabBricks[i][j].xContourBrickDroite) && (ball.y < tabBricks[i][j].yContourBrickHaut) && (ball.yBas > tabBricks[i][j].yContourBrickHaut) ||
                    (ball.vitesseX < 0) && (ball.vitesseY < 0) && (tabBricks[i+1][j].vie < 0) && (ball.x > tabBricks[i][j].xContourBrickDroite) && (ball.xGauche <= tabBricks[i][j].xContourBrickDroite) && (ball.y > tabBricks[i][j].yContourBrickBas) && (ball.yHaut < tabBricks[i][j].yContourBrickBas)
                   ){
                ball.vitesseX = -ball.vitesseX;
                ball.vitesseY = -ball.vitesseY;
                brickCasse(i,j);
            }
        }

        if((j>=0) && (j<=nombreColonne-1)){
            //COINS : HAUTGAUCHE / BASGAUCHE / HAUTDROITE / BASGAUCHE
            if((ball.vitesseX > 0) && (ball.vitesseY > 0) && (tabBricks[i][j-1].vie > 0) && (ball.x < tabBricks[i][j].xContourBrickGauche) && (ball.xDroite >= tabBricks[i][j].xContourBrickGauche) && (ball.y < tabBricks[i][j].yContourBrickHaut) && (ball.yBas > tabBricks[i][j].yContourBrickHaut) ||
               (ball.vitesseX > 0) && (ball.vitesseY < 0) && (tabBricks[i][j-1].vie > 0) && (ball.x < tabBricks[i][j].xContourBrickGauche) && (ball.xDroite >= tabBricks[i][j].xContourBrickGauche) && (ball.y > tabBricks[i][j].yContourBrickBas) && (ball.yHaut < tabBricks[i][j].yContourBrickBas) ||
               (ball.vitesseX < 0) && (ball.vitesseY > 0) && (tabBricks[i][j+1].vie > 0) && (ball.x > tabBricks[i][j].xContourBrickDroite) && (ball.xDroite <= tabBricks[i][j].xContourBrickDroite) && (ball.y < tabBricks[i][j].yContourBrickHaut) && (ball.yBas > tabBricks[i][j].yContourBrickHaut) ||
               (ball.vitesseX < 0) && (ball.vitesseY < 0) && (tabBricks[i][j+1].vie > 0) && (ball.x > tabBricks[i][j].xContourBrickDroite) && (ball.xGauche <= tabBricks[i][j].xContourBrickDroite) && (ball.y > tabBricks[i][j].yContourBrickBas) && (ball.yHaut < tabBricks[i][j].yContourBrickBas)
              ){
                ball.vitesseY = -ball.vitesseY;
                brickCasse(i,j);
            }
            else if((ball.vitesseX > 0) && (ball.vitesseY > 0) && (tabBricks[i][j-1].vie < 0) && (ball.x < tabBricks[i][j].xContourBrickGauche) && (ball.xDroite >= tabBricks[i][j].xContourBrickGauche) && (ball.y < tabBricks[i][j].yContourBrickHaut) && (ball.yBas > tabBricks[i][j].yContourBrickHaut) ||
                    (ball.vitesseX > 0) && (ball.vitesseY < 0) && (tabBricks[i][j-1].vie < 0) && (ball.x < tabBricks[i][j].xContourBrickGauche) && (ball.xDroite >= tabBricks[i][j].xContourBrickGauche) && (ball.y > tabBricks[i][j].yContourBrickBas) && (ball.yHaut < tabBricks[i][j].yContourBrickBas) ||
                    (ball.vitesseX < 0) && (ball.vitesseY > 0) && (tabBricks[i][j+1].vie < 0) && (ball.x > tabBricks[i][j].xContourBrickDroite) && (ball.xDroite <= tabBricks[i][j].xContourBrickDroite) && (ball.y < tabBricks[i][j].yContourBrickHaut) && (ball.yBas > tabBricks[i][j].yContourBrickHaut) ||
                    (ball.vitesseX < 0) && (ball.vitesseY < 0) && (tabBricks[i][j+1].vie < 0) && (ball.x > tabBricks[i][j].xContourBrickDroite) && (ball.xGauche <= tabBricks[i][j].xContourBrickDroite) && (ball.y > tabBricks[i][j].yContourBrickBas) && (ball.yHaut < tabBricks[i][j].yContourBrickBas)
                   ){
                ball.vitesseX = -ball.vitesseX;
                ball.vitesseY = -ball.vitesseY;
                brickCasse(i,j);
            }
        }
        
        //securite
        if(((ball.vitesseX > 0) && (ball.vitesseY > 0) && (ball.x < tabBricks[i][j].xContourBrickGauche) && (ball.xDroite >= tabBricks[i][j].xContourBrickGauche) && (ball.y < tabBricks[i][j].yContourBrickHaut) && (ball.yBas > tabBricks[i][j].yContourBrickHaut)) ||
                    ((ball.vitesseX > 0) && (ball.vitesseY < 0) && (ball.x < tabBricks[i][j].xContourBrickGauche) && (ball.xDroite >= tabBricks[i][j].xContourBrickGauche) && (ball.y > tabBricks[i][j].yContourBrickBas) && (ball.yHaut < tabBricks[i][j].yContourBrickBas)) ||
                    ((ball.vitesseX < 0) && (ball.vitesseY > 0) && (ball.x > tabBricks[i][j].xContourBrickDroite) && (ball.xGauche <= tabBricks[i][j].xContourBrickDroite) && (ball.y < tabBricks[i][j].yContourBrickHaut) && (ball.yBas > tabBricks[i][j].yContourBrickHaut)) ||
                    ((ball.vitesseX < 0) && (ball.vitesseY < 0) && (ball.x > tabBricks[i][j].xContourBrickDroite) && (ball.xGauche <= tabBricks[i][j].xContourBrickDroite) && (ball.y > tabBricks[i][j].yContourBrickBas) && (ball.yHaut < tabBricks[i][j].yContourBrickBas))
                   ){
                ball.vitesseX = -ball.vitesseX;
                ball.vitesseY = -ball.vitesseY;
                brickCasse(i,j);
        }
    }
    /*-------------------------------------------------------------*/
}

void afficherContourBrick(int i, int j){
    drawRect(tabBricks[i][j].xContourBrickGauche, 
             tabBricks[i][j].yContourBrickHaut,
             longueurBrickContour,
             largeurBrickContour
            );
}

void afficherBrick(int i, int j){
    drawRect(tabBricks[i][j].xContourBrickGauche + 1, 
             tabBricks[i][j].yContourBrickHaut + 1,
             longueurBrick,
             largeurBrick
            );
}

void afficherBricks(int i, int j){
    //--------------------En fonction de la vie des briques--------------------
    switch (tabBricks[i][j].vie)
    {
        case 1:
            //contour Brick
            changeColor(92, 225, 230, 255);
            afficherContourBrick(i, j);

            //Brick
            changeColor(89,170,173,255);
            afficherBrick(i, j);
            if(daltonien == 1){                                                 // - 8 : moitié de la longueur de l'image du nombre à afficher
                sprite(tabBricks[i][j].xContourBrickGauche+(longueurBrickContour/2 - 8), tabBricks[i][j].yContourBrickHaut+5, "assets/img1BricksDalt.bmp");
            }
            break;
        case 2:
            //contour Brick
            changeColor(130, 188, 255, 255);
            afficherContourBrick(i, j);

            //Brick
            changeColor(89, 128, 173, 255);
            afficherBrick(i, j);
            if(daltonien == 1){                                                 // - 8 : moitié de la longueur de l'image du nombre à afficher
                sprite(tabBricks[i][j].xContourBrickGauche+(longueurBrickContour/2 - 8), tabBricks[i][j].yContourBrickHaut+5, "assets/img2BricksDalt.bmp");
            }
            break;
        case 3:
            //contour Brick
            changeColor(15, 127, 244, 255);
            afficherContourBrick(i, j);

            //Brick
            changeColor(73, 77, 184, 255);
            afficherBrick(i, j);
            if(daltonien == 1){                                                 // - 8 : moitié de la longueur de l'image du nombre à afficher
                sprite(tabBricks[i][j].xContourBrickGauche+(longueurBrickContour/2 - 8), tabBricks[i][j].yContourBrickHaut+5, "assets/img3BricksDalt.bmp");
            }
            break;
        case 4:
            //contour Brick
            changeColor(171, 126, 255, 255);
            afficherContourBrick(i, j);

            //Brick
            changeColor(118, 89, 173, 255);
            afficherBrick(i, j);
            if(daltonien == 1){                                                 // - 8 : moitié de la longueur de l'image du nombre à afficher
                sprite(tabBricks[i][j].xContourBrickGauche+(longueurBrickContour/2 - 8), tabBricks[i][j].yContourBrickHaut+5, "assets/img4BricksDalt.bmp");
            }
            break;
        case 5:
            //contour Brick
            changeColor(233, 122, 255, 255);
            afficherContourBrick(i, j);

            //Brick
            changeColor(159, 89, 173, 255);
            afficherBrick(i, j);
            if(daltonien == 1){                                                 // - 8 : moitié de la longueur de l'image du nombre à afficher
                sprite(tabBricks[i][j].xContourBrickGauche+(longueurBrickContour/2 - 8), tabBricks[i][j].yContourBrickHaut+5, "assets/img5BricksDalt.bmp");
            }
            break;
        case 6:
            //contour Brick
            changeColor(248, 122, 189, 255);
            afficherContourBrick(i, j);

            //Brick
            changeColor(173, 89, 134, 255);
            afficherBrick(i, j);
            if(daltonien == 1){                                                 // - 8 : moitié de la longueur de l'image du nombre à afficher
                sprite(tabBricks[i][j].xContourBrickGauche+(longueurBrickContour/2 - 8), tabBricks[i][j].yContourBrickHaut+5, "assets/img6BricksDalt.bmp");
            }
            break;
        default:
            break;
    }
}

void drawBricks(){
    for(int i=0 ; i<nombreLigne ; i++){
        for(int j=0 ; j<nombreColonne; j++){
            if(tabBricks[i][j].vie>0){
                afficherBricks(i, j);

                rebonBalleBricks(i, j);                
            }
            else{
                if(tabArtefact[i][j].type == 0){
                    afficherCoins(i, j);
                }
                else{
                    afficherBonus(i,j);
                }
            }
        }
    }
}
/*-----------------------------------------------------------------------------*/

/*------------------------------------BARRE------------------------------------*/
void afficheBarre(){
    changeColor(couleurBarre.rouge, couleurBarre.vert, couleurBarre.bleu, 255);
    drawRect(barr.x, barr.y, barr.longueur, barr.largeur);
}

void drawBarre(){
    //---------------------------------VITESSE---------------------------------
    barr.x += barr.vitesse;

    /*------------------------LIMITE DEPLACEMENT BORDURE-----------------------*/
    if(barr.x < 0){
        barr.x = 0;
        barr.vitesse = 0;
    }else if((barr.x + barr.longueur) > WINDOW_WIDTH){
        barr.x = WINDOW_WIDTH - barr.longueur;
        barr.vitesse = 0;
    }
    /*-------------------------------------------------------------------------*/

    afficheBarre();
}
/*-----------------------------------------------------------------------------*/

/*------------------------------------BALLE------------------------------------*/
void updateCoordBalle(){
    ball.xDroite = ball.x + ball.rayon;
    ball.xGauche = ball.x - ball.rayon;
    ball.yHaut = ball.y - ball.rayon;
    ball.yBas = ball.y + ball.rayon;
}

void rebondBalle(){
    /*----------------------------Bords de l'écran-----------------------------*/
    if(((ball.vitesseX < 0) && (ball.xGauche<=0)) || ((ball.vitesseX > 0) && (ball.xDroite>=WINDOW_WIDTH))){
        ball.vitesseX = -ball.vitesseX;
    }
    else if((ball.vitesseY < 0) && (ball.yHaut<=ecartBandeJeuY)){
        ball.vitesseY = -ball.vitesseY;
    }
    /*-------------------------------------------------------------------------*/
    /*----------------------------------Barre----------------------------------*/
    if((((ball.vitesseX < 0) && (ball.x >= barr.x+barr.longueur) && (ball.xGauche <= barr.x+barr.longueur)) && (ball.y >= barr.y) && (ball.y <= barr.y+barr.largeur)) ||
       (((ball.vitesseX > 0) && (ball.x < barr.x) && (ball.xDroite >= barr.x)) && (ball.y >= barr.y) && (ball.y <= barr.y+barr.largeur))
      ){
        ball.vitesseX = -ball.vitesseX;
    }
    else if((ball.vitesseY > 0) && ((ball.y < barr.y) && (ball.yBas >= barr.y)) && (ball.x >= barr.x) && (ball.x <= barr.x+barr.longueur)){
        ball.vitesseY = -ball.vitesseY;
    }
    else if(((ball.vitesseX > 0) && (ball.vitesseY > 0) && (ball.x <= barr.x) && (ball.xDroite >= barr.x) && (ball.y <= barr.y) && (ball.yBas >= barr.y)) ||
            ((ball.vitesseX < 0) && (ball.vitesseY > 0) && (ball.x >= barr.x+barr.longueur) && (ball.xGauche <= barr.x+barr.longueur) && (ball.y <= barr.y) && (ball.yBas >= barr.y))
           ){
        ball.vitesseX = -ball.vitesseX;
        ball.vitesseY = -ball.vitesseY;
    }
    /*-------------------------------------------------------------------------*/
    /*----------------------------------Bricks---------------------------------*/
    //Dans drawBrick() à cause du tabBrick. Pas de solution sans tout casser
    /*-------------------------------------------------------------------------*/
}

void afficherBalle(){
    //TRAINEE
    if(start==1){
        for(int i=ball.rayon ; i>0 ; i--){
            if(((ball.xGauche - (ball.vitesseX * i)) > 0) && ((ball.xDroite - (ball.vitesseX * i)) < WINDOW_WIDTH) && ((ball.yHaut - (ball.vitesseY * i)) > ecartBandeJeuY)){
                changeColor(couleurTrainee.rouge, couleurTrainee.vert, couleurTrainee.bleu, (255 / i));
                drawCircle((ball.x - (ball.vitesseX * i)), (ball.y - (ball.vitesseY * i)), (ball.rayon - i));
            }
        }
    }
    //-------
    changeColor(couleurBalle.rouge, couleurBalle.vert, couleurBalle.bleu, 255);
    drawCircle(ball.x, ball.y, ball.rayon);
}

void drawBalle(){
    updateCoordBalle();

    rebondBalle();

    //VITESSE
    ball.x += ball.vitesseX;
    ball.y += ball.vitesseY;

    afficherBalle();
}
/*-----------------------------------------------------------------------------*/

void resetVarGame(){
    /*----------------------Initialisation BALLE et BARRE----------------------*/
    resetBalleEtBarre();    
    /*-------------------------------------------------------------------------*/

    /*--------------------------Initialisation COEUR---------------------------*/
    chargementCoeur = 1;
    /*-------------------------------------------------------------------------*/
    
    /*--------------------------Initialisation BRICKS--------------------------*/
    init_chargeBricks = 0;

    chargementBrick = 1;
    nbRestantBrick = 0;
    /*-------------------------------------------------------------------------*/

    /*--------------------------Initialisation BONUS---------------------------*/
    // for(int i=0 ; i<NB_LIGNE_BRICK ; i++){
    //     for(int j=0 ; j<NB_COLONNE_BRICK ; j++){
    //         // tabArtefact[i][j].spawn = rand() % 2;
    //         tabArtefact[i][j].spawn = rand() % 101;

    //         // tabArtefact[i][j].type = rand() % 4;
    //         tabArtefact[i][j].type = 1;

    //         tabArtefact[i][j].x = tabBricks[i][j].xContourBrickGauche + longueurBrickContour/2;
    //         tabArtefact[i][j].y = tabBricks[i][j].yContourBrickHaut + largeurBrickContour/2;
    //     }
    // }
    /*-------------------------------------------------------------------------*/

    /*---------------------------Initialisation GAME---------------------------*/
    start = 0;
    
    niveau = 0;
    pageMenu = 0;
    pauseGame = -1;
    pauseMenu = -1;
    /*-------------------------------------------------------------------------*/
}

void background(){
    clear();
    changeColor(115, 115, 115, 255);
    drawRect(0, ecartBandeJeuY, WINDOW_WIDTH, WINDOW_HEIGHT-ecartBandeJeuY);
}

void game(){
    background();

    drawCoeurs();
    drawCoins();

    drawBalle();
    drawBarre();
    drawBricks();

    //Arrêt du jeu et renvoie au menu
    if((ball.vie == 0) || (nbRestantBrick == 0)){
        clear();
        start--;
        niveau = 0;
        pageMenu = 0;
    }
}

void afficherAvantGame(){
    if(chargementBrick==1){
        initBricks();
    }

    background();

    resetBalleEtBarre();

    drawCoeurs();
    drawCoins();

    afficherBalle();        
    afficheBarre();   
    drawBricks();

    sprite(0, 500, "assets/tuto.bmp");

    actualize();
}

/*-----------------------------------NIVEAUX-----------------------------------*/
void lancerNiveau(){
    if(init_chargeBricks==0){
        chargementBrick = 1;
        init_chargeBricks++;
    }

    if((start==0) && (pauseGame<0)){
        afficherAvantGame();
    }
    if(start==1){
        game();
    }
}
//-----------------------------------NIVEAU1-----------------------------------
void niveau1(){
    nombreLigne=6;
    nombreColonne=6;

    lancerNiveau();
}
//-----------------------------------NIVEAU2-----------------------------------
void niveau2(){
    nombreLigne=6;
    nombreColonne=6;

    lancerNiveau();
}
//-----------------------------------NIVEAU3-----------------------------------
void niveau3(){
    nombreLigne=10;
    nombreColonne=6;

    lancerNiveau();
}
//-----------------------------------NIVEAU4-----------------------------------
void niveau4(){
    nombreLigne=10;
    nombreColonne=6;

    lancerNiveau();
}
//-----------------------------------NIVEAU5-----------------------------------
void niveau5(){
    nombreLigne=10;
    nombreColonne=6;

    lancerNiveau();
}
/*-----------------------------------------------------------------------------*/

void menuChoixNiveau(){
    //RESET VARIABLE avant chaque début de niveau
    resetVarGame();

    changeColor(115, 115, 115, 255);
    drawRect(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);

    if(hoverNiveau1==1){
        sprite(72, 177, "assets/niveau/imgGrandN1.bmp");
    }else{
        sprite(87,180,"assets/niveau/imgPetitN1.bmp");
    }

    if(hoverNiveau2==1){
        sprite(72, 267, "assets/niveau/imgGrandN2.bmp");
    }else{
        sprite(87,270,"assets/niveau/imgPetitN2.bmp");
    }

    if(hoverNiveau3==1){
        sprite(72, 357, "assets/niveau/imgGrandN3.bmp");
    }else{
        sprite(87,360,"assets/niveau/imgPetitN3.bmp");
    }

    if(hoverNiveau4==1){
        sprite(72, 452, "assets/niveau/imgGrandN4.bmp");
    }else{
        sprite(87,455,"assets/niveau/imgPetitN4.bmp");
    }

    if(hoverNiveau5==1){
        sprite(72, 542, "assets/niveau/imgGrandN5.bmp");
    }else{
        sprite(87,545,"assets/niveau/imgPetitN5.bmp");
    }

/*-------------------------------Menu bas écran--------------------------------*/
    ecartLogo = (WINDOW_WIDTH - (3*90 + 2*19))/2; //90: largeurIMG ; 19: espace entre IMG

    if(hoverLogoNiveau == 1){
        sprite(ecartLogo, WINDOW_HEIGHT - 121, "assets/logoNiveauNiveau.bmp");
    }else{
        sprite(ecartLogo, WINDOW_HEIGHT - 101, "assets/logoNiveauNiveau.bmp");
    }
    if(hoverLogoUp== 1){
        sprite(ecartLogo + 109, WINDOW_HEIGHT - 121, "assets/logoNiveauUp.bmp"); //109 = largeurIMG + espace entre IMG
    }else{
        sprite(ecartLogo + 109, WINDOW_HEIGHT - 101, "assets/logoNiveauUp.bmp");
    }
    if(hoverLogoSkin == 1){
        sprite(ecartLogo + 2 * 109, WINDOW_HEIGHT - 121, "assets/logoNiveauSkin.bmp");
    }else{
        sprite(ecartLogo + 2 * 109, WINDOW_HEIGHT - 101, "assets/logoNiveauSkin.bmp");
    }
/*-----------------------------------------------------------------------------*/
}

void menuChoixUp(){
    coordXEmplacementUP = ecartCadreX + ecartXEmplacementUP;
    coordXEmplacementUPCoin = coordXEmplacementUP + longueurEmplacementUP + espaceXEmplacementUP;

    coordYEmplacementUP = ecartYEmplacementUP + largeurEmplacementUP + espaceYEmplacementUP;

    coordYCadreUPJoueur = ecartCadreY + varScroll;
    coordYCadreUPBalle = coordYCadreUPJoueur + cadreUpJoueurH + espaceCadre;
    coordYCadreUPBarre = coordYCadreUPBalle + cadreUpBalleH + espaceCadre;
    coordYCadreUPCoin = coordYCadreUPBarre + cadreUpBarreH + espaceCadre;
    coordYCadreUPBonus = coordYCadreUPCoin + cadreUpCoinH + espaceCadre;

    changeColor(71, 2, 14, 255);
    drawRect(0, ecartCadreY, WINDOW_WIDTH, WINDOW_HEIGHT - ecartCadreY);

/*------------------------------Partie SCROLLING-------------------------------*/
    //--------------------------------Cadre JOUEUR-------------------------------
    sprite(ecartCadreX, coordYCadreUPJoueur, "assets/cadre/cadreUpJoueur.bmp");
    if(tabPrixUP[0]<=money){
        if(hoverUPEmplacementCoinJoueur == 1){
            changeColor(255,255,60,255);
            drawRect(coordXEmplacementUPCoin - 2, coordYCadreUPJoueur + ecartYEmplacementUP - 2, longueurEmplacementUPPrix+4, largeurEmplacementUP+4);
        }
        sprite(coordXEmplacementUP, coordYCadreUPJoueur + ecartYEmplacementUP, "assets/description/emplacementUpOrangeCoeur.bmp");
        sprite(coordXEmplacementUPCoin, coordYCadreUPJoueur + ecartYEmplacementUP, "assets/description/emplacementUpOrangeCoin.bmp");
        afficherPrixUp(coordYCadreUPJoueur + ecartYEmplacementUP, tabPrixUP[0]);
    }else{
        sprite(coordXEmplacementUP, coordYCadreUPJoueur + ecartYEmplacementUP, "assets/description/emplacementUpCoeur.bmp");
        sprite(coordXEmplacementUPCoin, coordYCadreUPJoueur + ecartYEmplacementUP, "assets/description/emplacementUpCoin.bmp");
        afficherPrixUp(coordYCadreUPJoueur + ecartYEmplacementUP, tabPrixUP[0]);
    }
    if(max[0] == 1){
        sprite(coordXEmplacementUPCoin, coordYCadreUPJoueur + ecartYEmplacementUP, "assets/imgMAX.bmp");
    }
    //---------------------------------------------------------------------------
    
    //--------------------------------Cadre BALLE--------------------------------
    sprite(ecartCadreX, coordYCadreUPBalle, "assets/cadre/cadreUpBalle.bmp");
    if(tabPrixUP[1]<=money){
        if(hoverUPEmplacementCoinBalle1 == 1){
            changeColor(255,255,60,255);
            drawRect(coordXEmplacementUPCoin - 2, coordYCadreUPBalle + ecartYEmplacementUP - 2, longueurEmplacementUPPrix+4, largeurEmplacementUP+4);
        }
        sprite(coordXEmplacementUP, coordYCadreUPBalle + ecartYEmplacementUP, "assets/description/emplacementUpOrangeVitesseBalle.bmp");
        sprite(coordXEmplacementUPCoin, coordYCadreUPBalle + ecartYEmplacementUP, "assets/description/emplacementUpOrangeCoin.bmp");
        afficherPrixUp(coordYCadreUPBalle + ecartYEmplacementUP, tabPrixUP[1]);
    }else{
        sprite(coordXEmplacementUP, coordYCadreUPBalle + ecartYEmplacementUP, "assets/description/emplacementUpVitesseBalle.bmp");
        sprite(coordXEmplacementUPCoin, coordYCadreUPBalle + ecartYEmplacementUP, "assets/description/emplacementUpCoin.bmp");
        afficherPrixUp(coordYCadreUPBalle + ecartYEmplacementUP, tabPrixUP[1]);
    }
    if(max[1] == 1){
        sprite(coordXEmplacementUPCoin, coordYCadreUPBalle + ecartYEmplacementUP, "assets/imgMAX.bmp");
    }

    if(tabPrixUP[2]<=money){
        if(hoverUPEmplacementCoinBalle2 == 1){
            changeColor(255,255,60,255);
            drawRect(coordXEmplacementUPCoin - 2, coordYCadreUPBalle + coordYEmplacementUP - 2, longueurEmplacementUPPrix+4, largeurEmplacementUP+4);
        }
        sprite(coordXEmplacementUP, coordYCadreUPBalle + coordYEmplacementUP, "assets/description/emplacementUpOrangeDegatBalle.bmp");
        sprite(coordXEmplacementUPCoin, coordYCadreUPBalle + coordYEmplacementUP, "assets/description/emplacementUpOrangeCoin.bmp");
        afficherPrixUp(coordYCadreUPBalle + coordYEmplacementUP, tabPrixUP[2]);  
    }else{
        sprite(coordXEmplacementUP, coordYCadreUPBalle + coordYEmplacementUP, "assets/description/emplacementUpDegatBalle.bmp");
        sprite(coordXEmplacementUPCoin, coordYCadreUPBalle + coordYEmplacementUP, "assets/description/emplacementUpCoin.bmp");
        afficherPrixUp(coordYCadreUPBalle + coordYEmplacementUP, tabPrixUP[2]);  
    }
    if(max[2] == 1){
        sprite(coordXEmplacementUPCoin, coordYCadreUPBalle + coordYEmplacementUP, "assets/imgMAX.bmp");
    }
    //---------------------------------------------------------------------------

    //--------------------------------Cadre BARRE--------------------------------
    sprite(ecartCadreX, coordYCadreUPBarre, "assets/cadre/cadreUpBarre.bmp");
    if(tabPrixUP[3]<=money){
        if(hoverUPEmplacementCoinBarre1 == 1){
            changeColor(255,255,60,255);
            drawRect(coordXEmplacementUPCoin - 2, coordYCadreUPBarre + ecartYEmplacementUP - 2, longueurEmplacementUPPrix+4, largeurEmplacementUP+4);
        }
        sprite(coordXEmplacementUP, coordYCadreUPBarre + ecartYEmplacementUP, "assets/description/emplacementUpOrangeVitesseBarre.bmp");
        sprite(coordXEmplacementUPCoin, coordYCadreUPBarre + ecartYEmplacementUP, "assets/description/emplacementUpOrangeCoin.bmp");
        afficherPrixUp(coordYCadreUPBarre + ecartYEmplacementUP, tabPrixUP[3]);
    }else{
        sprite(coordXEmplacementUP, coordYCadreUPBarre + ecartYEmplacementUP, "assets/description/emplacementUpVitesseBarre.bmp");
        sprite(coordXEmplacementUPCoin, coordYCadreUPBarre + ecartYEmplacementUP, "assets/description/emplacementUpCoin.bmp");
        afficherPrixUp(coordYCadreUPBarre + ecartYEmplacementUP, tabPrixUP[3]);
    }
    if(max[3] == 1){
        sprite(coordXEmplacementUPCoin, coordYCadreUPBarre + ecartYEmplacementUP, "assets/imgMAX.bmp");
    }

    if(tabPrixUP[4]<=money){
        if(hoverUPEmplacementCoinBarre2 == 1){
            changeColor(255,255,60,255);
            drawRect(coordXEmplacementUPCoin - 2, coordYCadreUPBarre + coordYEmplacementUP - 2, longueurEmplacementUPPrix+4, largeurEmplacementUP+4);
        }
        sprite(coordXEmplacementUP, coordYCadreUPBarre + coordYEmplacementUP, "assets/description/emplacementUpOrangeLongBarre.bmp");
        sprite(coordXEmplacementUPCoin, coordYCadreUPBarre + coordYEmplacementUP, "assets/description/emplacementUpOrangeCoin.bmp");
        afficherPrixUp(coordYCadreUPBarre + coordYEmplacementUP, tabPrixUP[4]);
    }else{
        sprite(coordXEmplacementUP, coordYCadreUPBarre + coordYEmplacementUP, "assets/description/emplacementUpLongBarre.bmp");
        sprite(coordXEmplacementUPCoin, coordYCadreUPBarre + coordYEmplacementUP, "assets/description/emplacementUpCoin.bmp");
        afficherPrixUp(coordYCadreUPBarre + coordYEmplacementUP, tabPrixUP[4]);
    }
    if(max[4] == 1){
        sprite(coordXEmplacementUPCoin, coordYCadreUPBarre + coordYEmplacementUP, "assets/imgMAX.bmp");
    }
    //---------------------------------------------------------------------------

    //--------------------------------Cadre COIN---------------------------------
    sprite(ecartCadreX, coordYCadreUPCoin, "assets/cadre/cadreUpCoin.bmp");
    if(tabPrixUP[5]<=money){
        if(hoverUPEmplacementCoin1 == 1){
            changeColor(255,255,60,255);
            drawRect(coordXEmplacementUPCoin - 2, coordYCadreUPCoin + ecartYEmplacementUP - 2, longueurEmplacementUPPrix+4, largeurEmplacementUP+4);
        }
        sprite(coordXEmplacementUP, coordYCadreUPCoin + ecartYEmplacementUP, "assets/description/emplacementUpOrangeTauxCoin.bmp");
        sprite(coordXEmplacementUPCoin, coordYCadreUPCoin + ecartYEmplacementUP, "assets/description/emplacementUpOrangeCoin.bmp");
        afficherPrixUp(coordYCadreUPCoin + ecartYEmplacementUP, tabPrixUP[5]);
    }else{
        sprite(coordXEmplacementUP, coordYCadreUPCoin + ecartYEmplacementUP, "assets/description/emplacementUpTauxCoin.bmp");
        sprite(coordXEmplacementUPCoin, coordYCadreUPCoin + ecartYEmplacementUP, "assets/description/emplacementUpCoin.bmp");
        afficherPrixUp(coordYCadreUPCoin + ecartYEmplacementUP, tabPrixUP[5]);
    }
    if(max[5] == 1){
        sprite(coordXEmplacementUPCoin, coordYCadreUPCoin + ecartYEmplacementUP, "assets/imgMAX.bmp");
    }

    if(tabPrixUP[6]<=money){
        if(hoverUPEmplacementCoin2 == 1){
            changeColor(255,255,60,255);
            drawRect(coordXEmplacementUPCoin - 2, coordYCadreUPCoin + coordYEmplacementUP - 2, longueurEmplacementUPPrix+4, largeurEmplacementUP+4);
        }
        sprite(coordXEmplacementUP, coordYCadreUPCoin + coordYEmplacementUP, "assets/description/emplacementUpOrangeValeurCoin.bmp");
        sprite(coordXEmplacementUPCoin, coordYCadreUPCoin + coordYEmplacementUP, "assets/description/emplacementUpOrangeCoin.bmp");
        afficherPrixUp(coordYCadreUPCoin + coordYEmplacementUP, tabPrixUP[6]);

    }else{
        sprite(coordXEmplacementUP, coordYCadreUPCoin + coordYEmplacementUP, "assets/description/emplacementUpValeurCoin.bmp");
        sprite(coordXEmplacementUPCoin, coordYCadreUPCoin + coordYEmplacementUP, "assets/description/emplacementUpCoin.bmp");
        afficherPrixUp(coordYCadreUPCoin + coordYEmplacementUP, tabPrixUP[6]);
    }
    if(max[6] == 1){
        sprite(coordXEmplacementUPCoin, coordYCadreUPCoin + coordYEmplacementUP, "assets/imgMAX.bmp");
    }
    //---------------------------------------------------------------------------

    //--------------------------------Cadre BONUS--------------------------------
    sprite(ecartCadreX, coordYCadreUPBonus, "assets/cadre/cadreUpBonus.bmp");
    if(tabPrixUP[7]<=money){
        if(hoverUPEmplacementCoinBonus1 == 1){
            changeColor(255,255,60,255);
            drawRect(coordXEmplacementUPCoin - 2, coordYCadreUPBonus + ecartYEmplacementUP - 2, longueurEmplacementUPPrix+4, largeurEmplacementUP+4);
        }
        sprite(coordXEmplacementUP, coordYCadreUPBonus + ecartYEmplacementUP, "assets/description/emplacementUpOrangeTauxCoin.bmp");
        sprite(coordXEmplacementUPCoin, coordYCadreUPBonus + ecartYEmplacementUP, "assets/description/emplacementUpOrangeCoin.bmp");
        afficherPrixUp(coordYCadreUPBonus + ecartYEmplacementUP, tabPrixUP[7]);
    }else{
        sprite(coordXEmplacementUP, coordYCadreUPBonus + ecartYEmplacementUP, "assets/description/emplacementUpTauxCoin.bmp");
        sprite(coordXEmplacementUPCoin, coordYCadreUPBonus + ecartYEmplacementUP, "assets/description/emplacementUpCoin.bmp");
        afficherPrixUp(coordYCadreUPBonus + ecartYEmplacementUP, tabPrixUP[7]);
    }
    if(max[7] == 1){
        sprite(coordXEmplacementUPCoin, coordYCadreUPBonus + ecartYEmplacementUP, "assets/imgMAX.bmp");
    }

    if(tabPrixUP[8]<=money){
        if(hoverUPEmplacementCoinBonus2 == 1){
            changeColor(255,255,60,255);
            drawRect(coordXEmplacementUPCoin - 2, coordYCadreUPBonus + coordYEmplacementUP - 2, longueurEmplacementUPPrix+4, largeurEmplacementUP+4);
        }
        sprite(coordXEmplacementUP, coordYCadreUPBonus + coordYEmplacementUP, "assets/description/emplacementUpOrangePowerBonus.bmp");
        sprite(coordXEmplacementUPCoin, coordYCadreUPBonus + coordYEmplacementUP, "assets/description/emplacementUpOrangeCoin.bmp");
        afficherPrixUp(coordYCadreUPBonus + coordYEmplacementUP, tabPrixUP[8]);
    }else{
        sprite(coordXEmplacementUP, coordYCadreUPBonus + coordYEmplacementUP, "assets/description/emplacementUpPowerBonus.bmp");
        sprite(coordXEmplacementUPCoin, coordYCadreUPBonus + coordYEmplacementUP, "assets/description/emplacementUpCoin.bmp");
        afficherPrixUp(coordYCadreUPBonus + coordYEmplacementUP, tabPrixUP[8]);
    }
    if(max[8] == 1){
        sprite(coordXEmplacementUPCoin, coordYCadreUPBonus + coordYEmplacementUP, "assets/imgMAX.bmp");
    }
    //---------------------------------------------------------------------------
/*-----------------------------------------------------------------------------*/

/*-----------------------------Affiche COIN Total------------------------------*/
    changeColor(71, 2, 14, 255);
    drawRect(0, 0, WINDOW_WIDTH, ecartCadreY);

    drawCoins();
/*-----------------------------------------------------------------------------*/
/*-------------------------------Menu bas écran--------------------------------*/
    changeColor(71, 2, 14, 255);
    drawRect(0, WINDOW_HEIGHT - 125, WINDOW_WIDTH, 125);

    ecartLogo = (WINDOW_WIDTH - (3*90 + 2*19))/2;

    if(hoverLogoNiveau == 1){
        sprite(ecartLogo, WINDOW_HEIGHT - 121, "assets/logoUpNiveau.bmp");
    }else{
        sprite(ecartLogo, WINDOW_HEIGHT - 101, "assets/logoUpNiveau.bmp");
    }
    if(hoverLogoUp== 1){
        sprite(ecartLogo + 109, WINDOW_HEIGHT - 121, "assets/logoUpUp.bmp");
    }else{
        sprite(ecartLogo + 109, WINDOW_HEIGHT - 101, "assets/logoUpUp.bmp");
    }
    if(hoverLogoSkin == 1){
        sprite(ecartLogo + 2 * 109, WINDOW_HEIGHT - 121, "assets/logoUpSkin.bmp");
    }else{
        sprite(ecartLogo + 2 * 109, WINDOW_HEIGHT - 101, "assets/logoUpSkin.bmp");
    }
/*-----------------------------------------------------------------------------*/
}

void menuChoixSkin(){
    changeColor(2, 71, 54, 255);
    drawRect(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);

    coordYSkinCadreBalle = 70;
    coordYSkinCadreBarre = coordYSkinCadreBalle + 320 + espaceYEmplacementUP; //320 = hauteur du cadre BALLE    70 + 320 + 9
    coordYSkinCadreBrique = coordYSkinCadreBarre + 237 + espaceYEmplacementUP; //237 = hauteur du cadre BARRE   70 + 320 + 9 + 237 + 9
    
    //Cadre BALLE
    sprite(ecartCadreX, coordYSkinCadreBalle, "assets/cadre/cadreSkinBalle.bmp");
    for(int i=0 ; i<8 ; i++){
        if(hoverCouleurSkinBalle==i){
            changeColor(255, 0, 0, 255);
        }else{
            changeColor(255, 255, 255, 255);
        }
        drawCircle(WINDOW_WIDTH/2 - 7*25 + i*50, coordYSkinCadreBalle + 95, 17);
        changeColor(tabCouleurPlacementSkin[i].rouge, tabCouleurPlacementSkin[i].vert, tabCouleurPlacementSkin[i].bleu, 255);
        drawCircle(WINDOW_WIDTH/2 - 7*25 + i*50, coordYSkinCadreBalle + 95, 15);
    }
    for(int i=0 ; i<8 ; i++){
        if(hoverCouleurSkinTrainee==i){
            changeColor(255, 0, 0, 255);
        }else{
            changeColor(255, 255, 255, 255);
        }
        drawCircle(WINDOW_WIDTH/2 - 7*25 + i*50, coordYSkinCadreBalle + 95 + 85, 17);
        changeColor(tabCouleurPlacementSkin[i].rouge, tabCouleurPlacementSkin[i].vert, tabCouleurPlacementSkin[i].bleu, 255);
        drawCircle(WINDOW_WIDTH/2 - 7*25 + i*50, coordYSkinCadreBalle + 95 + 85, 15);
    }
    //-----------
    //Cadre BARRE
    sprite(ecartCadreX, coordYSkinCadreBarre, "assets/cadre/cadreSkinBarre.bmp");
    for(int i=0 ; i<8 ; i++){
        if(hoverCouleurSkinBarre==i){
            changeColor(255, 0, 0, 255);
        }else{
            changeColor(255, 255, 255, 255);
        }
        drawCircle(WINDOW_WIDTH/2 - 7*25 + i*50, coordYSkinCadreBarre + 90, 17);
        changeColor(tabCouleurPlacementSkin[i].rouge, tabCouleurPlacementSkin[i].vert, tabCouleurPlacementSkin[i].bleu, 255);
        drawCircle(WINDOW_WIDTH/2 - 7*25 + i*50, coordYSkinCadreBarre + 90, 15);
    }
    //-----------

    //Cadre BRIQUE
    sprite(ecartCadreX, coordYSkinCadreBrique, "assets/cadre/cadreSkinBrique.bmp");

    if(hoverOUI==1){
        changeColor(255,0,0,255);
        drawRect(ecartCadreX + 47 - 2, coordYSkinCadreBrique + 70 - 2, 46, 25);
    }
    sprite(ecartCadreX + 47, coordYSkinCadreBrique + 70, "assets/cadre/cadreSkinBriqueOUI.bmp");
    
    if(hoverNON==1){
        changeColor(255,0,0,255);
        drawRect(ecartCadreX + 47 + 55 - 2, coordYSkinCadreBrique + 70 - 2, 46, 25);
    }
    sprite(ecartCadreX + 47 + 55, coordYSkinCadreBrique + 70, "assets/cadre/cadreSkinBriqueNON.bmp");
    
    //-----------

/*-------------------------------Menu bas écran--------------------------------*/
    ecartLogo = (WINDOW_WIDTH - (3*90 + 2*19))/2;

    if(hoverLogoNiveau == 1){
        sprite(ecartLogo, WINDOW_HEIGHT - 121, "assets/logoSkinNiveau.bmp");
    }else{
        sprite(ecartLogo, WINDOW_HEIGHT - 101, "assets/logoSkinNiveau.bmp");
    }
    if(hoverLogoUp == 1){
        sprite(ecartLogo + 109, WINDOW_HEIGHT - 121, "assets/logoSkinUp.bmp");
    }else{
        sprite(ecartLogo + 109, WINDOW_HEIGHT - 101, "assets/logoSkinUp.bmp");
    }
    if(hoverLogoSkin == 1){
        sprite(ecartLogo + 2 * 109, WINDOW_HEIGHT - 121, "assets/logoSkinSkin.bmp");
    }else{
        sprite(ecartLogo + 2 * 109, WINDOW_HEIGHT - 101, "assets/logoSkinSkin.bmp");
    }
/*-----------------------------------------------------------------------------*/
}

void menuChoix(){
    switch (pageMenu)
    {
        case 0:
            menuChoixNiveau();
            break;
        case 1:
            menuChoixUp();
            break;
        case 2:
            menuChoixSkin();
            break;
        default:
            break;
    }
}

void init_game(){
    ecartBandeJeuY = 75;
    vitesseBarre = 2;
    daltonien = 0;

    /*----------------------Initialisation BALLE et BARRE----------------------*/
    couleurBalle.rouge = 255;
    couleurBalle.vert = 255;
    couleurBalle.bleu = 255;

    couleurTrainee.rouge = 255;
    couleurTrainee.vert = 255;
    couleurTrainee.bleu = 255;

    couleurBarre.rouge = 255;
    couleurBarre.vert = 255;
    couleurBarre.bleu = 255;

    resetBalleEtBarre();    
    /*-------------------------------------------------------------------------*/

    /*--------------------------Initialisation COEURS--------------------------*/
    chargementCoeur = 1;
    /*-------------------------------------------------------------------------*/

    /*--------------------------Initialisation BRICKS--------------------------*/
    espaceEntreBrick = 3;

    longueurBrick = 60;
    largeurBrick = 30;
    longueurBrickContour = longueurBrick + 2;
    largeurBrickContour = largeurBrick + 2;
    espaceXDebutBricks = (WINDOW_WIDTH - ((NB_COLONNE_BRICK)*longueurBrickContour + (NB_COLONNE_BRICK-1)*espaceEntreBrick))/2;
    espaceYDebutBricks = ecartBandeJeuY +49;

    init_chargeBricks = 0;

    chargementBrick = 1;
    nbRestantBrick = 0;

    for(int i=0 ; i<NB_LIGNE_BRICK ; i++){
        for(int j=0 ; j<NB_COLONNE_BRICK ; j++){
            tabBricks[i][j].vie = 0;
            tabBricks[i][j].spawn = 0;

            tabBricks[i][j].xContourBrickGauche = espaceXDebutBricks + j*(longueurBrickContour+espaceEntreBrick);
            tabBricks[i][j].xContourBrickDroite = tabBricks[i][j].xContourBrickGauche + longueurBrickContour;
            tabBricks[i][j].yContourBrickHaut = espaceYDebutBricks + i*(largeurBrickContour+espaceEntreBrick);
            tabBricks[i][j].yContourBrickBas = tabBricks[i][j].yContourBrickHaut + largeurBrickContour;
        }
    }
    /*-------------------------------------------------------------------------*/

    /*--------------------------Initialisation COINS---------------------------*/
    srand(time(NULL));

    pourcentageSpawnCoin = 40;
    pourcentageSpawnBonus = 20;

    puissanceBonus = 0;

    bonus1actif = 0;
    bonus2actif = 0;
    bonus3actif = 0;

    money = 60000;

    vitesseCoins = 3;
    rayonCoins = 9;
    rayonContourCoins = rayonCoins + 1;

    vitesseBonus = 8;
    rayonBonus = 11;
    rayonContourBonus = rayonBonus + 1;

    for(int i=0 ; i<NB_LIGNE_BRICK ; i++){
        for(int j=0 ; j<NB_COLONNE_BRICK ; j++){
            // tabArtefact[i][j].spawn = rand() % 2;
            tabArtefact[i][j].spawn = rand() % 101;

            tabArtefact[i][j].type = rand() % 4;
            // tabArtefact[i][j].type = 1;

            tabArtefact[i][j].x = tabBricks[i][j].xContourBrickGauche + longueurBrickContour/2;
            tabArtefact[i][j].y = tabBricks[i][j].yContourBrickHaut + largeurBrickContour/2;
        }
    }
    //Prix
    tabPrixUP[0] = 2; //Nombre de coeur
    tabPrixUP[1] = 5; //Vitesse de la balle
    tabPrixUP[2] = 10; //Dégât de la balle
    tabPrixUP[3] = 5; //Vitesse de la barre
    tabPrixUP[4] = 7; //Longueur de la barre
    tabPrixUP[5] = 2; //Taux d'apparition
    tabPrixUP[6] = 2; //Valeur des coins
    tabPrixUP[7] = 2; //Taux d'apparition
    tabPrixUP[8] = 2; //Puissance des bonus
    tabPrixUP[9];
    //----
    for(int i=0 ; i < 10 ; i++){
        max[i] = 0;
    }
    /*-------------------------------------------------------------------------*/

    /*--------------------------Initialisation MENU UP-------------------------*/
    ecartCadreX = 17;
    ecartCadreY = 113;
    cadreUpJoueurH = 161;
    cadreUpBalleH = 252;
    cadreUpBarreH = 251;
    cadreUpBonusH = 251;
    cadreUpCoinH = 249;

    espaceCadre = 24;

    varScroll = 0;

    ecartXEmplacementUP = 17;
    ecartYEmplacementUP = 40;

    longueurEmplacementUP = 324;
    longueurEmplacementUPPrix = 90;
    largeurEmplacementUP = 83;
    espaceXEmplacementUP = 12;
    espaceYEmplacementUP = 9;
    /*-------------------------------------------------------------------------*/

    /*--------------------------Initialisation MENU UP-------------------------*/
    tabCouleurPlacementSkin[0].rouge = 255;
    tabCouleurPlacementSkin[0].vert = 255;
    tabCouleurPlacementSkin[0].bleu = 255;

    tabCouleurPlacementSkin[1].rouge = 255;
    tabCouleurPlacementSkin[1].vert = 49;
    tabCouleurPlacementSkin[1].bleu = 49;

    tabCouleurPlacementSkin[2].rouge = 255;
    tabCouleurPlacementSkin[2].vert = 145;
    tabCouleurPlacementSkin[2].bleu = 77;

    tabCouleurPlacementSkin[3].rouge = 255;
    tabCouleurPlacementSkin[3].vert = 222;
    tabCouleurPlacementSkin[3].bleu = 89;
    
    tabCouleurPlacementSkin[4].rouge = 0;
    tabCouleurPlacementSkin[4].vert = 191;
    tabCouleurPlacementSkin[4].bleu = 99;

    tabCouleurPlacementSkin[5].rouge = 0;
    tabCouleurPlacementSkin[5].vert = 151;
    tabCouleurPlacementSkin[5].bleu = 178;

    tabCouleurPlacementSkin[6].rouge = 0;
    tabCouleurPlacementSkin[6].vert = 74;
    tabCouleurPlacementSkin[6].bleu = 173;

    tabCouleurPlacementSkin[7].rouge = 0;
    tabCouleurPlacementSkin[7].vert = 23;
    tabCouleurPlacementSkin[7].bleu = 235;
    /*-------------------------------------------------------------------------*/
    start = 0;
    
    niveau = 0;
    pageMenu = 0;
    pauseGame = -1;
    pauseMenu = -1;
}

void drawGame(){
    switch (niveau)
    {
        case 0:
            if(pauseMenu<0){
                menuChoix();
            }
            break;
        case 1:
            niveau1();
            break;
        case 2:
            niveau2();
            break; 
        case 3:
            niveau3();
            break; 
        case 4:
            niveau4();
            break;
        case 5:
            niveau5();
            break;
        default:
            break;
    }
    actualize();
    usleep(1000000 / FPS); // 60 images par seconde | 1000000 = 1 seconde
}

void MouseButton(int mouseX, int mouseY){
    printf("position de la souris x : %d , y : %d\n", mouseX, mouseY);

    if((niveau==0) && (pauseMenu<0)){
        if(((mouseX - ecartLogo - 44) * (mouseX - ecartLogo - 44) +  (mouseY - 799 - 44) * (mouseY - 799 - 44)) <= 44 * 44){
            pageMenu = 0;
        }else if(((mouseX - ecartLogo - 109 - 44) * (mouseX - ecartLogo - 109 - 44) +  (mouseY - 799 - 44) * (mouseY - 799 - 44)) <= 44 * 44){
            pageMenu = 1;
            varScroll = 0;
        }else if(((mouseX - ecartLogo - 2 * 109 - 44) * (mouseX - ecartLogo - 2 * 109 - 44) +  (mouseY - 799 - 44) * (mouseY - 799 - 44)) <= 44 * 44){
            pageMenu = 2;
            varScroll = 0;
        }else if(((mouseX - ecartLogo - 3 * 109 - 44) * (mouseX - ecartLogo - 3 * 109 - 44) +  (mouseY - 799 - 44) * (mouseY - 799 - 44)) <= 44 * 44){
            pageMenu = 3;
        }
    }

    //Sélection du niveau UNIQUEMENT lorsque l'on est dans le menu
    if((pageMenu==0) && (pauseMenu<0)){
        if(((mouseX >= 87) && (mouseX <= 412)) && ((mouseY >= 180) && (mouseY <= 240))){
            niveau = 1;
        }

        else if(((mouseX >= 87) && (mouseX <= 412)) && ((mouseY >= 270) && (mouseY <= 330))){
            niveau = 2;
        }

        else if(((mouseX >= 87) && (mouseX <= 412)) && ((mouseY >= 360) && (mouseY <= 420))){
            niveau = 3;
        }

        else if(((mouseX >= 87) && (mouseX <= 412)) && ((mouseY >= 455) && (mouseY <= 515))){
            niveau = 4;
        }

        else if(((mouseX >= 87) && (mouseX <= 412)) && ((mouseY >= 545) && (mouseY <= 605))){
            niveau = 5;
        }
    }

    //PAGE UP
    if((pageMenu==1) && (pauseMenu<0)){
        if((tabPrixUP[0] < money) && (mouseY > coordYCadreUPJoueur + ecartYEmplacementUP) && (mouseY < coordYCadreUPJoueur + ecartYEmplacementUP + largeurEmplacementUP) && (mouseX > coordXEmplacementUPCoin) && (mouseX < coordXEmplacementUPCoin + 90)){
            if(ball.vie < 5){
                ball.vie++;
                money -= tabPrixUP[0];
                tabPrixUP[0] = tabPrixUP[0] + tabPrixUP[0] * 0.5;
            }else{
                max[0] = 1;
            }
        }
        else if((tabPrixUP[1] < money) && (mouseY > coordYCadreUPBalle + ecartYEmplacementUP) && (mouseY < coordYCadreUPBalle + ecartYEmplacementUP + largeurEmplacementUP) && (mouseX > coordXEmplacementUPCoin) && (mouseX < coordXEmplacementUPCoin + 90)){
            if(ball.vitesseX < 5){
                ball.vitesseX++;
                ball.vitesseY++;
                money -= tabPrixUP[1];
                tabPrixUP[1] = tabPrixUP[1] + tabPrixUP[1] * 0.5;
            }else{
                max[1] = 1;
            }
        }
        else if((tabPrixUP[2] < money) && (mouseY > coordYCadreUPBalle + coordYEmplacementUP) && (mouseY < coordYCadreUPBalle + coordYEmplacementUP + largeurEmplacementUP) && (mouseX > coordXEmplacementUPCoin) && (mouseX < coordXEmplacementUPCoin + 90)){
            if(ball.degat < 6){
                ball.degat++;
                money -= tabPrixUP[2];
                tabPrixUP[2] = tabPrixUP[2] + tabPrixUP[2] * 0.5; 
            }else{
                max[2] = 1;
            }
            
        }
        else if((tabPrixUP[3] < money) && (mouseY > coordYCadreUPBarre + ecartYEmplacementUP) && (mouseY < coordYCadreUPBarre + ecartYEmplacementUP + largeurEmplacementUP) && (mouseX > coordXEmplacementUPCoin) && (mouseX < coordXEmplacementUPCoin + 90)){
            if(barr.vitesse < 7){
                barr.vitesse++;
                vitesseBarre++;
                money -= tabPrixUP[3];
                tabPrixUP[3] = tabPrixUP[3] + tabPrixUP[3] * 0.5;
            }else{
                max[3] = 1;
            }
        }
        else if((tabPrixUP[4] < money) && (mouseY > coordYCadreUPBarre + coordYEmplacementUP) && (mouseY < coordYCadreUPBarre + coordYEmplacementUP + largeurEmplacementUP) && (mouseX > coordXEmplacementUPCoin) && (mouseX < coordXEmplacementUPCoin + 90)){
            if(barr.longueur < 150){
                barr.longueur += 5;
                money -= tabPrixUP[4];
                tabPrixUP[4] = tabPrixUP[4] + tabPrixUP[4] * 0.5;
            }else{
                max[4] = 1;
            }
        }
        else if((tabPrixUP[5] < money) && (mouseY > coordYCadreUPCoin + ecartYEmplacementUP) && (mouseY < coordYCadreUPCoin + ecartYEmplacementUP + largeurEmplacementUP) && (mouseX > coordXEmplacementUPCoin) && (mouseX < coordXEmplacementUPCoin + 90)){
            if(pourcentageSpawnCoin < 100){
                pourcentageSpawnCoin += 10;
                money -= tabPrixUP[5];
                tabPrixUP[5] = tabPrixUP[5] + tabPrixUP[5] * 0.5;
            }else{
                max[5] = 1;
            }
        }
        else if((tabPrixUP[6] < money) && (mouseY > coordYCadreUPCoin + coordYEmplacementUP) && (mouseY < coordYCadreUPCoin + coordYEmplacementUP + largeurEmplacementUP) && (mouseX > coordXEmplacementUPCoin) && (mouseX < coordXEmplacementUPCoin + 90)){
            for(int i=0 ; i<nombreLigne ; i++){
                for(int j=0 ; j<nombreColonne ; j++){
                    valeurCoin++;
                }
            }
            money -= tabPrixUP[6];
            tabPrixUP[6] = tabPrixUP[6] + tabPrixUP[6] * 0.5;
        }
        else if((mouseY > coordYCadreUPBonus + ecartYEmplacementUP) && (mouseY < coordYCadreUPBonus + ecartYEmplacementUP + largeurEmplacementUP) && (mouseX > coordXEmplacementUPCoin) && (mouseX < coordXEmplacementUPCoin + 90)){
            if(pourcentageSpawnBonus < 70){
                pourcentageSpawnBonus += 5;
                money -= tabPrixUP[7];
                tabPrixUP[7] = tabPrixUP[7] + tabPrixUP[7] * 0.5;
            }else{
                max[7] = 1;
            }
        }
        else if((mouseY > coordYCadreUPBonus + coordYEmplacementUP) && (mouseY < coordYCadreUPBonus + coordYEmplacementUP + largeurEmplacementUP) && (mouseX > coordXEmplacementUPCoin) && (mouseX < coordXEmplacementUPCoin + 90)){
            if(puissanceBonus < 3){
                puissanceBonus++;
                money -= tabPrixUP[8];
                tabPrixUP[8] = tabPrixUP[8] + tabPrixUP[8] * 0.5;
            }else{
                max[8] = 1;
            }
        }    
    }

    //PAGE SKIN
    if((pageMenu==2) && (pauseMenu<0)){
        for(int i=0 ; i<8 ; i++){
            if((mouseX - (WINDOW_WIDTH/2 - 7*25 + i*50)) * (mouseX - (WINDOW_WIDTH/2 - 7*25 + i*50)) + (mouseY - (coordYSkinCadreBalle + 95)) * (mouseY - (coordYSkinCadreBalle + 95)) <= 17 * 17){
                couleurBalle.rouge = tabCouleurPlacementSkin[i].rouge;
                couleurBalle.vert = tabCouleurPlacementSkin[i].vert;
                couleurBalle.bleu = tabCouleurPlacementSkin[i].bleu;
            }
            else if((mouseX - (WINDOW_WIDTH/2 - 7*25 + i*50)) * (mouseX - (WINDOW_WIDTH/2 - 7*25 + i*50)) + (mouseY - (coordYSkinCadreBalle + 95 + 85)) * (mouseY - (coordYSkinCadreBalle + 95 + 85)) <= 17 * 17){
                couleurTrainee.rouge = tabCouleurPlacementSkin[i].rouge;
                couleurTrainee.vert = tabCouleurPlacementSkin[i].vert;
                couleurTrainee.bleu = tabCouleurPlacementSkin[i].bleu;
            }
            else if((mouseX - (WINDOW_WIDTH/2 - 7*25 + i*50)) * (mouseX - (WINDOW_WIDTH/2 - 7*25 + i*50)) + (mouseY - (coordYSkinCadreBarre + 90)) * (mouseY - (coordYSkinCadreBarre + 90)) <= 17 * 17){
                couleurBarre.rouge = tabCouleurPlacementSkin[i].rouge;
                couleurBarre.vert = tabCouleurPlacementSkin[i].vert;
                couleurBarre.bleu = tabCouleurPlacementSkin[i].bleu;
            }
        }

        if((mouseY > coordYSkinCadreBrique + 70 - 2) && (mouseY < coordYSkinCadreBrique + 70 + 21) && (mouseX > ecartCadreX + 47 - 2) && (mouseX < ecartCadreX + 47 + 2 + 42)){
            daltonien = 1;
        }
        else if((mouseY > coordYSkinCadreBrique + 70 - 2) && (mouseY < coordYSkinCadreBrique + 70 + 21) && (mouseX > ecartCadreX + 47 + 55 - 2 ) && (mouseX < ecartCadreX + 47 + 55 + 2 + 42)){
            daltonien = 0;
        }
    }

    if((niveau>0) && (pauseGame>0)){
        if((mouseY >= 522) && (mouseY <= 572)){
            if((mouseX >= 105) && (mouseX <= 243)){
                //NON
                pauseGame = -pauseGame;
            }
            else if((mouseX >= 260) && (mouseX <= 397)){
                //OUI
                niveau = 0;
                pageMenu = 0;
            }
        }
    }

    if((niveau==0) && (pauseMenu>0)){
        if((mouseY >= 396) && (mouseY <= 447)){
            if((mouseX >= 106) && (mouseX <= 245)){
                //NON
                pauseMenu = -pauseMenu;
            }
            else if((mouseX >= 261) && (mouseX <= 398)){
                //OUI
                freeAndTerminate();
            }
        }
    }
}

void MouseMotion(int mouseX, int mouseY){
    if((niveau==0) && (pauseMenu<0)){
        // mouseX - ecartLogo - rayonLogo
        if(((mouseX - ecartLogo - 44) * (mouseX - ecartLogo - 44) +  (mouseY - 799 - 44) * (mouseY - 799 - 44)) <= 44 * 44){
            hoverLogoNiveau = 1;
        }else if(((mouseX - ecartLogo - 109 - 44) * (mouseX - ecartLogo - 109 - 44) +  (mouseY - 799 - 44) * (mouseY - 799 - 44)) <= 44 * 44){
            hoverLogoUp = 1;
        }else if(((mouseX - ecartLogo - 2 * 109 - 44) * (mouseX - ecartLogo - 2 * 109 - 44) +  (mouseY - 799 - 44) * (mouseY - 799 - 44)) <= 44 * 44){
            hoverLogoSkin = 1;
        }else{
            hoverLogoNiveau = 0;
            hoverLogoUp = 0;
            hoverLogoSkin = 0;
        }
    }

    if((pageMenu==0) && (pauseMenu<0)){
        if(((mouseX >= 87) && (mouseX <= 412)) && ((mouseY >= 180) && (mouseY <= 240))){
            hoverNiveau1 = 1;
        }else if(((mouseX >= 87) && (mouseX <= 412)) && ((mouseY >= 270) && (mouseY <= 330))){
            hoverNiveau2 = 1;
        }else if(((mouseX >= 87) && (mouseX <= 412)) && ((mouseY >= 360) && (mouseY <= 420))){
            hoverNiveau3 = 1;
        }else if(((mouseX >= 87) && (mouseX <= 412)) && ((mouseY >= 455) && (mouseY <= 515))){
            hoverNiveau4 = 1;
        }else if(((mouseX >= 87) && (mouseX <= 412)) && ((mouseY >= 545) && (mouseY <= 605))){
            hoverNiveau5 = 1;
        }else{
            hoverNiveau1 = 0;
            hoverNiveau2 = 0;
            hoverNiveau3 = 0;
            hoverNiveau4 = 0;
            hoverNiveau5 = 0;
        }
    }

    if((pageMenu==1) && (pauseMenu<0)){
        if((mouseY > coordYCadreUPJoueur + ecartYEmplacementUP) && (mouseY < coordYCadreUPJoueur + ecartYEmplacementUP + largeurEmplacementUP) && (mouseX > coordXEmplacementUPCoin) && (mouseX < coordXEmplacementUPCoin + 90)){
            hoverUPEmplacementCoinJoueur = 1;
        }else if((mouseY > coordYCadreUPBalle + ecartYEmplacementUP) && (mouseY < coordYCadreUPBalle + ecartYEmplacementUP + largeurEmplacementUP) && (mouseX > coordXEmplacementUPCoin) && (mouseX < coordXEmplacementUPCoin + 90)){
            hoverUPEmplacementCoinBalle1 = 1;
        }
        else if((mouseY > coordYCadreUPBalle + coordYEmplacementUP) && (mouseY < coordYCadreUPBalle + coordYEmplacementUP + largeurEmplacementUP) && (mouseX > coordXEmplacementUPCoin) && (mouseX < coordXEmplacementUPCoin + 90)){
            hoverUPEmplacementCoinBalle2 = 1;
        }
        else if((mouseY > coordYCadreUPBarre + ecartYEmplacementUP) && (mouseY < coordYCadreUPBarre + ecartYEmplacementUP + largeurEmplacementUP) && (mouseX > coordXEmplacementUPCoin) && (mouseX < coordXEmplacementUPCoin + 90)){
            hoverUPEmplacementCoinBarre1 = 1;
        }
        else if((mouseY > coordYCadreUPBarre + coordYEmplacementUP) && (mouseY < coordYCadreUPBarre + coordYEmplacementUP + largeurEmplacementUP) && (mouseX > coordXEmplacementUPCoin) && (mouseX < coordXEmplacementUPCoin + 90)){
            hoverUPEmplacementCoinBarre2 = 1;
        }
        else if((mouseY > coordYCadreUPCoin + ecartYEmplacementUP) && (mouseY < coordYCadreUPCoin + ecartYEmplacementUP + largeurEmplacementUP) && (mouseX > coordXEmplacementUPCoin) && (mouseX < coordXEmplacementUPCoin + 90)){
            hoverUPEmplacementCoin1 = 1;
        }
        else if((mouseY > coordYCadreUPCoin + coordYEmplacementUP) && (mouseY < coordYCadreUPCoin + coordYEmplacementUP + largeurEmplacementUP) && (mouseX > coordXEmplacementUPCoin) && (mouseX < coordXEmplacementUPCoin + 90)){
            hoverUPEmplacementCoin2 = 1;
        }
        else if((mouseY > coordYCadreUPBonus + ecartYEmplacementUP) && (mouseY < coordYCadreUPBonus + ecartYEmplacementUP + largeurEmplacementUP) && (mouseX > coordXEmplacementUPCoin) && (mouseX < coordXEmplacementUPCoin + 90)){
            hoverUPEmplacementCoinBonus1 = 1;
        }
        else if((mouseY > coordYCadreUPBonus + coordYEmplacementUP) && (mouseY < coordYCadreUPBonus + coordYEmplacementUP + largeurEmplacementUP) && (mouseX > coordXEmplacementUPCoin) && (mouseX < coordXEmplacementUPCoin + 90)){
            hoverUPEmplacementCoinBonus2 = 1;
        }else{
            hoverUPEmplacementCoinJoueur = 0;
            hoverUPEmplacementCoinBalle1 = 0;
            hoverUPEmplacementCoinBalle2 = 0;
            hoverUPEmplacementCoinBarre1 = 0;
            hoverUPEmplacementCoinBarre2 = 0;
            hoverUPEmplacementCoin1 = 0;
            hoverUPEmplacementCoin2 = 0;
            hoverUPEmplacementCoinBonus1 = 0;
            hoverUPEmplacementCoinBonus2 = 0;
        }      
    }

    if((pageMenu==2) && (pauseMenu<0)){
        for(int i=0 ; i<8 ; i++){
            if((mouseX - (WINDOW_WIDTH/2 - 7*25 + i*50)) * (mouseX - (WINDOW_WIDTH/2 - 7*25 + i*50)) + (mouseY - (70 + 95)) * (mouseY - (70 + 95)) <= 17 * 17){
                hoverCouleurSkinBalle = i;
            }
            else if((mouseX - (WINDOW_WIDTH/2 - 7*25 + i*50)) * (mouseX - (WINDOW_WIDTH/2 - 7*25 + i*50)) + (mouseY - (70 + 95 + 85)) * (mouseY - (70 + 95 + 85)) <= 17 * 17){
                hoverCouleurSkinTrainee = i;
            }
            else if((mouseX - (WINDOW_WIDTH/2 - 7*25 + i*50)) * (mouseX - (WINDOW_WIDTH/2 - 7*25 + i*50)) + (mouseY - (70 + 320 + 9 + 90)) * (mouseY - (70 + 320 + 9 + 90)) <= 17 * 17){
                hoverCouleurSkinBarre = i;
            }
        }

        if((mouseY > coordYSkinCadreBrique + 70 - 2) && (mouseY < coordYSkinCadreBrique + 70 + 21) && (mouseX > ecartCadreX + 47 - 2) && (mouseX < ecartCadreX + 47 + 2 + 42)){
            hoverOUI = 1;
        }
        else if((mouseY > coordYSkinCadreBrique + 70 - 2) && (mouseY < coordYSkinCadreBrique + 70 + 21) && (mouseX > ecartCadreX + 47 + 55 - 2 ) && (mouseX < ecartCadreX + 47 + 55 + 2 + 42)){
            hoverNON = 1;
        }else{
            hoverOUI = 0;
            hoverNON = 0;
        }
    }
}

void MouseWheel(int wheelY){
    // fenetre vers le haut
    if ((wheelY < 0) && (varScroll > -600))
    {
        varScroll -= MY_SCROLL_SPEED;
    } 

    // fenetre vers le bas
    else if ((wheelY > 0) && (varScroll < 0))
    {
        varScroll += MY_SCROLL_SPEED;
    }
}

void KeyPressed(SDL_Keycode touche){
    /** @brief event.key.keysym.sym renvoi la touche appuyé
     *
     */
    switch (touche) {
        // Voir doc SDL_Keycode pour plus de touches https://wiki.libsdl.org/SDL_Keycode
        case SDLK_q:
            //touche Q appuyé
            // printf("Q\n");
            barr.vitesse = -vitesseBarre;
            break;
        case SDLK_d:
            //touche D appuyé
            // printf("D\n");
            barr.vitesse = vitesseBarre;
            break;
        case SDLK_SPACE:
            //touche ESPACE
            start=1;
            break;
        case SDLK_ESCAPE:
            if(niveau==0){
                pauseMenu = -pauseMenu;
                changeColor(0,0,0,100);
                drawRect(0,0,WINDOW_WIDTH, WINDOW_HEIGHT);

                sprite(90,274,"assets/menuQuit.bmp");
            }else{
                start = 0;
                pauseGame = -pauseGame;
                changeColor(0,0,0,100);
                drawRect(0,0,WINDOW_WIDTH, WINDOW_HEIGHT);

                sprite(88,288,"assets/menuPause.bmp");
            }
            
            break;
        default:
            break;
    }
}

void gameLoop() {
    int programLaunched = 1;
    while (programLaunched == 1) {
        // Boucle pour garder le programme ouvert
        // lorsque programLaunched est different de 1
        // on sort de la boucle, donc de la fonction
        SDL_Event event;
        while (SDL_PollEvent(&event)) {
            // boucle pour la gestion d'évenement
            switch (event.type) {
                /** event.type renvoi le type d'evenement qui se passe
                 * (si on appuie sur une touche, clique de souris...)
                 * en fonction du type d'évènement on à alors
                 * différentes information accessibles
                 * voir doc pour plus d'event https://wiki.libsdl.org/SDL_EventType
                 */
                case SDL_QUIT:
                    // quand on clique sur fermer la fénêtre en haut à droite
                    programLaunched = 0;
                    break;
                case SDL_MOUSEBUTTONDOWN:
                    /* clique de la souris
                     * event.motion.y | event.motion.x pour les positions de la souris
                     */
                    MouseButton(event.motion.x, event.motion.y);
                    break;
                case SDL_MOUSEMOTION:
                    MouseMotion(event.motion.x, event.motion.y);
                    break;
                case SDL_KEYDOWN:
                    KeyPressed(event.key.keysym.sym);
                    break;
                case SDL_MOUSEWHEEL:
                    MouseWheel(event.wheel.y);
                    break;

                case SDL_JOYBUTTONDOWN:
                    break;
                default:
                    break;
            }
        }
        drawGame();
    }
}

int main(){
    /** @description 3 fonctions dans le main qui permettent de créer l'application et la maintenir ouverte :
     *  init(...) : initialiser la SDL/ fenêtre
     *  gameLoop() : boucle de jeu dans laquelle l'application reste ouverte
     *  freeAndTerminate() : quitte le programme proprement
     */
    init(WINDOW_WIDTH, WINDOW_HEIGHT);
    init_game();
    gameLoop();
    printf("Fin du programme\n");
    freeAndTerminate();
}
