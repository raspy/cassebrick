/*--------------------------------Description--------------------------------*/
/*Nom projet: Brick Breaker             Edit: RaspY             Version: 0_0_1

Description: Jeu qui consiste à intercepter la balle grâce à une barre restant
            en bas de l'écran, que l'on peut déplacer de droite à gauche. Si la
            balle touche le bas de l'écran (en dessous de la barre) la balle est
            détruite, réinitialiser au dessus de la barre et relancer tant qu'il
            reste dans la vie. Cette dernière diminue à chaque fois que la balle
            est détruite.
            Le jeu s'arrête lorsqu'il n'y a plus de briques sur l'écran ou qu'il
            ne reste plus de vie.

Objectif:   Créer une barre vers le bas de l'écran que l'on peut déplacer grâce
            aux touche Q(gauche) et D(droite).
            Créer une balle que se déplace seule sur l'écran du jeu et faire en 
            sorte qu'elle rebondisse sur les bords de l'écran et sur la barre.

Atteint:    Oui    
*/
/*---------------------------------------------------------------------------*/

#include <SDL2/SDL.h>
#include <unistd.h>
#include "function.h"

#define WINDOW_WIDTH 500
#define WINDOW_HEIGHT 900
#define FPS 60

#define VITESSE_BALLE 5
#define VITESSE_BARRE 5

/*---------------Initialisation des variables liées à la BALLE---------------*/
//Coordonnées
int xBall = WINDOW_WIDTH/2;
int yBall = WINDOW_HEIGHT-125; //25 au-dessus de la barre
//Vitesse
int vxBall = VITESSE_BALLE;
int vyBall = -VITESSE_BALLE;

int rayonBall = 10;
int rebond = 0;
/*---------------------------------------------------------------------------*/
/*---------------Initialisation des variables liées à la BARRE---------------*/
//Coordonnnées
int xBar = WINDOW_WIDTH/2 -75;
int yBar = WINDOW_HEIGHT-50;
//Vitesse
int vxBar = 0;

int longueurBar = 75;
int largeurBar = 20;
/*---------------------------------------------------------------------------*/

void init_game(){
    //mettre votre code d'initialisation ici
}

void drawGame(){
    clear();
    changeColor(166,166,166);
    drawRect(0, 75, WINDOW_WIDTH, WINDOW_HEIGHT-75);

    changeColor(255,255,255);
    yBall+=vyBall;
	xBall+=vxBall;
	drawCircle(xBall, yBall, rayonBall);

	xBar+=vxBar;
	drawRect(xBar, yBar, longueurBar, largeurBar);

	if((yBall<=rayonBall+75 && rebond==1) || yBall+rayonBall>=WINDOW_HEIGHT || 
       (yBall+rayonBall>=yBar && xBall+rayonBall>=xBar && xBall+rayonBall<=xBar+longueurBar)){
		vyBall = -vyBall;
		rebond = 1;
	}
	if((xBall<=rayonBall && rebond==1) || xBall+rayonBall>=WINDOW_WIDTH ){
		vxBall = -vxBall;
		rebond = 1;
	}
	if(xBar<=0){
        xBar=0;
		vxBar=0;
	}else if(xBar+longueurBar>=WINDOW_WIDTH){
        xBar=WINDOW_WIDTH-longueurBar;
		vxBar=0;
    }
    actualize();
    usleep(1000000 / FPS); // 60 images par seconde | 1000000 = 1 seconde
}

void KeyPressed(SDL_Keycode touche){
    /** @brief event.key.keysym.sym renvoi la touche appuyé
     *
     */
    switch (touche) {
        // Voir doc SDL_Keycode pour plus de touches https://wiki.libsdl.org/SDL_Keycode
        // ici exemple ou appuyer sur les touches Q | D modifie x (position du carré dans drawGame())
        case SDLK_q:
            //touche q appuyé
            printf("Q\n");
            vxBar=-VITESSE_BARRE;
            break;
        case SDLK_d:
            //touche d appuyé
            printf("D\n");
            vxBar=VITESSE_BARRE;
            break;
        case SDLK_ESCAPE:
            freeAndTerminate();
            break;
        default:
            break;
    }
}

void joyButtonPressed(){
}

void gameLoop() {
    int programLaunched = 1;
    while (programLaunched == 1) {
        // Boucle pour garder le programme ouvert
        // lorsque programLaunched est different de 1
        // on sort de la boucle, donc de la fonction
        SDL_Event event;
        while (SDL_PollEvent(&event)) {
            // boucle pour la gestion d'évenement
            switch (event.type) {
                /** event.type renvoi le type d'evenement qui se passe
                 * (si on appuie sur une touche, clique de souris...)
                 * en fonction du type d'évènement on à alors
                 * différentes information accessibles
                 * voir doc pour plus d'event https://wiki.libsdl.org/SDL_EventType
                 */
                case SDL_QUIT:
                    // quand on clique sur fermer la fénêtre en haut à droite
                    programLaunched = 0;
                    break;
                case SDL_MOUSEBUTTONUP:
                    /* clique de la souris
                     * event.motion.y | event.motion.x pour les positions de la souris
                     */
                    printf("position de la souris x : %d , y : %d\n", event.motion.x, event.motion.y);
                    break;
                case SDL_KEYDOWN:
                    KeyPressed(event.key.keysym.sym);
                    break;
                case SDL_JOYBUTTONDOWN:
                    break;
                default:
                    break;
            }
        }
        /* coder ici pour que le code s'execute après chaque évenement
         * exemple dessiner un carré avec position int x, int y qu'on peut
         * deplacer lorsqu'on appuie sur une touche
         */
        drawGame();
    }
}

int main(){
    /** @description 3 fonctions dans le main qui permettent de créer l'application et la maintenir ouverte :
     *  init(...) : initialiser la SDL/ fenêtre
     *  gameLoop() : boucle de jeu dans laquelle l'application reste ouverte
     *  freeAndTerminate() : quitte le programme proprement
     */
    init(WINDOW_WIDTH, WINDOW_HEIGHT);
    init_game();
    gameLoop();
    printf("Fin du programme\n");
    freeAndTerminate();
}