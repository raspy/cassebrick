/*---------------------------------Description---------------------------------*/
/*Nom projet: Brick Breaker             Edit: RaspY             Version: 0_1_0

Description: Jeu qui consiste à intercepter la balle grâce à une barre restant
            en bas de l'écran, que l'on peut déplacer de droite à gauche. Si la
            balle touche le bas de l'écran (en dessous de la barre) la balle est
            détruite, réinitialiser au dessus de la barre et relancer tant qu'il
            reste dans la vie. Cette dernière diminue à chaque fois que la balle
            est détruite.
            Le jeu s'arrête lorsqu'il n'y a plus de briques sur l'écran ou qu'il
            ne reste plus de vie.

Description de ce qui a été fait : 
            La balle rebondit sur les bords de l'écran, la barre et les bricks.
            La barre peut être bouger avec les touches 'Q' et 'D'.
            Les bricks se cassent lorsqu'elles sont touchées par la balle puis
            disparaissent

Ce qu'il reste à faire :
            Corrigé les bug de la balle qui apparaissent quelque fois.
            Faire le systeme de vie du joueur.
            Faire tous les menus.

Objectif:   Réécriture du code + corrigé le ptit bug avec la balle

Atteint:    Oui
*/
/*-----------------------------------------------------------------------------*/

#include <SDL2/SDL.h>
#include <unistd.h>
#include "function.h"

#define WINDOW_WIDTH 500
#define WINDOW_HEIGHT 900
#define FPS 60

#define VITESSE_BALL 5
#define VITESSE_BARR 5

/*----------------Initialisation des variables liées à la BALLE----------------*/
typedef struct Balle
{
    int rayon;
    int x;
    int y;
    int vitesseX;
    int vitesseY;

    int xDroite;
    int xGauche;
    int yHaut;
    int yBas;

    int degat;
    int vie;
}Balle;

Balle ball={10, WINDOW_WIDTH/2, WINDOW_HEIGHT-75, 5, -5, 0, 0, 0, 0, 1, 3};
/*-----------------------------------------------------------------------------*/

/*----------------Initialisation des variables liées à la BARRE----------------*/
typedef struct Barre
{
    int longueur;
    int largeur;
    int vitesse;
    int x;
    int y;
}Barre;

Barre barr={75, 20, 0, 0, 0};
/*-----------------------------------------------------------------------------*/

/*-----------------Initialisation des variables liées au BRICK-----------------*/
typedef struct Brick
{
    int longueur;
    int largeur;
    int contour_longueur;
    int contour_largeur;

    int xBlocBricks;
    int yBlocBricks;

    int xContourBrickGauche;
    int xContourBrickDroite;
    int yContourBrickHaut;
    int yContourBrickBas;

    int espace;

    int vie;
}Brick;
//vie sera plus tard un rand()
Brick brick={60, 30, 62, 32, 56, 49+75, 0, 0, 0, 0, 3, 1};

int tabBrick[6][6];

int chargementBrick = 1;
/*-----------------------------------------------------------------------------*/
int start = 0;
int chargementCoeur = 1;


/*-----------------------------------BRICKS------------------------------------*/
void afficherBricks(){
    switch (brick.vie)
    {
        case 1:
            //contour Brick
            changeColor(92, 225, 230);
            drawRect(brick.xContourBrickGauche, 
                     brick.yContourBrickHaut,
                     brick.contour_longueur,
                     brick.contour_largeur
                    );

            //Brick
            changeColor(89,170,173);
            drawRect(1 + brick.xContourBrickGauche, 
                     1 + brick.yContourBrickHaut,
                     brick.longueur,
                     brick.largeur
                    );
            break;
        default:
            break;
    }
}

void initBricks(){
    for(int i=0 ; i<6 ; i++){
        for(int j=0 ; j<6 ; j++){
            //toutes à 1 (vie)
            tabBrick[i][j] = brick.vie;
        }
    }
    chargementBrick = 0;
}

void drawBricks(){
    if(chargementBrick==1){
        initBricks();
    }
    /*---------------------------Affichage et Rebond---------------------------*/
    for(int i=0 ; i<6 ; i++){
        for(int j=0 ; j<6 ; j++){
            if(tabBrick[i][j]>0){
                brick.xContourBrickGauche = brick.xBlocBricks + i * (brick.contour_longueur + brick.espace);
                brick.xContourBrickDroite = brick.xContourBrickGauche + brick.contour_longueur;

                brick.yContourBrickHaut = brick.yBlocBricks + j * (brick.contour_largeur + brick.espace);
                brick.yContourBrickBas = brick.yContourBrickHaut + brick.contour_largeur;

                afficherBricks();
                /*---------------------------Rebond----------------------------*/
                if(((ball.vitesseX > 0) && (ball.x < brick.xContourBrickGauche) && (ball.xDroite >= brick.xContourBrickGauche)) && (ball.y >= brick.yContourBrickHaut) && (ball.y <= brick.yContourBrickBas) ||
                   ((ball.vitesseX < 0) && (ball.x > brick.xContourBrickDroite) && (ball.xGauche <= brick.xContourBrickDroite)) && (ball.y >= brick.yContourBrickHaut) && (ball.y <= brick.yContourBrickBas)
                  ){
                    ball.vitesseX = -ball.vitesseX;
                    tabBrick[i][j]--;
                }
                else if(((ball.vitesseY < 0) && (ball.y > brick.yContourBrickBas) && (ball.yHaut <= brick.yContourBrickBas)) && (ball.x >= brick.xContourBrickGauche) && (ball.x <= brick.xContourBrickDroite) ||
                        ((ball.vitesseY > 0) && (ball.y < brick.yContourBrickHaut) && (ball.yBas >= brick.yContourBrickHaut)) && (ball.x >= brick.xContourBrickGauche) && (ball.x <= brick.xContourBrickDroite)
                       ){
                    ball.vitesseY = -ball.vitesseY;
                    tabBrick[i][j]--;
                }
                else if(((ball.vitesseX > 0) && (ball.vitesseY > 0) && (ball.x < brick.xContourBrickGauche) && (ball.xDroite >= brick.xContourBrickGauche) && (ball.y < brick.yContourBrickHaut) && (ball.yBas > brick.yContourBrickHaut)) ||
                        ((ball.vitesseX > 0) && (ball.vitesseY < 0) && (ball.x < brick.xContourBrickGauche) && (ball.xDroite >= brick.xContourBrickGauche) && (ball.y > brick.yContourBrickBas) && (ball.yHaut < brick.yContourBrickBas)) ||
                        ((ball.vitesseX < 0) && (ball.vitesseY > 0) && (ball.x > brick.xContourBrickDroite) && (ball.xGauche <= brick.xContourBrickDroite) && (ball.y < brick.yContourBrickHaut) && (ball.yBas > brick.yContourBrickHaut)) ||
                        ((ball.vitesseX < 0) && (ball.vitesseY < 0) && (ball.x > brick.xContourBrickDroite) && (ball.xGauche <= brick.xContourBrickDroite) && (ball.y > brick.yContourBrickBas) && (ball.yHaut < brick.yContourBrickBas))
                       ){
                    ball.vitesseX = -ball.vitesseX;
                    ball.vitesseY = -ball.vitesseY;
                    tabBrick[i][j]--;

                }
                /*-------------------------------------------------------------*/
            }
        }
    }
    /*-------------------------------------------------------------------------*/

}
/*-----------------------------------------------------------------------------*/

/*------------------------------------BARRE------------------------------------*/
void drawBarre(){
    //avant if pour éviter des saccadements visuels
    barr.x += barr.vitesse;

    if(barr.x < 0){
        barr.x = 0;
        barr.vitesse = 0;
    }else if((barr.x + barr.longueur) > WINDOW_WIDTH){
        barr.x = WINDOW_WIDTH - barr.longueur;
        barr.vitesse = 0;
    }

    //drawRect(barr.x, barr.y, barr.longueur, barr.largeur);
    drawRect(barr.x+(barr.largeur/2), barr.y, barr.longueur-barr.largeur, barr.largeur);
    drawCircle(barr.x+(barr.largeur/2), barr.y+(barr.largeur/2), barr.largeur/2);
    drawCircle(barr.x+barr.longueur-(barr.largeur/2), barr.y+(barr.largeur/2), barr.largeur/2);
}
/*-----------------------------------------------------------------------------*/

/*------------------------------------BALLE------------------------------------*/
void updateCoordBalle(){
    ball.xDroite = ball.x + ball.rayon;
    ball.xGauche = ball.x - ball.rayon;
    ball.yHaut = ball.y - ball.rayon;
    ball.yBas = ball.y + ball.rayon;
}

void rebondBalle(){
    // if(((ball.vitesseX < 0) && (ball.xGauche<=0)) || ((ball.vitesseX > 0) && (ball.xDroite>=WINDOW_WIDTH)) ||
    //    (((ball.vitesseX < 0) && (ball.x >= barr.x+barr.longueur) && (ball.xGauche <= barr.x+barr.longueur)) && (ball.y >= barr.y) && (ball.y <= barr.y+barr.largeur)) ||
    //    (((ball.vitesseX > 0) && (ball.x < barr.x) && (ball.xDroite >= barr.x)) && (ball.y >= barr.y) && (ball.y <= barr.y+barr.largeur))
    //   ){
    //     ball.vitesseX = -ball.vitesseX;
    // }
    // else if(((ball.vitesseY < 0) && (ball.yHaut<=75)) || (ball.yBas>=WINDOW_HEIGHT) ||
    //         (((ball.y < barr.y) && (ball.yBas >= barr.y)) && (ball.x >= barr.x) && (ball.x <= barr.x+barr.longueur))
    //   ){
    //     ball.vitesseY = -ball.vitesseY;
    // }
    /*----------------------------Bords de l'écran-----------------------------*/
    if(((ball.vitesseX < 0) && (ball.xGauche<=0)) || ((ball.vitesseX > 0) && (ball.xDroite>=WINDOW_WIDTH))){
        ball.vitesseX = -ball.vitesseX;
    }
    else if(((ball.vitesseY < 0) && (ball.yHaut<=75)) || (ball.yBas>=WINDOW_HEIGHT)){
        ball.vitesseY = -ball.vitesseY;
    }
    /*-------------------------------------------------------------------------*/
    /*----------------------------------Barre----------------------------------*/
    if((((ball.vitesseX < 0) && (ball.x >= barr.x+barr.longueur) && (ball.xGauche <= barr.x+barr.longueur)) && (ball.y >= barr.y) && (ball.y <= barr.y+barr.largeur)) ||
       (((ball.vitesseX > 0) && (ball.x < barr.x) && (ball.xDroite >= barr.x)) && (ball.y >= barr.y) && (ball.y <= barr.y+barr.largeur))
      ){
        ball.vitesseX = -ball.vitesseX;
    }
    else if(((ball.y < barr.y) && (ball.yBas >= barr.y)) && (ball.x >= barr.x) && (ball.x <= barr.x+barr.longueur)){
        ball.vitesseY = -ball.vitesseY;
    }
    else if(((ball.vitesseX > 0) && (ball.vitesseY > 0) && (ball.x < barr.x) && (ball.xDroite >= barr.x) && (ball.y < barr.y) && (ball.yBas >= barr.y)) ||
            ((ball.vitesseX < 0) && (ball.vitesseY > 0) && (ball.x > barr.x+barr.longueur) && (ball.xGauche <= barr.x+barr.longueur) && (ball.y < barr.y) && (ball.yBas >= barr.y))
           ){
        ball.vitesseX = -ball.vitesseX;
        ball.vitesseY = -ball.vitesseY;
    }
    /*-------------------------------------------------------------------------*/
    /*----------------------------------Bricks---------------------------------*/
    //Dans drawBrick() à cause du tabBrick. Pas de solution sans tout casser
    /*-------------------------------------------------------------------------*/
}

void drawBalle(){
    updateCoordBalle();

    //avant pour éviter de rester bloquer sur les côté de l'écran
    //probablement dû au pas, vitesse de la balle
    rebondBalle();

    ball.x += ball.vitesseX;
    ball.y += ball.vitesseY;

    changeColor(255,255,255);
    drawCircle(ball.x, ball.y, ball.rayon);
}
/*-----------------------------------------------------------------------------*/

void background(){
    clear();
    changeColor(155,155,155);
    drawRect(0, 75, WINDOW_WIDTH, WINDOW_HEIGHT-75);
}

void init_game(){
    background();

    barr.x = WINDOW_WIDTH/2 - barr.longueur/2;
    barr.y = WINDOW_HEIGHT - 2*barr.largeur;

    

    actualize();
}

void drawGame(){
    background();
    testText();

    for(int i=0 ; i<ball.vie ; i++){
        sprite(i*48,13,"assets/heart");
    }

    drawBalle();
    drawBarre();
    drawBricks();

    if(ball.yBas >= WINDOW_HEIGHT){
        ball.vie--;
    }

    if(ball.vie == 0){
        clear();
        start--;
    }

    actualize();
    usleep(1000000 / FPS); // 60 images par seconde | 1000000 = 1 seconde
}

void KeyPressed(SDL_Keycode touche){
    /** @brief event.key.keysym.sym renvoi la touche appuyé
     *
     */
    switch (touche) {
        // Voir doc SDL_Keycode pour plus de touches https://wiki.libsdl.org/SDL_Keycode
        case SDLK_q:
            //touche Q appuyé
            printf("Q\n");
            barr.vitesse = -VITESSE_BARR;
            break;
        case SDLK_d:
            //touche D appuyé
            printf("D\n");
            barr.vitesse = VITESSE_BARR;
            break;
        case SDLK_SPACE:
            //touche ESPACE
            start=1;
            break;
        case SDLK_ESCAPE:
            freeAndTerminate();
            break;
        default:
            break;
    }
}

void joyButtonPressed(){
}

void gameLoop() {
    int programLaunched = 1;
    while (programLaunched == 1) {
        // Boucle pour garder le programme ouvert
        // lorsque programLaunched est different de 1
        // on sort de la boucle, donc de la fonction
        SDL_Event event;
        while (SDL_PollEvent(&event)) {
            // boucle pour la gestion d'évenement
            switch (event.type) {
                /** event.type renvoi le type d'evenement qui se passe
                 * (si on appuie sur une touche, clique de souris...)
                 * en fonction du type d'évènement on à alors
                 * différentes information accessibles
                 * voir doc pour plus d'event https://wiki.libsdl.org/SDL_EventType
                 */
                case SDL_QUIT:
                    // quand on clique sur fermer la fénêtre en haut à droite
                    programLaunched = 0;
                    break;
                case SDL_MOUSEBUTTONUP:
                    /* clique de la souris
                     * event.motion.y | event.motion.x pour les positions de la souris
                     */
                    printf("position de la souris x : %d , y : %d\n", event.motion.x, event.motion.y);
                    break;
                case SDL_KEYDOWN:
                    KeyPressed(event.key.keysym.sym);
                    break;
                case SDL_JOYBUTTONDOWN:
                    break;
                default:
                    break;
            }
        }
        if(start==1){
            drawGame();
        }
    }
}

int main(){
    /** @description 3 fonctions dans le main qui permettent de créer l'application et la maintenir ouverte :
     *  init(...) : initialiser la SDL/ fenêtre
     *  gameLoop() : boucle de jeu dans laquelle l'application reste ouverte
     *  freeAndTerminate() : quitte le programme proprement
     */
    init(WINDOW_WIDTH, WINDOW_HEIGHT);
    init_game();
    gameLoop();
    printf("Fin du programme\n");
    freeAndTerminate();
}