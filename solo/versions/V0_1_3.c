/*---------------------------------Description---------------------------------*/
/*Nom projet: Brick Breaker             Edit: RaspY             Version: 0_1_3

Description: Jeu qui consiste à intercepter la balle grâce à une barre restant
            en bas de l'écran, que l'on peut déplacer de droite à gauche. Si la
            balle touche le bas de l'écran (en dessous de la barre) la balle est
            détruite, réinitialiser au dessus de la barre et relancer tant qu'il
            reste dans la vie. Cette dernière diminue à chaque fois que la balle
            est détruite.
            Le jeu s'arrête lorsqu'il n'y a plus de briques sur l'écran ou qu'il
            ne reste plus de vie.

Objectif:   Rajout des menuPause / Ajout des Coins

Atteint:    Oui / Non
*/
/*-----------------------------------------------------------------------------*/
#include <stdio.h> 
#include <stdlib.h> 
#include <time.h> 


#include <SDL2/SDL.h>
#include <unistd.h>
#include "function.h"

#define WINDOW_WIDTH 500
#define WINDOW_HEIGHT 900
#define FPS 60

#define VITESSE_BALL 5
#define VITESSE_BARR 5
#define NB_COLONNE_BRICK 6
#define NB_LIGNE_BRICK 10

/*----------------Initialisation des variables liées à la BALLE----------------*/
typedef struct Balle
{
    int rayon;
    int x;
    int y;
    int vitesseX;
    int vitesseY;

    int xDroite;
    int xGauche;
    int yHaut;
    int yBas;

    int degat;
    int vie;
}Balle;

Balle ball={10, WINDOW_WIDTH/2, WINDOW_HEIGHT-75, 5, -5, 0, 0, 0, 0, 1, 3};
/*-----------------------------------------------------------------------------*/

/*----------------Initialisation des variables liées à la BARRE----------------*/
typedef struct Barre
{
    int longueur;
    int largeur;
    int vitesse;
    int x;
    int y;
}Barre;

Barre barr={75, 20, 0, 0, 0};
/*-----------------------------------------------------------------------------*/

/*-----------------Initialisation des variables liées au BRICK-----------------*/
typedef struct Brick
{
    int longueur;
    int largeur;
    int contour_longueur;
    int contour_largeur;

    int xBlocBricks;
    int yBlocBricks;

    int xContourBrickGauche;
    int xContourBrickDroite;
    int yContourBrickHaut;
    int yContourBrickBas;

    int espace;

    int vie;
}Brick;
//vie sera plus tard un rand()
Brick brick={60, 30, 62, 32, 56, 49+75, 0, 0, 0, 0, 3, 1};

int tabBrick[NB_LIGNE_BRICK][NB_COLONNE_BRICK];

int chargementBrick;
int nbRestantBrick;
/*-----------------------------------------------------------------------------*/
int start; //0: Avant game ; 1: Game ; 2: Pause Game Balle Perdue
int init_chargeBricks;
int chargementCoeur;
int niveau;
int pauseGame;
int pauseMenu;

int tabLigne;
int tabColonne;

void resetBalleEtBarre(){
    ball.x = WINDOW_WIDTH/2; 
    ball.y = WINDOW_HEIGHT-75;
    ball.vitesseX = VITESSE_BALL;
    ball.vitesseY = -VITESSE_BALL;

    barr.x = WINDOW_WIDTH/2 - barr.longueur/2;
    barr.y = WINDOW_HEIGHT - 2*barr.largeur;

    sprite(0, 500, "assets/tuto.bmp");
}

void coins(){
    srand( time( NULL ) );


    int probaCoinsTomb = rand() % 2;
    //int probaCoinsValeur = rand() % 2;

    int vieCoins = 0;
    // changeColor(255, 247, 56, 255);
    // drawCircle((brick.xContourBrickGauche + (brick.contour_longueur/2)),
    //            (brick.yContourBrickHaut + (brick.contour_largeur/2)),
    //            11
    //           ); //les co sont pour le CENTER du cercle
    // changeColor(166, 162, 61, 255);
    // drawCircle((brick.xContourBrickGauche + (brick.contour_longueur/2)),
    //            (brick.yContourBrickHaut + (brick.contour_largeur/2)),
    //            9
    //           );
}

/*-----------------------------------COEURS------------------------------------*/
void drawCoeurs(){
    //---------------------------------AFFICHE---------------------------------
    for(int i=0 ; i<ball.vie ; i++){
        sprite(i*48,13,"assets/heart.bmp");
    }
    //--------------------------PERTE DE VIE et PAUSE--------------------------
    if(ball.yBas >= WINDOW_HEIGHT){
        ball.vie--;
        resetBalleEtBarre();
        start = 0;
    }
}
/*-----------------------------------------------------------------------------*/

/*-----------------------------------BRICKS------------------------------------*/
void afficherContourBrick(){
    drawRect(brick.xContourBrickGauche, 
             brick.yContourBrickHaut,
             brick.contour_longueur,
             brick.contour_largeur
            );
}

void afficherBrick(){
    drawRect(1 + brick.xContourBrickGauche, 
             1 + brick.yContourBrickHaut,
             brick.longueur,
             brick.largeur
            );
}

void afficherBricks(){
    //--------------------En fonction de la vie des briques--------------------
    switch (brick.vie)
    {
        case 1:
            //contour Brick
            changeColor(92, 225, 230, 255);
            afficherContourBrick();

            //Brick
            changeColor(89,170,173,255);
            afficherBrick();
            break;

        case 2:
            //contour Brick
            changeColor(130, 188, 255, 255);
            afficherContourBrick();

            //Brick
            changeColor(89, 128, 173, 255);
            afficherBrick();
            break;
        case 3:
            //contour Brick
            changeColor(15, 127, 244, 255);
            afficherContourBrick();

            //Brick
            changeColor(73, 77, 184, 255);
            afficherBrick();
            break;
        case 4:
            //contour Brick
            changeColor(171, 126, 255, 255);
            afficherContourBrick();

            //Brick
            changeColor(118, 89, 173, 255);
            afficherBrick();
            break;
        case 5:
            //contour Brick
            changeColor(233, 122, 255, 255);
            afficherContourBrick();

            //Brick
            changeColor(159, 89, 173, 255);
            afficherBrick();
            break;
        case 6:
            //contour Brick
            changeColor(248, 122, 189, 255);
            afficherContourBrick();

            //Brick
            changeColor(173, 89, 134, 255);
            afficherBrick();
            break;
        default:
            break;
    }
}

int rebonBalleBricks(int brique){
    /*---------------------------Rebond----------------------------*/
    if(((ball.vitesseX > 0) && (ball.x < brick.xContourBrickGauche) && (ball.xDroite >= brick.xContourBrickGauche)) && (ball.y >= brick.yContourBrickHaut) && (ball.y <= brick.yContourBrickBas) ||
       ((ball.vitesseX < 0) && (ball.x > brick.xContourBrickDroite) && (ball.xGauche <= brick.xContourBrickDroite)) && (ball.y >= brick.yContourBrickHaut) && (ball.y <= brick.yContourBrickBas)
      ){
        ball.vitesseX = -ball.vitesseX;
        brique--;
        nbRestantBrick--;
        coins();
    }
    else if(((ball.vitesseY < 0) && (ball.y > brick.yContourBrickBas) && (ball.yHaut <= brick.yContourBrickBas)) && (ball.x >= brick.xContourBrickGauche) && (ball.x <= brick.xContourBrickDroite) ||
            ((ball.vitesseY > 0) && (ball.y < brick.yContourBrickHaut) && (ball.yBas >= brick.yContourBrickHaut)) && (ball.x >= brick.xContourBrickGauche) && (ball.x <= brick.xContourBrickDroite)
           ){
        ball.vitesseY = -ball.vitesseY;
        brique--;
        nbRestantBrick--;
        coins();
    }
    else if(((ball.vitesseX > 0) && (ball.vitesseY > 0) && (ball.x < brick.xContourBrickGauche) && (ball.xDroite >= brick.xContourBrickGauche) && (ball.y < brick.yContourBrickHaut) && (ball.yBas > brick.yContourBrickHaut)) ||
            ((ball.vitesseX > 0) && (ball.vitesseY < 0) && (ball.x < brick.xContourBrickGauche) && (ball.xDroite >= brick.xContourBrickGauche) && (ball.y > brick.yContourBrickBas) && (ball.yHaut < brick.yContourBrickBas)) ||
            ((ball.vitesseX < 0) && (ball.vitesseY > 0) && (ball.x > brick.xContourBrickDroite) && (ball.xGauche <= brick.xContourBrickDroite) && (ball.y < brick.yContourBrickHaut) && (ball.yBas > brick.yContourBrickHaut)) ||
            ((ball.vitesseX < 0) && (ball.vitesseY < 0) && (ball.x > brick.xContourBrickDroite) && (ball.xGauche <= brick.xContourBrickDroite) && (ball.y > brick.yContourBrickBas) && (ball.yHaut < brick.yContourBrickBas))
           ){
        ball.vitesseX = -ball.vitesseX;
        ball.vitesseY = -ball.vitesseY;
        brique--;
        nbRestantBrick--;
        coins();
    }
    /*-------------------------------------------------------------*/

    return brique;
}

void updateCoordBricks(int i, int j){
    /*------------------Initialisation paramètres------------------*/
    brick.xContourBrickGauche = brick.xBlocBricks + j * (brick.contour_longueur + brick.espace);
    brick.xContourBrickDroite = brick.xContourBrickGauche + brick.contour_longueur;
    brick.yContourBrickHaut = brick.yBlocBricks + i * (brick.contour_largeur + brick.espace);
    brick.yContourBrickBas = brick.yContourBrickHaut + brick.contour_largeur;
    /*-------------------------------------------------------------*/
}

void initBricks(){
    if(niveau==1){
        for(int i=0 ; i<tabLigne ; i++){
            for(int j=0 ; j<tabColonne; j++){
                //toutes à 1 (vie)
                brick.vie = 1;
                tabBrick[i][j] = brick.vie;
                nbRestantBrick++;
            }
        }
        chargementBrick = 0;
    }
    else if(niveau==2){
        for(int i=0 ; i<tabLigne ; i++){
            for(int j=0 ; j<tabColonne; j++){
                if(((i==2) || (i==3)) && ((j==2) || (j==3))){
                    brick.vie = 1;
                    tabBrick[i][j] = brick.vie;
                    nbRestantBrick++;
                }
                else if(((i==4) || (i==5)) && ((j>=1) && (j<=4))){ //juste pour que se soit plus lisible
                    brick.vie = 1;
                    tabBrick[i][j] = brick.vie;
                    nbRestantBrick++;
                }
                else{
                    brick.vie = 2;
                    tabBrick[i][j] = brick.vie;
                    nbRestantBrick++;
                }
            }
        }
        chargementBrick = 0;
    }
    else if(niveau==3){
        printf("%d", niveau);
        for(int i=0 ; i<tabLigne ; i++){
            for(int j=0 ; j<tabColonne; j++){
                // tabBrick[i][j] = 1;
                if(i<=1){
                    brick.vie = 3;
                    tabBrick[i][j] = brick.vie;
                    nbRestantBrick++;
                }
                else if(((i>1) && (i<6)) || ((i==7) && (i<=9) && ((j<2) || (j>3)))){ //juste pour que se soit plus lisible
                    brick.vie = 2;
                    tabBrick[i][j] = brick.vie;
                    nbRestantBrick++;
                }
                else if((i>7) && (i<=9) && ((j<2) || (j>3))){
                    brick.vie = 1;
                    tabBrick[i][j] = brick.vie;
                    nbRestantBrick++;
                }
                else{
                    brick.vie = 0;
                    tabBrick[i][j] = brick.vie;
                }
            }
        }
        chargementBrick = 0;
    }
    else if(niveau==4){
        for(int i=0 ; i<tabLigne ; i++){
            for(int j=0 ; j<tabColonne; j++){
                if((i>=3) && (i<=6)){
                    if((((i==3) || (i==6)) && ((j<=1) || (j>=4))) ||
                       (((i>3) && (i<6)) && ((j>1) && (j<4)))
                      ){
                        brick.vie = 4;
                        tabBrick[i][j] = brick.vie;
                        nbRestantBrick++;
                    }
                    else{
                        brick.vie = 2;
                        tabBrick[i][j] = brick.vie;
                        nbRestantBrick++; 
                    }
                }
                else{
                    if((((i==2) || (i==7)) && ((j<=1) || (j>=4))) ||
                       (((i<2) || (i>7)) && ((j>1) && (j<4)))
                      ){
                        brick.vie = 3;
                        tabBrick[i][j] = brick.vie;
                        nbRestantBrick++;
                    }
                    else{
                        brick.vie = 1;
                        tabBrick[i][j] = brick.vie;
                        nbRestantBrick++;
                    }
                }
            }
        }
        chargementBrick = 0;
    }
    else if(niveau==5){
        for(int i=0 ; i<tabLigne ; i++){
            for(int j=0 ; j<tabColonne; j++){
                if(((i==0) && ((j==0) || (j==5))) ||
                   ((i==9) && ((j==0) || (j==5)))
                  ){
                    brick.vie = 1;
                    tabBrick[i][j] = brick.vie;
                    nbRestantBrick++;
                }
                else if(((i>=4) && (i<=5) && (j>=2) && (j<=3))){
                    brick.vie = 3;
                    tabBrick[i][j] = brick.vie;
                    nbRestantBrick++;
                }
                else if((((i==3) || (i==6)) && (j>=2) && (j<=3))){
                    brick.vie = 4;
                    tabBrick[i][j] = brick.vie;
                    nbRestantBrick++;
                }
                else if((((i>=3) && (i<=6)) && ((j>3) || (j<2))) ||
                        (((i==2) || (i==7)) && (j>=1) && (j<=4)) ||
                        (((i==1) || (i==8)) && (j>=2) && (j<=3))
                ){
                    brick.vie = 5;
                    tabBrick[i][j] = brick.vie;
                    nbRestantBrick++;
                }
                else{
                    brick.vie = 6;
                    tabBrick[i][j] = brick.vie;
                    nbRestantBrick++;
                }
            }
        }
        chargementBrick = 0;
    }
}

void drawBricks(){
    /*---------------------------AFFICHER et REBOND---------------------------*/
    for(int i=0 ; i<tabLigne ; i++){
        for(int j=0 ; j<tabColonne; j++){
            updateCoordBricks(i, j);
            if(tabBrick[i][j]>0){
                brick.vie = tabBrick[i][j];

                //---------------------------AFFICHER--------------------------
                afficherBricks();

                //----------------------------REBOND---------------------------
                tabBrick[i][j] = rebonBalleBricks(tabBrick[i][j]);                
            }
            else{
                coins();
            }
        }
    }
    /*-------------------------------------------------------------------------*/
}
/*-----------------------------------------------------------------------------*/

/*------------------------------------BARRE------------------------------------*/
void afficheBarre(){
    /*------------------------------AFFICHE BARRE------------------------------*/
    drawRect(barr.x+(barr.largeur/2), barr.y, barr.longueur-barr.largeur, barr.largeur);
    drawCircle(barr.x+(barr.largeur/2), barr.y+(barr.largeur/2), barr.largeur/2);
    drawCircle(barr.x+barr.longueur-(barr.largeur/2), barr.y+(barr.largeur/2), barr.largeur/2);
    /*-------------------------------------------------------------------------*/
}

void drawBarre(){
    //---------------------------------VITESSE---------------------------------
    barr.x += barr.vitesse;

    /*------------------------LIMITE DEPLACEMENT BORDURE-----------------------*/
    if(barr.x < 0){
        barr.x = 0;
        barr.vitesse = 0;
    }else if((barr.x + barr.longueur) > WINDOW_WIDTH){
        barr.x = WINDOW_WIDTH - barr.longueur;
        barr.vitesse = 0;
    }
    /*-------------------------------------------------------------------------*/

    afficheBarre();
}
/*-----------------------------------------------------------------------------*/

/*------------------------------------BALLE------------------------------------*/
void updateCoordBalle(){
    ball.xDroite = ball.x + ball.rayon;
    ball.xGauche = ball.x - ball.rayon;
    ball.yHaut = ball.y - ball.rayon;
    ball.yBas = ball.y + ball.rayon;
}

void rebondBalle(){
    /*----------------------------Bords de l'écran-----------------------------*/
    if(((ball.vitesseX < 0) && (ball.xGauche<=0)) || ((ball.vitesseX > 0) && (ball.xDroite>=WINDOW_WIDTH))){
        ball.vitesseX = -ball.vitesseX;
    }
    else if((ball.vitesseY < 0) && (ball.yHaut<=75)){
        ball.vitesseY = -ball.vitesseY;
    }
    /*-------------------------------------------------------------------------*/
    /*----------------------------------Barre----------------------------------*/
    if((((ball.vitesseX < 0) && (ball.x >= barr.x+barr.longueur) && (ball.xGauche <= barr.x+barr.longueur)) && (ball.y >= barr.y) && (ball.y <= barr.y+barr.largeur)) ||
       (((ball.vitesseX > 0) && (ball.x < barr.x) && (ball.xDroite >= barr.x)) && (ball.y >= barr.y) && (ball.y <= barr.y+barr.largeur))
      ){
        ball.vitesseX = -ball.vitesseX;
    }
    else if(((ball.y < barr.y) && (ball.yBas >= barr.y)) && (ball.x >= barr.x) && (ball.x <= barr.x+barr.longueur)){
        ball.vitesseY = -ball.vitesseY;
    }
    else if(((ball.vitesseX > 0) && (ball.vitesseY > 0) && (ball.x <= barr.x) && (ball.xDroite >= barr.x) && (ball.y <= barr.y) && (ball.yBas >= barr.y)) ||
            ((ball.vitesseX < 0) && (ball.vitesseY > 0) && (ball.x >= barr.x+barr.longueur) && (ball.xGauche <= barr.x+barr.longueur) && (ball.y <= barr.y) && (ball.yBas >= barr.y))
           ){
        ball.vitesseX = -ball.vitesseX;
        ball.vitesseY = -ball.vitesseY;
    }
    /*-------------------------------------------------------------------------*/
    /*----------------------------------Bricks---------------------------------*/
    //Dans drawBrick() à cause du tabBrick. Pas de solution sans tout casser
    /*-------------------------------------------------------------------------*/
}

void afficherBalle(){
    //---------------------------------AFFICHE---------------------------------
    changeColor(255,255,255,255);
    drawCircle(ball.x, ball.y, ball.rayon);
}

void drawBalle(){
    updateCoordBalle();

    rebondBalle();

    //---------------------------------VITESSE---------------------------------
    ball.x += ball.vitesseX;
    ball.y += ball.vitesseY;

    afficherBalle();
}
/*-----------------------------------------------------------------------------*/

void background(){
    clear();
    changeColor(115, 115, 115, 255);
    drawRect(0, 75, WINDOW_WIDTH, WINDOW_HEIGHT-75);
}

void game(){
    background();

    drawCoeurs();

    drawBalle();
    drawBarre();
    drawBricks();    

    //Arrêt du jeu et renvoie au menu
    if((ball.vie == 0) || (nbRestantBrick == 0)){
        clear();
        start--;
        niveau = 0;
    }
}

void afficherAvantGame(){
    if(chargementBrick==1){
        initBricks();
    }

    background();

    drawCoeurs();
    afficherBalle();        
    afficheBarre();   
    drawBricks();

    sprite(0, 500, "assets/tuto.bmp");

    actualize();
}

/*-----------------------------------NIVEAUX-----------------------------------*/
void lancerNiveau(){
    if(init_chargeBricks==0){
        chargementBrick = 1;
        init_chargeBricks++;
    }

    if((start==0) && (pauseGame<0)){
        afficherAvantGame();
    }
    if(start==1){
        game();
    }
}
//-----------------------------------NIVEAU1-----------------------------------
void niveau1(){
    tabLigne=6;
    tabColonne=6;

    lancerNiveau();
}
//-----------------------------------NIVEAU2-----------------------------------
void niveau2(){
    tabLigne=6;
    tabColonne=6;

    lancerNiveau();
}
//-----------------------------------NIVEAU3-----------------------------------
void niveau3(){
    tabLigne=10;
    tabColonne=6;

    lancerNiveau();
}
//-----------------------------------NIVEAU4-----------------------------------
void niveau4(){
    tabLigne=10;
    tabColonne=6;

    lancerNiveau();
}
//-----------------------------------NIVEAU5-----------------------------------
void niveau5(){
    tabLigne=10;
    tabColonne=6;

    lancerNiveau();
}
/*-----------------------------------------------------------------------------*/

void init_game(){
    ball.vie = 3;

    resetBalleEtBarre();    

    init_chargeBricks = 0;

    chargementBrick = 1;
    nbRestantBrick = 0;

    start = 0;
    chargementCoeur = 1;
    niveau = 0;
    pauseGame = -1;
    pauseMenu = -1;

}

void drawGame(){
    switch (niveau)
    {
        case 0:
            if(pauseMenu<0){
                //RESET VARIABLE avant chaque début de niveau
                init_game();
                sprite(0,0,"assets/menuStart2.bmp");
            }
            
            break;
        case 1:
            niveau1();
            break;
        case 2:
            niveau2();
            break; 
        case 3:
            niveau3();
            break; 
        case 4:
            niveau4();
            break;
        case 5:
            niveau5();
            break;
        default:
            break;
    }
    actualize();
    usleep(1000000 / FPS); // 60 images par seconde | 1000000 = 1 seconde
}

void MouseButton(int mouseX, int mouseY){
    printf("position de la souris x : %d , y : %d\n", mouseX, mouseY);

    //Sélection du niveau UNIQUEMENT lorsque l'on est dans le menu
    if((niveau==0) && (pauseMenu<0)){
        if(((mouseX >= 87) && (mouseX <= 412)) && ((mouseY >= 180) && (mouseY <= 240))){
            niveau = 1;
        }

        else if(((mouseX >= 87) && (mouseX <= 412)) && ((mouseY >= 270) && (mouseY <= 330))){
            niveau = 2;
        }

        else if(((mouseX >= 87) && (mouseX <= 412)) && ((mouseY >= 360) && (mouseY <= 420))){
            niveau = 3;
        }

        else if(((mouseX >= 87) && (mouseX <= 412)) && ((mouseY >= 455) && (mouseY <= 515))){
            niveau = 4;
        }

        else if(((mouseX >= 87) && (mouseX <= 412)) && ((mouseY >= 545) && (mouseY <= 605))){
            niveau = 5;
        }
    }

    if(pauseGame>0){
        if((mouseY >= 522) && (mouseY <= 572)){
            if((mouseX >= 105) && (mouseX <= 243)){
                //NON
                pauseGame = -pauseGame;
            }
            else if((mouseX >= 260) && (mouseX <= 397)){
                //OUI
                niveau = 0;
            }
        }
    }

    if(pauseMenu>0){
        if((mouseY >= 396) && (mouseY <= 447)){
            if((mouseX >= 106) && (mouseX <= 245)){
                //NON
                pauseMenu = -pauseMenu;
            }
            else if((mouseX >= 261) && (mouseX <= 398)){
                //OUI
                freeAndTerminate();
            }
        }
    }
}

void KeyPressed(SDL_Keycode touche){
    /** @brief event.key.keysym.sym renvoi la touche appuyé
     *
     */
    switch (touche) {
        // Voir doc SDL_Keycode pour plus de touches https://wiki.libsdl.org/SDL_Keycode
        case SDLK_q:
            //touche Q appuyé
            printf("Q\n");
            barr.vitesse = -VITESSE_BARR;
            break;
        case SDLK_d:
            //touche D appuyé
            printf("D\n");
            barr.vitesse = VITESSE_BARR;
            break;
        case SDLK_SPACE:
            //touche ESPACE
            start=1;
            break;
        case SDLK_ESCAPE:
            if(niveau==0){
                pauseMenu = -pauseMenu;
                changeColor(0,0,0,100);
                drawRect(0,0,WINDOW_WIDTH, WINDOW_HEIGHT);

                sprite(90,274,"assets/menuQuit.bmp");
            }else{
                start = 0;
                pauseGame = -pauseGame;
                changeColor(0,0,0,100);
                drawRect(0,0,WINDOW_WIDTH, WINDOW_HEIGHT);

                sprite(88,288,"assets/menuPause.bmp");
            }
            
            break;
        default:
            break;
    }
}

void joyButtonPressed(){
}

void gameLoop() {
    int programLaunched = 1;
    while (programLaunched == 1) {
        // Boucle pour garder le programme ouvert
        // lorsque programLaunched est different de 1
        // on sort de la boucle, donc de la fonction
        SDL_Event event;
        while (SDL_PollEvent(&event)) {
            // boucle pour la gestion d'évenement
            switch (event.type) {
                /** event.type renvoi le type d'evenement qui se passe
                 * (si on appuie sur une touche, clique de souris...)
                 * en fonction du type d'évènement on à alors
                 * différentes information accessibles
                 * voir doc pour plus d'event https://wiki.libsdl.org/SDL_EventType
                 */
                case SDL_QUIT:
                    // quand on clique sur fermer la fénêtre en haut à droite
                    programLaunched = 0;
                    break;
                case SDL_MOUSEBUTTONDOWN:
                    /* clique de la souris
                     * event.motion.y | event.motion.x pour les positions de la souris
                     */
                    MouseButton(event.motion.x, event.motion.y);
                    break;
                case SDL_KEYDOWN:
                    KeyPressed(event.key.keysym.sym);
                    break;
                case SDL_JOYBUTTONDOWN:
                    break;
                default:
                    break;
            }
        }
        drawGame();
    }
}

int main(){
    /** @description 3 fonctions dans le main qui permettent de créer l'application et la maintenir ouverte :
     *  init(...) : initialiser la SDL/ fenêtre
     *  gameLoop() : boucle de jeu dans laquelle l'application reste ouverte
     *  freeAndTerminate() : quitte le programme proprement
     */
    init(WINDOW_WIDTH, WINDOW_HEIGHT);
    init_game();
    gameLoop();
    printf("Fin du programme\n");
    freeAndTerminate();
}