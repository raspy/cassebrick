/*---------------------------------Description---------------------------------*/
/*Nom projet: Brick Breaker             Edit: RaspY             Version: 0_2_6

Description Jeu : Jeu qui consiste à intercepter la balle grâce à une barre 
            restant en bas de l'écran, que l'on peut déplacer de droite à gauche. 
            Si la balle touche le bas de l'écran (en dessous de la barre) la 
            balle est détruite, réinitialiser au dessus de la barre et relancer 
            tant qu'il reste dans la vie. Cette dernière diminue à chaque fois 
            que la balle est détruite. 
            Le jeu s'arrête lorsqu'il n'y a plus de briques sur l'écran ou qu'il 
            ne reste plus de vie.

Description : Changement visuel du menu de choix de niveau : hover sur le choix 
            des niveaux + fonctionnalité de la barre-menu + faire le visuel de
            la boutique UP

Objectif :  ajout de UP + HOVER des différents éléments MENU

Atteint :   OUI
*/
/*-----------------------------------------------------------------------------*/

#include <stdio.h> 
#include <stdlib.h> 
#include <math.h>
#include <time.h> 


#include <SDL2/SDL.h>
#include <unistd.h>
#include "function.h"

#define WINDOW_WIDTH 500
#define WINDOW_HEIGHT 900
#define FPS 60

#define VITESSE_BALL 5
#define VITESSE_BARR 5
#define NB_LIGNE_BRICK 10
#define NB_COLONNE_BRICK 6

#define MY_SCROLL_SPEED 20

/*----------------Initialisation des variables liées à la BALLE----------------*/
typedef struct Balle
{
    int rayon;
    int x;
    int y;
    int vitesseX;
    int vitesseY;

    int xDroite;
    int xGauche;
    int yHaut;
    int yBas;

    int degat;
    int vie;
}Balle;

Balle ball={10, WINDOW_WIDTH/2, WINDOW_HEIGHT-75, VITESSE_BALL, -VITESSE_BALL, 0, 0, 0, 0, 1, 3};
/*-----------------------------------------------------------------------------*/

/*----------------Initialisation des variables liées à la BARRE----------------*/
typedef struct Barre
{
    int longueur;
    int largeur;
    int vitesse;
    int x;
    int y;
}Barre;

Barre barr={75, 20, 0, 0, 0};
/*-----------------------------------------------------------------------------*/

/*-----------------Initialisation des variables liées au BRICK-----------------*/
typedef struct Bricks
{
    int x;
    int y;
    int vie;
    int spawn;

    int xContourBrickGauche;
    int xContourBrickDroite;
    int yContourBrickHaut;
    int yContourBrickBas;
}Bricks;

Bricks tabBricks[NB_LIGNE_BRICK][NB_COLONNE_BRICK];


int espaceEntreBrick;
int espaceXDebutBricks;
int espaceYDebutBricks;

int longueurBrick;
int largeurBrick;
int longueurBrickContour;
int largeurBrickContour;

int init_chargeBricks;
int chargementBrick;
int nbRestantBrick;
/*-----------------------------------------------------------------------------*/

/*-----------------Initialisation des variables liées au COINS-----------------*/
typedef struct Coins
{
    int spawn;
    int x;
    int y;
}Coins;

Coins tabPieces[NB_LIGNE_BRICK][NB_COLONNE_BRICK];

int vitesseCoins;
int rayonCoins;
int rayonContourCoins;

int money;
/*-----------------------------------------------------------------------------*/
int start; //0: Avant game ; 1: Game ; 2: Pause Game Balle Perdue

int chargementCoeur;
int niveau;
int pauseGame;
int pauseMenu;
int pageMenu;
int nombreLigne;
int nombreColonne;

int ecartBandeJeuY;

int unit;
int placementImgDoite;

int longueurImgChiffreNoir;
int largeurImgChiffreNoir;
int largeurImgCoinNoir;

int longueurImgChiffreRouge;
int largeurImgChiffreRouge;
int largeurImgCoinRouge;

int longueurImgChiffrePetit;
int largeurImgChiffrePetit;
int largeurImgCoinPetit;

int longueurImgVide;
int element;

int hoverNiveau1;
int hoverNiveau2;
int hoverNiveau3;
int hoverNiveau4;
int hoverNiveau5;

int ecartLogo;

int hoverLogoNiveau;
int hoverLogoUp;
int hoverLogoSkin;
int hoverLogoSetting;

int ecartCadreX;
int ecartCadreY;
int cadreUpJoueurH;
int cadreUpBalleH;
int cadreUpBarreH;
int cadreUpBonusH;
int cadreUpCoinH;

int ecartXEmplacementUP;
int ecartYEmplacementUP;

int longueurEmplacementUP;
int largeurEmplacementUP;
int espaceXEmplacementUP;
int espaceYEmplacementUP;

int coordXEmplacementUP;
int coordXEmplacementUPCoin;
int coordYEmplacementUP;

int coordYCadreUPJoueur;
int coordYCadreUPBalle;
int coordYCadreUPBarre;
int coordYCadreUPBonus;
int coordYCadreUPCoin;

int espaceCadre;

int varScroll;

int tabPrixUP[10];

int hoverUPEmplacementCoinJoueur;
int hoverUPEmplacementCoinBalle1;
int hoverUPEmplacementCoinBalle2;
int hoverUPEmplacementCoinBarre1;
int hoverUPEmplacementCoinBarre2;
int hoverUPEmplacementCoin1;
int hoverUPEmplacementCoin2;
int hoverUPEmplacementCoinBonus1;
int hoverUPEmplacementCoinBonus2;

void resetBalleEtBarre(){
    ball.x = WINDOW_WIDTH/2; 
    ball.y = WINDOW_HEIGHT-ecartBandeJeuY;
    ball.vitesseX = VITESSE_BALL;
    ball.vitesseY = -VITESSE_BALL;

    barr.x = WINDOW_WIDTH/2 - barr.longueur/2;
    barr.y = WINDOW_HEIGHT - 2*barr.largeur;

    sprite(0, 500, "assets/tuto.bmp");
}

void afficherPrixUp(int coordY, int prix){
    element = 1;
    int prixInit = prix;

    longueurImgChiffrePetit = 19;
    largeurImgChiffrePetit = 20;
    largeurImgCoinPetit = 23;

    longueurImgVide = 1;

    placementImgDoite = WINDOW_WIDTH - (WINDOW_WIDTH - (coordXEmplacementUPCoin)) + (3 * 83 / 4);


    if(prixInit<money){
        sprite((placementImgDoite), coordY + (83 / 2 - largeurImgChiffrePetit / 2), "assets/imgOrangeCoin.bmp");
    }else{
        sprite((placementImgDoite), coordY + (83 / 2 - largeurImgChiffrePetit / 2), "assets/imgGrisCoin.bmp");
    }
    

    do
    {
        switch (prix % 10)
        {
        case 0:
            if(prixInit<=money){
                sprite((placementImgDoite - element*(longueurImgChiffrePetit+longueurImgVide)), coordY + (83 / 2 - largeurImgChiffrePetit / 2), "assets/imgOrange0.bmp");
            }else{
                sprite((placementImgDoite - element*(longueurImgChiffrePetit+longueurImgVide)), coordY + (83 / 2 - largeurImgChiffrePetit / 2), "assets/imgGris0.bmp");
            }
            break;
        case 1:
            if(prixInit<=money){
                sprite((placementImgDoite - element*(longueurImgChiffrePetit+longueurImgVide)), coordY + (83 / 2 - largeurImgChiffrePetit / 2), "assets/imgOrange1.bmp");
            }else{
                sprite((placementImgDoite - element*(longueurImgChiffrePetit+longueurImgVide)), coordY + (83 / 2 - largeurImgChiffrePetit / 2), "assets/imgGris1.bmp");
            }
            break;
        case 2:
            if(prixInit<=money){
                sprite((placementImgDoite - element*(longueurImgChiffrePetit+longueurImgVide)), coordY + (83 / 2 - largeurImgChiffrePetit / 2), "assets/imgOrange2.bmp");
            }else{
                sprite((placementImgDoite - element*(longueurImgChiffrePetit+longueurImgVide)), coordY + (83 / 2 - largeurImgChiffrePetit / 2), "assets/imgGris2.bmp");
            }
            
            break;
        case 3:
            if(prixInit<=money){
                sprite((placementImgDoite - element*(longueurImgChiffrePetit+longueurImgVide)), coordY + (83 / 2 - largeurImgChiffrePetit / 2), "assets/imgOrange3.bmp");
            }else{
                sprite((placementImgDoite - element*(longueurImgChiffrePetit+longueurImgVide)), coordY + (83 / 2 - largeurImgChiffrePetit / 2), "assets/imgGris3.bmp");
            }
            
            break;
        case 4:
            if(prixInit<=money){
                sprite((placementImgDoite - element*(longueurImgChiffrePetit+longueurImgVide)), coordY + (83 / 2 - largeurImgChiffrePetit / 2), "assets/imgOrange4.bmp");
            }else{
                sprite((placementImgDoite - element*(longueurImgChiffrePetit+longueurImgVide)), coordY + (83 / 2 - largeurImgChiffrePetit / 2), "assets/imgGris4.bmp");
            }
            break;
        case 5:
            if(prixInit<=money){
                sprite((placementImgDoite - element*(longueurImgChiffrePetit+longueurImgVide)), coordY + (83 / 2 - largeurImgChiffrePetit / 2), "assets/imgOrange5.bmp");
            }else{
                sprite((placementImgDoite - element*(longueurImgChiffrePetit+longueurImgVide)), coordY + (83 / 2 - largeurImgChiffrePetit / 2), "assets/imgGris5.bmp");
            }
            break;
        case 6:
            if(prixInit<=money){
                sprite((placementImgDoite - element*(longueurImgChiffrePetit+longueurImgVide)), coordY + (83 / 2 - largeurImgChiffrePetit / 2), "assets/imgOrange6.bmp");
            }else{
                sprite((placementImgDoite - element*(longueurImgChiffrePetit+longueurImgVide)), coordY + (83 / 2 - largeurImgChiffrePetit / 2), "assets/imgGris6.bmp");
            }
            break;
        case 7:
            if(prixInit<=money){
                sprite((placementImgDoite - element*(longueurImgChiffrePetit+longueurImgVide)), coordY + (83 / 2 - largeurImgChiffrePetit / 2), "assets/imgOrange7.bmp");
            }else{
                sprite((placementImgDoite - element*(longueurImgChiffrePetit+longueurImgVide)), coordY + (83 / 2 - largeurImgChiffrePetit / 2), "assets/imgGris7.bmp");
            }
            break;
        case 8:
            if(prixInit<=money){
                sprite((placementImgDoite - element*(longueurImgChiffrePetit+longueurImgVide)), coordY + (83 / 2 - largeurImgChiffrePetit / 2), "assets/imgOrange8.bmp");
            }else{
                sprite((placementImgDoite - element*(longueurImgChiffrePetit+longueurImgVide)), coordY + (83 / 2 - largeurImgChiffrePetit / 2), "assets/imgGris8.bmp");
            }
            break;
        case 9:
            if(prixInit<=money){
                sprite((placementImgDoite - element*(longueurImgChiffrePetit+longueurImgVide)), coordY + (83 / 2 - largeurImgChiffrePetit / 2), "assets/imgOrange9.bmp");
            }else{
                sprite((placementImgDoite - element*(longueurImgChiffrePetit+longueurImgVide)), coordY + (83 / 2 - largeurImgChiffrePetit / 2), "assets/imgGris9.bmp");
            }
            break;
        default:
            break;
        }
        prix = prix / 10;
        element++;
    }while(prix>0);
}

void afficherCoins(int i, int j){
    if((tabPieces[i][j].spawn == 1) && (tabBricks[i][j].spawn == 1)){
        changeColor(255, 247, 56, 255);
        drawCircle((tabPieces[i][j].x), (tabPieces[i][j].y), rayonContourCoins);

        changeColor(166, 162, 61, 255);
        drawCircle((tabPieces[i][j].x), (tabPieces[i][j].y), rayonCoins);

        if(tabPieces[i][j].y + rayonContourCoins + vitesseCoins < WINDOW_HEIGHT){
            tabPieces[i][j].y += vitesseCoins;
        }else{
            // tabPieces[i][j].y = WINDOW_HEIGHT - (rayonContourCoins + 1);
            tabPieces[i][j].spawn = 0;
        }

        if(((tabPieces[i][j].y + rayonContourCoins) >= barr.y) && 
           ((tabPieces[i][j].y - rayonContourCoins)< barr.y) &&
           (tabPieces[i][j].x - rayonContourCoins >= barr.x) && 
           (tabPieces[i][j].x + rayonContourCoins <= barr.x + barr.longueur)
          ){
            tabPieces[i][j].spawn = 0;
            money++;
        }
    }
}

void drawCoins(){
    unit = money;

    longueurImgChiffreNoir = 22;
    largeurImgChiffreNoir = 24;
    largeurImgCoinNoir = 21;

    longueurImgChiffreRouge = 31;
    largeurImgChiffreRouge = 34;
    largeurImgCoinRouge = 21;
    longueurImgVide = 8;

    placementImgDoite = WINDOW_WIDTH - 50 - longueurImgVide;

    element = 1;
    if(niveau != 0){
        sprite((placementImgDoite), (ecartBandeJeuY / 2 - largeurImgCoinNoir / 2), "assets/imgNoirCoin.bmp");
    }else if(pageMenu == 1){
        sprite((placementImgDoite), (ecartCadreY / 2 - largeurImgCoinRouge / 2), "assets/imgRougeCoin.bmp");
    }
    

    do
    {
        switch (unit % 10)
        {
        case 0:
            if(niveau != 0){
                sprite((placementImgDoite - element*(longueurImgChiffreNoir+longueurImgVide)), (ecartBandeJeuY / 2 - largeurImgChiffreNoir / 2), "assets/imgNoir0.bmp");
            }else if(pageMenu == 1){
                sprite((placementImgDoite - element*(longueurImgChiffreRouge+longueurImgVide)), (ecartCadreY / 2 - largeurImgChiffreRouge / 2), "assets/imgRouge0.bmp");
            }
            
            break;
        case 1:
            if(niveau != 0){
                sprite((placementImgDoite - element*(longueurImgChiffreNoir+longueurImgVide)), (ecartBandeJeuY / 2 - largeurImgChiffreNoir / 2), "assets/imgNoir1.bmp");
            }else if(pageMenu == 1){
                sprite((placementImgDoite - element*(longueurImgChiffreRouge+longueurImgVide)), (ecartCadreY / 2 - largeurImgChiffreRouge / 2), "assets/imgRouge1.bmp");
            }
            
            break;
        case 2:
            if(niveau != 0){
                sprite((placementImgDoite - element*(longueurImgChiffreNoir+longueurImgVide)), (ecartBandeJeuY / 2 - largeurImgChiffreNoir / 2), "assets/imgNoir2.bmp");
            }else if(pageMenu == 1){
                sprite((placementImgDoite - element*(longueurImgChiffreRouge+longueurImgVide)), (ecartCadreY / 2 - largeurImgChiffreRouge / 2), "assets/imgRouge2.bmp");
            }
            break;
        case 3:
            if(niveau != 0){
                sprite((placementImgDoite - element*(longueurImgChiffreNoir+longueurImgVide)), (ecartBandeJeuY / 2 - largeurImgChiffreNoir / 2), "assets/imgNoir3.bmp");
            }else if(pageMenu == 1){
                sprite((placementImgDoite - element*(longueurImgChiffreRouge+longueurImgVide)), (ecartCadreY / 2 - largeurImgChiffreRouge / 2), "assets/imgRouge3.bmp");
            }
            break;
        case 4:
            if(niveau != 0){
                sprite((placementImgDoite - element*(longueurImgChiffreNoir+longueurImgVide)), (ecartBandeJeuY / 2 - largeurImgChiffreNoir / 2), "assets/imgNoir4.bmp");
            }else if(pageMenu == 1){
                sprite((placementImgDoite - element*(longueurImgChiffreRouge+longueurImgVide)), (ecartCadreY / 2 - largeurImgChiffreRouge / 2), "assets/imgRouge4.bmp");
            }
            break;
        case 5:
            if(niveau != 0){
                sprite((placementImgDoite - element*(longueurImgChiffreNoir+longueurImgVide)), (ecartBandeJeuY / 2 - largeurImgChiffreNoir / 2), "assets/imgNoir5.bmp");
            }else if(pageMenu == 1){
                sprite((placementImgDoite - element*(longueurImgChiffreRouge+longueurImgVide)), (ecartCadreY / 2 - largeurImgChiffreRouge / 2), "assets/imgRouge5.bmp");
            }
            break;
        case 6:
            if(niveau != 0){
                sprite((placementImgDoite - element*(longueurImgChiffreNoir+longueurImgVide)), (ecartBandeJeuY / 2 - largeurImgChiffreNoir / 2), "assets/imgNoir6.bmp");
            }else if(pageMenu == 1){
                sprite((placementImgDoite - element*(longueurImgChiffreRouge+longueurImgVide)), (ecartCadreY / 2 - largeurImgChiffreRouge / 2), "assets/imgRouge6.bmp");
            }
            break;
        case 7:
            if(niveau != 0){
                sprite((placementImgDoite - element*(longueurImgChiffreNoir+longueurImgVide)), (ecartBandeJeuY / 2 - largeurImgChiffreNoir / 2), "assets/imgNoir7.bmp");
            }else if(pageMenu == 1){
                sprite((placementImgDoite - element*(longueurImgChiffreRouge+longueurImgVide)), (ecartCadreY / 2 - largeurImgChiffreRouge / 2), "assets/imgRouge7.bmp");
            }
            break;
        case 8:
            if(niveau != 0){
                sprite((placementImgDoite - element*(longueurImgChiffreNoir+longueurImgVide)), (ecartBandeJeuY / 2 - largeurImgChiffreNoir / 2), "assets/imgNoir8.bmp");
            }else if(pageMenu == 1){
                sprite((placementImgDoite - element*(longueurImgChiffreRouge+longueurImgVide)), (ecartCadreY / 2 - largeurImgChiffreRouge / 2), "assets/imgRouge8.bmp");
            }
            break;
        case 9:
            if(niveau != 0){
                sprite((placementImgDoite - element*(longueurImgChiffreNoir+longueurImgVide)), (ecartBandeJeuY / 2 - largeurImgChiffreNoir / 2), "assets/imgNoir9.bmp");
            }else if(pageMenu == 1){
                sprite((placementImgDoite - element*(longueurImgChiffreRouge+longueurImgVide)), (ecartCadreY / 2 - largeurImgChiffreRouge / 2), "assets/imgRouge9.bmp");
            }
            break;
        default:
            break;
        }
        unit = unit / 10;
        element++;
    }while(unit>0);
}

/*-----------------------------------COEURS------------------------------------*/
void drawCoeurs(){
    //---------------------------------AFFICHE---------------------------------
    for(int i=0 ; i<ball.vie ; i++){
        sprite(i*48,13,"assets/heart.bmp");
    }
    //--------------------------PERTE DE VIE et PAUSE--------------------------
    if(ball.yBas >= WINDOW_HEIGHT){
        ball.vie--;
        resetBalleEtBarre();

        for(int i=0 ; i<nombreLigne ; i++){
            for(int j=0 ; j<nombreColonne ; j++){
                if((tabPieces[i][j].spawn == 1) && (tabBricks[i][j].spawn == 1) && (tabBricks[i][j].vie == 0)){
                    tabBricks[i][j].spawn = 0;
                }
            }
        }


        start = 0;


    }
    // if((ball.yBas >= WINDOW_HEIGHT) && (ball.y<WINDOW_HEIGHT) && (ball.vitesseY>0)){
    //     ball.vitesseY = -ball.vitesseY;
    // }
}
/*-----------------------------------------------------------------------------*/

/*-----------------------------------BRICKS------------------------------------*/
void initBricks(){
    if(niveau==1){
        for(int i=0 ; i<nombreLigne ; i++){
            for(int j=0 ; j<nombreColonne; j++){
                tabBricks[i][j].vie = 1;
                tabBricks[i][j].spawn = 1;
                nbRestantBrick++;
            }
        }
        chargementBrick = 0;
    }
    else if(niveau==2){
        for(int i=0 ; i<nombreLigne ; i++){
            for(int j=0 ; j<nombreColonne; j++){
                if(((i==2) || (i==3)) && ((j==2) || (j==3))){
                    tabBricks[i][j].vie = 1;
                    tabBricks[i][j].spawn = 1;
                    nbRestantBrick++;
                }
                else if(((i==4) || (i==5)) && ((j>=1) && (j<=4))){ //juste pour que se soit plus lisible
                    tabBricks[i][j].vie = 1;
                    tabBricks[i][j].spawn = 1;
                    nbRestantBrick++;
                }
                else{
                    tabBricks[i][j].vie = 2;
                    tabBricks[i][j].spawn = 1;
                    nbRestantBrick++;
                }
            }
        }
        chargementBrick = 0;
    }
    else if(niveau==3){
        for(int i=0 ; i<nombreLigne ; i++){
            for(int j=0 ; j<nombreColonne; j++){
                // tabBrick[i][j] = 1;
                if(i<=1){
                    tabBricks[i][j].vie = 3;
                    tabBricks[i][j].spawn = 1;
                    nbRestantBrick++;
                }
                else if(((i>1) && (i<6)) || ((i==7) && (i<=9) && ((j<2) || (j>3)))){ //juste pour que se soit plus lisible
                    tabBricks[i][j].vie = 2;
                    tabBricks[i][j].spawn = 1;
                    nbRestantBrick++;
                }
                else if((i>7) && (i<=9) && ((j<2) || (j>3))){
                    tabBricks[i][j].vie = 1;
                    tabBricks[i][j].spawn = 1;
                    nbRestantBrick++;
                }
                else{
                    tabBricks[i][j].vie = 0;
                    tabBricks[i][j].spawn = 0;
                }
            }
        }
        chargementBrick = 0;
    }
    else if(niveau==4){
        for(int i=0 ; i<nombreLigne ; i++){
            for(int j=0 ; j<nombreColonne; j++){
                if((i>=3) && (i<=6)){
                    if((((i==3) || (i==6)) && ((j<=1) || (j>=4))) ||
                       (((i>3) && (i<6)) && ((j>1) && (j<4)))
                      ){
                        tabBricks[i][j].vie = 4;
                        tabBricks[i][j].spawn = 1;
                        nbRestantBrick++;
                    }
                    else{
                        tabBricks[i][j].vie = 2;
                        tabBricks[i][j].spawn = 1;
                        nbRestantBrick++; 
                    }
                }
                else{
                    if((((i==2) || (i==7)) && ((j<=1) || (j>=4))) ||
                       (((i<2) || (i>7)) && ((j>1) && (j<4)))
                      ){
                        tabBricks[i][j].vie = 3;
                        tabBricks[i][j].spawn = 1;
                        nbRestantBrick++;
                    }
                    else{
                        tabBricks[i][j].vie = 1;
                        tabBricks[i][j].spawn = 1;
                        nbRestantBrick++;
                    }
                }
            }
        }
        chargementBrick = 0;
    }
    else if(niveau==5){
        for(int i=0 ; i<nombreLigne ; i++){
            for(int j=0 ; j<nombreColonne; j++){
                if(((i==0) && ((j==0) || (j==5))) ||
                   ((i==9) && ((j==0) || (j==5)))
                  ){
                    tabBricks[i][j].vie = 1;
                    tabBricks[i][j].spawn = 1;
                    nbRestantBrick++;
                }
                else if(((i>=4) && (i<=5) && (j>=2) && (j<=3))){
                    tabBricks[i][j].vie = 3;
                    tabBricks[i][j].spawn = 1;
                    nbRestantBrick++;
                }
                else if((((i==3) || (i==6)) && (j>=2) && (j<=3))){
                    tabBricks[i][j].vie = 4;
                    tabBricks[i][j].spawn = 1;
                    nbRestantBrick++;
                }
                else if((((i>=3) && (i<=6)) && ((j>3) || (j<2))) ||
                        (((i==2) || (i==7)) && (j>=1) && (j<=4)) ||
                        (((i==1) || (i==8)) && (j>=2) && (j<=3))
                ){
                    tabBricks[i][j].vie = 5;
                    tabBricks[i][j].spawn = 1;
                    nbRestantBrick++;
                }
                else{
                    tabBricks[i][j].vie = 6;
                    tabBricks[i][j].spawn = 1;
                    nbRestantBrick++;
                }
            }
        }
        chargementBrick = 0;
    }
}

void rebonBalleBricks(int i, int j){
    /*---------------------------Rebond----------------------------*/
    if(((ball.vitesseX > 0) && (ball.x < tabBricks[i][j].xContourBrickGauche) && (ball.xDroite >= tabBricks[i][j].xContourBrickGauche)) && (ball.y >= tabBricks[i][j].yContourBrickHaut) && (ball.y <= tabBricks[i][j].yContourBrickBas) ||
       ((ball.vitesseX < 0) && (ball.x > tabBricks[i][j].xContourBrickDroite) && (ball.xGauche <= tabBricks[i][j].xContourBrickDroite)) && (ball.y >= tabBricks[i][j].yContourBrickHaut) && (ball.y <= tabBricks[i][j].yContourBrickBas)
      ){
        ball.vitesseX = -ball.vitesseX;
        tabBricks[i][j].vie--;
        if(tabBricks[i][j].vie == 0){
            nbRestantBrick--;    
        }
        
    }
    else if(((ball.vitesseY < 0) && (ball.y > tabBricks[i][j].yContourBrickBas) && (ball.yHaut <= tabBricks[i][j].yContourBrickBas)) && (ball.x >= tabBricks[i][j].xContourBrickGauche) && (ball.x <= tabBricks[i][j].xContourBrickDroite) ||
            ((ball.vitesseY > 0) && (ball.y < tabBricks[i][j].yContourBrickHaut) && (ball.yBas >= tabBricks[i][j].yContourBrickHaut)) && (ball.x >= tabBricks[i][j].xContourBrickGauche) && (ball.x <= tabBricks[i][j].xContourBrickDroite)
           ){
        ball.vitesseY = -ball.vitesseY;
        tabBricks[i][j].vie--;
        if(tabBricks[i][j].vie == 0){
            nbRestantBrick--;    
        }
    }
    else{
        if((i>0) && (i<nombreLigne-1) || (j>0) && (j<nombreColonne-1)){
            if((i>0) && (i<nombreLigne-1)){
                if((ball.vitesseX > 0) && (ball.vitesseY > 0) && (tabBricks[i-1][j].vie > 0) && (ball.x < tabBricks[i][j].xContourBrickGauche) && (ball.xDroite >= tabBricks[i][j].xContourBrickGauche) && (ball.y < tabBricks[i][j].yContourBrickHaut) && (ball.yBas > tabBricks[i][j].yContourBrickHaut) ||
                   (ball.vitesseX > 0) && (ball.vitesseY < 0) && (tabBricks[i+1][j].vie > 0) && (ball.x < tabBricks[i][j].xContourBrickGauche) && (ball.xDroite >= tabBricks[i][j].xContourBrickGauche) && (ball.y > tabBricks[i][j].yContourBrickBas) && (ball.yHaut < tabBricks[i][j].yContourBrickBas) ||
                   (ball.vitesseX < 0) && (ball.vitesseY > 0) && (tabBricks[i-1][j].vie > 0) && (ball.x > tabBricks[i][j].xContourBrickDroite) && (ball.xGauche <= tabBricks[i][j].xContourBrickDroite) && (ball.y < tabBricks[i][j].yContourBrickHaut) && (ball.yBas > tabBricks[i][j].yContourBrickHaut) ||
                   (ball.vitesseX < 0) && (ball.vitesseY < 0) && (tabBricks[i+1][j].vie > 0) && (ball.x > tabBricks[i][j].xContourBrickDroite) && (ball.xGauche <= tabBricks[i][j].xContourBrickDroite) && (ball.y > tabBricks[i][j].yContourBrickBas) && (ball.yHaut < tabBricks[i][j].yContourBrickBas)
                  ){
                    ball.vitesseX = -ball.vitesseX;
                    tabBricks[i][j].vie--;
                    if(tabBricks[i][j].vie == 0){
                        nbRestantBrick--;    
                    }
                }
                else if((ball.vitesseX > 0) && (ball.vitesseY > 0) && (tabBricks[i-1][j].vie < 0) && (ball.x < tabBricks[i][j].xContourBrickGauche) && (ball.xDroite >= tabBricks[i][j].xContourBrickGauche) && (ball.y < tabBricks[i][j].yContourBrickHaut) && (ball.yBas > tabBricks[i][j].yContourBrickHaut) ||
                        (ball.vitesseX > 0) && (ball.vitesseY < 0) && (tabBricks[i+1][j].vie < 0) && (ball.x < tabBricks[i][j].xContourBrickGauche) && (ball.xDroite >= tabBricks[i][j].xContourBrickGauche) && (ball.y > tabBricks[i][j].yContourBrickBas) && (ball.yHaut < tabBricks[i][j].yContourBrickBas) ||
                        (ball.vitesseX < 0) && (ball.vitesseY > 0) && (tabBricks[i-1][j].vie < 0) && (ball.x > tabBricks[i][j].xContourBrickDroite) && (ball.xGauche <= tabBricks[i][j].xContourBrickDroite) && (ball.y < tabBricks[i][j].yContourBrickHaut) && (ball.yBas > tabBricks[i][j].yContourBrickHaut) ||
                        (ball.vitesseX < 0) && (ball.vitesseY < 0) && (tabBricks[i+1][j].vie < 0) && (ball.x > tabBricks[i][j].xContourBrickDroite) && (ball.xGauche <= tabBricks[i][j].xContourBrickDroite) && (ball.y > tabBricks[i][j].yContourBrickBas) && (ball.yHaut < tabBricks[i][j].yContourBrickBas)
                       ){
                    ball.vitesseX = -ball.vitesseX;
                    ball.vitesseY = -ball.vitesseY;
                    tabBricks[i][j].vie--;
                    if(tabBricks[i][j].vie == 0){
                        nbRestantBrick--;    
                    }
                }
            }

            if((j>0) && (j<nombreColonne-1)){
                if((ball.vitesseX > 0) && (ball.vitesseY > 0) && (tabBricks[i][j-1].vie > 0) && (ball.x < tabBricks[i][j].xContourBrickGauche) && (ball.xDroite >= tabBricks[i][j].xContourBrickGauche) && (ball.y < tabBricks[i][j].yContourBrickHaut) && (ball.yBas > tabBricks[i][j].yContourBrickHaut) ||
                   (ball.vitesseX > 0) && (ball.vitesseY < 0) && (tabBricks[i][j+1].vie > 0) && (ball.x < tabBricks[i][j].xContourBrickGauche) && (ball.xDroite >= tabBricks[i][j].xContourBrickGauche) && (ball.y > tabBricks[i][j].yContourBrickBas) && (ball.yHaut < tabBricks[i][j].yContourBrickBas) ||
                   (ball.vitesseX < 0) && (ball.vitesseY > 0) && (tabBricks[i][j-1].vie > 0) && (ball.x > tabBricks[i][j].xContourBrickDroite) && (ball.xDroite <= tabBricks[i][j].xContourBrickDroite) && (ball.y < tabBricks[i][j].yContourBrickHaut) && (ball.yBas > tabBricks[i][j].yContourBrickHaut) ||
                   (ball.vitesseX < 0) && (ball.vitesseY < 0) && (tabBricks[i][j+1].vie > 0) && (ball.x > tabBricks[i][j].xContourBrickDroite) && (ball.xGauche <= tabBricks[i][j].xContourBrickDroite) && (ball.y > tabBricks[i][j].yContourBrickBas) && (ball.yHaut < tabBricks[i][j].yContourBrickBas)
                  ){
                    ball.vitesseY = -ball.vitesseY;
                    tabBricks[i][j].vie--;
                    if(tabBricks[i][j].vie == 0){
                        nbRestantBrick--;    
                    }
                }
                else if((ball.vitesseX > 0) && (ball.vitesseY > 0) && (tabBricks[i][j-1].vie < 0) && (ball.x < tabBricks[i][j].xContourBrickGauche) && (ball.xDroite >= tabBricks[i][j].xContourBrickGauche) && (ball.y < tabBricks[i][j].yContourBrickHaut) && (ball.yBas > tabBricks[i][j].yContourBrickHaut) ||
                        (ball.vitesseX > 0) && (ball.vitesseY < 0) && (tabBricks[i][j+1].vie < 0) && (ball.x < tabBricks[i][j].xContourBrickGauche) && (ball.xDroite >= tabBricks[i][j].xContourBrickGauche) && (ball.y > tabBricks[i][j].yContourBrickBas) && (ball.yHaut < tabBricks[i][j].yContourBrickBas) ||
                        (ball.vitesseX < 0) && (ball.vitesseY > 0) && (tabBricks[i][j-1].vie < 0) && (ball.x > tabBricks[i][j].xContourBrickDroite) && (ball.xDroite <= tabBricks[i][j].xContourBrickDroite) && (ball.y < tabBricks[i][j].yContourBrickHaut) && (ball.yBas > tabBricks[i][j].yContourBrickHaut) ||
                        (ball.vitesseX < 0) && (ball.vitesseY < 0) && (tabBricks[i][j+1].vie < 0) && (ball.x > tabBricks[i][j].xContourBrickDroite) && (ball.xGauche <= tabBricks[i][j].xContourBrickDroite) && (ball.y > tabBricks[i][j].yContourBrickBas) && (ball.yHaut < tabBricks[i][j].yContourBrickBas)
                       ){
                    ball.vitesseX = -ball.vitesseX;
                    ball.vitesseY = -ball.vitesseY;
                    tabBricks[i][j].vie--;
                    if(tabBricks[i][j].vie == 0){
                        nbRestantBrick--;    
                    }
                }
            }
        }
        else if((i==0) || (j==0)){
            if(i==0){
                if((ball.vitesseX > 0) && (ball.vitesseY < 0) && (tabBricks[i+1][j].vie > 0) && (ball.x < tabBricks[i][j].xContourBrickGauche) && (ball.xDroite >= tabBricks[i][j].xContourBrickGauche) && (ball.y > tabBricks[i][j].yContourBrickBas) && (ball.yHaut < tabBricks[i][j].yContourBrickBas) ||
                   (ball.vitesseX < 0) && (ball.vitesseY < 0) && (tabBricks[i+1][j].vie > 0) && (ball.x > tabBricks[i][j].xContourBrickDroite) && (ball.xGauche <= tabBricks[i][j].xContourBrickDroite) && (ball.y > tabBricks[i][j].yContourBrickBas) && (ball.yHaut < tabBricks[i][j].yContourBrickBas)
                  ){
                    ball.vitesseX = -ball.vitesseX;
                    tabBricks[i][j].vie--;
                    if(tabBricks[i][j].vie == 0){
                        nbRestantBrick--;    
                    }
                }
                else if((ball.vitesseX > 0) && (ball.vitesseY < 0) && (tabBricks[i+1][j].vie < 0) && (ball.x < tabBricks[i][j].xContourBrickGauche) && (ball.xDroite >= tabBricks[i][j].xContourBrickGauche) && (ball.y > tabBricks[i][j].yContourBrickBas) && (ball.yHaut < tabBricks[i][j].yContourBrickBas) ||
                        (ball.vitesseX < 0) && (ball.vitesseY < 0) && (tabBricks[i+1][j].vie < 0) && (ball.x > tabBricks[i][j].xContourBrickDroite) && (ball.xGauche <= tabBricks[i][j].xContourBrickDroite) && (ball.y > tabBricks[i][j].yContourBrickBas) && (ball.yHaut < tabBricks[i][j].yContourBrickBas)
                       ){
                    ball.vitesseX = -ball.vitesseX;
                    ball.vitesseY = -ball.vitesseY;
                    tabBricks[i][j].vie--;
                    if(tabBricks[i][j].vie == 0){
                        nbRestantBrick--;    
                    }
                }
            }
            else if(j==0){
                if((ball.vitesseX < 0) && (ball.vitesseY > 0) && (tabBricks[i][j-1].vie > 0) && (ball.x > tabBricks[i][j].xContourBrickDroite) && (ball.xDroite <= tabBricks[i][j].xContourBrickDroite) && (ball.y < tabBricks[i][j].yContourBrickHaut) && (ball.yBas > tabBricks[i][j].yContourBrickHaut) ||
                   (ball.vitesseX < 0) && (ball.vitesseY < 0) && (tabBricks[i][j+1].vie > 0) && (ball.x > tabBricks[i][j].xContourBrickDroite) && (ball.xGauche <= tabBricks[i][j].xContourBrickDroite) && (ball.y > tabBricks[i][j].yContourBrickBas) && (ball.yHaut < tabBricks[i][j].yContourBrickBas)
                  ){
                    ball.vitesseY = -ball.vitesseY;
                    tabBricks[i][j].vie--;
                    if(tabBricks[i][j].vie == 0){
                        nbRestantBrick--;    
                    }
                }
                else if((ball.vitesseX < 0) && (ball.vitesseY > 0) && (tabBricks[i][j-1].vie < 0) && (ball.x > tabBricks[i][j].xContourBrickDroite) && (ball.xDroite <= tabBricks[i][j].xContourBrickDroite) && (ball.y < tabBricks[i][j].yContourBrickHaut) && (ball.yBas > tabBricks[i][j].yContourBrickHaut) ||
                        (ball.vitesseX < 0) && (ball.vitesseY < 0) && (tabBricks[i][j+1].vie < 0) && (ball.x > tabBricks[i][j].xContourBrickDroite) && (ball.xGauche <= tabBricks[i][j].xContourBrickDroite) && (ball.y > tabBricks[i][j].yContourBrickBas) && (ball.yHaut < tabBricks[i][j].yContourBrickBas)
                       ){
                    ball.vitesseX = -ball.vitesseX;
                    ball.vitesseY = -ball.vitesseY;
                    tabBricks[i][j].vie--;
                    if(tabBricks[i][j].vie == 0){
                        nbRestantBrick--;    
                    }
                }
            }
        }
        else if((i==nombreLigne-1) || (j==nombreColonne-1)){
            if(i==nombreLigne-1){
                if((ball.vitesseX > 0) && (ball.vitesseY > 0) && (tabBricks[i-1][j].vie > 0) && (ball.x < tabBricks[i][j].xContourBrickGauche) && (ball.xDroite >= tabBricks[i][j].xContourBrickGauche) && (ball.y < tabBricks[i][j].yContourBrickHaut) && (ball.yBas > tabBricks[i][j].yContourBrickHaut) ||
                   (ball.vitesseX < 0) && (ball.vitesseY > 0) && (tabBricks[i-1][j].vie > 0) && (ball.x > tabBricks[i][j].xContourBrickDroite) && (ball.xGauche <= tabBricks[i][j].xContourBrickDroite) && (ball.y < tabBricks[i][j].yContourBrickHaut) && (ball.yBas > tabBricks[i][j].yContourBrickHaut)
                  ){
                    ball.vitesseX = -ball.vitesseX;
                    tabBricks[i][j].vie--;
                    if(tabBricks[i][j].vie == 0){
                        nbRestantBrick--;    
                    }
                }
                else if((ball.vitesseX > 0) && (ball.vitesseY > 0) && (tabBricks[i-1][j].vie < 0) && (ball.x < tabBricks[i][j].xContourBrickGauche) && (ball.xDroite >= tabBricks[i][j].xContourBrickGauche) && (ball.y < tabBricks[i][j].yContourBrickHaut) && (ball.yBas > tabBricks[i][j].yContourBrickHaut) ||
                        (ball.vitesseX < 0) && (ball.vitesseY > 0) && (tabBricks[i-1][j].vie < 0) && (ball.x > tabBricks[i][j].xContourBrickDroite) && (ball.xGauche <= tabBricks[i][j].xContourBrickDroite) && (ball.y < tabBricks[i][j].yContourBrickHaut) && (ball.yBas > tabBricks[i][j].yContourBrickHaut)
                       ){
                    ball.vitesseX = -ball.vitesseX;
                    ball.vitesseY = -ball.vitesseY;
                    tabBricks[i][j].vie--;
                    if(tabBricks[i][j].vie == 0){
                        nbRestantBrick--;    
                    }
                }
            }
            if(j==nombreColonne-1){
                if((ball.vitesseX > 0) && (ball.vitesseY > 0) && (tabBricks[i][j-1].vie > 0) && (ball.x < tabBricks[i][j].xContourBrickGauche) && (ball.xDroite >= tabBricks[i][j].xContourBrickGauche) && (ball.y < tabBricks[i][j].yContourBrickHaut) && (ball.yBas > tabBricks[i][j].yContourBrickHaut) ||
                   (ball.vitesseX > 0) && (ball.vitesseY < 0) && (tabBricks[i][j-1].vie > 0) && (ball.x < tabBricks[i][j].xContourBrickGauche) && (ball.xDroite >= tabBricks[i][j].xContourBrickGauche) && (ball.y > tabBricks[i][j].yContourBrickBas) && (ball.yHaut < tabBricks[i][j].yContourBrickBas)
                  ){
                    ball.vitesseY = -ball.vitesseY;
                    tabBricks[i][j].vie--;
                    if(tabBricks[i][j].vie == 0){
                        nbRestantBrick--;    
                    }
                }
                else if((ball.vitesseX > 0) && (ball.vitesseY > 0) && (tabBricks[i][j-1].vie < 0) && (ball.x < tabBricks[i][j].xContourBrickGauche) && (ball.xDroite >= tabBricks[i][j].xContourBrickGauche) && (ball.y < tabBricks[i][j].yContourBrickHaut) && (ball.yBas > tabBricks[i][j].yContourBrickHaut) ||
                        (ball.vitesseX > 0) && (ball.vitesseY < 0) && (tabBricks[i][j-1].vie < 0) && (ball.x < tabBricks[i][j].xContourBrickGauche) && (ball.xDroite >= tabBricks[i][j].xContourBrickGauche) && (ball.y > tabBricks[i][j].yContourBrickBas) && (ball.yHaut < tabBricks[i][j].yContourBrickBas)
                       ){
                    ball.vitesseY = -ball.vitesseY;
                    tabBricks[i][j].vie--;
                    if(tabBricks[i][j].vie == 0){
                        nbRestantBrick--;    
                    }
                }
            }
        } 
        
        //securite
        if(((ball.vitesseX > 0) && (ball.vitesseY > 0) && (ball.x < tabBricks[i][j].xContourBrickGauche) && (ball.xDroite >= tabBricks[i][j].xContourBrickGauche) && (ball.y < tabBricks[i][j].yContourBrickHaut) && (ball.yBas > tabBricks[i][j].yContourBrickHaut)) ||
                    ((ball.vitesseX > 0) && (ball.vitesseY < 0) && (ball.x < tabBricks[i][j].xContourBrickGauche) && (ball.xDroite >= tabBricks[i][j].xContourBrickGauche) && (ball.y > tabBricks[i][j].yContourBrickBas) && (ball.yHaut < tabBricks[i][j].yContourBrickBas)) ||
                    ((ball.vitesseX < 0) && (ball.vitesseY > 0) && (ball.x > tabBricks[i][j].xContourBrickDroite) && (ball.xGauche <= tabBricks[i][j].xContourBrickDroite) && (ball.y < tabBricks[i][j].yContourBrickHaut) && (ball.yBas > tabBricks[i][j].yContourBrickHaut)) ||
                    ((ball.vitesseX < 0) && (ball.vitesseY < 0) && (ball.x > tabBricks[i][j].xContourBrickDroite) && (ball.xGauche <= tabBricks[i][j].xContourBrickDroite) && (ball.y > tabBricks[i][j].yContourBrickBas) && (ball.yHaut < tabBricks[i][j].yContourBrickBas))
                   ){
                ball.vitesseX = -ball.vitesseX;
                ball.vitesseY = -ball.vitesseY;
                tabBricks[i][j].vie--;
                if(tabBricks[i][j].vie == 0){
                    nbRestantBrick--;    
                }
            }
        
    }
    /*-------------------------------------------------------------*/
}

void afficherContourBrick(int i, int j){
    drawRect(tabBricks[i][j].xContourBrickGauche, 
             tabBricks[i][j].yContourBrickHaut,
             longueurBrickContour,
             largeurBrickContour
            );
}

void afficherBrick(int i, int j){
    drawRect(tabBricks[i][j].xContourBrickGauche + 1, 
             tabBricks[i][j].yContourBrickHaut + 1,
             longueurBrick,
             largeurBrick
            );
}

void afficherBricks(int i, int j){
    //--------------------En fonction de la vie des briques--------------------
    switch (tabBricks[i][j].vie)
    {
        case 1:
            //contour Brick
            changeColor(92, 225, 230, 255);
            afficherContourBrick(i, j);

            //Brick
            changeColor(89,170,173,255);
            afficherBrick(i, j);
            break;

        case 2:
            //contour Brick
            changeColor(130, 188, 255, 255);
            afficherContourBrick(i, j);

            //Brick
            changeColor(89, 128, 173, 255);
            afficherBrick(i, j);
            break;
        case 3:
            //contour Brick
            changeColor(15, 127, 244, 255);
            afficherContourBrick(i, j);

            //Brick
            changeColor(73, 77, 184, 255);
            afficherBrick(i, j);
            break;
        case 4:
            //contour Brick
            changeColor(171, 126, 255, 255);
            afficherContourBrick(i, j);

            //Brick
            changeColor(118, 89, 173, 255);
            afficherBrick(i, j);
            break;
        case 5:
            //contour Brick
            changeColor(233, 122, 255, 255);
            afficherContourBrick(i, j);

            //Brick
            changeColor(159, 89, 173, 255);
            afficherBrick(i, j);
            break;
        case 6:
            //contour Brick
            changeColor(248, 122, 189, 255);
            afficherContourBrick(i, j);

            //Brick
            changeColor(173, 89, 134, 255);
            afficherBrick(i, j);
            break;
        default:
            break;
    }
}

void drawBricks(){
    for(int i=0 ; i<nombreLigne ; i++){
        for(int j=0 ; j<nombreColonne; j++){
            if(tabBricks[i][j].vie>0){
                //---------------------------AFFICHER--------------------------
                afficherBricks(i, j);

                //----------------------------REBOND---------------------------
                rebonBalleBricks(i, j);                
            }
            else{
                afficherCoins(i, j);
            }
        }
    }
}
/*-----------------------------------------------------------------------------*/

/*------------------------------------BARRE------------------------------------*/
void afficheBarre(){
    /*------------------------------AFFICHE BARRE------------------------------*/
    // drawRect(barr.x+(barr.largeur/2), barr.y, barr.longueur-barr.largeur, barr.largeur);
    // drawCircle(barr.x+(barr.largeur/2), barr.y+(barr.largeur/2), barr.largeur/2);
    // drawCircle(barr.x+barr.longueur-(barr.largeur/2), barr.y+(barr.largeur/2), barr.largeur/2);

    drawRect(barr.x, barr.y, barr.longueur, barr.largeur);
    /*-------------------------------------------------------------------------*/
}

void drawBarre(){
    //---------------------------------VITESSE---------------------------------
    barr.x += barr.vitesse;

    /*------------------------LIMITE DEPLACEMENT BORDURE-----------------------*/
    if(barr.x < 0){
        barr.x = 0;
        barr.vitesse = 0;
    }else if((barr.x + barr.longueur) > WINDOW_WIDTH){
        barr.x = WINDOW_WIDTH - barr.longueur;
        barr.vitesse = 0;
    }
    /*-------------------------------------------------------------------------*/

    afficheBarre();
}
/*-----------------------------------------------------------------------------*/

/*------------------------------------BALLE------------------------------------*/
void updateCoordBalle(){
    ball.xDroite = ball.x + ball.rayon;
    ball.xGauche = ball.x - ball.rayon;
    ball.yHaut = ball.y - ball.rayon;
    ball.yBas = ball.y + ball.rayon;
}

void rebondBalle(){
    /*----------------------------Bords de l'écran-----------------------------*/
    if(((ball.vitesseX < 0) && (ball.xGauche<=0)) || ((ball.vitesseX > 0) && (ball.xDroite>=WINDOW_WIDTH))){
        ball.vitesseX = -ball.vitesseX;
    }
    else if((ball.vitesseY < 0) && (ball.yHaut<=ecartBandeJeuY)){
        ball.vitesseY = -ball.vitesseY;
    }
    /*-------------------------------------------------------------------------*/
    /*----------------------------------Barre----------------------------------*/
    if((((ball.vitesseX < 0) && (ball.x >= barr.x+barr.longueur) && (ball.xGauche <= barr.x+barr.longueur)) && (ball.y >= barr.y) && (ball.y <= barr.y+barr.largeur)) ||
       (((ball.vitesseX > 0) && (ball.x < barr.x) && (ball.xDroite >= barr.x)) && (ball.y >= barr.y) && (ball.y <= barr.y+barr.largeur))
      ){
        ball.vitesseX = -ball.vitesseX;
    }
    else if((ball.vitesseY > 0) && ((ball.y < barr.y) && (ball.yBas >= barr.y)) && (ball.x >= barr.x) && (ball.x <= barr.x+barr.longueur)){
        ball.vitesseY = -ball.vitesseY;
    }
    else if(((ball.vitesseX > 0) && (ball.vitesseY > 0) && (ball.x <= barr.x) && (ball.xDroite >= barr.x) && (ball.y <= barr.y) && (ball.yBas >= barr.y)) ||
            ((ball.vitesseX < 0) && (ball.vitesseY > 0) && (ball.x >= barr.x+barr.longueur) && (ball.xGauche <= barr.x+barr.longueur) && (ball.y <= barr.y) && (ball.yBas >= barr.y))
           ){
        ball.vitesseX = -ball.vitesseX;
        ball.vitesseY = -ball.vitesseY;
    }
    /*-------------------------------------------------------------------------*/
    /*----------------------------------Bricks---------------------------------*/
    //Dans drawBrick() à cause du tabBrick. Pas de solution sans tout casser
    /*-------------------------------------------------------------------------*/
}

void afficherBalle(){
    //---------------------------------AFFICHE---------------------------------
    if(start==1){
        for(int i=ball.rayon ; i>0 ; i--){
            if(((ball.xGauche - (ball.vitesseX * i)) > 0) && ((ball.xDroite - (ball.vitesseX * i)) < WINDOW_WIDTH) && ((ball.yHaut - (ball.vitesseY * i)) > ecartBandeJeuY)){
                changeColor(255,255,255,(255 / i));
                drawCircle((ball.x - (ball.vitesseX * i)), (ball.y - (ball.vitesseY * i)), (ball.rayon - i));
            }
             
        }
    }
    changeColor(255,255,255,255);
    drawCircle(ball.x, ball.y, ball.rayon);
}

void drawBalle(){
    updateCoordBalle();

    rebondBalle();

    //---------------------------------VITESSE---------------------------------
    ball.x += ball.vitesseX;
    ball.y += ball.vitesseY;

    afficherBalle();
}
/*-----------------------------------------------------------------------------*/

void resetVarGame(){
    /*----------------------Initialisation BALLE et BARRE----------------------*/
    ball.vie = 3;

    resetBalleEtBarre();    
    /*-------------------------------------------------------------------------*/

    /*--------------------------Initialisation COEUR---------------------------*/
    chargementCoeur = 1;
    /*-------------------------------------------------------------------------*/
    

    /*--------------------------Initialisation BRICKS--------------------------*/
    init_chargeBricks = 0;

    chargementBrick = 1;
    nbRestantBrick = 0;
    /*-------------------------------------------------------------------------*/

    /*--------------------------Initialisation COINS---------------------------*/
    /*-------------------------------------------------------------------------*/

    /*---------------------------Initialisation GAME---------------------------*/
    start = 0;
    
    niveau = 0;
    pageMenu = 0;
    pauseGame = -1;
    pauseMenu = -1;
    /*-------------------------------------------------------------------------*/
}

void background(){
    clear();
    changeColor(115, 115, 115, 255);
    drawRect(0, ecartBandeJeuY, WINDOW_WIDTH, WINDOW_HEIGHT-ecartBandeJeuY);
}

void game(){
    background();

    drawCoeurs();
    drawCoins();

    drawBalle();
    drawBarre();
    drawBricks();

    //Arrêt du jeu et renvoie au menu
    if((ball.vie == 0) || (nbRestantBrick == 0)){
        clear();
        start--;
        niveau = 0;
        pageMenu = 0;
    }
}

void afficherAvantGame(){
    if(chargementBrick==1){
        initBricks();
    }

    background();

    resetBalleEtBarre();

    drawCoeurs();
    drawCoins();

    afficherBalle();        
    afficheBarre();   
    drawBricks();

    sprite(0, 500, "assets/tuto.bmp");

    actualize();
}

/*-----------------------------------NIVEAUX-----------------------------------*/
void lancerNiveau(){
    if(init_chargeBricks==0){
        chargementBrick = 1;
        init_chargeBricks++;
    }

    if((start==0) && (pauseGame<0)){
        afficherAvantGame();
    }
    if(start==1){
        game();
    }
}
//-----------------------------------NIVEAU1-----------------------------------
void niveau1(){
    nombreLigne=6;
    nombreColonne=6;

    lancerNiveau();
}
//-----------------------------------NIVEAU2-----------------------------------
void niveau2(){
    nombreLigne=6;
    nombreColonne=6;

    lancerNiveau();
}
//-----------------------------------NIVEAU3-----------------------------------
void niveau3(){
    nombreLigne=10;
    nombreColonne=6;

    lancerNiveau();
}
//-----------------------------------NIVEAU4-----------------------------------
void niveau4(){
    nombreLigne=10;
    nombreColonne=6;

    lancerNiveau();
}
//-----------------------------------NIVEAU5-----------------------------------
void niveau5(){
    nombreLigne=10;
    nombreColonne=6;

    lancerNiveau();
}
/*-----------------------------------------------------------------------------*/

void menuChoixNiveau(){
    //RESET VARIABLE avant chaque début de niveau
    resetVarGame();

    changeColor(115, 115, 115, 255);
    drawRect(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);

    if(hoverNiveau1==1){
        sprite(72, 177, "assets/imgGrandN1.bmp");
    }else{
        sprite(87,180,"assets/imgPetitN1.bmp");
    }

    if(hoverNiveau2==1){
        sprite(72, 267, "assets/imgGrandN2.bmp");
    }else{
        sprite(87,270,"assets/imgPetitN2.bmp");
    }

    if(hoverNiveau3==1){
        sprite(72, 357, "assets/imgGrandN3.bmp");
    }else{
        sprite(87,360,"assets/imgPetitN3.bmp");
    }

    if(hoverNiveau4==1){
        sprite(72, 452, "assets/imgGrandN4.bmp");
    }else{
        sprite(87,455,"assets/imgPetitN4.bmp");
    }

    if(hoverNiveau5==1){
        sprite(72, 542, "assets/imgGrandN5.bmp");
    }else{
        sprite(87,545,"assets/imgPetitN5.bmp");
    }

    ecartLogo = (WINDOW_WIDTH - (4*90 + 3*19))/2;

    if(hoverLogoNiveau == 1){
        sprite(ecartLogo, WINDOW_HEIGHT - 121, "assets/logoNiveauNiveau.bmp");
    }else{
        sprite(ecartLogo, WINDOW_HEIGHT - 101, "assets/logoNiveauNiveau.bmp");
    }
    if(hoverLogoUp== 1){
        sprite(ecartLogo + 109, WINDOW_HEIGHT - 121, "assets/logoNiveauUp.bmp");
    }else{
        sprite(ecartLogo + 109, WINDOW_HEIGHT - 101, "assets/logoNiveauUp.bmp");
    }
    if(hoverLogoSkin == 1){
        sprite(ecartLogo + 2 * 109, WINDOW_HEIGHT - 121, "assets/logoNiveauSkin.bmp");
    }else{
        sprite(ecartLogo + 2 * 109, WINDOW_HEIGHT - 101, "assets/logoNiveauSkin.bmp");
    }
    if(hoverLogoSetting == 1){
        sprite(ecartLogo + 3 * 109, WINDOW_HEIGHT - 121, "assets/logoNiveauSetting.bmp");
    }else{
        sprite(ecartLogo + 3 * 109, WINDOW_HEIGHT - 101, "assets/logoNiveauSetting.bmp");
    }
}

void menuChoixUp(){
    coordXEmplacementUP = ecartCadreX + ecartXEmplacementUP;
    coordXEmplacementUPCoin = coordXEmplacementUP + longueurEmplacementUP + espaceXEmplacementUP;

    coordYEmplacementUP = ecartYEmplacementUP + largeurEmplacementUP + espaceYEmplacementUP;

    coordYCadreUPJoueur = ecartCadreY + varScroll;
    coordYCadreUPBalle = coordYCadreUPJoueur + cadreUpJoueurH + espaceCadre;
    coordYCadreUPBarre = coordYCadreUPBalle + cadreUpBalleH + espaceCadre;
    coordYCadreUPCoin = coordYCadreUPBarre + cadreUpBarreH + espaceCadre;
    coordYCadreUPBonus = coordYCadreUPCoin + cadreUpCoinH + espaceCadre;

    changeColor(71, 2, 14, 255);
    drawRect(0, ecartCadreY, WINDOW_WIDTH, WINDOW_HEIGHT - ecartCadreY);

/*------------------------------Partie SCROLLING-------------------------------*/
    //--------------------------------Cadre JOUEUR-------------------------------
    sprite(ecartCadreX, coordYCadreUPJoueur, "assets/cadreUpJoueur.bmp");
    if(tabPrixUP[0]<=money){
        if(hoverUPEmplacementCoinJoueur == 1){
            changeColor(255,255,60,255);
            drawRect(coordXEmplacementUPCoin - 2, coordYCadreUPJoueur + ecartYEmplacementUP - 2, 90+4, largeurEmplacementUP+4);
        }
        sprite(coordXEmplacementUP, coordYCadreUPJoueur + ecartYEmplacementUP, "assets/emplacementUpOrangeCoeur.bmp");
        sprite(coordXEmplacementUPCoin, coordYCadreUPJoueur + ecartYEmplacementUP, "assets/emplacementUpOrangeCoin.bmp");
    }else{
        sprite(coordXEmplacementUP, coordYCadreUPJoueur + ecartYEmplacementUP, "assets/emplacementUpCoeur.bmp");
        sprite(coordXEmplacementUPCoin, coordYCadreUPJoueur + ecartYEmplacementUP, "assets/emplacementUpCoin.bmp");
    }
    afficherPrixUp(coordYCadreUPJoueur + ecartYEmplacementUP, tabPrixUP[0]);
    //---------------------------------------------------------------------------
    
    //--------------------------------Cadre BALLE--------------------------------
    sprite(ecartCadreX, coordYCadreUPBalle, "assets/cadreUpBalle.bmp");
    if(tabPrixUP[1]<=money){
        if(hoverUPEmplacementCoinBalle1 == 1){
            changeColor(255,255,60,255);
            drawRect(coordXEmplacementUPCoin - 2, coordYCadreUPBalle + ecartYEmplacementUP - 2, 90+4, largeurEmplacementUP+4);
        }
        sprite(coordXEmplacementUP, coordYCadreUPBalle + ecartYEmplacementUP, "assets/emplacementUpOrangeVitesseBalle.bmp");
        sprite(coordXEmplacementUPCoin, coordYCadreUPBalle + ecartYEmplacementUP, "assets/emplacementUpOrangeCoin.bmp");
        afficherPrixUp(coordYCadreUPBalle + ecartYEmplacementUP, tabPrixUP[1]);
    }else{
        sprite(coordXEmplacementUP, coordYCadreUPBalle + ecartYEmplacementUP, "assets/emplacementUpVitesseBalle.bmp");
        sprite(coordXEmplacementUPCoin, coordYCadreUPBalle + ecartYEmplacementUP, "assets/emplacementUpCoin.bmp");
        afficherPrixUp(coordYCadreUPBalle + ecartYEmplacementUP, tabPrixUP[1]);
    }

    if(tabPrixUP[2]<=money){
        if(hoverUPEmplacementCoinBalle2 == 1){
            changeColor(255,255,60,255);
            drawRect(coordXEmplacementUPCoin - 2, coordYCadreUPBalle + coordYEmplacementUP - 2, 90+4, largeurEmplacementUP+4);
        }
        sprite(coordXEmplacementUP, coordYCadreUPBalle + coordYEmplacementUP, "assets/emplacementUpOrangeDegatBalle.bmp");
        sprite(coordXEmplacementUPCoin, coordYCadreUPBalle, "assets/emplacementUpOrangeCoin.bmp");
        afficherPrixUp(coordYCadreUPBalle + coordYEmplacementUP, tabPrixUP[2]);  
    }else{
        sprite(coordXEmplacementUP, coordYCadreUPBalle + coordYEmplacementUP, "assets/emplacementUpDegatBalle.bmp");
        sprite(coordXEmplacementUPCoin, coordYCadreUPBalle + coordYEmplacementUP, "assets/emplacementUpCoin.bmp");
        afficherPrixUp(coordYCadreUPBalle + coordYEmplacementUP, tabPrixUP[2]);  
    }
    //---------------------------------------------------------------------------

    //--------------------------------Cadre BARRE--------------------------------
    sprite(ecartCadreX, coordYCadreUPBarre, "assets/cadreUpBarre.bmp");
    if(tabPrixUP[3]<=money){
        if(hoverUPEmplacementCoinBarre1 == 1){
            changeColor(255,255,60,255);
            drawRect(coordXEmplacementUPCoin - 2, coordYCadreUPBarre + ecartYEmplacementUP - 2, 90+4, largeurEmplacementUP+4);
        }
        sprite(coordXEmplacementUP, coordYCadreUPBarre + ecartYEmplacementUP, "assets/emplacementUpOrangeVitesseBarre.bmp");
        sprite(coordXEmplacementUPCoin, coordYCadreUPBarre + ecartYEmplacementUP, "assets/emplacementUpOrangeCoin.bmp");
        afficherPrixUp(coordYCadreUPBarre + ecartYEmplacementUP, tabPrixUP[3]);
    }else{
        sprite(coordXEmplacementUP, coordYCadreUPBarre + ecartYEmplacementUP, "assets/emplacementUpVitesseBarre.bmp");
        sprite(coordXEmplacementUPCoin, coordYCadreUPBarre + ecartYEmplacementUP, "assets/emplacementUpCoin.bmp");
        afficherPrixUp(coordYCadreUPBarre + ecartYEmplacementUP, tabPrixUP[3]);
    }

    if(tabPrixUP[4]<=money){
        if(hoverUPEmplacementCoinBarre2 == 1){
            changeColor(255,255,60,255);
            drawRect(coordXEmplacementUPCoin - 2, coordYCadreUPBarre + coordYEmplacementUP - 2, 90+4, largeurEmplacementUP+4);
        }
        sprite(coordXEmplacementUP, coordYCadreUPBarre + coordYEmplacementUP, "assets/emplacementUpOrangeLongBarre.bmp");
        sprite(coordXEmplacementUPCoin, coordYCadreUPBarre + coordYEmplacementUP, "assets/emplacementUpOrangeCoin.bmp");
        afficherPrixUp(coordYCadreUPBarre + coordYEmplacementUP, tabPrixUP[4]);
    }else{
        sprite(coordXEmplacementUP, coordYCadreUPBarre + coordYEmplacementUP, "assets/emplacementUpLongBarre.bmp");
        sprite(coordXEmplacementUPCoin, coordYCadreUPBarre + coordYEmplacementUP, "assets/emplacementUpCoin.bmp");
        afficherPrixUp(coordYCadreUPBarre + coordYEmplacementUP, tabPrixUP[4]);
    }
    //---------------------------------------------------------------------------

    //--------------------------------Cadre COIN---------------------------------
    sprite(ecartCadreX, coordYCadreUPCoin, "assets/cadreUpCoin.bmp");
    if(tabPrixUP[5]<=money){
        if(hoverUPEmplacementCoin1 == 1){
            changeColor(255,255,60,255);
            drawRect(coordXEmplacementUPCoin - 2, coordYCadreUPCoin + ecartYEmplacementUP - 2, 90+4, largeurEmplacementUP+4);
        }
        sprite(coordXEmplacementUP, coordYCadreUPCoin + ecartYEmplacementUP, "assets/emplacementUpOrangeTauxCoin.bmp");
        sprite(coordXEmplacementUPCoin, coordYCadreUPCoin + ecartYEmplacementUP, "assets/emplacementUpOrangeCoin.bmp");
        afficherPrixUp(coordYCadreUPCoin + ecartYEmplacementUP, tabPrixUP[5]);
    }else{
        sprite(coordXEmplacementUP, coordYCadreUPCoin + ecartYEmplacementUP, "assets/emplacementUpTauxCoin.bmp");
        sprite(coordXEmplacementUPCoin, coordYCadreUPCoin + ecartYEmplacementUP, "assets/emplacementUpCoin.bmp");
        afficherPrixUp(coordYCadreUPCoin + ecartYEmplacementUP, tabPrixUP[5]);
    }

    if(tabPrixUP[6]<=money){
        if(hoverUPEmplacementCoin2 == 1){
            changeColor(255,255,60,255);
            drawRect(coordXEmplacementUPCoin - 2, coordYCadreUPCoin + coordYEmplacementUP - 2, 90+4, largeurEmplacementUP+4);
        }
        sprite(coordXEmplacementUP, coordYCadreUPCoin + coordYEmplacementUP, "assets/emplacementUpOrangeValeurCoin.bmp");
        sprite(coordXEmplacementUPCoin, coordYCadreUPCoin + coordYEmplacementUP, "assets/emplacementUpOrangeCoin.bmp");
        afficherPrixUp(coordYCadreUPCoin + coordYEmplacementUP, tabPrixUP[6]);

    }else{
        sprite(coordXEmplacementUP, coordYCadreUPCoin + coordYEmplacementUP, "assets/emplacementUpValeurCoin.bmp");
        sprite(coordXEmplacementUPCoin, coordYCadreUPCoin + coordYEmplacementUP, "assets/emplacementUpCoin.bmp");
        afficherPrixUp(coordYCadreUPCoin + coordYEmplacementUP, tabPrixUP[6]);
    }
    //---------------------------------------------------------------------------

    //--------------------------------Cadre BONUS--------------------------------
    sprite(ecartCadreX, coordYCadreUPBonus, "assets/cadreUpBonus.bmp");
    if(tabPrixUP[7]<=money){
        if(hoverUPEmplacementCoinBonus1 == 1){
            changeColor(255,255,60,255);
            drawRect(coordXEmplacementUPCoin - 2, coordYCadreUPBonus + ecartYEmplacementUP - 2, 90+4, largeurEmplacementUP+4);
        }
        sprite(coordXEmplacementUP, coordYCadreUPBonus + ecartYEmplacementUP, "assets/emplacementUpOrangeTauxCoin.bmp");
        sprite(coordXEmplacementUPCoin, coordYCadreUPBonus + ecartYEmplacementUP, "assets/emplacementUpOrangeCoin.bmp");
        afficherPrixUp(coordYCadreUPBonus + ecartYEmplacementUP, tabPrixUP[7]);
    }else{
        sprite(coordXEmplacementUP, coordYCadreUPBonus + ecartYEmplacementUP, "assets/emplacementUpTauxCoin.bmp");
        sprite(coordXEmplacementUPCoin, coordYCadreUPBonus + ecartYEmplacementUP, "assets/emplacementUpCoin.bmp");
        afficherPrixUp(coordYCadreUPBonus + ecartYEmplacementUP, tabPrixUP[7]);
    }

    if(tabPrixUP[8]<=money){
        if(hoverUPEmplacementCoinBonus2 == 1){
            changeColor(255,255,60,255);
            drawRect(coordXEmplacementUPCoin - 2, coordYCadreUPBonus + coordYEmplacementUP - 2, 90+4, largeurEmplacementUP+4);
        }
        sprite(coordXEmplacementUP, coordYCadreUPBonus + coordYEmplacementUP, "assets/emplacementUpOrangePowerBonus.bmp");
        sprite(coordXEmplacementUPCoin, coordYCadreUPBonus + coordYEmplacementUP, "assets/emplacementUpOrangeCoin.bmp");
        afficherPrixUp(coordYCadreUPBonus + coordYEmplacementUP, tabPrixUP[8]);
    }else{
        sprite(coordXEmplacementUP, coordYCadreUPBonus + coordYEmplacementUP, "assets/emplacementUpPowerBonus.bmp");
        sprite(coordXEmplacementUPCoin, coordYCadreUPBonus + coordYEmplacementUP, "assets/emplacementUpCoin.bmp");
        afficherPrixUp(coordYCadreUPBonus + coordYEmplacementUP, tabPrixUP[8]);
    }
    //---------------------------------------------------------------------------
/*-----------------------------------------------------------------------------*/

/*-----------------------------Affiche COIN Total------------------------------*/
    changeColor(71, 2, 14, 255);
    drawRect(0, 0, WINDOW_WIDTH, ecartCadreY);

    drawCoins();
/*-----------------------------------------------------------------------------*/
/*-------------------------------Menu bas écran--------------------------------*/
    changeColor(71, 2, 14, 255);
    drawRect(0, WINDOW_HEIGHT - 125, WINDOW_WIDTH, 125);

    ecartLogo = (WINDOW_WIDTH - (4*90 + 3*19))/2;

    if(hoverLogoNiveau == 1){
        sprite(ecartLogo, WINDOW_HEIGHT - 121, "assets/logoUpNiveau.bmp");
    }else{
        sprite(ecartLogo, WINDOW_HEIGHT - 101, "assets/logoUpNiveau.bmp");
    }
    if(hoverLogoUp== 1){
        sprite(ecartLogo + 109, WINDOW_HEIGHT - 121, "assets/logoUpUp.bmp");
    }else{
        sprite(ecartLogo + 109, WINDOW_HEIGHT - 101, "assets/logoUpUp.bmp");
    }
    if(hoverLogoSkin == 1){
        sprite(ecartLogo + 2 * 109, WINDOW_HEIGHT - 121, "assets/logoUpSkin.bmp");
    }else{
        sprite(ecartLogo + 2 * 109, WINDOW_HEIGHT - 101, "assets/logoUpSkin.bmp");
    }
    if(hoverLogoSetting == 1){
        sprite(ecartLogo + 3 * 109, WINDOW_HEIGHT - 121, "assets/logoUpSetting.bmp");
    }else{
        sprite(ecartLogo + 3 * 109, WINDOW_HEIGHT - 101, "assets/logoUpSetting.bmp");
    }
/*-----------------------------------------------------------------------------*/
}

void menuChoixSkin(){
    changeColor(2, 71, 54, 255);
    drawRect(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);

    ecartLogo = (WINDOW_WIDTH - (4*90 + 3*19))/2;

    if(hoverLogoNiveau == 1){
        sprite(ecartLogo, WINDOW_HEIGHT - 121, "assets/logoSkinNiveau.bmp");
    }else{
        sprite(ecartLogo, WINDOW_HEIGHT - 101, "assets/logoSkinNiveau.bmp");
    }
    if(hoverLogoUp== 1){
        sprite(ecartLogo + 109, WINDOW_HEIGHT - 121, "assets/logoSkinUp.bmp");
    }else{
        sprite(ecartLogo + 109, WINDOW_HEIGHT - 101, "assets/logoSkinUp.bmp");
    }
    if(hoverLogoSkin == 1){
        sprite(ecartLogo + 2 * 109, WINDOW_HEIGHT - 121, "assets/logoSkinSkin.bmp");
    }else{
        sprite(ecartLogo + 2 * 109, WINDOW_HEIGHT - 101, "assets/logoSkinSkin.bmp");
    }
    if(hoverLogoSetting == 1){
        sprite(ecartLogo + 3 * 109, WINDOW_HEIGHT - 121, "assets/logoSkinSetting.bmp");
    }else{
        sprite(ecartLogo + 3 * 109, WINDOW_HEIGHT - 101, "assets/logoSkinSetting.bmp");
    }}

void menuChoixSetting(){}

void menuChoix(){
    switch (pageMenu)
    {
        case 0:
            menuChoixNiveau();
            break;
        case 1:
            menuChoixUp();
            break;
        case 2:
            menuChoixSkin();
            break;
        case 3:
            menuChoixSetting();
            break;
        default:
            break;
    }
}

void init_game(){
    ecartBandeJeuY = 75;

    /*----------------------Initialisation BALLE et BARRE----------------------*/
    ball.vie = 3;

    resetBalleEtBarre();    
    /*-------------------------------------------------------------------------*/

    /*----------------------Initialisation COEUR----------------------*/
    chargementCoeur = 1;
    /*-------------------------------------------------------------------------*/

    /*--------------------------Initialisation BRICKS--------------------------*/
    espaceEntreBrick = 3;

    longueurBrick = 60;
    largeurBrick = 30;
    longueurBrickContour = longueurBrick + 2;
    largeurBrickContour = largeurBrick + 2;
    espaceXDebutBricks = (WINDOW_WIDTH - ((NB_COLONNE_BRICK)*longueurBrickContour + (NB_COLONNE_BRICK-1)*espaceEntreBrick))/2;
    espaceYDebutBricks = ecartBandeJeuY +49;

    init_chargeBricks = 0;

    chargementBrick = 1;
    nbRestantBrick = 0;

    for(int i=0 ; i<NB_LIGNE_BRICK ; i++){
        for(int j=0 ; j<NB_COLONNE_BRICK ; j++){
            tabBricks[i][j].vie = 0;
            tabBricks[i][j].spawn = 0;

            tabBricks[i][j].xContourBrickGauche = espaceXDebutBricks + j*(longueurBrickContour+espaceEntreBrick);
            tabBricks[i][j].xContourBrickDroite = tabBricks[i][j].xContourBrickGauche + longueurBrickContour;
            tabBricks[i][j].yContourBrickHaut = espaceYDebutBricks + i*(largeurBrickContour+espaceEntreBrick);
            tabBricks[i][j].yContourBrickBas = tabBricks[i][j].yContourBrickHaut + largeurBrickContour;
        }
    }
    /*-------------------------------------------------------------------------*/

    /*--------------------------Initialisation COINS---------------------------*/
    srand(time(NULL));

    money = 6;

    vitesseCoins = 3;
    rayonCoins = 9;
    rayonContourCoins = rayonCoins + 1;

    for(int i=0 ; i<NB_LIGNE_BRICK ; i++){
        for(int j=0 ; j<NB_COLONNE_BRICK ; j++){
            // tabPieces[i][j].spawn = rand() % 2;
            tabPieces[i][j].spawn = 1;

            tabPieces[i][j].x = tabBricks[i][j].xContourBrickGauche + longueurBrickContour/2;
            tabPieces[i][j].y = tabBricks[i][j].yContourBrickHaut + largeurBrickContour/2;
        }
    }

    tabPrixUP[0] = 2; //Augmente le nombre de coeur
    tabPrixUP[1] = 5; //Augmente vitesse de la balle
    tabPrixUP[2] = 10; //Augmente dégât de la balle
    tabPrixUP[3] = 5; //Augmente vitesse de la barre
    tabPrixUP[4] = 7; //Augmente longueur de la barre
    tabPrixUP[5] = 1; //Augmente le taux d'apparition
    tabPrixUP[6] = 1; //Augmente la valeur des coins
    tabPrixUP[7] = 1; //Augmente le taux d'apparition
    tabPrixUP[8] = 1; //Augmente la puissance des bonus
    tabPrixUP[9];

    /*-------------------------------------------------------------------------*/

    /*--------------------------Initialisation MENU UP-------------------------*/
    ecartCadreX = 17;
    ecartCadreY = 113;
    cadreUpJoueurH = 161;
    cadreUpBalleH = 252;
    cadreUpBarreH = 251;
    cadreUpBonusH = 251;
    cadreUpCoinH = 249;

    espaceCadre = 24;

    varScroll = 0;

    ecartXEmplacementUP = 17;
    ecartYEmplacementUP = 40;

    longueurEmplacementUP = 324;
    largeurEmplacementUP = 83;
    espaceXEmplacementUP = 12;
    espaceYEmplacementUP = 9;
    /*-------------------------------------------------------------------------*/

    start = 0;
    
    niveau = 0;
    pageMenu = 0;
    pauseGame = -1;
    pauseMenu = -1;
}

void drawGame(){
    switch (niveau)
    {
        case 0:
            if(pauseMenu<0){
                menuChoix();
            }
            break;
        case 1:
            niveau1();
            break;
        case 2:
            niveau2();
            break; 
        case 3:
            niveau3();
            break; 
        case 4:
            niveau4();
            break;
        case 5:
            niveau5();
            break;
        default:
            break;
    }
    actualize();
    usleep(1000000 / FPS); // 60 images par seconde | 1000000 = 1 seconde
}

void MouseButton(int mouseX, int mouseY){
    printf("position de la souris x : %d , y : %d\n", mouseX, mouseY);

    if((niveau==0) && (pauseMenu<0)){
        if(((mouseX - ecartLogo - 44) * (mouseX - ecartLogo - 44) +  (mouseY - 799 - 44) * (mouseY - 799 - 44)) <= 44 * 44){
            pageMenu = 0;
        }else if(((mouseX - ecartLogo - 109 - 44) * (mouseX - ecartLogo - 109 - 44) +  (mouseY - 799 - 44) * (mouseY - 799 - 44)) <= 44 * 44){
            pageMenu = 1;
            varScroll = 0;
        }else if(((mouseX - ecartLogo - 2 * 109 - 44) * (mouseX - ecartLogo - 2 * 109 - 44) +  (mouseY - 799 - 44) * (mouseY - 799 - 44)) <= 44 * 44){
            pageMenu = 2;
            varScroll = 0;
        }else if(((mouseX - ecartLogo - 3 * 109 - 44) * (mouseX - ecartLogo - 3 * 109 - 44) +  (mouseY - 799 - 44) * (mouseY - 799 - 44)) <= 44 * 44){
            pageMenu = 3;
        }
    }

    //Sélection du niveau UNIQUEMENT lorsque l'on est dans le menu
    if((pageMenu==0) && (pauseMenu<0)){
        if(((mouseX >= 87) && (mouseX <= 412)) && ((mouseY >= 180) && (mouseY <= 240))){
            niveau = 1;
        }

        else if(((mouseX >= 87) && (mouseX <= 412)) && ((mouseY >= 270) && (mouseY <= 330))){
            niveau = 2;
        }

        else if(((mouseX >= 87) && (mouseX <= 412)) && ((mouseY >= 360) && (mouseY <= 420))){
            niveau = 3;
        }

        else if(((mouseX >= 87) && (mouseX <= 412)) && ((mouseY >= 455) && (mouseY <= 515))){
            niveau = 4;
        }

        else if(((mouseX >= 87) && (mouseX <= 412)) && ((mouseY >= 545) && (mouseY <= 605))){
            niveau = 5;
        }
    }

    if(pauseGame>0){
        if((mouseY >= 522) && (mouseY <= 572)){
            if((mouseX >= 105) && (mouseX <= 243)){
                //NON
                pauseGame = -pauseGame;
            }
            else if((mouseX >= 260) && (mouseX <= 397)){
                //OUI
                niveau = 0;
                pageMenu = 0;
            }
        }
    }

    if(pauseMenu>0){
        if((mouseY >= 396) && (mouseY <= 447)){
            if((mouseX >= 106) && (mouseX <= 245)){
                //NON
                pauseMenu = -pauseMenu;
            }
            else if((mouseX >= 261) && (mouseX <= 398)){
                //OUI
                freeAndTerminate();
            }
        }
    }
}

void MouseMotion(int mouseX, int mouseY){
    if((niveau==0) && (pauseMenu<0)){
        // mouseX - ecartLogo - rayonLogo
        if(((mouseX - ecartLogo - 44) * (mouseX - ecartLogo - 44) +  (mouseY - 799 - 44) * (mouseY - 799 - 44)) <= 44 * 44){
            hoverLogoNiveau = 1;
        }else if(((mouseX - ecartLogo - 109 - 44) * (mouseX - ecartLogo - 109 - 44) +  (mouseY - 799 - 44) * (mouseY - 799 - 44)) <= 44 * 44){
            hoverLogoUp = 1;
        }else if(((mouseX - ecartLogo - 2 * 109 - 44) * (mouseX - ecartLogo - 2 * 109 - 44) +  (mouseY - 799 - 44) * (mouseY - 799 - 44)) <= 44 * 44){
            hoverLogoSkin = 1;
        }else if(((mouseX - ecartLogo - 3 * 109 - 44) * (mouseX - ecartLogo - 3 * 109 - 44) +  (mouseY - 799 - 44) * (mouseY - 799 - 44)) <= 44 * 44){
            hoverLogoSetting = 1;
        }else{
            hoverLogoNiveau = 0;
            hoverLogoUp = 0;
            hoverLogoSkin = 0;
            hoverLogoSetting = 0;
        }
    }

    if((pageMenu==0) && (pauseMenu<0)){
        if(((mouseX >= 87) && (mouseX <= 412)) && ((mouseY >= 180) && (mouseY <= 240))){
            hoverNiveau1 = 1;
        }else if(((mouseX >= 87) && (mouseX <= 412)) && ((mouseY >= 270) && (mouseY <= 330))){
            hoverNiveau2 = 1;
        }else if(((mouseX >= 87) && (mouseX <= 412)) && ((mouseY >= 360) && (mouseY <= 420))){
            hoverNiveau3 = 1;
        }else if(((mouseX >= 87) && (mouseX <= 412)) && ((mouseY >= 455) && (mouseY <= 515))){
            hoverNiveau4 = 1;
        }else if(((mouseX >= 87) && (mouseX <= 412)) && ((mouseY >= 545) && (mouseY <= 605))){
            hoverNiveau5 = 1;
        }else{
            hoverNiveau1 = 0;
            hoverNiveau2 = 0;
            hoverNiveau3 = 0;
            hoverNiveau4 = 0;
            hoverNiveau5 = 0;
        }
    }

    if((pageMenu==1) && (pauseMenu<0)){
        if((mouseY > coordYCadreUPJoueur + ecartYEmplacementUP) && (mouseY < coordYCadreUPJoueur + ecartYEmplacementUP + 83) && (mouseX > coordXEmplacementUPCoin) && (mouseX < coordXEmplacementUPCoin + 90)){
            hoverUPEmplacementCoinJoueur = 1;
        }else if((mouseY > coordYCadreUPBalle + ecartYEmplacementUP) && (mouseY < coordYCadreUPBalle + ecartYEmplacementUP + 83) && (mouseX > coordXEmplacementUPCoin) && (mouseX < coordXEmplacementUPCoin + 90)){
            hoverUPEmplacementCoinBalle1 = 1;
        }
        else if((mouseY > coordYCadreUPBalle + coordYEmplacementUP) && (mouseY < coordYCadreUPBalle + coordYEmplacementUP + 83) && (mouseX > coordXEmplacementUPCoin) && (mouseX < coordXEmplacementUPCoin + 90)){
            hoverUPEmplacementCoinBalle2 = 1;
        }
        else if((mouseY > coordYCadreUPBarre + ecartYEmplacementUP) && (mouseY < coordYCadreUPBarre + ecartYEmplacementUP + 83) && (mouseX > coordXEmplacementUPCoin) && (mouseX < coordXEmplacementUPCoin + 90)){
            hoverUPEmplacementCoinBarre1 = 1;
        }
        else if((mouseY > coordYCadreUPBarre + coordYEmplacementUP) && (mouseY < coordYCadreUPBarre + coordYEmplacementUP + 83) && (mouseX > coordXEmplacementUPCoin) && (mouseX < coordXEmplacementUPCoin + 90)){
            hoverUPEmplacementCoinBarre2 = 1;
        }
        else if((mouseY > coordYCadreUPCoin + ecartYEmplacementUP) && (mouseY < coordYCadreUPCoin + ecartYEmplacementUP + 83) && (mouseX > coordXEmplacementUPCoin) && (mouseX < coordXEmplacementUPCoin + 90)){
            hoverUPEmplacementCoin1 = 1;
        }
        else if((mouseY > coordYCadreUPCoin + coordYEmplacementUP) && (mouseY < coordYCadreUPCoin + coordYEmplacementUP + 83) && (mouseX > coordXEmplacementUPCoin) && (mouseX < coordXEmplacementUPCoin + 90)){
            hoverUPEmplacementCoin2 = 1;
        }
        else if((mouseY > coordYCadreUPBonus + ecartYEmplacementUP) && (mouseY < coordYCadreUPBonus + ecartYEmplacementUP + 83) && (mouseX > coordXEmplacementUPCoin) && (mouseX < coordXEmplacementUPCoin + 90)){
            hoverUPEmplacementCoinBonus1 = 1;
        }
        else if((mouseY > coordYCadreUPBonus + coordYEmplacementUP) && (mouseY < coordYCadreUPBonus + coordYEmplacementUP + 83) && (mouseX > coordXEmplacementUPCoin) && (mouseX < coordXEmplacementUPCoin + 90)){
            hoverUPEmplacementCoinBonus2 = 1;
        }else{
            hoverUPEmplacementCoinJoueur = 0;
            hoverUPEmplacementCoinBalle1 = 0;
            hoverUPEmplacementCoinBalle2 = 0;
            hoverUPEmplacementCoinBarre1 = 0;
            hoverUPEmplacementCoinBarre2 = 0;
            hoverUPEmplacementCoin1 = 0;
            hoverUPEmplacementCoin2 = 0;
            hoverUPEmplacementCoinBonus1 = 0;
            hoverUPEmplacementCoinBonus2 = 0;
        }      
    }
}

void MouseWheel(int wheelY){
    // fenetre vers le haut
    if ((wheelY < 0) && (varScroll > -600))
    {
        varScroll -= MY_SCROLL_SPEED;
    } 

    // fenetre vers le bas
    else if ((wheelY > 0) && (varScroll < 0))
    {
        varScroll += MY_SCROLL_SPEED;
    }
}

void KeyPressed(SDL_Keycode touche){
    /** @brief event.key.keysym.sym renvoi la touche appuyé
     *
     */
    switch (touche) {
        // Voir doc SDL_Keycode pour plus de touches https://wiki.libsdl.org/SDL_Keycode
        case SDLK_q:
            //touche Q appuyé
            printf("Q\n");
            barr.vitesse = -VITESSE_BARR;
            break;
        case SDLK_d:
            //touche D appuyé
            printf("D\n");
            barr.vitesse = VITESSE_BARR;
            break;
        case SDLK_SPACE:
            //touche ESPACE
            start=1;
            break;
        case SDLK_ESCAPE:
            if(niveau==0){
                pauseMenu = -pauseMenu;
                changeColor(0,0,0,100);
                drawRect(0,0,WINDOW_WIDTH, WINDOW_HEIGHT);

                sprite(90,274,"assets/menuQuit.bmp");
            }else{
                start = 0;
                pauseGame = -pauseGame;
                changeColor(0,0,0,100);
                drawRect(0,0,WINDOW_WIDTH, WINDOW_HEIGHT);

                sprite(88,288,"assets/menuPause.bmp");
            }
            
            break;
        default:
            break;
    }
}

void gameLoop() {
    int programLaunched = 1;
    while (programLaunched == 1) {
        // Boucle pour garder le programme ouvert
        // lorsque programLaunched est different de 1
        // on sort de la boucle, donc de la fonction
        SDL_Event event;
        while (SDL_PollEvent(&event)) {
            // boucle pour la gestion d'évenement
            switch (event.type) {
                /** event.type renvoi le type d'evenement qui se passe
                 * (si on appuie sur une touche, clique de souris...)
                 * en fonction du type d'évènement on à alors
                 * différentes information accessibles
                 * voir doc pour plus d'event https://wiki.libsdl.org/SDL_EventType
                 */
                case SDL_QUIT:
                    // quand on clique sur fermer la fénêtre en haut à droite
                    programLaunched = 0;
                    break;
                case SDL_MOUSEBUTTONDOWN:
                    /* clique de la souris
                     * event.motion.y | event.motion.x pour les positions de la souris
                     */
                    MouseButton(event.motion.x, event.motion.y);
                    break;
                case SDL_MOUSEMOTION:
                    MouseMotion(event.motion.x, event.motion.y);
                    break;
                case SDL_KEYDOWN:
                    KeyPressed(event.key.keysym.sym);
                    break;
                case SDL_MOUSEWHEEL:
                    MouseWheel(event.wheel.y);
                    break;

                case SDL_JOYBUTTONDOWN:
                    break;
                default:
                    break;
            }
        }
        drawGame();
    }
}

int main(){
    /** @description 3 fonctions dans le main qui permettent de créer l'application et la maintenir ouverte :
     *  init(...) : initialiser la SDL/ fenêtre
     *  gameLoop() : boucle de jeu dans laquelle l'application reste ouverte
     *  freeAndTerminate() : quitte le programme proprement
     */
    init(WINDOW_WIDTH, WINDOW_HEIGHT);
    init_game();
    gameLoop();
    printf("Fin du programme\n");
    freeAndTerminate();
}
